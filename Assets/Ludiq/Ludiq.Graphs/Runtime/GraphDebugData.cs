﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Ludiq
{
	public class GraphDebugData : IGraphDebugData
	{
		protected Dictionary<IGraphElementWithDebugData, IGraphElementDebugData> elementsData { get; } = new Dictionary<IGraphElementWithDebugData, IGraphElementDebugData>();
		
		protected Dictionary<IGraphParentElement, IGraphDebugData> childrenGraphsData { get; } = new Dictionary<IGraphParentElement, IGraphDebugData>();

		IEnumerable<IGraphElementDebugData> IGraphDebugData.elementsData => elementsData.Values;

		public GraphDebugData(IGraph definition)
		{

		}

		public IGraphElementDebugData GetOrCreateElementData(IGraphElementWithDebugData element)
		{
			if (!elementsData.TryGetValue(element, out var elementDebugData))
			{
				elementDebugData = element.CreateDebugData();

				// So the Mono vtable apparently gets very confused with
				// some types inheriting from UnitConnection and breaks type safety.
				// https://stackoverflow.com/questions/50051657
				if (!typeof(IGraphElementDebugData).IsAssignableFrom(elementDebugData))
				{
					elementDebugData = new Member(element.GetType(), nameof(IGraphElementWithDebugData.CreateDebugData), Empty<Type>.array, element).Invoke<IGraphElementDebugData>();

					if (!typeof(IGraphElementDebugData).IsAssignableFrom(elementDebugData))
					{
						Debug.LogWarning("Mono vtable error attempted reflection fix failed: \n" + element.GetType().FullName + " => " + elementDebugData.GetType().FullName);
					}
				}

				elementsData.Add(element, elementDebugData);
			}

			return elementDebugData;
		}

		public IGraphDebugData GetOrCreateChildGraphData(IGraphParentElement element)
		{
			if (!childrenGraphsData.TryGetValue(element, out var data))
			{
				data = new GraphDebugData(element.childGraph);
				childrenGraphsData.Add(element, data);
			}

			return data;
		}
	}
}
