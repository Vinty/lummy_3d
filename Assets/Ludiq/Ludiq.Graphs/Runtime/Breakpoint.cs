﻿using Ludiq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ludiq
{
	public sealed class Breakpoint : Exception
	{
		public Breakpoint() : base("Breakpoint encountered") {}
		public Breakpoint(IGraphElement element) : base($"Breakpoint encountered on {element.GetType()}") {}
		public Breakpoint(string description) : base($"Breakpoint encountered on {description}") {}
	}
}
