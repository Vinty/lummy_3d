﻿using System;
using UnityEngine;

namespace Ludiq
{
	public interface IGraph : IDisposable, IPrewarmable, IAotStubbable, ISerializationDepender
	{
		Vector2 pan { get; set; }

		float zoom { get; set; }
		
		MergedGraphElementCollection elements { get; }

		string title { get; }

		string summary { get; }

		ulong liveVersionID { get; set; }

		ulong generatedVersionID { get; set; }

		string machineScriptTypeName { get; set; }
		
		IGraphData CreateData();

		IGraphDebugData CreateDebugData();

		void Instantiate(GraphReference instance);
		
		void Uninstantiate(GraphReference instance);
	}
}