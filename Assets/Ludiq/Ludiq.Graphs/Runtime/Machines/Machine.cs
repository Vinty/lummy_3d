﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityObject = UnityEngine.Object;

namespace Ludiq
{
	public abstract class Machine<TGraph, TMacro> : LudiqBehaviour, IMachine
		where TGraph : class, IGraph, new()
		where TMacro : Macro<TGraph>
	{
		protected Machine()
		{
			nest.nester = this;
			nest.source = GraphSource.Macro;
		}
		
		[DoNotSerialize]
		private bool _alive;

		[DoNotSerialize]
		protected bool destroyedDuringSwap;

		[DoNotSerialize]
		public GameObject threadSafeGameObject { get; private set; }

		[DoNotSerialize]
		private bool isReferenceCached;

		[DoNotSerialize]
		private GraphReference _reference;

		[DoNotSerialize]
		protected GraphReference reference => isReferenceCached ? _reference : GraphReference.New(this, false);

		[DoNotSerialize]
		protected bool hasGraph => reference != null;

		[Serialize]
		public GraphNest<TGraph, TMacro> nest { get; private set; } = new GraphNest<TGraph, TMacro>();

		[DoNotSerialize]
		public TGraph graph => nest.graph;

		[DoNotSerialize]
		IGraphNest IGraphNester.nest => nest;

		[DoNotSerialize]
		bool IGraphParent.isSerializationRoot => true;

		[DoNotSerialize]
		UnityObject IGraphParent.serializedObject
		{
			get
			{
				switch (nest.source)
				{
					case GraphSource.Macro: return nest.macro;
					case GraphSource.Embed: return this;
					default: throw new UnexpectedEnumValueException<GraphSource>(nest.source);
				}
			}
		}

		[DoNotSerialize]
		IGraph IGraphParent.childGraph => graph;

		[DoNotSerialize]
		public IEnumerable<object> aotStubs => nest.aotStubs;

		[DoNotSerialize]
		public IGraphData graphData { get; set; }

		public IEnumerable<ISerializationDependency> deserializationDependencies => nest.deserializationDependencies;

		public bool isDescriptionValid
		{
			get => true;
			set { }
		}

		protected override void OnAfterDeserialize()
		{
			Serialization.AwaitDependencies(this);
		}

		public void OnAfterDependenciesDeserialized()
		{
			base.OnAfterDeserialize();
		}

		protected virtual void OnValidate()
		{
			threadSafeGameObject = gameObject;
		}

		public virtual void InstantiateNest()
		{
			if (!_alive)
			{
				return;
			}

			GraphInstances.Instantiate(reference);
		}

		public virtual void UninstantiateNest()
		{
			if (!_alive)
			{
				return;
			}

			var instances = GraphInstances.ChildrenOfPooled(this);

			foreach (var instance in instances)
			{
				GraphInstances.Uninstantiate(instance);
			}

			instances.Free();
		}

		protected virtual void Awake()
		{
			_alive = true;
			threadSafeGameObject = gameObject;

			nest.afterGraphChange += CacheReference;
			nest.beforeGraphChange += ClearCachedReference;

			CacheReference();

			if (graph != null)
			{
				InstantiateNest();
			}
		}

		protected virtual void OnDestroy()
		{
			if (destroyedDuringSwap)
			{
				return;
			}

			ClearCachedReference();

			if (graph != null)
			{
				UninstantiateNest();
			}

			threadSafeGameObject = null;
			_alive = false;
		}

		private void CacheReference()
		{
			_reference = GraphReference.New(this, false);
			isReferenceCached = true;
		}

		private void ClearCachedReference()
		{
			_reference = null;
		}

		public virtual void TriggerAnimationEvent(AnimationEvent animationEvent)
		{
			
		}

		public virtual void TriggerUnityEvent(string name)
		{
			
		}
	}
}