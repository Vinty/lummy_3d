﻿namespace Ludiq
{
	public interface IGraphNester : IGraphParent, ISerializationDepender
	{
		IGraphNest nest { get; }

		void InstantiateNest();
		void UninstantiateNest();
	}
}