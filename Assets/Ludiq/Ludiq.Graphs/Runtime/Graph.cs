using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Ludiq
{
	public abstract class Graph : IGraph
	{
		protected Graph()
		{
			elements = new MergedGraphElementCollection();
		}

		public override string ToString()
		{
			return StringUtility.FallbackWhitespace(title, GetType().Name) + "#" + GetHashCode();
		}

		public abstract IGraphData CreateData();

		public virtual IGraphDebugData CreateDebugData()
		{
			return new GraphDebugData(this);
		}

		public virtual void Instantiate(GraphReference instance)
		{
			// Debug.Log($"Instantiating graph {instance}");

			instance.CreateGraphData();

			foreach (var element in elements)
			{
				element.Instantiate(instance);
			}
		}

		public virtual void Uninstantiate(GraphReference instance)
		{
			// Debug.Log($"Uninstantiating graph {instance}");

			foreach (var element in elements)
			{
				element.Uninstantiate(instance);
			}

			instance.FreeGraphData();
		}



		#region Elements

		[SerializeAs(nameof(elements))]
		private List<IGraphElement> _elements = new List<IGraphElement>();

		[DoNotSerialize]
		public MergedGraphElementCollection elements { get; private set; }

		#endregion



		#region Metadata
		
		[Serialize]
		public string title { get; set; }
		
		[Serialize]
		[InspectorTextArea(minLines = 1, maxLines = 10)]
		public string summary { get; set; }

		[Serialize]
		public ulong liveVersionID { get; set; }

		[Serialize]
		public ulong generatedVersionID { get; set; }

		[Serialize]
		public string machineScriptTypeName { get; set; }

		#endregion



		#region Canvas

		[Serialize]
		public Vector2 pan { get; set; }

		[Serialize]
		public float zoom { get; set; } = 1;

		#endregion



		#region Serialization

		public IEnumerable<ISerializationDependency> deserializationDependencies => _elements.OfType<ISerializationDependency>();

		public virtual void OnBeforeSerialize()
		{
			_elements.Clear();
			_elements.AddRange(elements);
		}

		public void OnAfterDeserialize()
		{
			Serialization.AwaitDependencies(this);
		}

		public virtual void OnAfterDependenciesDeserialized()
		{
			elements.Clear();

			elements.AddRangeByDependencies(_elements);
		}

		#endregion



		#region Poutine
		
		public IEnumerable<object> aotStubs => elements.SelectMany(element => element.aotStubs);

		public void Prewarm()
		{
			foreach (var element in elements)
			{
				element.Prewarm();
			}
		}

		public virtual void Dispose()
		{
			foreach (var element in elements)
			{
				element.Dispose();
			}
		}

		#endregion
	}
}