﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ludiq
{
	public abstract class GraphElement<TGraph> : IGraphElement where TGraph : class, IGraph
	{
		[Serialize]
		public Guid guid { get; set; } = Guid.NewGuid();
		
		// To minimize the amount of implementation needed, and simplify inversion of control,
		// we provide instantiation routines that fit most types of elements in the right order and
		// we provide implemented defaults for interfaces that the element could implement.
		// Normally, an element shouldn't have to override instantiate or uninstantiate directly.

		public virtual void Instantiate(GraphReference instance)
		{
			// CreateElementData is a non-recursive operation that is
			// required for the nest instantiation, so we do it first.
			CreateElementData(instance);

			// InstantiateNest is a recursive operation that will
			// call Instantiate on descendant graphs and elements. 
			// Because event listening will descend graph data recursively,
			// we need to create it before.
			InstantiateNest(instance);

			// StartListening is a recursive operation that will
			// descend graphs recursively. It must be called after
			// instantiation of all child graphs because it will require
			// their data. The drawback with this approach is that it will be 
			// called at each step bubbling up, whereas it will only
			// effectively trigger once we reach the top level. 
			// Traversal is currently O(n!) where n is the number of descendants,
			// ideally it would be O(n) if only triggered from the root.
			StartListening(instance);
		}

		public virtual void Uninstantiate(GraphReference instance)
		{
			// See above comments, in reverse order.

			StopListening(instance);
			UninstantiateNest(instance);
			FreeElementData(instance);
		}

		public virtual void BeforeAdd() { }

		public virtual void AfterAdd()
		{
			var instances = GraphInstances.OfPooled(graph);

			foreach (var instance in instances)
			{
				Instantiate(instance);
			}

			instances.Free();
		}

		public virtual void BeforeRemove()
		{
			var instances = GraphInstances.OfPooled(graph);

			foreach (var instance in instances)
			{
				Uninstantiate(instance);
			}

			instances.Free();

			Dispose();
		}
		
		public virtual void AfterRemove() { }

		public virtual void Dispose() { }

		protected virtual void CreateElementData(GraphReference instance)
		{
			if (this is IGraphElementWithData withData)
			{
				instance.data.CreateElementData(withData);
			}
		}

		protected virtual void FreeElementData(GraphReference instance)
		{
			if (this is IGraphElementWithData withData)
			{
				instance.data.FreeElementData(withData);
			}
		}

		protected virtual void InstantiateNest(GraphReference instance)
		{
			if (this is IGraphNesterElement nester)
			{
				if (nester.nest.graph == null)
				{
					return;
				}

				GraphInstances.Instantiate(instance.ChildReference(nester, true));
			}
		}

		protected virtual void UninstantiateNest(GraphReference instance)
		{
			if (this is IGraphNesterElement nester)
			{
				if (nester.nest.graph == null)
				{
					return;
				}

				GraphInstances.Uninstantiate(instance.ChildReference(nester, true));
			}
		}
		
		protected virtual void InstantiateNest()
		{
			if (this is IGraphNesterElement nester)
			{
				if (graph == null)
				{
					return;
				}

				var instances = GraphInstances.OfPooled(graph);

				foreach (var instance in instances)
				{
					GraphInstances.Instantiate(instance.ChildReference(nester, true));
				}

				instances.Free();
			}
		}

		protected virtual void UninstantiateNest()
		{
			if (this is IGraphNesterElement nester)
			{
				var instances = GraphInstances.ChildrenOfPooled(nester);

				foreach (var instance in instances)
				{
					GraphInstances.Uninstantiate(instance);
				}

				instances.Free();
			}
		}

		protected virtual void StartListening(GraphReference instance)
		{

		}

		protected virtual void StopListening(GraphReference instance)
		{

		}


		#region Graph

		[DoNotSerialize]
		public virtual int dependencyOrder => 0;

		public virtual bool HandleDependencies() => true;

		[DoNotSerialize]
		public TGraph graph { get; set; }

		[DoNotSerialize]
		IGraph IGraphElement.graph
		{
			get => graph;
			set
			{
				Ensure.That(nameof(value)).IsOfType<TGraph>(value);
				graph = (TGraph)value;
			}
		}

		[DoNotSerialize]
		IGraph IGraphItem.graph => graph;

		#endregion



		#region Serialization

		public virtual void OnBeforeSerialize() { }

		public virtual void OnAfterDeserialize() { }

		#endregion



		#region Poutine

		public virtual IEnumerable<object> aotStubs => Enumerable.Empty<object>();

		public virtual void Prewarm() { }

		protected void CopyFrom(GraphElement<TGraph> source) { }

		public override string ToString()
		{
			var sb = new StringBuilder();
			sb.Append(GetType().Name);
			sb.Append("#");
			sb.Append(guid.ToString().Substring(0, 5));
			sb.Append("...");
			return sb.ToString();
		}

		#endregion
	}
}
