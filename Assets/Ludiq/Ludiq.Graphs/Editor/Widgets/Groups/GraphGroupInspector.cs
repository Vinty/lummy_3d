﻿namespace Ludiq
{
	[Inspector(typeof(GraphGroup))]
	public class GraphGroupInspector : ReflectedInspector
	{
		public GraphGroupInspector(Metadata metadata) : base(metadata) { }

		protected override void OnMemberChange(MemberMetadata member)
		{
			base.OnMemberChange(member);

			if (member.name == nameof(GraphGroup.commentAlignment))
			{
				LudiqGraphsEditorUtility.editedContext.value?.canvas.RepositionAll();
			}
		}
	}
}