using UnityEditor;
using UnityEngine;

namespace Ludiq
{
	public abstract class NodeWidget<TCanvas, TNode> : GraphElementWidget<TCanvas, TNode>, INodeWidget 
		where TCanvas : class, ICanvas
		where TNode : class, IGraphElement
	{
		protected NodeWidget(TCanvas canvas, TNode node) : base(canvas, node) { }



		#region Positioning

		public Rect outerPosition => EdgeToOuterPosition(edgePosition);

		public virtual Rect edgePosition => position;

		public Rect innerPosition => EdgeToInnerPosition(edgePosition);

		public override Rect clippingPosition => outerPosition;

		protected override Rect warningsPosition => edgePosition;

		protected virtual Rect EdgeToOuterPosition(Rect position)
		{
			return GraphGUI.GetNodeEdgeToOuterPosition(position, shape);
		}

		protected virtual Rect OuterToEdgePosition(Rect position)
		{
			return GraphGUI.GetNodeOuterToEdgePosition(position, shape);
		}

		protected virtual Rect EdgeToInnerPosition(Rect position)
		{
			return GraphGUI.GetNodeEdgeToInnerPosition(position, shape);
		}

		protected virtual Rect InnerToEdgePosition(Rect position)
		{
			return GraphGUI.GetNodeInnerToEdgePosition(position, shape);
		}

		#endregion



		#region Drawing

		protected abstract NodeShape shape { get; }

		protected abstract NodeColorMix color { get; }

		protected bool invertForeground => EditorGUIUtility.isProSkin && (color.IsPure(NodeColor.Yellow));
		
		public override void DrawForeground()
		{
			base.DrawForeground();

			if (e.IsRepaint)
			{
				// using (LudiqGUI.color.Override(isDragging ? ColorUtility.Gray(0.8f) : Color.white))
				{
					GraphGUI.Node(edgePosition.PixelPerfect(), shape, color, !canvas.isScreenshotting && (isMouseOver || isSelected));
				}
			}
		}

		public override void DrawOverlay()
		{
			if (LudiqCore.Configuration.developerMode && LudiqGraphs.Configuration.debug)
			{
				LudiqGUI.DrawEmptyRect(outerPosition, Color.yellow.WithAlpha(0.5f));
				LudiqGUI.DrawEmptyRect(edgePosition, Color.yellow.WithAlpha(0.5f));
				LudiqGUI.DrawEmptyRect(innerPosition, Color.yellow.WithAlpha(0.5f));
			}

			base.DrawOverlay();
		}

		#endregion
	}
}