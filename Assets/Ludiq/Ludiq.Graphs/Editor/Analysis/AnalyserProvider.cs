﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Ludiq
{
	public sealed class AnalyserProvider : SingleDecoratorProvider<object, IAnalyser, AnalyserAttribute>, IDisposable
	{
		protected override bool cache => true;

		public GraphReference reference { get; }

		public AnalyserProvider(GraphReference reference)
		{
			this.reference = reference;
		}

		protected override IAnalyser CreateDecorator(Type decoratorType, object decorated)
		{
			return (IAnalyser)decoratorType.Instantiate(true, reference, decorated);
		}

		public override bool IsValid(object analyzed)
		{
			return !analyzed.IsUnityNull();
		}

		public void Dispose()
		{
			ClearListeners();
		}


		#region Shortcuts

		public void Analyze(object analyzed)
		{
			GetDecorator(analyzed).isDirty = true;
		}

		public void AnalyzeAll()
		{
			foreach (var analyser in decorators.Values)
			{
				analyser.isDirty = true;
			}
		}

		#endregion


		#region Events
		
		private readonly Dictionary<object, HashSet<Action>> listeners = new Dictionary<object, HashSet<Action>>();
		
		public void AddListener(object analyzable, Action onAnalysisChange)
		{
			if (!listeners.ContainsKey(analyzable))
			{
				listeners.Add(analyzable, new HashSet<Action>());
			}

			listeners[analyzable].Add(onAnalysisChange);
		}

		public void RemoveListener(object analyzable, Action onAnalysisChange)
		{
			if (!listeners.ContainsKey(analyzable))
			{
				Debug.LogWarning($"Trying to remove unknown analysis change listener for '{analyzable}'.");

				return;
			}

			listeners[analyzable].Remove(onAnalysisChange);

			if (listeners[analyzable].Count == 0)
			{
				listeners.Remove(analyzable);
			}
		}

		public void ClearListeners()
		{
			listeners.Clear();
		}

		public void OnAnalysisChanged(object describable)
		{
			if (!listeners.ContainsKey(describable))
			{
				return;
			}

			foreach (var onDescriptionChange in listeners[describable])
			{
				onDescriptionChange?.Invoke();
			}
		}
		
		#endregion
	}

	public static class XAnalyserProvider
	{
		// Analysis are conceptually reference-bound, but practically context-bound,
		// so it's faster to avoid the reference-to-context lookup if we can avoid it.

		public static void Analyze(this object target, IGraphContext context)
		{
			context.analyserProvider.Analyze(target);
		}

		public static bool HasAnalyser(this object target, IGraphContext context)
		{
			Ensure.That(nameof(target)).IsNotNull(target);

			return context.analyserProvider.HasDecorator(target.GetType());
		}

		public static IAnalyser Analyser(this object target, IGraphContext context)
		{
			return context.analyserProvider.GetDecorator(target);
		}

		public static TAnalyser Analyser<TAnalyser>(this object target, IGraphContext context) where TAnalyser : IAnalyser
		{
			return context.analyserProvider.GetDecorator<TAnalyser>(target);
		}

		public static IAnalysis Analysis(this object target, IGraphContext context)
		{
			var analyser = target.Analyser(context);
			analyser.Validate();
			return analyser.analysis;
		}

		public static TAnalysis Analysis<TAnalysis>(this object target, IGraphContext context) where TAnalysis : IAnalysis
		{
			return (TAnalysis)target.Analysis(context);
		}
		
		// Shortcuts, but the above are faster because Context doesn't have to be looked up

		public static void Analyze(this object target, GraphReference reference)
		{
			target.Analyze(reference.Context());
		}

		public static bool HasAnalyser(this object target, GraphReference reference)
		{
			return target.HasAnalyser(reference.Context());
		}

		public static IAnalyser Analyser(this object target, GraphReference reference)
		{
			return target.Analyser(reference.Context());
		}

		public static TAnalyser Analyser<TAnalyser>(this object target, GraphReference reference) where TAnalyser : IAnalyser
		{
			return target.Analyser<TAnalyser>(reference.Context());
		}

		public static IAnalysis Analysis(this object target, GraphReference reference)
		{
			return target.Analysis(reference.Context());
		}

		public static TAnalysis Analysis<TAnalysis>(this object target, GraphReference reference) where TAnalysis : IAnalysis
		{
			return target.Analysis<TAnalysis>(reference.Context());
		}
	}
}
