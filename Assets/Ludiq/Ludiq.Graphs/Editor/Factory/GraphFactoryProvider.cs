﻿namespace Ludiq
{
	public class GraphFactoryProvider : SingleDecoratorProvider<IGraphParent, IGraphFactory, GraphFactoryAttribute>
	{
		public static GraphFactoryProvider instance { get; } = new GraphFactoryProvider();

		protected override bool cache => false;

		public override bool IsValid(IGraphParent decorated)
		{
			return true;
		}
	}

	public static class XGraphFactoryProvider
	{
		public static IGraphFactory GraphFactory(this IGraphParent parent)
		{
			return GraphFactoryProvider.instance.GetDecorator(parent);
		}
	}
}
