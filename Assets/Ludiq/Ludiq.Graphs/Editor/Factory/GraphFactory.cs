﻿namespace Ludiq
{
	public abstract class GraphFactory<TGraphParent, TGraph> : IGraphFactory
		where TGraphParent : IGraphParent
		where TGraph : IGraph
	{
		public GraphFactory(TGraphParent parent)
		{
			this.parent = parent;
		}

		public TGraphParent parent { get; }

		IGraph IGraphFactory.CreateDefaultGraph() => DefaultGraph();

		public abstract TGraph DefaultGraph();
	}
}
