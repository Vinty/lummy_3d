﻿using System;

namespace Ludiq
{
	[AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = true)]
	public sealed class GraphFactoryAttribute : Attribute, IDecoratorAttribute
	{
		public GraphFactoryAttribute(Type graphParentType)
		{
			Ensure.That(nameof(graphParentType)).IsNotNull(graphParentType);
			this.type = graphParentType;
		}

		public Type type { get; }
	}
}
