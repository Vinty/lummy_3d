﻿namespace Ludiq
{
	public interface IGraphFactory
	{
		IGraph CreateDefaultGraph();
	}
}
