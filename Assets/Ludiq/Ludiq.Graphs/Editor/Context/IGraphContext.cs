﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Ludiq
{
	public interface IGraphContext : IDisposable
	{
		GraphReference reference { get; }

		IEnumerable<IGraphContextExtension> extensions { get; }

		IGraph graph { get; }

		ICanvas canvas { get; }

		GraphSelection selection { get; }

		Metadata graphMetadata { get; }

		Metadata selectionMetadata { get; }

		Metadata ElementMetadata(IGraphElement element);

		AnalyserProvider analyserProvider { get; }

		string windowTitle { get; }
		
		bool isPrefabInstance { get; }
		
		event Action onEdited;

		void BeginEdit(bool disablePrefabInstance = true);

		void EndEdit();

		void DescribeAndAnalyze();
	}
}
