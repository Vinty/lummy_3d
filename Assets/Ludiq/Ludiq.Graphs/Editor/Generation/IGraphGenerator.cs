﻿using System;
using Ludiq.CodeDom;

namespace Ludiq
{
	public interface IGraphGenerator : IGenerator
	{
		GeneratedGraph Generate(GraphGenerationSystem generationSystem, string namePrefix);
	}
}
