﻿using Ludiq.CodeDom;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;
using Debug = UnityEngine.Debug;

namespace Ludiq
{
	[BackgroundWorker]
	public static class Generation
	{
		public static void Generate()
		{
			if (LudiqGraphs.Configuration.generateRootEmbedGraphs && !(Task.allowWindowRunner || EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo()))
			{
				return;
			}

			var system = new GraphGenerationSystem(GraphGenerationMode.Normal);

			Task.Run("Generating...", 4, generation =>
			{
				var aotStubs = new HashSet<object>();

				generation.StartStep("Generating graphs in assets...");

				var rootAssets = UnityAPI.Await(() => FindAllAssetGraphRoots().ToHashSet());

				if (rootAssets.Count > 0)
				{
					Task.Run("Generating graphs in assets...", rootAssets.Count, assetsGeneration =>
					{
						foreach (var rootAsset in rootAssets)
						{
							assetsGeneration.StartStep(rootAsset.childGraph.Description().title);
							Generate(system, rootAsset, null);
							assetsGeneration.CompleteStep();
						}
					});
				}

				generation.CompleteStep();
				
				generation.StartStep("Generating graphs in scenes...");

				if (LudiqGraphs.Configuration.generateRootEmbedGraphs)
				{
					var activeScenePath = UnityAPI.Await(() => SceneManager.GetActiveScene().path);

					var scenePaths = UnityAPI.Await(() => EditorBuildSettings.scenes.Select(s => s.path).ToList());

					Task.Run("Generating graphs in scenes...", scenePaths.Count + 1, scenesGeneration =>
					{
						GenerateActiveScene(system, scenesGeneration);
						
						foreach (var scenePath in scenePaths)
						{
							UnityAPI.Await(() => EditorSceneManager.OpenScene(scenePath));

							GenerateActiveScene(system, scenesGeneration);

							aotStubs.UnionWith(AotPreBuilder.FindAllSceneStubs());
						}
					});

					UnityAPI.Await(() => EditorSceneManager.OpenScene(activeScenePath));
				}

				generation.CompleteStep();
				
				generation.StartStep("Writing generated scripts to disk...");

				PathUtility.CreateDirectoryIfNeeded(LudiqGraphs.Paths.scripts);

				var writerSystem = new FileCodeWriterSystem(LudiqGraphs.Paths.scripts);
				
				system.WriteClasses(writerSystem);

				outdatedGraphs.Clear();

				generation.CompleteStep();

				generation.StartStep("Generating AOT compatibility patches...");

				aotStubs.UnionWith(AotPreBuilder.FindAllProjectStubs());
				
				var stubScriptPath = Path.Combine(LudiqCore.Paths.transientGenerated, "AotStubs.cs");
				AotPreBuilder.GenerateStubScript(stubScriptPath, aotStubs);
				
				var linkerPath = Path.Combine(LudiqCore.Paths.transientGenerated, "link.xml");
				AotPreBuilder.GenerateLinker(linkerPath);

				generation.CompleteStep();

				UnityAPI.Async(AssetDatabase.SaveAssets);

				UnityAPI.Async(AssetDatabase.Refresh);

				Debug.Log($"Script generation succeeded.\n{system.GeneratedGraphCount} graphs were converted to C#.");
			});
		}

		private static void Generate(GraphGenerationSystem system, IGraphRoot root, Scene? scene)
		{
			var graph = root.childGraph;

			if (graph == null)
			{
				return;
			}

			if (scene != null)
			{
				system.SetGraphContainerPrefix(graph, UnityAPI.Await(() => scene.Value.name));
			}

			var generatedGraph = system.GenerateGraph(root);

			graph.machineScriptTypeName = GraphGenerationSystem.GeneratedScriptNamespace + "." + generatedGraph.machineScript.classDeclaration.Name;

			// Imported legacy graphs will have a zero version, which we need
			// to bump in order for Bolt to recognize that the scripts are up to date
			if (graph.liveVersionID == 0)
			{
				graph.liveVersionID = 1;
			}

			graph.generatedVersionID = graph.liveVersionID;

			var serializedObject = root.serializedObject;

			if (serializedObject != null)
			{
				UnityAPI.Await(() => EditorUtility.SetDirty(serializedObject));

				if (scene != null)
				{
					UnityAPI.Await(() => EditorSceneManager.MarkSceneDirty(scene.Value));
				}
			}
		}
		
		private static void GenerateActiveScene(GraphGenerationSystem system, Task scenesGeneration)
		{
			var activeScene = UnityAPI.Await(SceneManager.GetActiveScene);

			var activeScenePath = UnityAPI.Await(() => activeScene.path);

			scenesGeneration.StartStep(activeScenePath);

			var sceneRoots = UnityAPI.Await(() => FindAllSceneGraphRoots().ToHashSet());

			if (sceneRoots.Count > 0)
			{
				Task.Run(activeScenePath, sceneRoots.Count, sceneGeneration =>
				{
					foreach (var sceneRoot in sceneRoots)
					{
						sceneGeneration.StartStep(sceneRoot.childGraph.Description().title);
						Generate(system, sceneRoot, activeScene);
						sceneGeneration.CompleteStep();
					}
				});
			}

			if (UnityAPI.Await(() => activeScene.IsValid() && activeScene.isDirty))
			{
				UnityAPI.Await(() => EditorSceneManager.SaveScene(activeScene));
			}
						
			scenesGeneration.CompleteStep();
		}
		
		public static void Clean()
		{
			var scriptDirectoryInfo = new DirectoryInfo(LudiqGraphs.Paths.scripts);
			
			if (scriptDirectoryInfo.Exists)
			{
				scriptDirectoryInfo.Delete(true);

				AssetDatabase.DeleteAsset(PathUtility.FromProject(LudiqGraphs.Paths.scripts));
			}

			AssetDatabase.Refresh();
		}

		private static IEnumerable<IGraphRoot> FindAllAssetGraphRoots()
		{
			foreach (var rootAsset in AssetUtility.GetAllAssetsOfType<IGraphRoot>())
			{
				if (rootAsset.childGraph != null)
				{
					yield return rootAsset;
				}
			}

			foreach (var prefab in AssetUtility.GetAllAssetsOfType<GameObject>())
			{
				foreach (var rootComponent in prefab.GetComponents<IGraphRoot>())
				{
					if (rootComponent.childGraph != null)
					{
						yield return rootComponent;
					}
				}
			}
		}

		private static IEnumerable<IGraphRoot> FindAllSceneGraphRoots()
		{
			if (!LudiqGraphs.Configuration.generateRootEmbedGraphs)
			{
				yield break;
			}

			foreach (var rootComponent in UnityObjectUtility.FindObjectsOfTypeIncludingInactive<IGraphRoot>())
			{
				if (rootComponent.childGraph != null)
				{
					yield return rootComponent;
				}
			}
		}

		#region Comparison

		public static HashSet<IGraph> outdatedGraphs { get; } = new HashSet<IGraph>();

		public static int changes => outdatedGraphs.Count;

		private static void BackgroundWork()
		{
			Compare();
		}

		public static void Compare()
		{
			UnityAPI.Async(() =>
			{
				outdatedGraphs.UnionWith(LinqUtility
					.Concat<IGraphRoot>(FindAllAssetGraphRoots(), FindAllSceneGraphRoots())
					.Select(r => r.childGraph)
					.Where(g => g.generatedVersionID == 0 || g.generatedVersionID != g.liveVersionID));
			});
		}

		#endregion

		public static void PreCloudBuild()
		{
			Task.allowWindowRunner = false;
			Generate();
			Task.allowWindowRunner = true;
		}
	}
}
