﻿using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;
using UnityEditor;
using UnityObject = UnityEngine.Object;

namespace Ludiq
{
	public class GraphClassGenerationContext : IGraphClassGenerationContext
	{
		public const string GraphScriptNameSuffix = "GraphScript";
		public const string MachineScriptNameSuffix = "MachineScript";

		private Dictionary<object, Dictionary<string, string>> associatedMemberNamesByOwner = new Dictionary<object, Dictionary<string, string>>();
		private IdentifierPool classIdentifiers = new IdentifierPool();
		private HashSet<CodeCompositeTypeMember> implementationMembers = new HashSet<CodeCompositeTypeMember>();
		public Dictionary<IGraphElement, HashSet<CodeElement>> graphElementCodeBindings = new Dictionary<IGraphElement, HashSet<CodeElement>>();
		public HashSet<string> providedMachineEvents = new HashSet<string>();

		public GraphClassGenerationContext(GraphGenerationSystem generationSystem, IGraph graph, string namePrefix, CodeCompileUnit compileUnit, CodeClassTypeDeclaration classDeclaration, Type baseClass)
		{
			this.generationSystem = generationSystem;
			this.graph = graph;
			this.namePrefix = namePrefix;

			graphScriptTypeReference = CodeFactory.TypeRef(GraphGenerationSystem.GeneratedScriptNamespace + "." +  namePrefix + GraphScriptNameSuffix);
			machineScriptTypeReference = CodeFactory.TypeRef(GraphGenerationSystem.GeneratedScriptNamespace + "." + namePrefix + MachineScriptNameSuffix);

			this.compileUnit = compileUnit;
			this.classDeclaration = classDeclaration;

			classIdentifiers.ReserveMembers(baseClass);
		}



		public GraphGenerationSystem generationSystem { get; }

		public IGraph graph { get; }

		public string namePrefix { get; }

		public CodeCompileUnit compileUnit { get; }

		public CodeTypeReference graphScriptTypeReference { get; }

		public CodeTypeReference machineScriptTypeReference { get; }

		public CodeClassTypeDeclaration classDeclaration { get; }

		public HashSet<CodeUsingImport> usings => compileUnit.Usings;

		public CodePredeclaredType predeclaredType => new CodePredeclaredType(GraphGenerationSystem.GeneratedScriptNamespace, classDeclaration.Name);

		public HashSet<string> requiredMachineEvents { get; private set; } = new HashSet<string>();



		public string DeclareMemberName(object owner, string originalName)
		{
			var associatedName = classIdentifiers.CreateIdentifier(originalName);
			AliasMemberName(owner, originalName, associatedName);
			return associatedName;
		}

		public bool HasMemberName(object owner, string originalName)
		{
			if (associatedMemberNamesByOwner.TryGetValue(owner, out var associatedMembers))
			{
				return associatedMembers.ContainsKey(originalName);
			}
			return false;
		}

		public bool TryGetMemberName(object owner, string originalName, out string associatedName)
		{
			if (associatedMemberNamesByOwner.TryGetValue(owner, out var associatedMembers))
			{
				return associatedMembers.TryGetValue(originalName, out associatedName);
			}

			associatedName = null;
			return false;
		}

		public string ExpectMemberName(object owner, string originalName)
		{
			if (TryGetMemberName(owner, originalName, out var associatedName))
			{
				return associatedName;
			}
			throw new KeyNotFoundException();
		}

		public void AliasMemberName(object owner, string originalName, string associatedName)
		{
			if (!associatedMemberNamesByOwner.TryGetValue(owner, out var associatedMembers))
			{
				associatedMembers = new Dictionary<string, string>();
				associatedMemberNamesByOwner[owner] = associatedMembers;
			}
			associatedMembers.Add(originalName, associatedName);
		}

		public bool IsImplementationMember(CodeCompositeTypeMember member)
		{
			return implementationMembers.Contains(member);
		}

		public CodeCompositeTypeMember AddImplementationMember(CodeCompositeTypeMember member)
		{
			implementationMembers.Add(member);
			return member;
		}

		public TMember AddImplementationMember<TMember>(TMember member) where TMember : CodeCompositeTypeMember
		{
			implementationMembers.Add(member);
			return member;
		}

		public void BindCode(IGraphElement graphElement, IEnumerable<CodeElement> codeElements)
		{
			if (!graphElementCodeBindings.TryGetValue(graphElement, out var graphElementCodeBinding))
			{
				graphElementCodeBinding = new HashSet<CodeElement>();
				graphElementCodeBindings[graphElement] = graphElementCodeBinding;
			}

			foreach (var codeElement in codeElements)
			{
				graphElementCodeBinding.Add(codeElement);

				BindCode(graphElement, codeElement.Children);
			}
		}

		public IEnumerable<CodeElement> CodeFor(IGraphElement graphElement)
		{
			if (graphElementCodeBindings.TryGetValue(graphElement, out var codeElementsByGraphElement))
			{
				return codeElementsByGraphElement;
			}

			return Enumerable.Empty<CodeElement>();
		}

		public void WriteClass(ICodeWriterSystem writerSystem, CodeGeneratorOptions options)
		{
			using (var writer = writerSystem.OpenWriter(classDeclaration.Name))
			{
				CodeGenerator.GenerateCodeFromCompileUnit(compileUnit, writer, options);

				writer.Flush();
			}
		}
	}
}
