﻿using Ludiq.CodeDom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ludiq
{
	public interface IGraphClassGenerationContext
	{
		IGraph graph { get; }

		void BindCode(IGraphElement graphElement, IEnumerable<CodeElement> codeElements);
	}
}
