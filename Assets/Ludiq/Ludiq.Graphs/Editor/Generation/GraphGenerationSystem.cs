﻿using Ludiq;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;

namespace Ludiq
{
	public sealed class GraphGenerationSystem
	{
        public const string GeneratedScriptNamespace = "Bolt.Generated";

		private IdentifierPool graphNamePool = new IdentifierPool();
		private Dictionary<IGraph, string> graphContainerPrefixes = new Dictionary<IGraph, string>();
		private Dictionary<IGraph, GeneratedGraph> generatedGraphs = new Dictionary<IGraph, GeneratedGraph>();

		public GraphGenerationSystem(GraphGenerationMode graphGenerationMode)
		{
			this.graphGenerationMode = graphGenerationMode;
		}

		public GraphGenerationMode graphGenerationMode { get; }

		public int GeneratedGraphCount => generatedGraphs.Count;

		public bool TryGetGraphContainerPrefix(IGraph graph, out string graphContainerPrefix)
		{
			return graphContainerPrefixes.TryGetValue(graph, out graphContainerPrefix);
		}

		public void SetGraphContainerPrefix(IGraph graph, string graphContainerPrefix)
		{
			graphContainerPrefixes[graph] = graphContainerPrefix;
		}

		public GeneratedGraph GetGeneratedGraph(IGraph graph)
		{
			return generatedGraphs[graph];
		}

		public GeneratedGraph GenerateGraph(IGraphParent graphParent)
		{
			var graph = graphParent.childGraph;

			if (!generatedGraphs.TryGetValue(graph, out var generatedGraph))
			{			
				var graphGenerator = graph.Generator<IGraphGenerator>();
				var title = GraphParentDescriptor.Title(graphParent);

				if (TryGetGraphContainerPrefix(graph, out var containerPrefix))
				{
					title = containerPrefix + " " + title;
				}

				var namePrefix = graphNamePool.CreateIdentifier(title.FirstCharacterToUpper());

				using (ProfilingUtility.SampleBlock($"Generate graph {namePrefix}"))
				{
					generatedGraph = graphGenerator.Generate(this, namePrefix);
					generatedGraphs[graph] = generatedGraph;
				}
			}

			return generatedGraph;
		}

		public void WriteClasses(ICodeWriterSystem writerSystem)
		{
			var options = new CodeGeneratorOptions(predeclaredTypes: generatedGraphs.Values.SelectMany(g => g.PredeclaredTypes));

			foreach (var generatedGraph in generatedGraphs.Values)
			{
				generatedGraph.WriteClasses(writerSystem, options);
			}
		}
	}
}
