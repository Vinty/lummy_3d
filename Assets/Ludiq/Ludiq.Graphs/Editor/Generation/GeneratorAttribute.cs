﻿using System;

namespace Ludiq
{
	[AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = true)]
	public sealed class GeneratorAttribute : Attribute, IDecoratorAttribute
	{
		public GeneratorAttribute(Type type)
		{
			this.type = type;
		}

		public Type type { get; private set; }
	}
}
