﻿using Ludiq.CodeDom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ludiq
{
	public abstract class GraphMethodGenerationScope<TGraphMethodGenerationScope>
		where TGraphMethodGenerationScope : GraphMethodGenerationScope<TGraphMethodGenerationScope>
	{
		protected TGraphMethodGenerationScope parentScope { get; }
		protected IdentifierPool identifierPool { get; }

		public GraphMethodGenerationScope(TGraphMethodGenerationScope parentScope, IdentifierPool identifierPool)
		{
			this.parentScope = parentScope;
			this.identifierPool = identifierPool;
		}

		public CodeVariableDeclarationStatement DeclareLocal(CodeTypeReference type, string originalName, CodeExpression initExpression = null)
		{
			var name = identifierPool.CreateIdentifier(originalName);
			return new CodeVariableDeclarationStatement(type, name, initExpression);
		}
	}
}
