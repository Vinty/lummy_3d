﻿using System;
using Ludiq.CodeDom;

namespace Ludiq
{
	[Generator(typeof(String))]	
	public class StringConstantGenerator : ConstantGenerator<String>
	{
		public StringConstantGenerator(String value) : base(value) {}

		public override CodeExpression GenerateExpression() => CodeFactory.Primitive(value);
	}
}
