﻿using System;
using Ludiq.CodeDom;

namespace Ludiq
{
	[Generator(typeof(Int16))]	
	public class Int16ConstantGenerator : ConstantGenerator<Int16>
	{
		public Int16ConstantGenerator(Int16 value) : base(value) {}

		public override CodeExpression GenerateExpression() => CodeFactory.Primitive(value);
	}
}
