﻿using System;
using Ludiq.CodeDom;

namespace Ludiq
{
	[Generator(typeof(bool))]	
	public class BoolConstantGenerator : ConstantGenerator<bool>
	{
		public BoolConstantGenerator(bool value) : base(value) {}

		public override CodeExpression GenerateExpression() => CodeFactory.Primitive(value);
	}
}
