﻿using System;
using Ludiq.CodeDom;

namespace Ludiq
{
	[Generator(typeof(Int64))]	
	public class Int64ConstantGenerator : ConstantGenerator<Int64>
	{
		public Int64ConstantGenerator(Int64 value) : base(value) {}

		public override CodeExpression GenerateExpression() => CodeFactory.Primitive(value);
	}
}
