﻿using System;
using Ludiq.CodeDom;

namespace Ludiq
{
	[Generator(typeof(SByte))]	
	public class SByteConstantGenerator : ConstantGenerator<SByte>
	{
		public SByteConstantGenerator(SByte value) : base(value) {}

		public override CodeExpression GenerateExpression() => CodeFactory.Primitive(value);
	}
}
