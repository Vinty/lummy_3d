﻿using System;
using Ludiq.CodeDom;

namespace Ludiq
{
	[Generator(typeof(UInt32))]	
	public class UInt32ConstantGenerator : ConstantGenerator<UInt32>
	{
		public UInt32ConstantGenerator(UInt32 value) : base(value) {}

		public override CodeExpression GenerateExpression() => CodeFactory.Primitive(value);
	}
}
