﻿using System;
using Ludiq.CodeDom;

namespace Ludiq
{
	[Generator(typeof(Decimal))]	
	public class DecimalConstantGenerator : ConstantGenerator<Decimal>
	{
		public DecimalConstantGenerator(Decimal value) : base(value) {}

		public override CodeExpression GenerateExpression() => CodeFactory.Primitive(value);
	}
}
