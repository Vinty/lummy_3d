﻿using System;
using Ludiq.CodeDom;

namespace Ludiq
{
	[Generator(typeof(Type))]	
	public class TypeConstantGenerator : ConstantGenerator<Type>
	{
		public TypeConstantGenerator(Type value) : base(value) {}

		public override CodeExpression GenerateExpression() => CodeFactory.TypeRef(value).TypeOf();
	}
}
