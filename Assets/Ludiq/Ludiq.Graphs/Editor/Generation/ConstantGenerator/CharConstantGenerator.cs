﻿using System;
using Ludiq.CodeDom;

namespace Ludiq
{
	[Generator(typeof(Char))]	
	public class CharConstantGenerator : ConstantGenerator<Char>
	{
		public CharConstantGenerator(char value) : base(value) {}

		public override CodeExpression GenerateExpression() => CodeFactory.Primitive(value);
	}
}
