﻿using Ludiq.CodeDom;

namespace Ludiq
{
	public interface IConstantGenerator : IGenerator
	{
		CodeExpression GenerateExpression();
	}
}
