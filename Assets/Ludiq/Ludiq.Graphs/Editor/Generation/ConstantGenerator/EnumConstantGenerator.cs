﻿using System;
using Ludiq;
using Ludiq.CodeDom;

namespace Ludiq
{
	[Generator(typeof(Enum))]	
	public class EnumConstantGenerator : ConstantGenerator<Enum>
	{
		public EnumConstantGenerator(Enum value) : base(value) {}

		public override CodeExpression GenerateExpression()
		{
			var enumType = value.GetType();
			var typeRef = CodeFactory.TypeRef(enumType);

			if (enumType.IsFlagsEnum())
			{
				var result = (CodeExpression) null;
				var remainingFlags = Convert.ToInt64(value);

				foreach (var enumMember in EnumUtility.ValuesByNames(enumType))
				{
					var enumMemberFlags = Convert.ToInt64(enumMember.Value);

					if (((remainingFlags & enumMemberFlags) == enumMemberFlags && enumMemberFlags != 0)
					|| (result == null && remainingFlags == 0))
					{
						var flagsExpression = typeRef.Expression().Field(enumMember.Key);
						result = result != null ? result.BitwiseOr(flagsExpression) : (CodeExpression) flagsExpression;
						remainingFlags &= ~enumMemberFlags;
					}
				}

				if (remainingFlags != 0)
				{
					var flagsExpression = CodeFactory.Primitive(remainingFlags.ConvertTo(Enum.GetUnderlyingType(enumType))).Cast(typeRef);
					result = result != null ? result.BitwiseOr(flagsExpression) : (CodeExpression) flagsExpression;
				}

				if (result == null)
				{
					result = CodeFactory.Primitive(0);
				}

				return result;
			}
			else
			{
				var name = Enum.GetName(enumType, value);

				if (name != null)
				{
					return typeRef.Expression().Field(name);
				}
				else
				{
					return CodeFactory.Primitive(value.ConvertTo(Enum.GetUnderlyingType(enumType))).Cast(typeRef);
				}
			}
		}
	}
}
