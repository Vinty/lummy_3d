﻿using System;
using Ludiq.CodeDom;

namespace Ludiq
{
	[Generator(typeof(Single))]	
	public class SingleConstantGenerator : ConstantGenerator<Single>
	{
		public SingleConstantGenerator(Single value) : base(value) {}

		public override CodeExpression GenerateExpression() => CodeFactory.Primitive(value);
	}
}
