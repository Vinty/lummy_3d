﻿using System;
using Ludiq.CodeDom;
using UnityEngine;

namespace Ludiq
{
	[Generator(typeof(RectInt))]	
	public class RectIntConstantGenerator : ConstantGenerator<RectInt>
	{
		public RectIntConstantGenerator(RectInt value) : base(value) {}

		public override CodeExpression GenerateExpression()
		{
			return CodeFactory.TypeRef(typeof(RectInt)).ObjectCreate(
				CodeFactory.Primitive(value.x),
				CodeFactory.Primitive(value.y),
				CodeFactory.Primitive(value.width),
				CodeFactory.Primitive(value.height)
			);
		}
	}
}
