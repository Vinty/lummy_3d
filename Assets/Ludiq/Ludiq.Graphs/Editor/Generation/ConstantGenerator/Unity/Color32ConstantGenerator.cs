﻿using System;
using Ludiq.CodeDom;
using UnityEngine;

namespace Ludiq
{
	[Generator(typeof(Color32))]	
	public class Color32ConstantGenerator : ConstantGenerator<Color32>
	{
		public Color32ConstantGenerator(Color32 value) : base(value) {}

		public override CodeExpression GenerateExpression()
		{
			return CodeFactory.TypeRef(typeof(Color32)).ObjectCreate(
				CodeFactory.Primitive(value.r),
				CodeFactory.Primitive(value.g),
				CodeFactory.Primitive(value.b),
				CodeFactory.Primitive(value.a));
		}
	}
}
