﻿using System;
using Ludiq.CodeDom;
using UnityEngine;

namespace Ludiq
{
	[Generator(typeof(LayerMask))]	
	public class LayerMaskConstantGenerator : ConstantGenerator<LayerMask>
	{
		public LayerMaskConstantGenerator(LayerMask value) : base(value) {}

		public override CodeExpression GenerateExpression()
		{
			return CodeFactory.Primitive(value.value).Cast(CodeFactory.TypeRef(typeof(LayerMask)));
		}
	}
}

