﻿using System;
using Ludiq.CodeDom;
using UnityEngine;

namespace Ludiq
{
	[Generator(typeof(Color))]	
	public class ColorConstantGenerator : ConstantGenerator<Color>
	{
		public ColorConstantGenerator(Color value) : base(value) {}

		public override CodeExpression GenerateExpression()
		{
			var typeRef = CodeFactory.TypeRef(typeof(Color));

			if (value == Color.clear) return typeRef.Expression().Field("clear");
			else if (value == Color.red) return typeRef.Expression().Field("red");
			else if (value == Color.green) return typeRef.Expression().Field("green");
			else if (value == Color.blue) return typeRef.Expression().Field("blue");
			else if (value == Color.cyan) return typeRef.Expression().Field("cyan");
			else if (value == Color.magenta) return typeRef.Expression().Field("magenta");
			else if (value == Color.yellow) return typeRef.Expression().Field("yellow");
			else if (value == Color.black) return typeRef.Expression().Field("black");
			else if (value == Color.white) return typeRef.Expression().Field("white");
			else
			{
				return typeRef.ObjectCreate(
					CodeFactory.Primitive(value.r),
					CodeFactory.Primitive(value.g),
					CodeFactory.Primitive(value.b),
					CodeFactory.Primitive(value.a));
			}
		}
	}
}
