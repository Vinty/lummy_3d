﻿using System;
using Ludiq.CodeDom;
using UnityEngine;

namespace Ludiq
{
	[Generator(typeof(Ray))]	
	public class RayConstantGenerator : ConstantGenerator<Ray>
	{
		public RayConstantGenerator(Ray value) : base(value) {}

		public override CodeExpression GenerateExpression()
		{
			return CodeFactory.TypeRef(typeof(Ray)).ObjectCreate(
				value.origin.Generator<IConstantGenerator>().GenerateExpression(),
				value.direction.Generator<IConstantGenerator>().GenerateExpression()
			);
		}
	}
}
