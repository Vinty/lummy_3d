﻿using System;
using Ludiq.CodeDom;
using UnityEngine;

namespace Ludiq
{
	[Generator(typeof(Vector2Int))]	
	public class Vector2IntConstantGenerator : ConstantGenerator<Vector2Int>
	{
		public Vector2IntConstantGenerator(Vector2Int value) : base(value) {}

		public override CodeExpression GenerateExpression()
		{
			var typeRef = CodeFactory.TypeRef(typeof(Vector2Int));

			if (value == Vector2Int.zero) return typeRef.Expression().Field("zero");
			else if (value == Vector2Int.one) return typeRef.Expression().Field("one");
			else if (value == Vector2Int.left) return typeRef.Expression().Field("left");
			else if (value == Vector2Int.right) return typeRef.Expression().Field("right");
			else if (value == Vector2Int.up) return typeRef.Expression().Field("up");
			else if (value == Vector2Int.down) return typeRef.Expression().Field("down");
			else
			{
				return typeRef.ObjectCreate(
					CodeFactory.Primitive(value.x),
					CodeFactory.Primitive(value.y)
				);
			}
		}
	}
}
