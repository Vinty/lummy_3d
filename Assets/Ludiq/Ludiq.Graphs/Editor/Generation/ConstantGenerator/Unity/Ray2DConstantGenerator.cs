﻿using System;
using Ludiq.CodeDom;
using UnityEngine;

namespace Ludiq
{
	[Generator(typeof(Ray2D))]	
	public class Ray2DConstantGenerator : ConstantGenerator<Ray2D>
	{
		public Ray2DConstantGenerator(Ray2D value) : base(value) {}

		public override CodeExpression GenerateExpression()
		{
			return CodeFactory.TypeRef(typeof(Ray2D)).ObjectCreate(
				value.origin.Generator<IConstantGenerator>().GenerateExpression(),
				value.direction.Generator<IConstantGenerator>().GenerateExpression()
			);
		}
	}
}
