﻿using System;
using Ludiq.CodeDom;
using UnityEngine;

namespace Ludiq
{
	[Generator(typeof(Vector3Int))]	
	public class Vector3IntConstantGenerator : ConstantGenerator<Vector3Int>
	{
		public Vector3IntConstantGenerator(Vector3Int value) : base(value) {}

		public override CodeExpression GenerateExpression()
		{
			var typeRef = CodeFactory.TypeRef(typeof(Vector3Int));

			if (value == Vector3Int.zero) return typeRef.Expression().Field("zero");
			else if (value == Vector3Int.one) return typeRef.Expression().Field("one");
			else if (value == Vector3Int.left) return typeRef.Expression().Field("left");
			else if (value == Vector3Int.right) return typeRef.Expression().Field("right");
			else if (value == Vector3Int.up) return typeRef.Expression().Field("up");
			else if (value == Vector3Int.down) return typeRef.Expression().Field("down");
			else
			{
				return typeRef.ObjectCreate(
					CodeFactory.Primitive(value.x),
					CodeFactory.Primitive(value.y),
					CodeFactory.Primitive(value.z)
				);
			}
		}
	}
}
