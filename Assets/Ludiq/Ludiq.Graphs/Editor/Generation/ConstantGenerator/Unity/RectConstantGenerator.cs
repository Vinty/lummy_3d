﻿using System;
using Ludiq.CodeDom;
using UnityEngine;

namespace Ludiq
{
	[Generator(typeof(Rect))]	
	public class RectConstantGenerator : ConstantGenerator<Rect>
	{
		public RectConstantGenerator(Rect value) : base(value) {}

		public override CodeExpression GenerateExpression()
		{
			var typeRef = CodeFactory.TypeRef(typeof(Rect));

			if (value == Rect.zero) return typeRef.Expression().Field("zero");
			else
			{
				return typeRef.ObjectCreate(
					CodeFactory.Primitive(value.x),
					CodeFactory.Primitive(value.y),
					CodeFactory.Primitive(value.width),
					CodeFactory.Primitive(value.height)
				);
			}
		}
	}
}
