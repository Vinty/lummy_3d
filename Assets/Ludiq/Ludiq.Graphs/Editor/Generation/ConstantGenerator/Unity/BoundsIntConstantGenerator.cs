﻿using System;
using Ludiq.CodeDom;
using UnityEngine;

namespace Ludiq
{
	[Generator(typeof(BoundsInt))]	
	public class BoundsIntConstantGenerator : ConstantGenerator<BoundsInt>
	{
		public BoundsIntConstantGenerator(BoundsInt value) : base(value) {}

		public override CodeExpression GenerateExpression()
		{
			return CodeFactory.TypeRef(typeof(BoundsInt)).ObjectCreate(
				value.position.Generator<IConstantGenerator>().GenerateExpression(),
				value.size.Generator<IConstantGenerator>().GenerateExpression()
			);
		}
	}
}
