﻿using System;
using Ludiq.CodeDom;
using UnityEngine;

namespace Ludiq
{
	[Generator(typeof(Bounds))]	
	public class BoundsConstantGenerator : ConstantGenerator<Bounds>
	{
		public BoundsConstantGenerator(Bounds value) : base(value) {}

		public override CodeExpression GenerateExpression()
		{
			return CodeFactory.TypeRef(typeof(Bounds)).ObjectCreate(
				value.center.Generator<IConstantGenerator>().GenerateExpression(),
				value.size.Generator<IConstantGenerator>().GenerateExpression()
			);
		}
	}
}
