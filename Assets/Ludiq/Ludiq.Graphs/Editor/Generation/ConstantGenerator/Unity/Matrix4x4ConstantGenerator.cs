﻿using System;
using Ludiq.CodeDom;
using UnityEngine;

namespace Ludiq
{
	[Generator(typeof(Matrix4x4))]	
	public class Matrix4x4ConstantGenerator : ConstantGenerator<Matrix4x4>
	{
		public Matrix4x4ConstantGenerator(Matrix4x4 value) : base(value) {}

		public override CodeExpression GenerateExpression()
		{
			return CodeFactory.TypeRef(typeof(Matrix4x4)).ObjectCreate(
				value.GetColumn(0).Generator<IConstantGenerator>().GenerateExpression(),
				value.GetColumn(1).Generator<IConstantGenerator>().GenerateExpression(),
				value.GetColumn(2).Generator<IConstantGenerator>().GenerateExpression(),
				value.GetColumn(3).Generator<IConstantGenerator>().GenerateExpression()
			);
		}
	}
}
