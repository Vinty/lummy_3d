﻿
namespace Ludiq
{
	// FIXME: actually have pooled GeneratorProviders, so that they have independent caches.

	public class GeneratorProvider : SingleDecoratorProvider<object, IGenerator, GeneratorAttribute>
	{
		static GeneratorProvider()
		{
			instance = new GeneratorProvider();
		}

		protected override bool cache => true;

		public override bool IsValid(object decorated)
		{
			return true;
		}

		public static GeneratorProvider instance { get; }
	}

	public static class XGeneratorProvider
	{
		public static IGenerator Generator(this object o)
		{
			return GeneratorProvider.instance.GetDecorator(o);
		}

		public static TGenerator Generator<TGenerator>(this object o) where TGenerator : IGenerator
		{
			return GeneratorProvider.instance.GetDecorator<TGenerator>(o); 
		}

		public static bool HasGenerator(this object o)
		{
			Ensure.That(nameof(o)).IsNotNull(o);
			return GeneratorProvider.instance.HasDecorator(o.GetType());
		}

		public static bool HasGenerator<TGenerator>(this object o) where TGenerator : IGenerator
		{
			Ensure.That(nameof(o)).IsNotNull(o);
			return GeneratorProvider.instance.TryGetDecoratorType(o.GetType(), out var decoratorType)
				&& typeof(TGenerator).IsAssignableFrom(decoratorType);
		}

		public static bool TryGetGenerator(this object o, out IGenerator generator)
		{
			if (HasGenerator(o))
			{
				generator = Generator(o);
				return true;
			}
			else
			{
				generator = null;
				return false;
			}
		}

		public static bool TryGetGenerator<TGenerator>(this object o, out TGenerator generator) where TGenerator : IGenerator
		{
			if (HasGenerator<TGenerator>(o))
			{
				generator = Generator<TGenerator>(o);
				return true;
			}
			else
			{
				generator = default(TGenerator);
				return false;
			}
		}
	}
}