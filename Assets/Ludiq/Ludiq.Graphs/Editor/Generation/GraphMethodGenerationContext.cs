﻿using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Ludiq
{
	public abstract class GraphMethodGenerationContext<TGraphMethodGenerationContext, TGraphMethodGenerationScope> : IGraphMethodGenerationContext
		where TGraphMethodGenerationContext : GraphMethodGenerationContext<TGraphMethodGenerationContext, TGraphMethodGenerationScope>
		where TGraphMethodGenerationScope : GraphMethodGenerationScope<TGraphMethodGenerationScope>
	{
		protected IdentifierPool identifierPool = new IdentifierPool();
		protected List<TGraphMethodGenerationScope> scopeChain = new List<TGraphMethodGenerationScope>();
		protected List<IGraphParentElement> parentElementStack = new List<IGraphParentElement>();



		public GraphMethodGenerationContext(GraphClassGenerationContext graphClassContext, bool coroutine)
		{
			this.graphClassContext = graphClassContext;
			this.coroutine = coroutine;
		}



		IGraphClassGenerationContext IGraphMethodGenerationContext.graphClassContext => graphClassContext;

		public GraphClassGenerationContext graphClassContext { get; }

		public bool coroutine { get; }

		public Dictionary<string, CodeVariableDeclarationStatement> argumentLocals { get; } = new Dictionary<string, CodeVariableDeclarationStatement>();

		public List<CodeStatement> enterStatements { get; } = new List<CodeStatement>();

		public List<CodeStatement> exitStatements { get; } = new List<CodeStatement>();

		public TGraphMethodGenerationScope currentScope => scopeChain.Count > 0 ? scopeChain[scopeChain.Count - 1] : null;

		public IGraphParent currentParentElement => parentElementStack.Count > 0 ? parentElementStack[parentElementStack.Count - 1] : null;

		public bool canExitGraph => parentElementStack.Count > 0;



		public CodeMethodMember GenerateMethod(CodeMemberModifiers modifiers, CodeTypeReference returnType, string name, IEnumerable<CodeParameterDeclaration> parameters, Func<TGraphMethodGenerationContext, IEnumerable<CodeStatement>> generator)
		{
			return new CodeMethodMember(modifiers, returnType, name, parameters, GenerateMethodBody(parameters, generator));
		}

		public CodeConstructorMember GenerateConstructor(CodeMemberModifiers modifiers, IEnumerable<CodeParameterDeclaration> parameters, Func<TGraphMethodGenerationContext, IEnumerable<CodeStatement>> generator)
		{
			return new CodeConstructorMember(modifiers, parameters, GenerateMethodBody(parameters, generator));
		}

		public CodeConstructorMember GenerateConstructor(CodeMemberModifiers modifiers, IEnumerable<CodeParameterDeclaration> parameters, CodeConstructorInitializer initializer, Func<TGraphMethodGenerationContext, IEnumerable<CodeStatement>> generator)
		{
			return new CodeConstructorMember(modifiers, parameters, initializer, GenerateMethodBody(parameters, generator));
		}

		public CodeStaticConstructorMember GenerateStaticConstructor(Func<TGraphMethodGenerationContext, IEnumerable<CodeStatement>> generator)
		{
			return new CodeStaticConstructorMember(GenerateMethodBody(Enumerable.Empty<CodeParameterDeclaration>(), generator));
		}

		public CodeUserPropertyAccessor GeneratePropertyAccessor(CodeMemberModifiers modifiers, IEnumerable<CodeParameterDeclaration> parameters, Func<TGraphMethodGenerationContext, IEnumerable<CodeStatement>> generator)
		{
			return new CodeUserPropertyAccessor(modifiers, GenerateMethodBody(Enumerable.Empty<CodeParameterDeclaration>(), generator));
		}

		private IEnumerable<CodeStatement> GenerateMethodBody(IEnumerable<CodeParameterDeclaration> parameters, Func<TGraphMethodGenerationContext, IEnumerable<CodeStatement>> generator)
		{
			EnterScope();
			argumentLocals.Clear();
			foreach (var parameter in parameters)
			{
				argumentLocals[parameter.Name] = currentScope.DeclareLocal(parameter.Type, parameter.Name);
			}

			var result = GenerateMethodBody(generator((TGraphMethodGenerationContext) this).ToList());
			ExitScope();

			return result;
		}

		private IEnumerable<CodeStatement> GenerateMethodBody(List<CodeStatement> statements)
		{
			foreach (var statement in enterStatements) yield return statement;
			foreach (var statement in statements) yield return statement;
			foreach (var statement in exitStatements) yield return statement;
		}

		protected abstract TGraphMethodGenerationScope CreateScope(IdentifierPool identifierPool);

		public void EnterScope()
		{
			scopeChain.Add(CreateScope(identifierPool));
		}

		public void ExitScope()
		{
			scopeChain.RemoveAt(scopeChain.Count - 1);
		}

        public int GetParentElementStackTop()
		{
			return parentElementStack.Count;
		}

        public void RestoreParentElementStackTop(int top)
		{
			if (top > parentElementStack.Count)
			{
				throw new InvalidOperationException();
			}
			while (parentElementStack.Count > top)
			{
				parentElementStack.RemoveAt(parentElementStack.Count - 1);
			}
		}

        public void EnterParentElement(IGraphParentElement parentElement)
        {
            parentElementStack.Add(parentElement);
        }

        public void ExitParentElement()
        {
            parentElementStack.RemoveAt(parentElementStack.Count - 1);
        }

		public CodeExpression GetInstanceExpression(GraphClassGenerationContext instanceGraphClassContext)
		{
			var instance = (CodeExpression)CodeFactory.ThisRef;
			var currentGraphClassContext = graphClassContext;

			if (instanceGraphClassContext == currentGraphClassContext)
			{
				return instance;
			}

			foreach (var parentElement in parentElementStack)
			{
				Debug.Log($"looking for childGraphScript of {parentElement.GetType().Name} unit in {currentGraphClassContext.classDeclaration.Name} class context");

				instance = instance.Field(currentGraphClassContext.ExpectMemberName(parentElement, "childGraphScript"));
				currentGraphClassContext = currentGraphClassContext.generationSystem.GenerateGraph(parentElement).graphScript;

				if (instanceGraphClassContext == currentGraphClassContext)
				{
					return instance;
				}
			}

			throw new InvalidOperationException($"could not find graph script associated with {nameof(instanceGraphClassContext)} in parent element stack.");
		}

		public bool TryGetMemberName(object owner, string originalName, out KeyValuePair<GraphClassGenerationContext, string> graphClassContextAndAssociatedName)
		{
			string associatedName;

			for (int i = parentElementStack.Count - 1; i >= 0; i--)
			{
				var parentElement = parentElementStack[i];
				var graphScript = graphClassContext.generationSystem.GenerateGraph(currentParentElement).graphScript;

				if (graphScript.TryGetMemberName(owner, originalName, out associatedName))
				{
					graphClassContextAndAssociatedName = new KeyValuePair<GraphClassGenerationContext, string>(graphScript, associatedName);
					return true;
				}				
			}

			if (graphClassContext.TryGetMemberName(owner, originalName, out associatedName))
			{
				graphClassContextAndAssociatedName = new KeyValuePair<GraphClassGenerationContext, string>(graphClassContext, associatedName);
				return true;
			}

			graphClassContextAndAssociatedName = default(KeyValuePair<GraphClassGenerationContext, string>);
			return false;
		}

		public KeyValuePair<CodeExpression, string> GetInstanceExpressionAndMemberName(object owner, string originalName)
		{
			if (TryGetMemberName(owner, originalName, out var graphClassContextAndAssociatedName))
			{
				return new KeyValuePair<CodeExpression, string>(GetInstanceExpression(graphClassContextAndAssociatedName.Key), graphClassContextAndAssociatedName.Value);
			}
			throw new KeyNotFoundException();
		}

		public CodeFieldReferenceExpression InstanceField(object owner, string originalName)
		{
			var result = GetInstanceExpressionAndMemberName(owner, originalName);
			return result.Key.Field(result.Value);
		}

		public CodeMethodReferenceExpression InstanceMethod(object owner, string originalName, params CodeTypeReference[] typeArguments)
		{
			var result = GetInstanceExpressionAndMemberName(owner, originalName);
			return result.Key.Method(result.Value, typeArguments);
		}

		public CodeMethodReferenceExpression InstanceMethod(object owner, string originalName, IEnumerable<CodeTypeReference> typeArguments)
		{
			var result = GetInstanceExpressionAndMemberName(owner, originalName);
			return result.Key.Method(result.Value, typeArguments);
		}

		public void BindCode(IGraphElement graphElement, IEnumerable<CodeElement> codeElements) => graphClassContext.BindCode(graphElement, codeElements);
	}
}
