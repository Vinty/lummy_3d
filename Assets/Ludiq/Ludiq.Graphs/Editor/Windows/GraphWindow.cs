using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using GUIEvent = UnityEngine.Event;
using Object = UnityEngine.Object;
using SystemTask = System.Threading.Tasks.Task;

namespace Ludiq
{
	public sealed class GraphWindow : LudiqEditorWindow, ICanvasWindow
	{
		private static readonly string defaultTitle = "Graph";

		[MenuItem("Window/Bolt/Graph", priority = 0)]
		public static void OpenTab()
		{
			var tab = CreateInstance<GraphWindow>();
			tab.Show();
		}

		public static void OpenTab(GraphReference reference)
		{
			var tab = CreateInstance<GraphWindow>();
			tab.reference = reference;
			tab.Show();
		}

		public static void OpenActive(GraphReference reference)
		{
			if (active == null)
			{
				active = GetWindow<GraphWindow>();
				active.reference = reference;
				active.Show();
			}
			else
			{
				active.reference = reference;
				FocusWindowIfItsOpen<GraphWindow>();
			}
		}


		#region Context

		[Serialize]
		public bool locked { get; set; }

		[DoNotSerialize]
		public IGraphContext context { get; private set; }

		[Serialize] private GraphPointerData referenceData;

		[DoNotSerialize] private GraphReference _reference;

		[DoNotSerialize]
		public GraphReference reference
		{
			get => _reference;
			set
			{
				if (value == reference)
				{
					return;
				}

				value?.EnsureValid();

				context?.canvas.Close();

				_reference = value;

				// Only preserve the last valid reference
				// This is needed because the window revalidates before exiting playmode
				referenceData = GraphPointerData.FromPointer(reference) ?? referenceData;

				context = reference?.Context();

				context?.canvas.Open();

				OnContextChange();

				CheckForActiveContextChange();

				if (isActive && LudiqGUIUtility.isWithinGUI)
				{
					GUIUtility.ExitGUI();
				}
			}
		}

		public void Validate()
		{
			if (reference != null && !reference.isValid)
			{
				reference = reference.Revalidate(false);
			}
		}

		public void Clear()
		{
			reference = null;
		}

		public void MatchSelection()
		{
			if (locked)
			{
				return;
			}

			var potentialRootObjects = Enumerable.Empty<Object>();

			if (Selection.objects.Length == 1)
			{
				var selection = Selection.activeObject;

				if (selection is GameObject)
				{
					potentialRootObjects = ((GameObject)selection).GetComponents<Component>();
				}
				else if (selection is Component)
				{
					potentialRootObjects = ((Component)selection).GetComponents<Component>();
				}
				else
				{
					potentialRootObjects = selection.Yield();
				}
			}

			var newRoot = potentialRootObjects.NotUnityNull()
				.OfType<IGraphRoot>()
				.FirstOrDefault();

			if (newRoot != null)
			{
				var previousRoot = reference?.root;
				var previousRootGameObject = (previousRoot as Component)?.gameObject;
				var newRootGameObject = (newRoot as Component)?.gameObject;

				// Tired of rewriting this so I'm making it crystal clear in naming
				var rootWasEmpty = previousRoot == null;
				var rootIsMacro = newRoot is IMacro;
				var rootIsMachine = newRoot is IMachine;
				var rootChanged = previousRoot != newRoot;
				var rootGameObjectChanged = !UnityObjectUtility.TrulyEqual(previousRootGameObject, newRootGameObject);
				
				if (rootWasEmpty || (rootChanged && (rootIsMacro || (rootIsMachine && rootGameObjectChanged))))
				{
					reference = GraphReference.New(newRoot, false);
				}
			}
			else if (LudiqGraphs.Configuration.clearGraphSelection)
			{
				Clear();
			}
		}

		#endregion


		#region Context Shortcuts

		private ICanvas canvas => context.canvas;

		private IGraph graph => context.graph;

		#endregion


		#region Active

		private static readonly HashSet<GraphWindow> _tabs = new HashSet<GraphWindow>();

		public static IEnumerable<GraphWindow> tabs => _tabs;

		public static HashSet<GraphWindow> tabsNoAlloc => _tabs;

		public bool isActive { get; private set; }

		private static GraphWindow _active;

		public static GraphWindow active
		{
			get => _active;
			set
			{
				if (value == _active)
				{
					return;
				}

				_active = value;

				CheckForActiveContextChange();
			}
		}

		public static GraphReference activeReference
		{
			get => active?.reference;
			set
			{
				if (active != null)
				{
					active.reference = value;
				}
			}
		}

		private static IGraphContext lastActiveContext;

		public static IGraphContext activeContext => active?.context;

		public static event Action<IGraphContext> activeContextChanged;

		private static bool CheckForActiveContextChange()
		{
			if (activeContext == lastActiveContext)
			{
				return false;
			}

			lastActiveContext = activeContext;

			activeContextChanged?.Invoke(activeContext);

			return true;
		}

		#endregion


		#region Lifecycle

		protected override void _OnSelectionChange()
		{
			base._OnSelectionChange();
			Validate();
			MatchSelection();
		}

		protected override void _OnProjectChange()
		{
			base._OnProjectChange();
			Validate();
			MatchSelection();
			context?.DescribeAndAnalyze();
		}

		protected override void _OnHierarchyChange()
		{
			base._OnHierarchyChange();
			Validate();
			MatchSelection();
			context?.DescribeAndAnalyze();
		}

		protected override void _OnModeChange()
		{
			base._OnModeChange();
			reference = referenceData?.ToReference(false);
			Validate();
			MatchSelection();
		}

		protected override void _OnUndoRedo()
		{
			base._OnUndoRedo();
			Validate();
			MatchSelection();

			if (context != null)
			{
				context.BeginEdit();
				context.DescribeAndAnalyze();
				context.canvas.CacheWidgetCollections();
				context.selection.Clear();
				context.EndEdit();
			}
		}

		protected override void OnEnable()
		{
			base.OnEnable();

			_tabs.Add(this);
			titleContent = new GUIContent(defaultTitle, LudiqGraphs.Icons.graphWindow[IconSize.Small]);

			PluginContainer.delayCall += () =>
			{
				try
				{
					reference = referenceData?.ToReference(false);
				}
				catch (ExitGUIException) { }

				Validate();
				MatchSelection();
			};
		}

		private void OnFocus()
		{
			active = this;
			isActive = true;
		}

		private void OnLostFocus()
		{
			isActive = false;
		}

		private void FixActive()
		{
			// Needed for Shift+Space support
			// Unity probably changes the underlying instance without sending OnEnable/OnDisable

			if (isActive)
			{
				active = this;
			}
		}

		private void OnContextChange()
		{
			context?.DescribeAndAnalyze();

			titleContent = new GUIContent(context?.windowTitle ?? defaultTitle, LudiqGraphs.Icons.graphWindow?[IconSize.Small]);

			if (context != null && context.isPrefabInstance)
			{
				var prefabGraphPointer = GraphReference.New((IGraphRoot)reference.rootObject.GetPrefabDefinition(), true);
				context.graph.pan = prefabGraphPointer.graph.pan;
				context.graph.zoom = prefabGraphPointer.graph.zoom;
			}
		}

		protected override void OnDisable()
		{
			base.OnDisable();

			_tabs.Remove(this);

			if (isActive)
			{
				active = null;
				isActive = false;
			}
		}

		protected override void Update()
		{
			base.Update();

			FixActive();

			Validate();

			context?.canvas.Update();

			Repaint();
		}

		protected override void OnGUI()
		{
			base.OnGUI();

			FixActive();

			if (PluginContainer.anyVersionMismatch)
			{
				LudiqGUI.BeginVertical();
				LudiqGUI.FlexibleSpace();
				LudiqGUI.BeginHorizontal();
				LudiqGUI.FlexibleSpace();
				LudiqGUI.VersionMismatchShieldLayout();
				LudiqGUI.FlexibleSpace();
				LudiqGUI.EndHorizontal();
				LudiqGUI.FlexibleSpace();
				LudiqGUI.EndHorizontal();

				return;
			}

			Validate();

			// We always fetch the control IDs of the canvas first,
			// to make sure they are consistent no matter what gets draw
			// before or after it.
			if (context != null)
			{
				canvas.window = this;
				canvas.RegisterControls();
			}

			LudiqGUI.BeginHorizontal();

			LudiqGUI.BeginVertical();

			if (reference != null && reference.isChild && e.type == EventType.KeyDown && e.keyCode == KeyCode.PageUp)
			{
				reference = reference.ParentReference(true);
			}

			if (context != null)
			{
				LudiqGUI.BeginHorizontal(LudiqStyles.toolbarBackground);

				LudiqGUI.Space(-6);

				// Lock Button

				EditorGUI.BeginChangeCheck();

				locked = GUILayout.Toggle(locked, GraphGUI.Styles.lockIcon, LudiqStyles.toolbarButton);

				if (EditorGUI.EndChangeCheck())
				{
					MatchSelection();
				}

				LudiqGUI.Space(6);

				// Breadcrumbs

				foreach (var breadcrumb in reference.GetBreadcrumbs())
				{
					var title = breadcrumb.parent.Description().ToGUIContent(IconSize.Small);
					title.text = " " + title.text;
					var style = breadcrumb.isRoot ? LudiqStyles.toolbarBreadcrumbRoot : LudiqStyles.toolbarBreadcrumb;
					var isCurrent = breadcrumb == reference;

					if (GUILayout.Toggle(isCurrent, title, style) && !isCurrent)
					{
						reference = breadcrumb;
					}
				}

				LudiqGUI.Space(10);

				GUILayout.Label("Zoom", LudiqStyles.toolbarLabel);
				context.graph.zoom = GUILayout.HorizontalSlider(context.graph.zoom, GraphGUI.MinZoom, GraphGUI.MaxZoom, GUILayout.Width(100));
				GUILayout.Label(context.graph.zoom.ToString("0.#") + "x", LudiqStyles.toolbarLabel);

				LudiqGUI.FlexibleSpace();

				LudiqGUI.EndHorizontal();
			}

			var toolbarPosition = Rect.zero;

			if (context != null)
			{
				context.BeginEdit();
				var toolbarWidth = GetToolbarWidth();
				context.EndEdit();

				toolbarPosition = new Rect
				(
					position.width - toolbarWidth - 8,
					24,
					toolbarWidth,
					LudiqStyles.commandButton.fixedHeight
				);
			}

			var canvasContainer = GUILayoutUtility.GetRect(GUIContent.none, GUIStyle.none, GUILayout.ExpandHeight(true));

			// The Unity doc says Layout is sent once before all events and Repaint once after all events. 
			// From my observation, this is false: 
			// - Layout is sent before each event
			// - Repaint is sent once before all events
			// Therefore the logical candidate for BeforeFrame seems to be Layout
			// Hopefully this is a cross-platform consistency
			if (e.type == EventType.Repaint)
			{
				if (context != null)
				{
					context.BeginEdit();
					canvas.BeforeFrame();
					context.EndEdit();
				}
			}

			var skipToolbarEvent = e.isMouse && toolbarPosition.Contains(e.mousePosition);
			var eventType = e.type;

			// Skip layouting, which is called once per frame.
			// This removes the ability to use GUILayout,
			// but doubles the performance.
			if (e.type != EventType.Layout && !e.ShouldSkip())
			{
				if (skipToolbarEvent)
				{
					// Used type messes up the button down rendering for the toolbar
					// while Ignore doesn't. Go figure!
					e.type = EventType.Ignore;
				}

				BeginDisabledGroup();
				GraphGUI.DrawBackground(canvasContainer);
				EndDisabledGroup();

				if (LudiqGraphs.Configuration.disablePlaymodeTint)
				{
					GUI.color = Color.white;
				}

				if (context != null)
				{
					// Draw the graph here:
					// The strategy here is to zoom out, then clip.
					//
					// This may sound counter-intuitive, but clipping before zooming
					// out will cause pixels outside the zoomed out equivalent of the
					// window viewport to get clipped, because GUIClip seems to ignore
					// zoom values when doing its calculations.
					// 
					// To succeed then, we need to create a clipping area that is
					// scaled up according to the zoom factor, but then also moved down
					// from the top of the graph window by the tab offset, also multiplied
					// by the zoom factor.

					// Store the start of the window (inside the tab, top-left) in screen coordinates
					var originInsideTab = GUIUtility.GUIToScreenPoint(Vector2.zero);
					
					GUI.EndClip(); // Break out of the default window clip (the tab contents)

					// Determine the offset that the tab represented
					var originOutsideTab = GUIUtility.GUIToScreenPoint(Vector2.zero);
					var tabOffset = originInsideTab - originOutsideTab;

					// Update the pan and zoom values with tweening if need be
					canvas.UpdateViewport();

					// Calculate a position that respects the default window clip, but extends according to the zoom factor. 
					// This position is in a weird in-between "offset canvas-space" or "zoomed window-space".
					var canvasArea = canvasContainer;
					canvasArea.size /= canvas.zoom;
					canvasArea.position /= canvas.zoom;
					canvasArea.position += tabOffset / canvas.zoom;

					// Calculate the canvas' viewport
					var size = canvasArea.size;
					var scroll = canvas.pan - size / 2;
					canvas.viewport = new Rect(scroll, size);

					// Make the scroll pixel perfect to avoid blurriness
					scroll = scroll.PixelPerfect();

					using (LudiqGUI.matrix.Override(Matrix4x4.Scale(canvas.zoom * Vector3.one)))
					{
						if (LudiqGraphs.Configuration.showGrid)
						{
							GUI.BeginClip(canvasArea);
							{
								BeginDisabledGroup();
								GraphGUI.DrawGrid(scroll, new Rect(Vector2.zero, canvasArea.size), canvas.zoom);
								EndDisabledGroup();
							}
							GUI.EndClip();
						}

						GUI.BeginClip(canvasArea, -scroll, Vector2.zero, false);
						{
							context.BeginEdit();
							canvas.OnGUI();
							context.EndEdit();
						}
						GUI.EndClip();
					}

					// Show a warning if we're editing a prefab instance
					if (context.isPrefabInstance)
					{
						DrawPrefabInstanceWarning();
					}
					
					// Restore the window stack and clip at the start of the canvas area
					GUI.BeginClip(new Rect(tabOffset + canvasContainer.position, new Vector2(Screen.width, Screen.height)));

					// Delayed calls are useful for any code that requires GUIClip.Unclip,
					// because unclip fails to work with altered GUI matrices like zoom.
					// https://fogbugz.unity3d.com/default.asp?883652_e64gesk95q8c840s
					lock (canvas.delayedCalls)
					{
						context.BeginEdit();

						while (canvas.delayedCalls.Count > 0)
						{
							canvas.delayedCalls.Dequeue()?.Invoke();
						}

						context.EndEdit();
					}

					// Restore the real normal window stack (which is needed to match the calculated toolbar position)
					GUI.EndClip();
					GUI.BeginClip(new Rect(tabOffset, new Vector2(Screen.width, Screen.height)));
				}
				else
				{
					// Draw an empty grid if no graph is selected
					if (LudiqGraphs.Configuration.showGrid)
					{
						BeginDisabledGroup();
						GraphGUI.DrawGrid(Vector2.zero, canvasContainer);
						EndDisabledGroup();
					}
				}
			}

			LudiqGUI.EndHorizontal();

			LudiqGUI.EndVertical();

			if (skipToolbarEvent)
			{
				e.type = eventType;
			}

			if (context != null)
			{
				OnToolbarGUI(toolbarPosition);
				canvas.window = null;
			}
		}

		private float GetToolbarWidth()
		{
			return context.canvas.GetToolbarWidth();
		}

		private void OnToolbarGUI(Rect position)
		{
			GUILayout.BeginArea(position);

			GUILayout.BeginHorizontal();
			
			GUILayout.FlexibleSpace();

			// Custom Toolbar

			context.BeginEdit();
			context.canvas.OnToolbarGUI();
			context.EndEdit();

			LudiqGUI.EndHorizontal();

			GUILayout.EndArea();
		}

		#endregion


		#region Positioning

		public Vector2 UnclipPoint(Vector2 p)
		{
			return UnclipVector(p - canvas.viewport.position);
		}

		public Vector2 UnclipVector(Vector2 v)
		{
			return v * canvas.zoom;
		}

		public Rect Unclip(Rect rect)
		{
			return new Rect(UnclipPoint(rect.position), UnclipVector(rect.size));
		}

		#endregion


		#region Prefab Instance

		private const string disabledMessage = "Component graph editing is disabled on prefab instances.";

		private void BeginDisabledGroup()
		{
			EditorGUI.BeginDisabledGroup(context == null || context.isPrefabInstance);
		}

		private void EndDisabledGroup()
		{
			EditorGUI.EndDisabledGroup();
		}

		private void DrawPrefabInstanceWarning()
		{
			var warningPadding = 20;
			var warningWidth = 200;
			var warningHeight = LudiqGUIUtility.GetHelpBoxHeight(disabledMessage, MessageType.Warning, warningWidth);
			var buttonWidth = 120;
			var buttonHeight = 20;
			var spaceBetweenWarningAndButton = 5;

			var warningPosition = new Rect
			(
				warningPadding,
				position.height - warningHeight - buttonHeight - spaceBetweenWarningAndButton,
				warningWidth,
				warningHeight
			);

			var buttonPosition = new Rect
			(
				warningPadding,
				position.height - buttonHeight,
				buttonWidth,
				buttonHeight
			);

			EditorGUI.HelpBox(warningPosition, disabledMessage, MessageType.Warning);

			if (GUI.Button(buttonPosition, "Edit Prefab Graph"))
			{
				var prefabGraphPointer = GraphReference.New((IGraphRoot)reference.rootObject.GetPrefabDefinition(), true);
				prefabGraphPointer.graph.pan = context.graph.pan;
				prefabGraphPointer.graph.zoom = context.graph.zoom;
				Selection.activeObject = prefabGraphPointer.rootObject;
				reference = prefabGraphPointer;
			}
		}

		#endregion
	}
}