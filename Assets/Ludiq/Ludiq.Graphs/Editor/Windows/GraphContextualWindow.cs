﻿namespace Ludiq
{
	public abstract class GraphContextualWindow : LudiqEditorWindow
	{	
		protected override void OnEnable()
		{
			base.OnEnable();
			GraphWindow.activeContextChanged += ChangeContext;
			PluginContainer.delayCall += () => ChangeContext(GraphWindow.activeContext);
		}

		protected override void OnDisable()
		{
			base.OnDisable();
			GraphWindow.activeContextChanged -= ChangeContext;
		}

		protected IGraphContext context { get; private set; }

		protected void ChangeContext(IGraphContext newContext)
		{
			var previousContext = this.context;
			context = newContext;
			_OnContextChange(previousContext, newContext);
		}

		protected virtual void _OnContextChange(IGraphContext previous, IGraphContext newContext)
		{
		}

		protected override void OnGUI()
		{
			base.OnGUI();
			GraphWindow.active?.Validate();
		}
	}
}
