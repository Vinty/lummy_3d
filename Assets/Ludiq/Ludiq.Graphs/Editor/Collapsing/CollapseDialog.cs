﻿using UnityEditor;
using UnityEngine;
using UnityObject = UnityEngine.Object;

namespace Ludiq
{
	public sealed class CollapseDialog : LudiqEditorWindow
	{
		[DoNotSerialize]
		private IGraphContext context;

		[DoNotSerialize]
		private CollapseRequest request;

		[DoNotSerialize]
		private Metadata requestMetadata;

		[DoNotSerialize]
		private Metadata titleMetadata => requestMetadata[nameof(CollapseRequest.title)];

		[DoNotSerialize]
		private Metadata summaryMetadata => requestMetadata[nameof(CollapseRequest.summary)];

		private bool focusedTitleField;

		private static CollapseDialog instance;

		public static void Open(IGraphContext context, CollapseRequest request)
		{
			Ensure.That(nameof(context)).IsNotNull(context);
			Ensure.That(nameof(request)).IsNotNull(request);

			if (instance == null)
			{
				instance = CreateInstance<CollapseDialog>();
			}

			instance.context = context;
			instance.request = request;
			instance.requestMetadata = Metadata.Root().StaticObject(instance.request);
			instance.titleContent = new GUIContent("Collapse");
			instance.SizeToFit();
			instance.ShowUtility();
			instance.Center();
		}

		protected override void OnDisable()
		{
			base.OnDisable();
			instance = null;
		}

		private void OnLostFocus()
		{
			Close();
		}

		private void SizeToFit()
		{
			minSize = maxSize = new Vector2(300, GetHeaderHeight() + 155);
		}

		private float GetHeaderHeight()
		{
			var icon = context.graph.GetType().Icon();
			return LudiqGUI.GetHeaderHeight(null, titleMetadata, summaryMetadata, icon, position.width, false);
		}

		protected override void OnGUI()
		{
			base.OnGUI();

			if (request == null)
			{
				Close();
				return;
			}

			var icon = context.graph.GetType().Icon();

			var headerHeight = GetHeaderHeight();

			LudiqGUI.BeginVertical();

			var headerPosition = GUILayoutUtility.GetRect(GUIContent.none, GUIStyle.none, GUILayout.Height(headerHeight));

			var y = 0f;
			
			EditorGUI.BeginChangeCheck();
			
			var hitEnter = Event.current.type == EventType.KeyDown && Event.current.keyCode == KeyCode.Return;

			LudiqGUI.OnHeaderGUI(titleMetadata, summaryMetadata, icon, headerPosition, ref y, false);

			if (EditorGUI.EndChangeCheck())
			{
				SizeToFit();
			}

			if (!focusedTitleField)
			{
				EditorGUI.FocusTextInControl(titleMetadata.path);
				focusedTitleField = true;
			}

			LudiqGUI.BeginVertical(Styles.background);
			
			if (LudiqGUI.BigButtonLayout(Contents.embed) || hitEnter)
			{
				context.BeginEdit();
				UndoUtility.RecordEditedObject("Collapse to Embed");
				context.canvas.CollapseToEmbed(request);
				GUI.changed = true;
				context.EndEdit();
				Close();
			}

			LudiqGUI.FlexibleSpace();
			
			if (LudiqGUI.BigButtonLayout(Contents.macro))
			{
				var path = EditorUtility.SaveFilePanelInProject("Collapse to Macro", request.title, "asset", null);

				if (!string.IsNullOrEmpty(path))
				{
					context.BeginEdit();
					UndoUtility.RecordEditedObject("Collapse to Macro");
					var macro = context.canvas.CollapseToMacro(request);
					GUI.changed = true;
					context.EndEdit();
					AssetDatabase.CreateAsset((UnityObject)macro, path);
					Close();
				}
			}

			LudiqGUI.EndVertical();

			LudiqGUI.EndVertical();
		}

		public static class Contents
		{
			public static readonly GUIContent embed = new GUIContent("Collapse to Embed", LudiqGraphs.Icons.collapseToEmbed[IconSize.Medium], "Merges the selected nodes into a super node without creating an asset.");
			public static readonly GUIContent macro = new GUIContent("Collapse to Macro", LudiqGraphs.Icons.collapseToMacro[IconSize.Medium], "Creates a reusable graph asset from the selected nodes.");
		}

		public static class Styles
		{
			static Styles()
			{
				background = new GUIStyle(LudiqStyles.windowBackground);
				background.padding = new RectOffset(10, 10, 10, 10);
			}

			public static readonly GUIStyle background;
		}
	}
}