﻿using System.Collections.Generic;

namespace Ludiq
{
	public sealed class CollapseRequest
	{
		public string title { get; set; }
		public string summary { get; set; }
		public HashSet<IGraphElement> elements { get; }
		public GraphGroup masterGroup { get; set; }

		public CollapseRequest(IEnumerable<IGraphElement> elements)
		{
			Ensure.That(nameof(elements)).IsNotNull(elements);

			this.elements = new HashSet<IGraphElement>(elements);
		}
	}
}
