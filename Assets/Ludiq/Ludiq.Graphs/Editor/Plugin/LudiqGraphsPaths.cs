﻿using System.IO;

namespace Ludiq
{
	[Plugin(LudiqGraphs.ID)]
	public class LudiqGraphsPaths : PluginPaths
	{
		public LudiqGraphsPaths(Plugin plugin) : base(plugin) { }

		public string scripts => Path.Combine(transientGenerated, "Scripts");
	}
}