﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Ludiq
{
	[Plugin(ID)]
	[PluginDependency(LudiqCore.ID)]
	[Product(LudiqProduct.ID)]
	[PluginRuntimeAssembly(ID + ".Runtime")]
	public sealed class LudiqGraphs : Plugin
	{
		public const string ID = "Ludiq.Graphs";

		public LudiqGraphs() : base()
		{
			instance = this;
		}

		public static LudiqGraphs instance { get; private set; }

		public static LudiqGraphsManifest Manifest => (LudiqGraphsManifest)instance.manifest;
		public static LudiqGraphsConfiguration Configuration => (LudiqGraphsConfiguration)instance.configuration;
		public static LudiqGraphsResources Resources => (LudiqGraphsResources)instance.resources;
		public static LudiqGraphsPaths Paths => (LudiqGraphsPaths)instance.paths;
		public static LudiqGraphsResources.Icons Icons => Resources.icons;

		public const string LegacyRuntimeDllGuid = "efc4f8221b9f2e04998ad78e9b2b01b6";
		public const string LegacyEditorDllGuid = "e7591b9110e32ca45b2c5f1372169149";

		public override IEnumerable<ScriptReferenceReplacement> scriptReferenceReplacements
		{
			get
			{
				yield break;
			}
		}

		public override IEnumerable<string> tips
		{
			get
			{
				yield return "Hold Ctrl (Cmd on Mac) while dragging a node to invert carry behaviour.";
				yield return "Double-click a super node to enter its nested graph.";
				yield return "Hit F to focus the graph view on the selected nodes.";
			}
		}

		public static class Styles
		{
			static Styles()
			{
				nodeLabel = new GUIStyle(EditorStyles.label);

				if (EditorGUIUtility.isProSkin)
				{
					nodeLabel.normal.textColor = ColorUtility.Gray(0.92f);
				}
			}

			public static readonly GUIStyle nodeLabel;
		}
	}
}