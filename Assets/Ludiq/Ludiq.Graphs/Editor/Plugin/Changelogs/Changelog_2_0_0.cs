﻿using System;
using System.Collections.Generic;

namespace Ludiq
{
	[Plugin(LudiqGraphs.ID)]
	internal class Changelog_2_0_0a1 : PluginChangelog
	{
		public Changelog_2_0_0a1(Plugin plugin) : base(plugin) { }

		public override SemanticVersion version => "2.0.0a1";
		public override DateTime date => new DateTime(2018, 11, 07);

		public override IEnumerable<string> changes
		{
			get 
			{
				yield return "[Added] Node Deselection";
				yield return "[Removed] Graph Window Sidebars";
			}
		}
	}

	[Plugin(LudiqGraphs.ID)]
	internal class Changelog_2_0_0a2 : PluginChangelog
	{
		public Changelog_2_0_0a2(Plugin plugin) : base(plugin) { }

		public override SemanticVersion version => "2.0.0a2";
		public override DateTime date => new DateTime(2018, 11, 30);

		public override IEnumerable<string> changes
		{
			get
			{
				yield return "[Added] Snapping";
				yield return "[Added] Group Comments";
				yield return "[Fixed] Graph scripts folder not getting created automatically";
				yield return "[Changed] Focus keyboard shortcut from Home to F";
				yield return "[Removed] Snap to Grid";
			}
		}
	}

	[Plugin(LudiqGraphs.ID)]
	internal class Changelog_2_0_0a3 : PluginChangelog
	{
		public Changelog_2_0_0a3(Plugin plugin) : base(plugin) { }

		public override SemanticVersion version => "2.0.0a3";
		public override DateTime date => new DateTime(2018, 12, 21);

		public override IEnumerable<string> changes
		{
			get
			{
				yield return "[Added] Graph Sharing";
				yield return "[Added] Node Collapsing";
				yield return "[Added] Redesigned Toolbar";
				yield return "[Added] Graph Inspector Popup";
				yield return "[Added] Warnings display on hover";
				yield return "[Added] Support for resizing graph elements while zoomed out";
				yield return "[Changed] C# Preview font to improve type-hinting";
			}
		}
	}
}