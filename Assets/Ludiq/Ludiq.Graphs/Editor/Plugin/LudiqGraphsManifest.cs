﻿namespace Ludiq
{
	[Plugin(LudiqGraphs.ID)]
	public sealed class LudiqGraphsManifest : PluginManifest
	{
		private LudiqGraphsManifest(LudiqGraphs plugin) : base(plugin) { }

		public override string name => "Ludiq Graphs";
		public override string author => "Ludiq";
		public override string description => "Toolset for Unity graph editing.";
		public override SemanticVersion version => "2.0.0a3";
	}
}