﻿namespace Ludiq
{
	[Plugin(LudiqGraphs.ID)]
	public sealed class LudiqGraphsResources : PluginResources
	{
		private LudiqGraphsResources(LudiqGraphs plugin) : base(plugin)
		{
			icons = new Icons(this);
		}

		public override void Initialize()
		{
			base.Initialize();

			icons.Load();
		}

		public Icons icons { get; private set; }

		public class Icons
		{
			public Icons(LudiqGraphsResources resources)
			{
				this.resources = resources;
			}

			private readonly LudiqGraphsResources resources;

			public EditorTexture graphWindow { get; private set; }
			public EditorTexture graphInspectorWindow { get; private set; }
			public EditorTexture codePreviewWindow { get; private set; }
			public EditorTexture droplet { get; private set; }
			
			public EditorTexture snapCommand { get; private set; }
			public EditorTexture carryCommand { get; private set; }
			public EditorTexture debugCommand { get; private set; }
			public EditorTexture dimCommand { get; private set; }
			public EditorTexture fullScreenCommand { get; private set; }
			public EditorTexture overviewCommand { get; private set; }
			public EditorTexture alignCommand { get; private set; }
			public EditorTexture distributeCommand { get; private set; }
			public EditorTexture clearErrorsCommand { get; private set; }
			public EditorTexture collapseCommand { get; private set; }
			public EditorTexture shareCommand { get; private set; }

			public EditorTexture collapseToEmbed { get; private set; }
			public EditorTexture collapseToMacro { get; private set; }

			public void Load()
			{
				graphWindow = resources.LoadIcon("Windows/GraphWindow.png");
				graphInspectorWindow = resources.LoadIcon("Windows/GraphInspectorWindow.png");
				codePreviewWindow = resources.LoadIcon("Windows/CodePreviewWindow.png");
				droplet = resources.LoadTexture("Droplet.png", EditorTexture.UnitResolutions, CreateTextureOptions.PixelPerfect);
				
				snapCommand = resources.LoadTexture("Icons/Commands/Snap.png", EditorTexture.UnitResolutions, CreateTextureOptions.PixelPerfect);
				carryCommand = resources.LoadTexture("Icons/Commands/Carry.png", EditorTexture.UnitResolutions, CreateTextureOptions.PixelPerfect);
				debugCommand = resources.LoadTexture("Icons/Commands/Debug.png", EditorTexture.UnitResolutions, CreateTextureOptions.PixelPerfect);
				dimCommand = resources.LoadTexture("Icons/Commands/Dim.png", EditorTexture.UnitResolutions, CreateTextureOptions.PixelPerfect);
				fullScreenCommand = resources.LoadTexture("Icons/Commands/FullScreen.png", EditorTexture.UnitResolutions, CreateTextureOptions.PixelPerfect);
				overviewCommand = resources.LoadTexture("Icons/Commands/Overview.png", EditorTexture.UnitResolutions, CreateTextureOptions.PixelPerfect);
				alignCommand = resources.LoadTexture("Icons/Commands/Align.png", EditorTexture.UnitResolutions, CreateTextureOptions.PixelPerfect);
				distributeCommand = resources.LoadTexture("Icons/Commands/Distribute.png", EditorTexture.UnitResolutions, CreateTextureOptions.PixelPerfect);
				clearErrorsCommand = resources.LoadTexture("Icons/Commands/ClearErrors.png", EditorTexture.UnitResolutions, CreateTextureOptions.PixelPerfect);
				collapseCommand = resources.LoadTexture("Icons/Commands/Collapse.png", EditorTexture.UnitResolutions, CreateTextureOptions.PixelPerfect);
				shareCommand = resources.LoadTexture("Icons/Commands/Share.png", EditorTexture.UnitResolutions, CreateTextureOptions.PixelPerfect);
				
				droplet = resources.LoadTexture("Droplet.png", new TextureResolution[] { 1, 2 }, CreateTextureOptions.PixelPerfect);

				collapseToEmbed = resources.LoadIcon("Icons/CollapseToEmbed.png");
				collapseToMacro = resources.LoadIcon("Icons/CollapseToMacro.png");

				if (GraphWindow.active != null)
				{
					GraphWindow.active.titleContent.image = graphWindow?[IconSize.Small];
				}
				
				if (GraphInspectorWindow.instance != null)
				{
					GraphInspectorWindow.instance.titleContent.image = graphInspectorWindow?[IconSize.Small];
				}
				
				if (CodePreviewWindow.instance != null)
				{
					CodePreviewWindow.instance.titleContent.image = codePreviewWindow?[IconSize.Small];
				}
			}
		}
	}
}