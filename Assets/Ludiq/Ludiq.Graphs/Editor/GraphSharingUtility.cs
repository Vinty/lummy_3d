﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;
using Ludiq.CodeDom;
using Ludiq.TinyJson;
using UnityEditor;
using UnityEngine;

namespace Ludiq
{
	public static class GraphSharingUtility
	{
		private static readonly HttpClient client = new HttpClient();

		private static readonly object @lock = new object();

		private const string ImgurApiClientID = "966e080dc170f6a";
		private const string GithubToken = "179163fac940697f5596518ead5f2999d45c5601";

		public static Texture2D Screenshot(IGraphContext context, IEnumerable<IGraphElement> elements)
		{
			Ensure.That(nameof(context)).IsNotNull(context);
			Ensure.That(nameof(elements)).IsNotNull(elements);

			using (LudiqGUIUtility.noClip)
			{
				context.canvas.BeginScreenshot(elements);

				var previousPan = context.canvas.pan;
				context.canvas.pan = Vector2.zero;

				var previousZoom = context.canvas.zoom;
				context.canvas.zoom = 1;

				var previousMatrix = GUI.matrix;
				GUI.matrix = Matrix4x4.identity;

				context.canvas.Cache();
				var viewport = context.canvas.viewport;
				var width = (int)viewport.width;
				var height = (int)viewport.height;
				var size = new Vector2(width, height);
				var origin = viewport.position;

				var texture = new Texture2D(width, height);

				var previousRenderTexture = RenderTexture.active;
				var renderTexture = new RenderTexture(width, height, 0);
				RenderTexture.active = renderTexture;

				var previousEvent = Event.current;
				Event.current = new Event() { type = EventType.Repaint };

				context.BeginEdit();
				context.canvas.viewport = viewport;
				context.canvas.BeforeFrame();

				// Unity doesn't seem to allow rendering or clipping
				// past screen dimensions in a lot of cases, so we need
				// to stitch the screenshot from multiple renders.
				var maxRenderWidth = Screen.width;
				var maxRenderHeight = Screen.height;

				var renderBackground = true;

				for (int x = 0; x < width; x += maxRenderWidth)
				{
					for (int y = 0; y < height; y += maxRenderHeight)
					{
						var renderWidth = Mathf.Min(width - x, maxRenderWidth);
						var renderHeight = Mathf.Min(height - y, maxRenderHeight);
						var renderOrigin = new Vector2(x, y);
						var renderSize = new Vector2(renderWidth, renderHeight);
						var clipPosition = new Rect(Vector2.zero, renderSize);
						var clipScrollOffset = -(origin + renderOrigin);
						GL.Clear(true, true, ColorPalette.transparent);
						//Debug.Log($"clipPosition: {clipPosition}\nclipScrollOffset: {clipScrollOffset}");
						//LudiqGUI.DrawEmptyRect(new Rect(Vector2.zero, renderSize), Random.ColorHSV(0, 1, 1, 1, 1, 1));
						//GUI.Label(new Rect(Vector2.zero, renderSize), $"{x}, {y}");

						if (renderBackground)
						{
							GUI.BeginClip(clipPosition, clipScrollOffset, Vector2.zero, false);
							GraphGUI.DrawBackground(new Rect(origin, size));
							GUI.EndClip();

							GUI.BeginClip(clipPosition);
							GraphGUI.DrawGrid(-clipScrollOffset, new Rect(Vector2.zero, renderSize), context.canvas.zoom);
							GUI.EndClip();
						}

						GUI.BeginClip(clipPosition, clipScrollOffset, Vector2.zero, false);
						context.canvas.OnGUI();
						GUI.EndClip();

						// The y coordinate is inverted in textures, so we have to flip it
						texture.ReadPixels(new Rect(0, 0, renderWidth, renderHeight), x, height - renderHeight - y, false);
					}
				}

				texture.Apply();

				context.canvas.EndScreenshot();

				context.EndEdit();

				RenderTexture.active = previousRenderTexture;
				renderTexture.Release();

				Event.current = previousEvent;

				GUI.matrix = previousMatrix;

				context.canvas.pan = previousPan;

				context.canvas.zoom = previousZoom;

				return texture;
			}
		}

		public static string SaveTemporaryImage(Texture2D screenshot)
		{
			Ensure.That(nameof(screenshot)).IsNotNull(screenshot);

			var path = Path.Combine(Path.GetTempPath(), Guid.NewGuid() + ".png");
			var screenshotBytes = screenshot.EncodeToPNG();
			File.WriteAllBytes(path, screenshotBytes);
			return path;
		}

		public static string UploadToImgur(byte[] image, string title = null, string description = null)
		{
			Ensure.That(nameof(image)).IsNotNull(image);

			try
			{
				using (var webClient = new WebClient())
				{
					webClient.Headers.Add("Authorization", "Client-ID " + ImgurApiClientID);

					var values = new NameValueCollection();

					values.Add("image", Convert.ToBase64String(image));

					if (!string.IsNullOrEmpty(title))
					{
						values.Add("title", title);
					}

					if (!string.IsNullOrEmpty(description))
					{
						values.Add("description", description);
					}

					var response = webClient.UploadValues("https://api.imgur.com/3/upload.xml", values);

					using (var responseStream = new MemoryStream(response))
					{
						var responseXml = XDocument.Load(responseStream);

						return GetImgurPostLink(responseXml.Element("data").Element("link").Value);
					}
				}
			}
			catch (Exception ex)
			{
				Debug.LogWarning("Failed to upload to imgur:\n" + ex);
				return null;
			}
		}

		private static string GetImgurPostLink(string directLink)
		{
			Ensure.That(nameof(directLink)).IsNotNull(directLink);

			return directLink.Replace("i.imgur.com", "imgur.com").TrimEnd(".png");
		}

		public static string UploadGist(Dictionary<string, string> files, out Dictionary<string, string> rawUrls, string description = null)
		{
			Ensure.That(nameof(files)).IsNotNull(files);
			Ensure.That(nameof(files)).HasItems(files);

			try
			{
				var input = new Dictionary<string, object>();

				input["public"] = true;

				if (!string.IsNullOrEmpty(description))
				{
					input["description"] = description;
				}

				var filesInput = new Dictionary<string, object>();

				foreach (var file in files)
				{
					filesInput[file.Key] = new Dictionary<string, object> {{"content", file.Value}};
				}

				input["files"] = filesInput;
				
				using (var requestMessage = new HttpRequestMessage(HttpMethod.Post, "https://api.github.com/gists"))
				{
					requestMessage.Headers.UserAgent.ParseAdd("Bolt");
					requestMessage.Headers.Authorization = new AuthenticationHeaderValue("token", GithubToken);
					requestMessage.Content = new StringContent(input.ToJson(), Encoding.UTF8, "application/json");
					var response = client.SendAsync(requestMessage).Result;

					if (response.IsSuccessStatusCode)
					{
						var output = JsonParser.FromJson<Dictionary<string, object>>(response.Content.ReadAsStringAsync().Result);

						rawUrls = new Dictionary<string, string>();

						foreach (var _fileOutput in (Dictionary<string, object>)output["files"])
						{
							var fileName = _fileOutput.Key;
							var fileOutput = (Dictionary<string, object>)_fileOutput.Value;
							rawUrls.Add(fileName, (string)fileOutput["raw_url"]);
						}

						return (string)output["html_url"];
					}
					else
					{
						throw new Exception(response.Content.ReadAsStringAsync().Result);
					}
				}
			}
			catch (Exception ex)
			{
				Debug.LogWarning("Failed to upload Gist:\n" + ex);
				rawUrls = null;
				return null;
			}
		}

		public static void Share(IGraphContext context)
		{
			Ensure.That(nameof(context)).IsNotNull(context);

			var fullGraph = context.selection.Count == 0;

			var elements = fullGraph ? context.graph.elements : (IEnumerable<IGraphElement>)context.selection;

			var screenshot = Screenshot(context, elements);

			var screenshotData = screenshot.EncodeToPNG();

			//var path = GraphSharingUtility.SaveScreenshot(screenshot);
			//Process.Start(path);

			var graphDescription = context.graph.Description();
			var graphTitle = graphDescription.title;

			string script = null;

			if (fullGraph)
			{
				try
				{
					script = GenerateScript(context);
				}
				catch (Exception ex)
				{
					script = ex.ToString();
				}
			}

			var clip = GraphClipboard.GetSystemCopy(elements, out var containsReferences);

			Debug.Log("Uploading graph data...\n");

			var environmentDescription = new StringBuilder();
			
			environmentDescription.AppendLine($"\u2022 Unity: v.{Application.unityVersion}");
			environmentDescription.AppendLine($"\u2022 Scripting Runtime: {PlayerSettings.scriptingRuntimeVersion}");
			environmentDescription.AppendLine($"\u2022 Build Target: {EditorUserBuildSettings.activeBuildTarget}");
			environmentDescription.AppendLine($"\u2022 OS: {Environment.OSVersion}");
			
			new Thread(() =>
			{
				lock (@lock)
				{
					var baseFileName = PathUtility.MakeSafeFilename(graphTitle.Filter(whitespace: false, symbols: false, punctuation: false), '_');
					
					var imgurDescription = new StringBuilder();

					imgurDescription.AppendLine("This graph was created with Bolt. \u26A1 (http://ludiq.io/bolt)");
					imgurDescription.AppendLine();
					imgurDescription.AppendLine($"Tip: Paste (Ctrl/Cmd+V) the Clip below in your graph to recreate it!");
					imgurDescription.AppendLine();

					if (containsReferences)
					{
						imgurDescription.AppendLine("Warning: The graph contains Unity object references that are not available from the clip.");
						imgurDescription.AppendLine();
					}

					var clipGistFiles = new Dictionary<string, string>();
					var clipFileName = baseFileName + ".Clip.base64";
					clipGistFiles.Add(clipFileName, clip);
					var rawGistUrl = UploadGist(clipGistFiles, out var rawUrls);
					var clipUrl = rawUrls[clipFileName];
					imgurDescription.AppendLine($"Clip: {clipUrl}");

					imgurDescription.AppendLine();

					if (fullGraph)
					{
						var scriptGistFiles = new Dictionary<string, string>();
						var scriptFileName = baseFileName + ".cs";
						scriptGistFiles.Add(scriptFileName, script);
						var scriptGistUrl = UploadGist(scriptGistFiles, out var rawScriptUrls);
						var scriptUrl = scriptGistUrl;
						imgurDescription.AppendLine($"C#: {scriptUrl}");

						imgurDescription.AppendLine();
					}

					if (fullGraph)
					{
						imgurDescription.AppendLine($"Graph:");
						imgurDescription.AppendLine($"\u2022 Title: {graphDescription.title}");

						if (!string.IsNullOrWhiteSpace(graphDescription.summary))
						{
							imgurDescription.AppendLine($"\u2022 Summary: {graphDescription.summary}");
						}

						imgurDescription.AppendLine($"\u2022 Type: {context.graph.GetType().HumanName()}");
						imgurDescription.AppendLine($"\u2022 Source: {context.reference.serializedObject.GetType().HumanName()}");

						imgurDescription.AppendLine();
					}
					
					imgurDescription.AppendLine($"Debug:");

					foreach (var product in ProductContainer.products)
					{
						imgurDescription.AppendLine($"\u2022 {product.name}: v.{product.version}");
					}

					foreach (var plugin in PluginContainer.plugins)
					{
						// imgurDescription.AppendLine($"\u2022 {plugin.manifest.name}: v.{plugin.manifest.version}");
					}

					imgurDescription.Append(environmentDescription);
					
					string imgurPostUrl = UploadToImgur(screenshotData, graphTitle, imgurDescription.ToString());

					if (imgurPostUrl != null)
					{
						UnityAPI.Await(() => EditorGUIUtility.systemCopyBuffer = imgurPostUrl);
						Debug.Log($"Graph data uploaded, URL copied to the clipboard:\n{imgurPostUrl}");
					}
				}
			}).Start();
		}

		private static string GenerateScript(IGraphContext context)
		{
			GeneratorProvider.instance.FreeAll();
			var graphGenerationSystem = new GraphGenerationSystem(GraphGenerationMode.Normal);
			var writerSystem = new StringCodeWriterSystem();
			var generatedGraph = graphGenerationSystem.GenerateGraph(context.reference.parent);
			graphGenerationSystem.WriteClasses(writerSystem);
			return writerSystem.GetString(generatedGraph.graphScript.classDeclaration.Name);
		}
	}
}
