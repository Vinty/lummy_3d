﻿using Ludiq;

namespace Bolt
{
	[Inspector(typeof(IGraph))]
	public class GraphInspector : ReflectedInspector
	{
		public GraphInspector(Metadata metadata) : base(metadata) { }
	}
}