﻿using Ludiq;

namespace Bolt
{
	public abstract class GraphElementInspector<TGraphContext> : ReflectedInspector where TGraphContext : class, IGraphContext
	{
		public GraphElementInspector(Metadata metadata) : base(metadata) { }

		#region Context Shortcuts

		protected TGraphContext context => (TGraphContext)LudiqGraphsEditorUtility.editedContext.value;
		
		protected GraphReference reference => context.reference;

		protected IGraph graph => context.graph;

		protected ICanvas canvas => context.canvas;

		protected GraphSelection selection => context.selection;

		#endregion
	}
}