using UnityEngine;

namespace Ludiq
{
	[Editor(typeof(IGraph))]
	public abstract class GraphEditor<TGraphContext> : Inspector where TGraphContext : class, IGraphContext
	{
		public GraphEditor(Metadata metadata) : base(metadata) { }

		#region Context Shortcuts

		protected TGraphContext context => (TGraphContext)LudiqGraphsEditorUtility.editedContext.value;
		
		protected GraphReference reference => context.reference;

		protected ICanvas canvas => context.canvas;

		protected GraphSelection selection => context.selection;

		#endregion

		private Metadata titleMetadata => metadata[nameof(IGraph.title)];

		private Metadata summaryMetadata => metadata[nameof(IGraph.summary)];

		protected virtual EditorTexture icon => metadata.definedType.Icon();

		protected IGraph graph => (IGraph)metadata.value;

		protected override float GetHeight(float width, GUIContent label)
		{
			var height = 0f;

			height += GetHeaderHeight(width);

			height += GetWrappedInspectorHeight(width);

			return height;
		}

		protected float GetHeaderHeight(float width)
		{
			return LudiqGUI.GetHeaderHeight
			(
				this,
				titleMetadata,
				summaryMetadata,
				icon,
				width
			);
		}
		
		protected void OnHeaderGUI(Rect position)
		{
			LudiqGUI.OnHeaderGUI
			(
				titleMetadata,
				summaryMetadata,
				icon,
				position,
				ref y
			);
		}
		
		protected float GetWrappedInspectorHeight(float totalWidth)
		{
			var innerWidth = GetInspectorInnerWidth(totalWidth);

			var height = GetInspectorHeight(innerWidth);

			if (height > 0)
			{
				height += Styles.inspectorBackground.padding.top;
				height += Styles.inspectorBackground.padding.bottom;
			}

			return height;
		}

		protected virtual float GetInspectorHeight(float width)
		{
			return LudiqGUI.GetInspectorHeight(this, metadata, width, GUIContent.none);
		}

		protected override void OnGUI(Rect position, GUIContent label)
		{
			y = 0;

			OnHeaderGUI(position);

			OnWrappedInspectorGUI(position);
		}

		protected void OnWrappedInspectorGUI(Rect position)
		{
			var innerWidth = GetInspectorInnerWidth(position.width);

			var inspectorHeight = GetInspectorHeight(innerWidth);

			if (inspectorHeight == 0)
			{
				return;
			}

			var backgroundPosition = new Rect
			(
				position.x,
				y,
				position.width,
				GetWrappedInspectorHeight(position.width)
			);

			if (e.type == EventType.Repaint)
			{
				Styles.inspectorBackground.Draw(backgroundPosition, false, false, false, false);
			}

			y += Styles.inspectorBackground.padding.top;

			var innerPosition = new Rect
			(
				position.x + Styles.inspectorBackground.padding.left,
				y,
				innerWidth,
				GetInspectorHeight(innerWidth)
			);

			var inspectorPosition = innerPosition.VerticalSection(ref y, inspectorHeight);
			
			OnInspectorGUI(inspectorPosition);

			y += Styles.inspectorBackground.padding.bottom;
		}

		protected virtual void OnInspectorGUI(Rect position)
		{
			LudiqGUI.Inspector(metadata, position, GUIContent.none);
		}

		protected float GetInspectorInnerWidth(float totalWidth)
		{
			return totalWidth - Styles.inspectorBackground.padding.left - Styles.inspectorBackground.padding.right;
		}

		public static class Styles
		{
			static Styles()
			{
				inspectorBackground = new GUIStyle();
				inspectorBackground.padding = new RectOffset(10, 10, 10, 10);
			}

			public static readonly GUIStyle inspectorBackground;
		}
	}
}