﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityObject = UnityEngine.Object;

namespace Ludiq
{
	public static class GraphParentDescriptor
	{
		public static string Title(IGraphParent parent)
		{
			var graph = parent.childGraph;

			if (!StringUtility.IsNullOrWhiteSpace(graph?.title))
			{
				return graph?.title;
			}

			if (parent is IGraphNester nester)
			{
				return GraphNesterDescriptor.Title(nester);
			}

			if (parent is UnityObject unityObject)
			{
				return UnityAPI.Await(() => unityObject.name);
			}

			return parent.GetType().HumanName();
		}

		public static string Summary(IGraphParent parent)
		{
			var graph = parent.childGraph;

			if (!StringUtility.IsNullOrWhiteSpace(graph?.summary))
			{
				return graph?.summary;
			}

			if (parent is IGraphNester nester)
			{
				return GraphNesterDescriptor.Summary(nester);
			}

			return parent.GetType().Summary();
		}
	}
}
