﻿using UnityObject = UnityEngine.Object;

namespace Ludiq
{
	[Descriptor(typeof(IMacro))]
	public class MacroDescriptor<TMacro, TMacroDescription> : Descriptor<TMacro, TMacroDescription>
		where TMacro : UnityObject, IMacro
		where TMacroDescription : class, IMacroDescription, new()
	{
		protected MacroDescriptor(TMacro target) : base(target) { }

		protected TMacro macro => target;

		[Assigns(cache = false)]
		[RequiresUnityAPI]
		public override string Title()
		{
			var graph = macro.childGraph;

			if (!StringUtility.IsNullOrWhiteSpace(graph?.title))
			{
				return graph?.title;
			}

			return macro.name;
		}

		[Assigns]
		[RequiresUnityAPI]
		public override string Summary()
		{
			var graph = macro.childGraph;

			if (!StringUtility.IsNullOrWhiteSpace(graph?.summary))
			{
				return graph?.summary;
			}

			return macro.GetType().Summary();
		}

		[Assigns]
		[RequiresUnityAPI]
		public override EditorTexture Icon()
		{
			return macro.GetType().Icon();
		}
	}
}