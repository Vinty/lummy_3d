﻿using UnityEngine;

namespace Ludiq
{
	public struct SnappingPair
	{
		public SnappingAnchor snappee { get; }
		public SnappingAnchor snapper { get; }
		public Axis2 parallelAxis { get; }
		public Axis2 perpendicularAxis { get; }
		public float parallelDistance { get; }
		public float perpendicularDistance { get; }

		public SnappingPair(SnappingAnchor snappee, SnappingAnchor snapper, Axis2 parallelAxis)
		{
			this.snappee = snappee;
			this.snapper = snapper;
			this.parallelAxis = parallelAxis;
			this.perpendicularAxis = parallelAxis.Perpendicular();
			this.parallelDistance = SnappingUtility.Distance(snappee.position, snapper.position, parallelAxis);
			this.perpendicularDistance = SnappingUtility.Distance(snappee.position, snapper.position, parallelAxis.Perpendicular());
		}

		public Vector2 parallelOffset => parallelAxis.UnaryVector() * (snapper.position - snappee.position).Component(parallelAxis);
	}
}
