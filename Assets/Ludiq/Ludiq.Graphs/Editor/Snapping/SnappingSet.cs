﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Ludiq
{
	public class SnappingSet
	{
		internal readonly HashSet<SnappingAnchor> snappees = new HashSet<SnappingAnchor>();
		internal readonly HashSet<SnappingAnchor> snappers = new HashSet<SnappingAnchor>();

		public Axis2 axis { get; set; }

		public SnappingSet(Axis2 axis)
		{
			this.axis = axis;
		}

		public void Clear()
		{
			snappees.Clear();
			snappers.Clear();
		}
		
		private HashSet<SnappingAnchor> GetCollection(SnappingAnchorType type)
		{
			switch (type)
			{
				case SnappingAnchorType.Snappee: return snappees;
				case SnappingAnchorType.Snapper: return snappers;
				default: throw new UnexpectedEnumValueException<SnappingAnchorType>(type);
			}
		}

		public void Add(object snappable, Vector2 position, SnappingAnchorType type)
		{
			GetCollection(type).Add(new SnappingAnchor(snappable, position));
		}

		public SnappingPair? FindClosestPair(float maxDistance)
		{
			var lowestDistance = float.MaxValue;
			SnappingAnchor? closestSnappee = null;
			SnappingAnchor? closestSnapper = null;
			var found = false;

			foreach (var snappee in snappees)
			{
				foreach (var snapper in snappers)
				{
					var parallelDistance = SnappingUtility.Distance(snappee.position, snapper.position, axis);

					if (parallelDistance > maxDistance)
					{
						continue;
					}

					if (parallelDistance < lowestDistance)
					{
						lowestDistance = parallelDistance;
						closestSnappee = snappee;
						closestSnapper = snapper;
						found = true;
					}
				}
			}

			if (!found)
			{
				return null;
			}

			return new SnappingPair(closestSnappee.Value, closestSnapper.Value, axis);
		}
	}
}
