﻿using System;
using System.Collections.Generic;

namespace Ludiq
{
	public class SnappingSystem
	{
		private readonly Dictionary<object, SnappingSet> sets = new Dictionary<object, SnappingSet>();
		private readonly HashSet<Tuple<object, object>> matches = new HashSet<Tuple<object, object>>();

		public SnappingSystem()
		{

		}

		public SnappingSet this[object key] => sets[key];

		public void AddSet(object key, Axis2 axis)
		{
			if (!sets.TryGetValue(key, out var set))
			{
				set = new SnappingSet(axis);
				sets.Add(key, set);
			}

			set.axis = axis;
		}

		public void AddMatch(object a, object b)
		{
			matches.Add(new Tuple<object, object>(a, b));
		}

		public void Clear()
		{
			foreach (var set in sets) set.Value.Clear();
		}
	
		public SnappingPair? FindClosestPair(Axis2 axis, float maxDistance)
		{
			var lowestDistance = maxDistance;
			SnappingPair? closestPair = null;

			foreach (var match in matches)
			{
				var a = sets[match.Item1];
				var b = sets[match.Item2];

				if (a.axis != axis || b.axis != axis) continue;

				var abPair = FindClosestPair(axis, maxDistance, a.snappees, b.snappers);
				var baPair = FindClosestPair(axis, maxDistance, b.snappees, a.snappers);

				if (abPair != null)
				{
					var abDistance = abPair.Value.parallelDistance;

					if (abDistance < maxDistance)
					{
						closestPair = abPair;
						lowestDistance = abDistance;
					}
				}

				if (baPair != null)
				{
					var baDistance = baPair.Value.parallelDistance;

					if (baDistance < maxDistance)
					{
						closestPair = baPair;
						lowestDistance = baDistance;
					}
				}
			}

			return closestPair;
		}

		private static SnappingPair? FindClosestPair(Axis2 axis, float maxDistance, HashSet<SnappingAnchor> snappees, HashSet<SnappingAnchor> snappers)
		{
			var lowestDistance = maxDistance;
			SnappingAnchor? closestSnappee = null;
			SnappingAnchor? closestSnapper = null;
			var found = false;

			foreach (var snappee in snappees)
			foreach (var snapper in snappers)
			{
				var distance = SnappingUtility.Distance(snappee.position, snapper.position, axis);
					
				if (distance < lowestDistance)
				{
					lowestDistance = distance;
					closestSnappee = snappee;
					closestSnapper = snapper;
					found = true;
				}
			}

			if (!found) return null;

			return new SnappingPair(closestSnappee.Value, closestSnapper.Value, axis);
		}
	}
}
