﻿using System;
using UnityEngine;

namespace Ludiq
{
	public struct SnappingAnchor : IEquatable<SnappingAnchor>
	{
		public object parent { get; }
		public Vector2 position { get; }

		public SnappingAnchor(object parent, Vector2 position)
		{
			this.parent = parent;
			this.position = position;
		}

		public bool Equals(SnappingAnchor other)
		{
			return Equals(parent, other.parent) && 
			       position.Equals(other.position);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj))
			{
				return false;
			}

			return obj is SnappingAnchor other && Equals(other);
		}

		public override int GetHashCode()
		{
			return HashUtility.GetHashCode(parent, position);
		}
	}
}
