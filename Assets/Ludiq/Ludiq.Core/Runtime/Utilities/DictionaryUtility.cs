﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ludiq
{
	public static class DictionaryUtility
	{
		public static IDictionary Merge(this IDictionary destination, IDictionary source)
		{
			var sourceEnumerator = source.GetEnumerator();

			while (sourceEnumerator.MoveNext())
			{
				if (!destination.Contains(sourceEnumerator.Key))
				{
					destination.Add(sourceEnumerator.Key, sourceEnumerator.Value);
				}
			}

			return destination;
		}

		public static IDictionary Merge(this IDictionary destination, params IDictionary[] sources)
		{
			foreach (var source in sources)
			{
				destination.Merge(source);
			}

			return destination;
		}
	}
}
