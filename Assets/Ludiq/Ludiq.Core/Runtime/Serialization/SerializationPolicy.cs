﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using Ludiq.OdinSerializer;
using Ludiq.OdinSerializer.Utilities;
using Ludiq.FullSerializer;
using UnityEngine;

namespace Ludiq
{
	public class SerializationPolicy : ISerializationPolicy
	{
		public static SerializationPolicy instance { get; } = new SerializationPolicy();

		public string ID => "Ludiq";

		public bool AllowNonSerializableTypes => true;
		
		public bool ShouldSerializeMember(MemberInfo member)
		{
			if (member.HasAttribute<DoNotSerializeAttribute>() ||
			    member.HasAttribute<NonSerializedAttribute>())
			{
				return false;
			}

			if (member.HasAttribute<SerializeAttribute>() ||
				member.HasAttribute<SerializeAsAttribute>() ||
			    member.HasAttribute<SerializeField>() ||
			    member.HasAttribute<OdinSerializeAttribute>())
			{
				return true;
			}

			if (member is FieldInfo field)
			{
				if (typeof(Delegate).IsAssignableFrom(field.FieldType))
				{
					return false;
				}
				
				if (field.HasAttribute<CompilerGeneratedAttribute>(false))
				{
					return false;
				}

				if (field.IsStatic)
				{
					return false;
				}
				
				if (!field.IsPublic)
				{
					return false;
				}

				return true;
			}
			else if (member is PropertyInfo property)
			{
				if (typeof(Delegate).IsAssignableFrom(property.PropertyType))
				{
					return false;
				}
				
				if (!property.CanRead)
				{
					return false;
				}
				
				if (!property.CanWrite)
				{
					return false;
				}

				if (property.IsStatic())
				{
					return false;
				}
				
				if (property.IsIndexer())
				{
					return false;
				}

				if (!property.IsPubliclyGettable())
				{
					return false;
				}
				
				if (!IsAutoProperty(property))
				{
					return false;
				}
				
				return true;
			}

			return false;
		}

		private static bool IsAutoProperty(PropertyInfo property)
		{
			// Odin's implementation is weirdly restrictive and expensive, using FullSerializer's
			return property.CanWrite && property.CanRead && property.GetGetMethod(true).HasAttribute<CompilerGeneratedAttribute>();
		}
	}
}
 