using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Ludiq.FullSerializer;
using Ludiq.OdinSerializer;
using Debug = UnityEngine.Debug;
using UnityObject = UnityEngine.Object;
using OdinSerializationData = Ludiq.OdinSerializer.SerializationData;
using OdinSerializationContext = Ludiq.OdinSerializer.SerializationContext;
using OdinDeserializationContext = Ludiq.OdinSerializer.DeserializationContext;
using System.Threading;

namespace Ludiq
{
	public static class Serialization
	{
		static Serialization()
		{
			freeFullOperations = new HashSet<FullSerializationOperation>();
			busyFullOperations = new HashSet<FullSerializationOperation>();

			freeOdinSerializationContexts = new HashSet<OdinSerializationContext>();
			busyOdinSerializationContexts = new HashSet<OdinSerializationContext>();

			freeOdinDeserializationContexts = new HashSet<OdinDeserializationContext>();
			busyOdinDeserializationContexts = new HashSet<OdinDeserializationContext>();
		}

		public const string ConstructorWarning = "This parameterless constructor is only made public for serialization. Use another constructor instead.";

		private static readonly object @lock = new object();

		public static bool isUnitySerializing { get; set; }

		public static bool isSerializing => isUnitySerializing || isFullSerializing || isOdinSerializing;
		
		public static string PrettyPrint(string json)
		{
			return fsJsonPrinter.PrettyJson(fsJsonParser.Parse(json));
		}



		#region Full Serializer
		
		private static readonly HashSet<FullSerializationOperation> freeFullOperations;

		private static readonly HashSet<FullSerializationOperation> busyFullOperations;

		public static bool isFullSerializing => busyFullOperations.Count > 0;
		
		private static FullSerializationOperation BeginFullOperation()
		{
			lock (@lock)
			{
				if (freeFullOperations.Count == 0)
				{
					freeFullOperations.Add(new FullSerializationOperation());
				}

				var operation = freeFullOperations.First();
				freeFullOperations.Remove(operation);
				busyFullOperations.Add(operation);
				return operation;
			}
		}

		private static void EndFullOperation(FullSerializationOperation operation)
		{
			lock (@lock)
			{
				if (!busyFullOperations.Contains(operation))
				{
					throw new InvalidOperationException("Trying to finish an operation that isn't started.");
				}

				operation.Reset();
				busyFullOperations.Remove(operation);
				freeFullOperations.Add(operation);
			}
		}

		public static FullSerializationData FullSerialize(this object value, bool forceReflected = false)
		{
			try
			{
				var operation = BeginFullOperation();
				var json = FullSerializeJson(operation.serializer, value, forceReflected);
				var objectReferences = operation.objectReferences.ToArray();
				var data = new FullSerializationData(json, objectReferences);
				EndFullOperation(operation);

#if DEBUG_SERIALIZATION
				Debug.Log(data.ToString($"<color=#88FF00>Serialized: <b>{value?.GetType().Name ?? "null"} [{value?.GetHashCode().ToString() ?? "N/A"}]</b></color>"));
#endif

				return data;
			}
			catch (Exception ex)
			{
				throw new SerializationException($"Serialization of '{value?.GetType().ToString() ?? "null"}' failed.", ex);
			}
		}

		public static void FullDeserializeInto(this FullSerializationData data, ref object instance, bool forceReflected = false)
		{
			try
			{
				if (string.IsNullOrEmpty(data.json))
				{
					instance = null;
					return;
				}

				var operation = BeginFullOperation();
				operation.objectReferences.AddRange(data.objectReferences);
				FullDeserializeJson(operation.serializer, data.json, ref instance, forceReflected);
				EndFullOperation(operation);
			}
			catch (Exception ex)
			{
				try
				{
					Debug.LogWarning(data.ToString("Deserialization Failure Data"), instance as UnityObject);
				}
				catch (Exception ex2)
				{
					Debug.LogWarning("Failed to log deserialization failure data:\n" + ex2, instance as UnityObject);
				}

				throw new SerializationException($"Deserialization into '{instance?.GetType().ToString() ?? "null"}' failed.", ex);
			}
		}

		public static object FullDeserialize(this FullSerializationData data, bool forceReflected = false)
		{
			object instance = null;
			FullDeserializeInto(data, ref instance, forceReflected);
			return instance;
		}

		private static string FullSerializeJson(fsSerializer serializer, object instance, bool forceReflected)
		{
			using (ProfilingUtility.SampleBlock("SerializeJson"))
			{
				fsData data;

				fsResult result;

				if (forceReflected)
				{
					result = serializer.TrySerialize(instance.GetType(), typeof(fsReflectedConverter), instance, out data);
				}
				else
				{
					result = serializer.TrySerialize(instance, out data);
				}

				HandleFullResult("Serialization", result, instance as UnityObject);

				return fsJsonPrinter.CompressedJson(data);
			}
		}

		private static void FullDeserializeJson(fsSerializer serializer, string json, ref object instance, bool forceReflected)
		{
			using (ProfilingUtility.SampleBlock("DeserializeJson"))
			{
				var fsData = fsJsonParser.Parse(json);

				fsResult result;

				if (forceReflected)
				{
					result = serializer.TryDeserialize(fsData, instance.GetType(), typeof(fsReflectedConverter), ref instance);
				}
				else
				{
					result = serializer.TryDeserialize(fsData, ref instance);
				}

				HandleFullResult("Deserialization", result, instance as UnityObject);
			}
		}

		private static void HandleFullResult(string label, fsResult result, UnityObject context = null)
		{
			result.AssertSuccess();

			if (result.HasWarnings)
			{
				foreach (var warning in result.RawMessages)
				{
					Debug.LogWarning($"[{label}] {warning}\n", context);
				}
			}
		}
		
		#endregion
		


		#region Odin Serializer
		
		private static readonly HashSet<OdinSerializationContext> freeOdinSerializationContexts;

		private static readonly HashSet<OdinSerializationContext> busyOdinSerializationContexts;
		
		private static readonly HashSet<OdinDeserializationContext> freeOdinDeserializationContexts;

		private static readonly HashSet<OdinDeserializationContext> busyOdinDeserializationContexts;

		private static OdinSerializationContext BeginOdinSerializationContext()
		{
			lock (@lock)
			{
				if (freeOdinSerializationContexts.Count == 0)
				{
					freeOdinSerializationContexts.Add(new OdinSerializationContext());
				}

				var context = freeOdinSerializationContexts.First();
				ConfigureOdinSerializationContext(context);
				freeOdinSerializationContexts.Remove(context);
				busyOdinSerializationContexts.Add(context);
				return context;
			}
		}

		private static void EndOdinSerializationContext(OdinSerializationContext context)
		{
			lock (@lock)
			{
				if (!busyOdinSerializationContexts.Contains(context))
				{
					throw new InvalidOperationException("Trying to finish an operation that isn't started.");
				}

				context.ResetToDefault();
				busyOdinSerializationContexts.Remove(context);
				freeOdinSerializationContexts.Add(context);
			}
		}

		private static OdinDeserializationContext BeginOdinDeserializationContext()
		{
			lock (@lock)
			{
				if (freeOdinDeserializationContexts.Count == 0)
				{
					freeOdinDeserializationContexts.Add(new OdinDeserializationContext());
				}

				var context = freeOdinDeserializationContexts.First();
				ConfigureOdinDeserializationContext(context);
				freeOdinDeserializationContexts.Remove(context);
				busyOdinDeserializationContexts.Add(context);
				return context;
			}
		}

		private static void EndOdinDeserializationContext(OdinDeserializationContext context)
		{
			lock (@lock)
			{
				if (!busyOdinDeserializationContexts.Contains(context))
				{
					throw new InvalidOperationException("Trying to finish an operation that isn't started.");
				}
				
				context.Reset();
				busyOdinDeserializationContexts.Remove(context);
				freeOdinDeserializationContexts.Add(context);
			}
		}

		private static void ConfigureOdinSerializationContext(OdinSerializationContext context)
		{
			ConfigureOdinConfig(context.Config);
		}

		private static void ConfigureOdinDeserializationContext(OdinDeserializationContext context)
		{
			ConfigureOdinConfig(context.Config);
		}

		private static void ConfigureOdinConfig(SerializationConfig config)
		{
			config.SerializationPolicy = SerializationPolicy.instance;
			config.DebugContext.Logger = SerializationLogger.instance;
		}

		public static bool isOdinSerializing => busyOdinSerializationContexts.Count > 0 || busyOdinDeserializationContexts.Count > 0;
		
		public static OdinSerializationData OdinSerialize<T>(this T value, DataFormat format = DataFormat.Binary)
		{
			try
			{
				var data = new OdinSerializationData();
				var context = BeginOdinSerializationContext();

				if (value is UnityObject uo)
				{
					UnitySerializationUtility.SerializeUnityObject(uo, ref data, true, context);
				}
				else
				{
					var bytes = SerializationUtility.SerializeValue(value, format, out var unityObjects, context);
					data.SerializedFormat = format;
					data.SerializedBytes = bytes;
					data.ReferencedUnityObjects = unityObjects;
				}

				EndOdinSerializationContext(context);

				return data;
			}
			catch (Exception ex)
			{
				throw new SerializationException($"Serialization of '{value?.GetType().ToString() ?? "null"}' failed.", ex);
			}
		}

		public static void OdinDeserializeInto<T>(this OdinSerializationData data, ref T instance)
		{
			try
			{
				var context = BeginOdinDeserializationContext();

				if (instance is UnityObject uo)
				{
					UnitySerializationUtility.DeserializeUnityObject(uo, ref data, context);
				}
				else
				{
					instance = SerializationUtility.DeserializeValue<T>(data.SerializedBytes, data.SerializedFormat, data.ReferencedUnityObjects, context);
				}

				EndOdinDeserializationContext(context);
			}
			catch (ThreadAbortException) { }
			catch (Exception ex)
			{
				throw new SerializationException($"Deserialization into '{instance?.GetType().ToString() ?? "null"}' failed.", ex);
			}
		}

		public static T OdinDeserialize<T>(this OdinSerializationData data)
		{
			T instance = default(T);
			OdinDeserializeInto(data, ref instance);
			return instance;
		}

		public static OdinSerializationData ToOdinData(this byte[] bytes)
		{
			return new OdinSerializationData { SerializedBytes = bytes };
		}

		#endregion



		#region Dependencies

		public static readonly HashSet<ISerializationDepender> awaitingDependers = new HashSet<ISerializationDepender>();

		public static readonly HashSet<ISerializationDependency> availableDependencies = new HashSet<ISerializationDependency>();

		public static void AwaitDependencies(ISerializationDepender depender)
		{
			awaitingDependers.Add(depender);

			CheckIfDependenciesMet(depender);
		}

		public static void NotifyDependencyDeserialized(ISerializationDependency dependency)
		{
			availableDependencies.Add(dependency);

			foreach (var awaitingDepender in awaitingDependers.ToArray())
			{
				if (!awaitingDependers.Contains(awaitingDepender))
				{
					// In case the depender was already handled by a recursive 
					// dependency via OnAfterDependenciesDeserialized,
					// we skip it. This is necessary because we duplicated
					// the set to safely iterate over it with removal.
					// 
					// This should prevent OnAfterDependenciesDeserialized from
					// running twice on any given depender in a single deserialization
					// operation.
					continue;
				}

				CheckIfDependenciesMet(awaitingDepender);
			}
		}

		private static void CheckIfDependenciesMet(ISerializationDepender depender)
		{
			if (availableDependencies.IsSupersetOf(depender.deserializationDependencies))
			{
				awaitingDependers.Remove(depender);
				depender.OnAfterDependenciesDeserialized();
			}
		}

		#endregion
	}
}