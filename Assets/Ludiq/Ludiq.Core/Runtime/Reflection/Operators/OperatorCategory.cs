﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ludiq
{
	public enum OperatorCategory
	{
		None,

		Math,
		Logic,
		Comparison,
		Bitwise,
		Incrementation,

		Count,
	}
}
