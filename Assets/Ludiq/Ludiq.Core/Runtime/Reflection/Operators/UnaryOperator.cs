﻿namespace Ludiq
{
	public enum UnaryOperator
	{
		BitwiseNegation,
		NumericNegation,
		Increment,
		Decrement,
		Plus
	}
}