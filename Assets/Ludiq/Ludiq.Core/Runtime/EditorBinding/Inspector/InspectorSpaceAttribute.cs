﻿using System;

namespace Ludiq
{
	[AttributeUsage(AttributeTargets.All, AllowMultiple = false, Inherited = true)]
	public sealed class InspectorSpaceAttribute : Attribute
	{
		public InspectorSpaceAttribute(float space)
		{
			this.space = space;
		}

		public float space { get;}
	}
}