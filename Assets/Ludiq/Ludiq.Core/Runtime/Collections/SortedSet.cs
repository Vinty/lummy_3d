﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ludiq
{
	public class SortedSet<T> : ICollection<T>, IEnumerable<T>, ISet<T>, ICollection
	{
		private List<T> list;
		private IComparer<T> comparer;

		public SortedSet() : this(Comparer<T>.Default) {}

		public SortedSet(IComparer<T> comparer)
		{
			list = new List<T>();
			this.comparer = comparer;
		}

		public SortedSet(IEnumerable<T> enumerable) : this(enumerable, Comparer<T>.Default) {}

		public SortedSet(IEnumerable<T> enumerable, IComparer<T> comparer)
		{
			list = new List<T>(enumerable.Distinct());
			this.comparer = comparer;
			list.Sort(comparer);
		}

		public T Min => list.Count > 0 ? list[0] : default(T);
		public T Max => list.Count > 0 ? list[list.Count - 1] : default(T);

		public void CopyTo(T[] array)
		{
			list.CopyTo(array);
		}

		public void CopyTo(T[] array, int arrayIndex, int count)
		{
			list.CopyTo(0, array, arrayIndex, count);
		}

		public SortedSet<T> GetViewBetween(T lowerValue, T upperValue)
		{
			return new SortedSet<T>(GetItemsBetween(lowerValue, upperValue));
		}

		private IEnumerable<T> GetItemsBetween(T lowerValue, T upperValue)
		{
			if (comparer.Compare(lowerValue, upperValue) > 0)
			{
				throw new ArgumentException(nameof(lowerValue) + " is greater than " + nameof(upperValue));
			}
			if (list.Count == 0 || comparer.Compare(list[0], lowerValue) > 0)
			{
				throw new ArgumentException(nameof(lowerValue));
			}
			if (comparer.Compare(list[list.Count - 1], upperValue) < 0)
			{
				throw new ArgumentException(nameof(upperValue));
			}

			int index = list.BinarySearch(lowerValue, comparer);
			if (index < 0)
			{
				index = ~index;
			}
			
			do
			{
				yield return list[index];
			} while(index < list.Count && comparer.Compare(list[index], upperValue) <= 0);
		}

		#region IEnumerable<T>
		public IEnumerator<T> GetEnumerator()
		{
			return list.GetEnumerator();
		}
		#endregion

		#region ICollection<T>
		public int Count => list.Count;

		public bool IsReadOnly => false;

		void ICollection<T>.Add(T item)
		{
			Add(item);
		}

		public void Clear()
		{
			list.Clear();
		}

		public bool Contains(T item)
		{
			int index = list.BinarySearch(item, comparer);
			return index >= 0;
		}

		public void CopyTo(T[] array, int arrayIndex)
		{
			list.CopyTo(array, arrayIndex);
		}

		public bool Remove(T item)
		{
			return list.Remove(item);
		}
		#endregion

		#region ISet<T>
		public bool Add(T item)
		{
			int index = list.BinarySearch(item, comparer);
			if (index < 0)
			{
				index = ~index;
				list.Insert(index, item);
				return true;
			}

			return false;
		}

		public void UnionWith(IEnumerable<T> other)
		{
			foreach (var item in other)
			{
				Add(item);
			}
		}

		public void IntersectWith(IEnumerable<T> other)
		{
			foreach (var item in other)
			{
				if (!Contains(item))
				{
					Remove(item);
				}
			}
		}

		public void ExceptWith(IEnumerable<T> other)
		{
			foreach (var item in other)
			{
				Remove(item);
			}
		}

		public void SymmetricExceptWith(IEnumerable<T> other)
		{
			foreach (var item in other.Distinct())
			{
				if (Contains(item))
				{
					Remove(item);
				}
				else
				{
					Add(item);
				}
			}
		}

		public bool IsSubsetOf(IEnumerable<T> other)
		{
			foreach (var item in this)
			{
				if (!other.Contains(item))
				{
					return false;
				}
			}

			return true;
		}

		public bool IsSupersetOf(IEnumerable<T> other)
		{
			foreach (var item in other)
			{
				if (!Contains(item))
				{
					return false;
				}
			}

			return true;
		}

		public bool IsProperSubsetOf(IEnumerable<T> other)
		{
			if (IsSubsetOf(other))
			{
				foreach (var item in other)
				{
					if (!Contains(item))
					{
						return true;
					}
				}
			}

			return false;
		}

		public bool IsProperSupersetOf(IEnumerable<T> other)
		{
			if (IsSupersetOf(other))
			{
				foreach (var item in this)
				{
					if (!other.Contains(item))
					{
						return true;
					}
				}
			}

			return false;
		}

		public bool Overlaps(IEnumerable<T> other)
		{
			foreach (var item in this)
			{
				if (other.Contains(item))
				{
					return true;
				}
			}
			return false;
		}

		public bool SetEquals(IEnumerable<T> other)
		{
			foreach (var item in other)
			{
				if (!Contains(item))
				{
					return false;
				}
			}

			foreach (var item in this)
			{
				if (!other.Contains(item))
				{
					return false;
				}
			}

			return true;
		}
		#endregion

		#region ICollection
		object ICollection.SyncRoot => ((ICollection)list).SyncRoot;

		bool ICollection.IsSynchronized => ((ICollection)list).IsSynchronized;

		IEnumerator IEnumerable.GetEnumerator()
		{
			return list.GetEnumerator();
		}

		public void CopyTo(Array array, int arrayIndex)
		{
			((ICollection)list).CopyTo(array, arrayIndex);
		}
		#endregion
	}
}
