﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Ludiq
{
	public class Bictionary<TLeft, TRight> : IEnumerable<KeyValuePair<TLeft, TRight>>
	{
		[Serialize]
		private IDictionary<TLeft, TRight> leftToRight = new Dictionary<TLeft, TRight>();

		[Serialize]
		private IDictionary<TRight, TLeft> rightToLeft = new Dictionary<TRight, TLeft>();

		public int Count => leftToRight.Count;
		
		public TRight this[TLeft left]
		{
			get { return leftToRight[left]; }
			set
			{
				var right = value;
				leftToRight[left] = right;
				rightToLeft[right] = left;
			}
		}

		public TLeft this[TRight right]
		{
			get { return rightToLeft[right]; }
			set
			{
				var left = value;
				rightToLeft[right] = left;
				leftToRight[left] = right;
			}
		}

		public ICollection<TLeft> Left => leftToRight.Keys;
		
		public ICollection<TRight> Right => rightToLeft.Keys;
		
		public IEnumerator<KeyValuePair<TLeft, TRight>> GetEnumerator()
		{
			return leftToRight.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public void Add(TLeft left, TRight right)
		{
			leftToRight.Add(left, right);
			rightToLeft.Add(right, left);
		}

		public bool ContainsLeft(TLeft left)
		{
			return leftToRight.ContainsKey(left);
		}

		public bool ContainsRight(TRight right)
		{
			return rightToLeft.ContainsKey(right);
		}

		public bool TryGetRight(TLeft left, out TRight right)
		{
			return leftToRight.TryGetValue(left, out right);
		}

		public bool TryGetLeft(TRight right, out TLeft left)
		{
			return rightToLeft.TryGetValue(right, out left);
		}

		public bool Remove(TLeft left)
		{
			if (leftToRight.TryGetValue(left, out var right))
			{
				leftToRight.Remove(left);
				rightToLeft.Remove(right);
				return true;
			}
			else
			{
				return false;
			}
		}

		public bool Remove(TRight right)
		{
			if (rightToLeft.TryGetValue(right, out var left))
			{
				rightToLeft.Remove(right);
				leftToRight.Remove(left);
				return true;
			}
			else
			{
				return false;
			}
		}

		public void Clear()
		{
			leftToRight.Clear();
			rightToLeft.Clear();
		}
	}
}