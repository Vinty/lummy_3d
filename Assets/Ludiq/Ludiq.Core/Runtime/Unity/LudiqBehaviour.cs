﻿using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace Ludiq
{
	public abstract class LudiqBehaviour : MonoBehaviour, ISerializationCallbackReceiver
	{
		[SerializeField, DoNotSerialize, FormerlySerializedAs("_data")] // Serialize with Unity, but not with FullSerializer.
		protected FullSerializationData _fullData;

		void ISerializationCallbackReceiver.OnBeforeSerialize()
		{
			if (Serialization.isFullSerializing)
			{
				return;
			}

			Serialization.isUnitySerializing = true;

			try
			{
				OnBeforeSerialize();
				_fullData = this.FullSerialize(true);
			}
			catch (Exception ex)
			{
				// Don't abort the whole serialization thread because this one object failed
				Debug.LogError($"Failed to serialize behaviour.\n{ex}", this);
			}

			Serialization.isUnitySerializing = false;
		}

		void ISerializationCallbackReceiver.OnAfterDeserialize()
		{
			if (Serialization.isFullSerializing)
			{
				return;
			}

			Serialization.isUnitySerializing = true;

			try
			{
				object @this = this;
				_fullData.FullDeserializeInto(ref @this, true);
				OnAfterDeserialize();
				UnityThread.EditorAsync(OnPostDeserializeInEditor);
			}
			catch (Exception ex)
			{
				// Don't abort the whole deserialization thread because this one object failed
				Debug.LogError($"Failed to deserialize behaviour.\n{ex}", this);
			}

			Serialization.isUnitySerializing = false;
		}

		protected virtual void OnBeforeSerialize() { }

		protected virtual void OnAfterDeserialize() { }

		protected virtual void OnPostDeserializeInEditor() { }

		protected virtual void ShowData()
		{
			_fullData.ShowString(ToString());
		}

		public override string ToString()
		{
			return this.ToSafeString();
		}
	}
}