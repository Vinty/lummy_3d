﻿namespace Ludiq.CodeDom
{
	public interface ICodeWriterSystem
	{
		ICodeWriter OpenWriter(string className);
	}
}
