﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ludiq.CodeDom
{
	public abstract class CodeElement
	{
		public virtual IEnumerable<CodeElement> Children => Enumerable.Empty<CodeElement>();
	}
}
