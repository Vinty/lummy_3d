﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ludiq.CodeDom
{
	public enum CodeCompoundAssignmentOperatorType
	{
        Add,
        Subtract,
        Multiply,
        Divide,
        Modulo,
        BitwiseOr,
        BitwiseAnd,
		BitwiseXor,
		BitwiseShiftLeft,
		BitwiseShiftRight,
	}
}