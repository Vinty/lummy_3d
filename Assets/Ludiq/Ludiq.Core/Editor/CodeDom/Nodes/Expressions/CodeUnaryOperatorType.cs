﻿using System;

namespace Ludiq.CodeDom
{
    public enum CodeUnaryOperatorType
    {
        Positive,
		Negative,
		LogicalNot,
		BitwiseNot,
		PreIncrement,
		PreDecrement,
		AddressOf,
		Dereference,
		PostIncrement,
		PostDecrement,
    }
}
