﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ludiq.CodeDom
{
	public sealed class CodeInterfaceTypeDeclaration : CodeCompositeTypeDeclaration
	{
		public CodeInterfaceTypeDeclaration(CodeMemberModifiers modifiers, string name)
			: base(modifiers, name)
		{
		}

		public override bool IsInterface => true;
		public override string Keyword => "interface";
	}
}
