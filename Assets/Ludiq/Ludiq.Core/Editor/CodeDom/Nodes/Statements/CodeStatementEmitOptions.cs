﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ludiq.CodeDom
{
	[Flags]
	public enum CodeStatementEmitOptions
	{
		OmitSemiColon = 0x01
	}
}
