﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ludiq
{
	public interface IDescriptorWithTitleInformation : IDescriptor
	{
		string Title();
		string Summary();
	}
}
