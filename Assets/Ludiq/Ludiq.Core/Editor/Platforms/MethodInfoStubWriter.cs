﻿using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Ludiq
{
	[AotStubWriter(typeof(MethodInfo))]
	public class MethodInfoStubWriter : MethodBaseStubWriter<MethodInfo>
	{
		public MethodInfoStubWriter(MethodInfo methodInfo) : base(methodInfo) { }

		public override IEnumerable<CodeStatement> GetStubStatements()
		{
			/* 
			 * Required output:
			 * 1. Create a target expression
			 * 2. Call its method with the correct number of args
			 * 3. Call its optimized method with the correct number of args
			 * 4. Call its optimized method with an args array
			*/

			var targetType = CodeFactory.TypeRef(manipulator.targetType, true);
			var declaringType = CodeFactory.TypeRef(stub.DeclaringType, true);

			CodeExpression targetValue;
			CodeExpression targetReference;

			if (manipulator.requiresTarget && !manipulator.isExtension)
			{
				// default(Material)
				targetValue = targetType.DefaultValue();

				// 1. Material target = default(Material);
				yield return CodeFactory.VarDecl(targetType, "target", targetValue);

				targetReference = CodeFactory.VarRef("target");
			}
			else
			{
				targetValue = CodeFactory.Primitive(null);
				targetReference = targetType.Expression();

				if (manipulator.isExtension)
				{
					// 1. ShortcutExtensions
					targetReference = new CodeTypeReferenceExpression(declaringType); 
				}
				else
				{
					// 1. Material
					targetReference = new CodeTypeReferenceExpression(targetType);
				}
			}

			// target.SetColor
			var methodReference = targetReference.Method(manipulator.name);

			var arguments = new List<CodeExpression>();

			var includesOutOrRef = false;

			foreach (var parameterInfo in stub.GetParameters())
			{
				var parameterType = CodeFactory.TypeRef(parameterInfo.UnderlyingParameterType(), true);
				var argumentName = $"arg{arguments.Count}";

				// arg0 = default(string)
				// arg1 = default(Color)
				yield return CodeFactory.VarDecl(parameterType, argumentName, parameterType.DefaultValue());

				CodeParameterDirection direction;

				if (parameterInfo.IsOut)
				{
					direction = CodeParameterDirection.Out;
					includesOutOrRef = true;
				}
				else if (parameterInfo.ParameterType.IsByRef)
				{
					direction = CodeParameterDirection.Ref;
					includesOutOrRef = true;
				}
				else
				{
					direction = CodeParameterDirection.Default;
				}

				var argument = CodeFactory.ArgumentDirection(direction, CodeFactory.VarRef(argumentName));

				arguments.Add(argument);
			}

			if (operatorTypes.ContainsKey(manipulator.name))
			{
				// arg0 * arg1
				var operation = arguments[0].BinaryOp(operatorTypes[manipulator.name], arguments[1]);

				// 2. var operator = arg0 * arg1;
				yield return CodeFactory.VarDecl(CodeFactory.TypeRef(manipulator.type), "operator", operation);
			}
			else if (manipulator.isConversion)
			{
				// (Vector3)arg0
				var cast = arguments[0].Cast(CodeFactory.TypeRef(manipulator.type));

				// 2. var conversion = (Vector3)arg0;
				yield return CodeFactory.VarDecl(CodeFactory.TypeRef(manipulator.type), "conversion", cast);
			}
			else if (manipulator.isPubliclyInvocable && !manipulator.isConversion)
			{
				// 2. target.SetColor(arg0, arg1);
				yield return methodReference.Invoke(arguments.ToArray()).Statement();
			}

			var optimizedInvokerType = CodeFactory.TypeRef(stub.Prewarm().GetType(), true);

			// var invoker = new InstanceActionInvoker<Material, string, Color>(default(MethodInfo));
			yield return CodeFactory.VarDecl(optimizedInvokerType, "optimized", optimizedInvokerType.ObjectCreate(CodeFactory.TypeRef(typeof(MethodInfo), true).DefaultValue()));

			// [default(Material), arg0, arg1]
			var argumentsWithTarget = targetValue.Yield().Concat(arguments).ToArray();

			// Ref and out parameters are not supported in the numbered argument signatures
			if (!includesOutOrRef)
			{
				// 3. invoker.Invoke(default(Material), arg0, arg1);
				yield return CodeFactory.VarRef("optimized").Method(nameof(IOptimizedInvoker.Invoke)).Invoke(argumentsWithTarget).Statement();
			}

			// 4. invoker.Invoke(default(Material), default(object[]));
			yield return CodeFactory.VarRef("optimized").Method(nameof(IOptimizedInvoker.Invoke)).Invoke(CodeFactory.TypeRef(typeof(object[])).DefaultValue()).Statement();
		}

		public static readonly Dictionary<string, CodeBinaryOperatorType> operatorTypes = new Dictionary<string, CodeBinaryOperatorType>
		{
			{ "op_Addition", CodeBinaryOperatorType.Add },
			{ "op_Subtraction", CodeBinaryOperatorType.Subtract },
			{ "op_Multiply", CodeBinaryOperatorType.Multiply },
			{ "op_Division", CodeBinaryOperatorType.Divide },
			{ "op_Modulus", CodeBinaryOperatorType.Modulo },
			{ "op_BitwiseAnd", CodeBinaryOperatorType.BitwiseAnd },
			{ "op_BitwiseOr", CodeBinaryOperatorType.BitwiseOr },
			{ "op_LogicalAnd", CodeBinaryOperatorType.LogicalAnd },
			{ "op_LogicalOr", CodeBinaryOperatorType.LogicalOr },
			{ "op_Equality", CodeBinaryOperatorType.Equality },
			{ "op_GreaterThan", CodeBinaryOperatorType.GreaterThan },
			{ "op_LessThan", CodeBinaryOperatorType.LessThan },
			{ "op_Inequality", CodeBinaryOperatorType.Inequality },
			{ "op_GreaterThanOrEqual", CodeBinaryOperatorType.GreaterThanOrEqual },
			{ "op_LessThanOrEqual", CodeBinaryOperatorType.LessThanOrEqual }
		};
	}
}