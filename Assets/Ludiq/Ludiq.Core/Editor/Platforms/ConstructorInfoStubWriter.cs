﻿using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Reflection;

namespace Ludiq
{
	[AotStubWriter(typeof(ConstructorInfo))]
	public class ConstructorInfoStubWriter : MethodBaseStubWriter<ConstructorInfo>
	{
		public ConstructorInfoStubWriter(ConstructorInfo constructorInfo) : base(constructorInfo) { }

		public override IEnumerable<CodeStatement> GetStubStatements()
		{
			/* 
			 * Required output:
			 * 1. Call the constructor with the correct number of args
			 * (No optimization available for constructors)
			*/

			var arguments = new List<CodeExpression>();

			foreach (var parameterInfo in stub.GetParameters())
			{
				var parameterType = CodeFactory.TypeRef(parameterInfo.UnderlyingParameterType(), true);
				var argumentName = $"arg{arguments.Count}";

				yield return CodeFactory.VarDecl(parameterType, argumentName, parameterType.DefaultValue());

				CodeParameterDirection direction;

				if (parameterInfo.IsOut)
				{
					direction = CodeParameterDirection.Out;
				}
				else if (parameterInfo.ParameterType.IsByRef)
				{
					direction = CodeParameterDirection.Ref;
				}
				else
				{
					direction = CodeParameterDirection.Default;
				}

				var argument = CodeFactory.ArgumentDirection(direction, CodeFactory.VarRef(argumentName));

				arguments.Add(argument);
			}

			if (manipulator.isPubliclyInvocable)
			{
				yield return CodeFactory.TypeRef(stub.DeclaringType).ObjectCreate(arguments).Statement();
			}
		}
	}
}