﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using UnityEditor;
using UnityEngine;

namespace Ludiq
{
	public sealed class FuzzyWindow : EditorWindow
	{
		public void Populate(FuzzyOptionNode node, IEnumerable<IFuzzyOption> children, CancellationToken? cancellation = null)
		{
			if (node.isPopulated)
			{
				return;
			}
			
			if (node.option.mode == FuzzyOptionMode.Branch)
			{
				var i = 0;
					
				var _children = children.ToArray();

				lock (guiLock)
				{
					node.hasChildren = _children.Length > 0;
				}

				var childNodes = new List<FuzzyOptionNode>();
				
				foreach (var child in _children)
				{
					try
					{
						child.OnPopulate();
					}
					catch (Exception ex)
					{
						Debug.LogWarning($"Failed to display {child.GetType()}: \n{ex}");
						continue;
					}

					string label;
					
					if (node.option is SearchOption)
					{
						label = tree.SearchResultLabel(child, query);
					}
					else if (node == favoritesRoot || (node == root && tree.UseExplicitRootLabel(child)))
					{
						label = tree.ExplicitLabel(child);
					}
					else
					{
						label = child.label;
					}

					FuzzyOptionNode childNode;
					try
					{
						childNode = new FuzzyOptionNode(child, label);
					}
					catch (Exception ex)
					{
						Debug.LogWarning($"Failed to create option node for {child.GetType()} (value = {child.value}): \n{ex}");
						continue;
					}

					DisplayProgressBar($"{child.label}... ({++i} / {_children.Length})", (float) i / _children.Length);
					
					if (child.mode == FuzzyOptionMode.Leaf)
					{
						childNodes.Add(childNode);
					}
					else if (child.mode == FuzzyOptionMode.Branch)
					{
						childNode.hasChildren = tree.Children(child).Any();

						if (childNode.hasChildren)
						{
							childNodes.Add(childNode);
						}
					}

					cancellation?.ThrowIfCancellationRequested();
				}

				lock (guiLock)
				{
					node.children.AddRange(childNodes);
				}
			}

			lock (guiLock)
			{
				node.isPopulated = true;
			}
		}

		static FuzzyWindow()
		{
			ShowAsDropDownFitToScreen = typeof(FuzzyWindow).GetMethod("ShowAsDropDownFitToScreen", BindingFlags.Instance | BindingFlags.NonPublic);
		}

		public static GUIStyle defaultOptionStyle => Styles.optionWithIcon;

		private static Event e => Event.current;

		public static class Styles
		{
			static Styles()
			{
				var headerFontSize = new GUIStyle("In BigTitle").fontSize;

				headerBackground = new GUIStyle(EditorStyles.toolbar)
				{
					font = EditorStyles.boldLabel.font,
					fontSize = headerFontSize,
					fixedHeight = headerHeight,
					alignment = TextAnchor.MiddleCenter,
					margin = new RectOffset(1, 1, 0, 0),
				};
				headerBreadcrumbRoot = new GUIStyle(LudiqStyles.toolbarBreadcrumbRoot)
				{
					font = EditorStyles.label.font,
					fontSize = headerFontSize,
					fixedHeight = headerHeight,
					alignment = TextAnchor.MiddleCenter,
				};
				headerBreadcrumbElement = new GUIStyle(LudiqStyles.toolbarBreadcrumb)
				{
					font = EditorStyles.label.font,
					fontSize = headerFontSize,
					fixedHeight = headerHeight,
					alignment = TextAnchor.MiddleCenter,
				};
				headerBreadcrumbRootCurrent = new GUIStyle(headerBreadcrumbRoot) { font = EditorStyles.boldLabel.font };
				headerBreadcrumbElementCurrent = new GUIStyle(headerBreadcrumbElement) { font = EditorStyles.boldLabel.font };

				footerBackground = new GUIStyle("In BigTitle");

				optionWithIcon = new GUIStyle("PR Label");
				optionWithIcon.richText = true;
				optionWithIcon.alignment = TextAnchor.MiddleLeft;
				optionWithIcon.padding.left -= 15;
				optionWithIcon.fixedHeight = 20f;

				optionWithoutIcon = new GUIStyle(optionWithIcon);
				optionWithoutIcon.padding.left += 17;

				optionWithIconDim = new GUIStyle(optionWithIcon);
				optionWithIconDim.normal.textColor = ColorPalette.unityForegroundDim;

				optionWithoutIconDim = new GUIStyle(optionWithIcon);
				optionWithoutIconDim.normal.textColor = ColorPalette.unityForegroundDim;

				background = new GUIStyle("grey_border");

				rightArrow = new GUIStyle("AC RightArrow");

				leftArrow = new GUIStyle("AC LeftArrow");

				searchField = new GUIStyle("SearchTextField");

				searchFieldCancelButton = new GUIStyle("SearchCancelButton");

				searchFieldCancelButtonEmpty = new GUIStyle("SearchCancelButtonEmpty");

				searchNotFound = new GUIStyle(EditorStyles.centeredGreyMiniLabel);
				searchNotFound.wordWrap = true;
				searchNotFound.padding = new RectOffset(10, 10, 10, 10);
				searchNotFound.alignment = TextAnchor.MiddleCenter;

				check = new GUIStyle();
				var checkTexture = LudiqCore.Resources.LoadTexture("Fuzzy/Check.png", new TextureResolution[] { 12, 24 }, CreateTextureOptions.PixelPerfect);
				check.normal.background = checkTexture[12];
				check.normal.scaledBackgrounds = new[] { checkTexture[24] };
				check.fixedHeight = 12;
				check.fixedWidth = 12;

				searchIcon = LudiqCore.Resources.LoadIcon("Fuzzy/Search.png");

				star = new GUIStyle();
				var starOffTexture = LudiqCore.Resources.LoadIcon("Fuzzy/StarOff.png");
				var starOnTexture = LudiqCore.Resources.LoadIcon("Fuzzy/StarOn.png");
				star.normal.background = starOffTexture[16];
				star.normal.scaledBackgrounds = new[] { starOffTexture[32] };
				star.onNormal.background = starOnTexture[16];
				star.onNormal.scaledBackgrounds = new[] { starOnTexture[32] };
				star.fixedHeight = 16;
				star.fixedWidth = 16;
				favoritesIcon = starOnTexture;
			}

			public static readonly GUIStyle headerBackground;
			public static readonly GUIStyle headerBreadcrumbRoot;
			public static readonly GUIStyle headerBreadcrumbElement;
			public static readonly GUIStyle headerBreadcrumbRootCurrent;
			public static readonly GUIStyle headerBreadcrumbElementCurrent;
			public static readonly GUIStyle footerBackground;
			public static readonly GUIStyle optionWithIcon;
			public static readonly GUIStyle optionWithoutIcon;
			public static readonly GUIStyle optionWithIconDim;
			public static readonly GUIStyle optionWithoutIconDim;
			public static readonly GUIStyle background;
			public static readonly GUIStyle rightArrow;
			public static readonly GUIStyle leftArrow;
			public static readonly GUIStyle searchField;
			public static readonly GUIStyle searchFieldCancelButton;
			public static readonly GUIStyle searchFieldCancelButtonEmpty;
			public static readonly GUIStyle searchNotFound;
			public static readonly GUIStyle check;
			public static readonly GUIStyle star;
			public static readonly EditorTexture searchIcon;
			public static readonly EditorTexture favoritesIcon;
			public static readonly float searchFieldInnerHeight = 20;
			public static readonly float searchFieldOuterHeight = 32;
			public static readonly float headerHeight = 25;
			public static readonly float optionHeight = 20;
			public static readonly float maxOptionWidth = 800;

			public static void Initialize() { }
		}

		public class Root : FuzzyOption<object>
		{
			public Root(GUIContent header) : base(FuzzyOptionMode.Branch)
			{
				label = header.text;
				icon = EditorTexture.Single(header.image);
			}
		}

		public class SearchOption : FuzzyOption<object>
		{
			public SearchOption(string query) : base(FuzzyOptionMode.Branch)
			{
				this.query = query;
				label = string.Format(searchHeaderFormat, query);
			}

			public string query { get; private set; }

			public override EditorTexture Icon()
			{
				return Styles.searchIcon;
			}
		}

		public class FavoritesRoot : FuzzyOption<object>
		{
			public FavoritesRoot() : base(FuzzyOptionMode.Branch)
			{
				label = "Favorites";
			}

			public override EditorTexture Icon()
			{
				return Styles.favoritesIcon;
			}
		}

		#region Lifecycle

		private Action<IFuzzyOption> callback;

		public static FuzzyWindow instance { get; private set; }

		private IFuzzyOptionTree tree;

		public static void Show(Rect activatorPosition, IFuzzyOptionTree optionTree, Action<IFuzzyOption> callback)
		{
			Ensure.That(nameof(optionTree)).IsNotNull(optionTree);

			// Makes sure control exits DelayedTextFields before opening the window
			GUIUtility.keyboardControl = 0;

			if (instance == null)
			{
				instance = CreateInstance<FuzzyWindow>();
			}
			else
			{
				throw new InvalidOperationException("Cannot open two instances of the fuzzy window at once.");
			}

			instance.Initialize(activatorPosition, optionTree, callback);
		}

		private void OnEnable()
		{
			instance = this;
			query = string.Empty;
		}

		private void OnDisable()
		{
			instance = null;
		}

		private void Update()
		{
			if (activeParent.isPopulated)
			{
				if (activeParent.isLoading && (animTarget == 0 || prevAnim == 1))
				{
					activeParent.isLoading = false;
				}
			}
			else
			{
				activeParent.isLoading = true;
			}

			if (requireRepaint)
			{
				Repaint();
				requireRepaint = false;
			}
		}

		private bool requireRepaint;

		private void Initialize(Rect activatorPosition, IFuzzyOptionTree optionTree, Action<IFuzzyOption> callback)
		{
			tree = optionTree;

			// Port the activator position to screen space
			
			activatorPosition.position = GUIUtility.GUIToScreenPoint(activatorPosition.position);
			this.activatorPosition = activatorPosition;

			// Create the hierarchy

			stack = new List<FuzzyOptionNode>();
			root = new FuzzyOptionNode(new Root(optionTree.header));
			favoritesRoot = new FuzzyOptionNode(new FavoritesRoot());
			stack.Add(root);

			Styles.Initialize();
			
			ExecuteTask(() =>
			{
				optionTree.Prewarm();

				Populate(root, optionTree.Root());

				UpdateFavorites();

				// Fit height to children if there is no depth and no search

				var hasSubChildren = root.children.Any(option => option.hasChildren);

				if (!optionTree.searchable && !hasSubChildren)
				{
					var height = 0f;

					if (!string.IsNullOrEmpty(root.option.headerLabel))
					{
						height += Styles.headerHeight;
					}

					height += root.children.Count * Styles.optionHeight + 1;

					this.height = height;
				}
			});


			// Setup the search

			Search();

			// Assign the callback

			this.callback = callback;

			// Show and focus the window
			
			wantsMouseMove = true;
			var initialSize = new Vector2(activatorPosition.width, height);
			this.ShowAsDropDown(activatorPosition, initialSize);
			Focus();
		}

		#endregion

		#region Hierarchy

		private FuzzyOptionNode root;
		private List<FuzzyOptionNode> stack;

		private FuzzyOptionNode activeParent => stack[stack.Count - 1];

		private int activeSelectedIndex
		{
			get
			{
				return activeParent.selectedIndex;
			}
			set
			{
				lock (guiLock)
				{
					activeParent.selectedIndex = value;
				}
			}
		}

		private IList<FuzzyOptionNode> activeNodes => activeParent.children;

		private FuzzyOptionNode activeNode
		{
			get
			{
				if (activeSelectedIndex >= 0 && activeSelectedIndex < activeNodes.Count)
				{
					return activeNodes[activeSelectedIndex];
				}
				else
				{
					return null;
				}
			}
		}

		private void SelectParent()
		{
			if (stack.Count > 1)
			{
				SelectAncestor(stack[stack.Count - 2]);
			}
		}

		private void SelectAncestor(FuzzyOptionNode node)
		{
			if (stack.Contains(node))
			{
				animTarget = 0;
				animAncestor = node;
				lastRepaintTime = DateTime.UtcNow;

				query = node.option is SearchOption searchOption ? searchOption.query : string.Empty;
				delayedQuery = null;
				GUIUtility.keyboardControl = 0;
				letQueryClear = true;
			}
		}

		private void EnterChild(FuzzyOptionNode node)
		{
			if (node == null || !node.hasChildren)
			{
				return;
			}

			ExecuteTask(() =>
			{
				Populate(node, tree.Children(node.option));
			});

			lastRepaintTime = DateTime.UtcNow;

			query = string.Empty;
			delayedQuery = null;
			GUIUtility.keyboardControl = 0;
			letQueryClear = true;

			if (animTarget == 0)
			{
				animTarget = 1;
				animAncestor = null;
			}
			else if (anim == 1)
			{
				anim = 0;
				prevAnim = 0;
				stack.Add(node);
			}
		}

		private void SelectChild(FuzzyOptionNode node)
		{
			if (node == null)
			{
				return;
			}

			if (node.hasChildren)
			{
				EnterChild(node);
			}
			else if (callback != null)
			{
				callback(node.option);
			}
		}

		#endregion

		#region Search

		private string query;
		private string delayedQuery;
		private bool letQueryClear;
		private string searchFieldName = "FuzzySearch";
		private static string searchHeaderFormat = "\"{0}\"";
		private static string searchNotFoundFormat = "No results found for \"{0}\".";
		private CancellationTokenSource searchCancellationTokenSource;

		private bool hasSearch => !string.IsNullOrEmpty(query);

		private void Search()
		{
			while (stack.Count > 0 && stack[stack.Count - 1].option is SearchOption)
			{
				stack.RemoveAt(stack.Count - 1);
			}

			if (hasSearch)
			{
				searchCancellationTokenSource?.Cancel();

				var query = this.query;

				var parent = (stack.Count > 1 ? stack[stack.Count - 1] : null);
				var searchNode = new FuzzyOptionNode(new SearchOption(query));

				searchCancellationTokenSource = new CancellationTokenSource();
				var searchCancellationToken = searchCancellationTokenSource.Token;

				ExecuteTask(() =>
				{
					DisplayProgressBar($"Searching for \"{query}\"...", 0);
						
					if (searchCancellationToken.IsCancellationRequested)
					{
						return;
					}

					lock (guiLock)
					{
						searchNode.children.Clear();
						searchNode.isPopulated = false;
					}

					Populate(searchNode, tree.OrderedSearchResults(query, parent?.option, searchCancellationToken).Take(LudiqCore.Configuration.maxSearchResults));
					activeSelectedIndex = activeNodes.Count >= 1 ? 0 : -1;
				});				

				stack.Add(searchNode);
			}
			else
			{				
				animTarget = 1;
				animAncestor = null;
				lastRepaintTime = DateTime.UtcNow;
			}
		}

		#endregion

		#region Favorites

		private FuzzyOptionNode favoritesRoot;

		private void UpdateFavorites()
		{
			if (tree.favorites != null)
			{
				DisplayProgressBar("Fetching favorites...", 0);
				favoritesRoot.children.Clear();
				favoritesRoot.isPopulated = false;
				// Adding a where clause in case a favorited item was later changed to be unfavoritable.
				Populate(favoritesRoot, tree.favorites.Where(favorite => tree.CanFavorite(favorite)));
			}
			else
			{
				favoritesRoot.children.Clear();
				favoritesRoot.isPopulated = true;
			}

			root.children.Remove(favoritesRoot);

			if (favoritesRoot.hasChildren)
			{
				root.children.Insert(0, favoritesRoot);
			}
		}

		#endregion

		#region Animation

		private float anim = 1;
		private float prevAnim = 1;
		private int animTarget = 1;
		private FuzzyOptionNode animAncestor = null;
		private float animationSpeed = 4;

		private DateTime lastRepaintTime;

		public float repaintDeltaTime => (float)(DateTime.UtcNow - lastRepaintTime).TotalSeconds;

		private bool isAnimating => anim != animTarget;

		#endregion

		#region Positioning

		private static readonly MethodInfo ShowAsDropDownFitToScreen;

		private float maxHeight = 320;
		private float height = 320;
		private float minWidth = 200;
		private float minOptionWidth;
		private float headerWidth;
		private float footerHeight;
		private Rect activatorPosition;
		private bool scrollToSelected;
		private float initialY;
		private bool initialYSet;
		private float totalWidth;
		private float totalHeight;

		private void OnPositioning()
		{
			lock (guiLock)
			{
				if (!initialYSet)
				{
					initialY = this.position.y;
					initialYSet = true;
				}

				var totalWidth = Mathf.Max(minWidth, activatorPosition.width, minOptionWidth + 36, headerWidth + 36);

				var totalHeight = Mathf.Min(height, maxHeight);

				var position = (Rect)ShowAsDropDownFitToScreen.Invoke(this, new object[] { activatorPosition, new Vector2(totalWidth, totalHeight), null });

				position.y = initialY;

				if (!isAnimating && !activeParent.isLoading && activeNode?.option != null && activeNode.option.hasFooter)
				{
					footerHeight = activeNode.option.GetFooterHeight(activeNode, totalWidth);

					position.height += footerHeight;
				}

				if (Application.platform == RuntimePlatform.OSXEditor && LudiqCore.Configuration.limitFuzzyFinderHeight)
				{
					// OSX disregards the Y entirely if the window is higher than the desktop space
					// and will try to move it up until it fits. Therefore, we'll cut the window down here.
					// However, we can't use the screen resolution, because it doesn't include the dock.
				
					var maxY = LudiqGUIUtility.mainEditorWindowPosition.yMax;

					if (position.yMax > maxY)
					{
						position.height -= (position.yMax - maxY);
					}
				}

				minSize = maxSize = position.size;
				this.position = position;
			}
		}

		#endregion

		#region GUI

		private void OnGUI()
		{
			try
			{
				lock (guiLock)
				{
					GUI.Label(new Rect(0, 0, position.width, position.height), GUIContent.none, Styles.background);

					HandleKeyboard();

					if (tree.searchable)
					{
						LudiqGUI.Space(7);

						if (letQueryClear)
						{
							letQueryClear = false;
						}
						else
						{
							EditorGUI.FocusTextInControl(searchFieldName);
						}

						var searchFieldPosition = GUILayoutUtility.GetRect(10, Styles.searchFieldInnerHeight);
						searchFieldPosition.x += 8;
						searchFieldPosition.width -= 16;

						var newQuery = OnSearchGUI(searchFieldPosition, delayedQuery ?? query);

						if (newQuery != query || delayedQuery != null)
						{
							if (!isAnimating)
							{
								query = delayedQuery ?? newQuery;
								Search();
								delayedQuery = null;
							}
							else
							{
								delayedQuery = newQuery;
							}
						}

						LudiqGUI.Space(5);
					}

					OnLevelGUI(anim);

					prevAnim = anim;

					if (isAnimating && e.type == EventType.Repaint)
					{
						anim = Mathf.MoveTowards(anim, animTarget, repaintDeltaTime * animationSpeed);

						if (animTarget == 0 && anim == 0 && animAncestor != null)
						{
							while (stack.Count > 1 && stack[stack.Count - 1] != animAncestor)
							{
								stack.RemoveAt(stack.Count - 1);
							}

							anim = 1;
							prevAnim = 1;
							animTarget = 1;
							animAncestor = null;
						}

						Repaint();
					}

					if (e.type == EventType.Repaint)
					{
						lastRepaintTime = DateTime.UtcNow;
					}

					if (!activeParent.isLoading)
					{
						if (tree.searchable && hasSearch && !activeParent.hasChildren)
						{
							var searchNotFoundLabel = new GUIContent(string.Format(searchNotFoundFormat, query));

							minOptionWidth = Styles.searchNotFound.CalcSize(searchNotFoundLabel).x;

							EditorGUI.LabelField
							(
								GUILayoutUtility.GetRect(GUIContent.none, GUIStyle.none, GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true)),
								searchNotFoundLabel,
								Styles.searchNotFound
							);
						}

						if (activeNode != null && activeNode.option.hasFooter)
						{
							OnFooterGUI();
						}
					}

					OnPositioning();
				}
			}
			catch (ArgumentException ex)
			{
				if (tree.multithreaded && ex.Message.StartsWith("Getting control "))
				{
					// A bunch of happens that might affect the GUI could happen on a 
					// secondary thread, leading to Unity complaining about the amount
					// of controls changing between the draw call and the layout call.
					// Because these are hamless and last just one frame, we can safely
					// ignore them and repaint right away.
					requireRepaint = true;
				}
				else
				{
					throw;
				}
			}
		}

		private string OnSearchGUI(Rect position, string query)
		{
			var fieldPosition = position;
			fieldPosition.width -= 15;

			var cancelButtonPosition = position;
			cancelButtonPosition.x += position.width - 15;
			cancelButtonPosition.width = 15;
			
			GUI.SetNextControlName(searchFieldName);
			query = EditorGUI.TextField(fieldPosition, query, Styles.searchField);

			if (GUI.Button(cancelButtonPosition, GUIContent.none, string.IsNullOrEmpty(query) ? Styles.searchFieldCancelButtonEmpty : Styles.searchFieldCancelButton) && query != string.Empty)
			{
				query = string.Empty;
				GUIUtility.keyboardControl = 0;
				letQueryClear = true;
			}

			return query;
		}

		private void OnHeaderGUI(float anim)
		{	
			if (e.type == EventType.Layout || e.type == EventType.Repaint)
			{
				headerWidth = 0;
			}

			var previousIconSize = EditorGUIUtility.GetIconSize();
			EditorGUIUtility.SetIconSize(new Vector2(IconSize.Small, IconSize.Small));

			LudiqGUI.BeginHorizontal(Styles.headerBackground);

			foreach (var node in stack)
			{
				node.EnsureDrawable();

				var content = new GUIContent(node.option.headerLabel, node.option.showHeaderIcon ? node.icon?[IconSize.Small] : null);
				var isCurrent = node == stack[stack.Count - 1];

				var style = node == stack[0] ? Styles.headerBreadcrumbRoot : Styles.headerBreadcrumbElement;
				if (isCurrent)
				{
					style = node == stack[0] ? Styles.headerBreadcrumbRootCurrent : Styles.headerBreadcrumbElementCurrent;
				}
				var width = style.CalcSize(content).x;

				if (GUILayout.Toggle(isCurrent, content, style) && !isCurrent)
				{
					SelectAncestor(node);
				}

				if (e.type == EventType.Layout || e.type == EventType.Repaint)
				{
					headerWidth += width;
				}
			}

			LudiqGUI.FlexibleSpace();
			LudiqGUI.EndHorizontal();

			EditorGUIUtility.SetIconSize(previousIconSize);
		}

		private void OnLevelGUI(float anim)
		{
			var parent = stack[stack.Count - 1];

			anim = Mathf.Floor(anim) + Mathf.SmoothStep(0, 1, Mathf.Repeat(anim, 1));

			if (stack.Count > 1 || !string.IsNullOrEmpty(parent.option.headerLabel))
			{
				OnHeaderGUI(anim);
			}

			if (e.type == EventType.Layout || e.type == EventType.Repaint)
			{
				minOptionWidth = 0;
			}

			OnOptionsGUI(anim, parent);

			if (anim < 1)
			{
				FuzzyOptionNode grandparent = null;

				if (animTarget == 0 && animAncestor != null)
				{
					grandparent = animAncestor;
				}
				else if (stack.Count > 1)
				{
					grandparent = stack[stack.Count - 2];
				}

				OnOptionsGUI(anim + 1, grandparent);
			}
		}

		private Vector2 lastMouseMovePosition;

		private void OnOptionsGUI(float anim, FuzzyOptionNode parent)
		{
			var levelPosition = new Rect
			(
				position.width * (1 - anim) + 1,
				(tree.searchable ? Styles.searchFieldOuterHeight : 0) + Styles.headerHeight,
				position.width - 2,
				height - ((tree.searchable ? Styles.searchFieldOuterHeight : 0) + 1) - Styles.headerHeight
			);

			if (e.type == EventType.MouseDown && e.button == (int)MouseButton.Right && levelPosition.Contains(e.mousePosition))
			{
				SelectParent();
				e.Use();
			}

			GUILayout.BeginArea(levelPosition);

			if (parent.isLoading)
			{
				LudiqGUI.BeginVertical();
				LudiqGUI.FlexibleSpace();

				LudiqGUI.BeginHorizontal();
				LudiqGUI.FlexibleSpace();
				LudiqGUI.LoaderLayout();
				LudiqGUI.FlexibleSpace();
				LudiqGUI.EndHorizontal();

				LudiqGUI.Space(16);

				LudiqGUI.BeginHorizontal();
				LudiqGUI.Space(10);
				var progressBarPosition = GUILayoutUtility.GetRect(GUIContent.none, GUIStyle.none, GUILayout.Height(19), GUILayout.ExpandWidth(true));
				if (showProgress)
				{
					EditorGUI.ProgressBar(progressBarPosition, progress, progressText);
				}

				LudiqGUI.Space(10);
				LudiqGUI.EndHorizontal();
				LudiqGUI.Space(LudiqGUI.loaderSize * 1.5f);

				LudiqGUI.FlexibleSpace();
				LudiqGUI.EndVertical();
				Repaint();
			}
			else
			{

				parent.scroll = GUILayout.BeginScrollView(parent.scroll);

				EditorGUIUtility.SetIconSize(new Vector2(IconSize.Small, IconSize.Small));

				var selectedOptionPosition = default(Rect);

				foreach (var node in parent.children)
				{
					node.EnsurePositionable();

					if (e.type == EventType.Layout || e.type == EventType.Repaint)
					{
						minOptionWidth = Mathf.Max(minOptionWidth, Mathf.Min(node.width, Styles.maxOptionWidth));
					}
				}

				for (var i = 0; i < parent.children.Count; i++)
				{
					var node = parent.children[i];

					var optionPosition = GUILayoutUtility.GetRect(IconSize.Small, Styles.optionHeight, GUILayout.ExpandWidth(true));

					if (((e.type == EventType.MouseMove && GUIUtility.GUIToScreenPoint(e.mousePosition) != lastMouseMovePosition) || e.type == EventType.MouseDown) &&
						parent.selectedIndex != i &&
						optionPosition.Contains(e.mousePosition))
					{
						parent.selectedIndex = i;
						Repaint();
						lastMouseMovePosition = GUIUtility.GUIToScreenPoint(e.mousePosition);
					}

					var optionIsSelected = false;

					if (i == parent.selectedIndex)
					{
						optionIsSelected = true;
						selectedOptionPosition = optionPosition;
					}

					// Clipping
					if (optionPosition.yMax < parent.scroll.y || optionPosition.yMin > parent.scroll.y + levelPosition.height)
					{
						continue;
					}

					node.EnsureDrawable();

					if (e.type == EventType.Repaint)
					{
						node.style.Draw(optionPosition, node.label, false, false, optionIsSelected, optionIsSelected);
					}

					var right = optionPosition.xMax;

					if (node.hasChildren)
					{
						right -= 13;
						var rightArrowPosition = new Rect(right, optionPosition.y + 4, 13, 13);

						if (e.type == EventType.Repaint)
						{
							Styles.rightArrow.Draw(rightArrowPosition, false, false, false, false);
						}
					}

					if (!node.hasChildren && tree.selected.Contains(node.option.value))
					{
						right -= 16;
						var checkPosition = new Rect(right, optionPosition.y + 4, 12, 12);

						if (e.type == EventType.Repaint)
						{
							Styles.check.Draw(checkPosition, false, false, false, false);
						}
					}

					if (tree.favorites != null && tree.CanFavorite(node.option) && (optionIsSelected || tree.favorites.Contains(node.option)))
					{
						right -= 19;
						var starPosition = new Rect(right, optionPosition.y + 2, IconSize.Small, IconSize.Small);

						EditorGUI.BeginChangeCheck();

						var isFavorite = tree.favorites.Contains(node.option);

						isFavorite = GUI.Toggle(starPosition, isFavorite, GUIContent.none, Styles.star);

						if (EditorGUI.EndChangeCheck())
						{
							if (isFavorite)
							{
								tree.favorites.Add(node.option);
							}
							else
							{
								tree.favorites.Remove(node.option);
							}

							tree.OnFavoritesChange();

							ExecuteTask(() => UpdateFavorites());							
						}
					}

					if (e.type == EventType.MouseDown && e.button == (int)MouseButton.Left && optionPosition.Contains(e.mousePosition))
					{
						e.Use();
						parent.selectedIndex = i;
						SelectChild(node);
					}
				}

				EditorGUIUtility.SetIconSize(default(Vector2));

				GUILayout.EndScrollView();

				if (scrollToSelected && e.type == EventType.Repaint)
				{
					scrollToSelected = false;

					var lastRect = GUILayoutUtility.GetLastRect();

					if (selectedOptionPosition.yMax - lastRect.height > parent.scroll.y)
					{
						var scroll = parent.scroll;
						scroll.y = selectedOptionPosition.yMax - lastRect.height;
						parent.scroll = scroll;
						Repaint();
					}

					if (selectedOptionPosition.y < parent.scroll.y)
					{
						var scroll = parent.scroll;
						scroll.y = selectedOptionPosition.y;
						parent.scroll = scroll;
						Repaint();
					}
				}
			}

			GUILayout.EndArea();
		}

		private void OnFooterGUI()
		{
			var footerPosition = new Rect
			(
				1,
				height - 1,
				position.width - 2,
				footerHeight
			);

			var backgroundPosition = footerPosition;
			backgroundPosition.height += 1;

			if (e.type == EventType.Repaint)
			{
				Styles.footerBackground.Draw(backgroundPosition, false, false, false, false);

				activeNode.option.OnFooterGUI(activeNode, footerPosition);
			}
		}

		private void HandleKeyboard()
		{
			if (e.type == EventType.KeyDown)
			{
				if (e.keyCode == KeyCode.DownArrow)
				{
					activeSelectedIndex = Mathf.Clamp(activeSelectedIndex + 1, 0, activeNodes.Count - 1);
					scrollToSelected = true;
					e.Use();
				}
				else if (e.keyCode == KeyCode.UpArrow)
				{
					activeSelectedIndex = Mathf.Clamp(activeSelectedIndex - 1, 0, activeNodes.Count);
					scrollToSelected = true;
					e.Use();
				}
				else if ((e.keyCode == KeyCode.Return || e.keyCode == KeyCode.KeypadEnter) && !activeParent.isLoading)
				{
					SelectChild(activeNode);
					e.Use();
				}
				else if ((e.keyCode == KeyCode.LeftArrow || e.keyCode == KeyCode.Backspace) && !hasSearch && activeParent != root)
				{
					SelectParent();
					e.Use();
				}
				else if (e.keyCode == KeyCode.RightArrow && !activeParent.isLoading)
				{
					EnterChild(activeNode);
					e.Use();
				}
				else if (e.keyCode == KeyCode.Escape)
				{
					Close();
					e.Use();
				}
			}
		}

		#endregion

		#region Threading

		private readonly object guiLock = new object(); 

		private void ExecuteTask(Action task)
		{
			Ensure.That(nameof(task)).IsNotNull(task);

			if (tree.multithreaded)
			{
				lock (workerLock)
				{
					queue.Enqueue(task);

					if (workerThread == null)
					{
						workerThread = new Thread(Work);
						workerThread.Name = "Fuzzy Window";
						workerThread.Start();
					}
				}
			}
			else
			{
				RunTaskSynchronous(task);
			}
		}

		private readonly Queue<Action> queue = new Queue<Action>();
		private Thread workerThread;

		private object workerLock = new object();

		private static object taskLock = new object();

		private void RunTaskSynchronous(Action task)
		{
			lock (taskLock)
			{
				try
				{
					task();
				}
				catch (OperationCanceledException) { }
				catch (Exception ex)
				{
					Debug.LogException(ex);
				}
				finally
				{
					ClearProgressBar();
					requireRepaint = true;
				}
			}
		}

		private void Work()
		{
			while (true)
			{
				Action task = null;

				lock (workerLock)
				{
					if (queue.Count > 0)
					{
						task = queue.Dequeue();
					}
					else
					{
						workerThread = null;
						return;
					}
				}

				RunTaskSynchronous(task);
			}
		}

		private string progressText;
		private float progress;
		private bool showProgress;

		public void DisplayProgressBar(string text, float progress)
		{
			progressText = text;
			this.progress = progress;
			showProgress = true;
		}

		public void DisplayProgressBar(float progress)
		{
			DisplayProgressBar(null, progress);
		}

		public static void ClearProgressBar()
		{
			if (instance == null)
			{
				return;
			}

			instance.progressText = null;
			instance.progress = 0;
			instance.showProgress = false;
		}

		#endregion
	}
}