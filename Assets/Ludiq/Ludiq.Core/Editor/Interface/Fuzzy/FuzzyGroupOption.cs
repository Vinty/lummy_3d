﻿namespace Ludiq
{
	[FuzzyOption(typeof(FuzzyGroup))]
	public class FuzzyGroupOption : FuzzyOption<object>
	{
		public FuzzyGroupOption(FuzzyGroup group) : base(FuzzyOptionMode.Branch)
		{
			value = group;
			label = group.label;
			icon = group.icon;
		}
	}
}