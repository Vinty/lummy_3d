﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ludiq
{
	public interface IFuzzyOptionCategory
	{
		string fullName { get; }
	}
}
