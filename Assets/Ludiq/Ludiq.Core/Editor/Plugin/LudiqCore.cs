using System.Collections.Generic;

namespace Ludiq
{
	[Plugin(ID)]
	[Product(LudiqProduct.ID)]
	[PluginRuntimeAssembly(ID + ".Runtime")]
	public class LudiqCore : Plugin
	{
		public LudiqCore() : base()
		{
			instance = this;
		}

		public static LudiqCore instance { get; private set; }

		public override IEnumerable<Page> SetupWizardPages()
		{
			yield return new NamingSchemePage();
		}

		public const string ID = "Ludiq.Core";

		public static LudiqCoreManifest Manifest => (LudiqCoreManifest)instance.manifest;
		public static LudiqCorePaths Paths => (LudiqCorePaths)instance.paths;
		public static LudiqCoreConfiguration Configuration => (LudiqCoreConfiguration)instance.configuration;
		public static LudiqCoreResources Resources => (LudiqCoreResources)instance.resources;
		public static LudiqCoreResources.Icons Icons => Resources.icons;

		public const string LegacyRuntimeDllGuid = "1eea3bf15bb7ddb4582c462beee0ad13";
		public const string LegacyEditorDllGuid = "8878d90c345be1a43ab0c9a9898ad433";

		public override IEnumerable<ScriptReferenceReplacement> scriptReferenceReplacements
		{
			get
			{
				yield return ScriptReferenceReplacement.FromDll<DictionaryAsset>(LegacyRuntimeDllGuid);
			}
		}
	}
}