﻿using System;
using UnityEditor;
using UnityEngine;

namespace Ludiq
{
	public abstract class ContinuousNumberInspector<T> : Inspector
	{
		private RangeAttribute rangeAttribute;

		protected ContinuousNumberInspector(Metadata metadata) : base(metadata)
		{
			rangeAttribute = metadata.GetAttribute<RangeAttribute>();
		}

		protected override void OnGUI(Rect position, GUIContent label)
		{
			position = BeginBlock(metadata, position, label);

			T newValue;
			var oldValue = Convert.ToSingle(metadata.value);

			var fieldPosition = new Rect
			(
				position.x,
				position.y,
				position.width,
				EditorGUIUtility.singleLineHeight
			);

			if (rangeAttribute != null)
			{
				newValue = (T)Convert.ChangeType(EditorGUI.Slider(fieldPosition, oldValue, rangeAttribute.min, rangeAttribute.max), typeof(T));
			}
			else
			{
				newValue = (T)Convert.ChangeType(LudiqGUI.DraggableFloatField(fieldPosition, oldValue), typeof(T));
			}

			if (EndBlock(metadata))
			{
				metadata.RecordUndo();
				metadata.value = newValue;
			}
		}

		protected override float GetHeight(float width, GUIContent label)
		{
			return HeightWithLabel(metadata, width, EditorGUIUtility.singleLineHeight, label);
		}

		public override float GetAdaptiveWidth()
		{
			if (rangeAttribute != null)
			{
				return 100;
			}
			else
			{
				return LudiqGUI.GetTextFieldAdaptiveWidth(metadata.value);
			}
		}
	}
}