﻿using UnityEditor;
using UnityEngine;

namespace Ludiq
{
	[Inspector(typeof(string))]
	public class StringInspector : Inspector
	{
		private readonly InspectorTextAreaAttribute textAreaAttribute;
		private readonly InspectorDelayedAttribute delayedAttribute;

		public StringInspector(Metadata metadata) : base(metadata)
		{
			textAreaAttribute = metadata.GetAttribute<InspectorTextAreaAttribute>();
			delayedAttribute = metadata.GetAttribute<InspectorDelayedAttribute>();
		}

		protected override bool cacheHeight => textAreaAttribute != null;

		private float GetFieldHeight(float width, GUIContent label)
		{
			if (textAreaAttribute != null)
			{
				var height = LudiqStyles.textAreaWordWrapped.CalcHeight(new GUIContent((string)metadata.value), WidthWithoutLabel(metadata, width, label));

				if (textAreaAttribute.hasMinLines)
				{
					var minHeight = EditorStyles.textArea.lineHeight * textAreaAttribute.minLines + EditorStyles.textArea.padding.top + EditorStyles.textArea.padding.bottom;

					height = Mathf.Max(height, minHeight);
				}

				if (textAreaAttribute.hasMaxLines)
				{
					var maxHeight = EditorStyles.textArea.lineHeight * textAreaAttribute.maxLines + EditorStyles.textArea.padding.top + EditorStyles.textArea.padding.bottom;

					height = Mathf.Min(height, maxHeight);
				}

				return height;
			}
			else
			{
				return EditorGUIUtility.singleLineHeight;
			}
		}

		protected override float GetHeight(float width, GUIContent label)
		{
			return HeightWithLabel(metadata, width, GetFieldHeight(width, label), label);
		}

		protected override void OnGUI(Rect position, GUIContent label)
		{
			position = BeginBlock(metadata, position, label);

			var fieldPosition = position.VerticalSection(ref y, GetFieldHeight(position.width, GUIContent.none));

			string newValue;

			if (textAreaAttribute != null)
			{
				newValue = EditorGUI.TextArea(fieldPosition, (string)metadata.value, EditorStyles.textArea);
			}
			else if (delayedAttribute != null)
			{
				newValue = EditorGUI.DelayedTextField(fieldPosition, (string)metadata.value, EditorStyles.textField);
			}
			else
			{
				newValue = EditorGUI.TextField(fieldPosition, (string)metadata.value, EditorStyles.textField);
			}

			if (EndBlock(metadata))
			{
				metadata.RecordUndo();
				metadata.value = newValue;
			}
		}

		public override float GetAdaptiveWidth()
		{
			return LudiqGUI.GetTextFieldAdaptiveWidth(metadata.value);
		}
	}
}