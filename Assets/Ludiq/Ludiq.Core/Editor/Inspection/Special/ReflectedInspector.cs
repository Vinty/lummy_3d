﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace Ludiq
{
	public class ReflectedInspector : Inspector
	{
		public ReflectedInspector(Metadata metadata) : base(metadata) { }

		public override void Initialize()
		{
			base.Initialize();

			bindingFlags = MemberMetadata.DefaultBindingFlags;

			metadata.valueTypeChanged += previousType => ReflectMetadata();
		}

		public BindingFlags bindingFlags { get; private set; }

		private float _adaptiveWidth;

		protected virtual bool Include(MemberInfo m)
		{
			return m.HasAttribute<InspectableAttribute>() || m.HasAttribute<InspectableIfAttribute>();
		}

		private readonly List<string> inspectedMemberNames = new List<string>();

		protected IEnumerable<MemberMetadata> inspectedMembers
		{
			get
			{
				return inspectedMemberNames.Select(name => metadata.Member(name, bindingFlags));
			}
		}

		public virtual void ReflectMetadata()
		{
			var adaptiveWidthAttribute = metadata.valueType.GetAttribute<InspectorAdaptiveWidthAttribute>();
			_adaptiveWidth = adaptiveWidthAttribute?.width ?? 200;

			inspectedMemberNames.Clear();

			inspectedMemberNames.AddRange(metadata.valueType
			                               .GetMembers(bindingFlags)
			                               .Where(Include)
			                               .Select(mi => mi.ToManipulator())
			                               .Where(m => m.isAccessor)
			                               .OrderBy(m => m.info.GetAttribute<InspectableAttribute>()?.order ?? int.MaxValue)
			                               .ThenBy(m => m.info.MetadataToken)
			                               .Select(m => m.name));

			SetHeightDirty();
		}

		public virtual bool Display(MemberMetadata member)
		{
			var conditionalAttribute = member.GetAttribute<InspectableIfAttribute>();

			if (conditionalAttribute != null)
			{
				return AttributeUtility.CheckCondition(metadata.valueType, metadata.value, conditionalAttribute.conditionMemberName, false);
			}

			return true;
		}

		protected override float GetHeight(float width, GUIContent label)
		{
			var height = 0f;

			var addedSpace = false;

			foreach (var member in inspectedMembers)
			{
				if (!Display(member))
				{
					continue;
				}

				height += GetMemberHeight(member, width);
				height += Styles.spaceBetweenMembers;

				addedSpace = true;
			}

			if (addedSpace)
			{
				height -= Styles.spaceBetweenMembers;
			}

			return height;
		}

		protected override void OnGUI(Rect position, GUIContent label)
		{
			BeginBlock(metadata, position, label);

			foreach (var member in inspectedMembers)
			{
				if (!Display(member))
				{
					continue;
				}

				var memberPosition = position.VerticalSection(ref y, GetMemberHeight(member, position.width));

				OnMemberGUI(member, memberPosition);

				y += Styles.spaceBetweenMembers;
			}

			EndBlock(metadata);
		}

		protected virtual float GetMemberHeight(MemberMetadata member, float width)
		{
			return LudiqGUI.GetInspectorHeight(this, member, width);
		}

		protected virtual void OnMemberGUI(MemberMetadata member, Rect memberPosition)
		{
			EditorGUI.BeginChangeCheck();

			LudiqGUI.Inspector(member, memberPosition);

			if (EditorGUI.EndChangeCheck())
			{
				OnMemberChange(member);
			}
		}

		protected virtual void OnMemberChange(MemberMetadata member) { }

		public override float GetAdaptiveWidth()
		{
			return _adaptiveWidth;
		}

		public static class Styles
		{
			public static readonly float spaceBetweenMembers = EditorGUIUtility.standardVerticalSpacing;
		}
	}
}