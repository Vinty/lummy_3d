using UnityEngine;

namespace Ludiq
{
	public class DictionaryInspector : Inspector
	{
		public DictionaryInspector(Metadata metadata) : base(metadata)
		{
			adaptor = new MetadataDictionaryAdaptor(metadata, this);
		}

		protected override bool cacheHeight => false;

		protected MetadataDictionaryAdaptor adaptor { get; private set; }

		protected override float GetHeight(float width, GUIContent label)
		{
			return adaptor.GetHeight(width, label);
		}

		protected override void OnGUI(Rect position, GUIContent label)
		{
			adaptor.Field(position, label);
		}

		public override float GetAdaptiveWidth()
		{
			return adaptor.GetAdaptiveWidth();
		}
	}
}