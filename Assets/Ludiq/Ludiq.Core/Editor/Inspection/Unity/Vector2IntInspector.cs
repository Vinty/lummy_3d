﻿using UnityEditor;
using UnityEngine;

namespace Ludiq
{
	[Inspector(typeof(Vector2Int))]
	public class Vector2IntInspector : VectorInspector
	{
		public Vector2IntInspector(Metadata metadata) : base(metadata) { }

		protected override void OnGUI(Rect position, GUIContent label)
		{
			position = BeginBlock(metadata, position, label);

			Vector2Int newValue;
			
			if (adaptiveWidth)
			{
				newValue = LudiqGUI.AdaptiveVector2IntField(position, GUIContent.none, (Vector2Int)metadata.value);
			}
			else if (position.width <= Styles.compactThreshold)
			{
				newValue = LudiqGUI.CompactVector2IntField(position, GUIContent.none, (Vector2Int)metadata.value);
			}
			else
			{
				newValue = EditorGUI.Vector2IntField(position, GUIContent.none, (Vector2Int)metadata.value);
			}

			if (EndBlock(metadata))
			{
				metadata.RecordUndo();
				metadata.value = newValue;
			}
		}

		public override float GetAdaptiveWidth()
		{
			var vector = (Vector2Int)metadata.value;

			return LudiqGUI.GetTextFieldAdaptiveWidth(vector.x) + LudiqStyles.compactHorizontalSpacing +
				   LudiqGUI.GetTextFieldAdaptiveWidth(vector.y) + LudiqStyles.compactHorizontalSpacing;
		}
	}
}