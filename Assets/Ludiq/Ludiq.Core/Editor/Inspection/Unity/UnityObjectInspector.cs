﻿using UnityEditor;
using UnityEngine;
using UnityObject = UnityEngine.Object;

namespace Ludiq
{
	public sealed class UnityObjectInspector : Inspector
	{
		public UnityObjectInspector(Metadata metadata) : base(metadata) { }

		public override void Initialize()
		{
			base.Initialize();

			nullMeansSelf = ComponentHolderProtocol.IsComponentHolderType(metadata.definedType) && metadata.AncestorHasAttribute<NullMeansSelfAttribute>();
		}

		private bool nullMeansSelf;

		protected override float GetHeight(float width, GUIContent label)
		{
			return HeightWithLabel(metadata, width, EditorGUIUtility.singleLineHeight, label);
		}

		protected override void OnGUI(Rect position, GUIContent label)
		{
			position = BeginBlock(metadata, position, label);

			var oldValue = (UnityObject)metadata.value;
			UnityObject newValue;

			var allowSceneObjects = LudiqEditorUtility.editedObject.value.AsUnityNull()?.IsSceneBound() ?? false;

			if (adaptiveWidth && metadata.value.IsUnityNull())
			{
				var targetPosition = new Rect
				(
					position.x,
					position.y,
					position.width,
					EditorGUIUtility.singleLineHeight
				);

				GUI.BeginClip(targetPosition);

				var fieldPadding = 100;

				var fieldPosition = new Rect
				(
					-fieldPadding + 3, // ?
					0,
					fieldPadding + targetPosition.width,
					targetPosition.height
				);
				
				newValue = EditorGUI.ObjectField(fieldPosition, oldValue, metadata.definedType, allowSceneObjects);

				GUI.EndClip();
			}
			else
			{
				var fieldPosition = new Rect
				(
					position.x,
					position.y,
					position.width,
					EditorGUIUtility.singleLineHeight
				);

				newValue = EditorGUI.ObjectField(fieldPosition, oldValue, metadata.definedType, allowSceneObjects);
			}

			if (EndBlock(metadata))
			{
				metadata.RecordUndo();
				metadata.value = newValue;
			}
		}

		public override float GetAdaptiveWidth()
		{
			string label;
			bool icon = false;

			if (metadata.value.IsUnityNull())
			{
				return Styles.targetSize;
			}
			else
			{
				label = ((UnityObject)metadata.value).name;
				icon = true;
			}

			var width = EditorStyles.objectField.CalcSize(new GUIContent(label)).x;

			if (icon)
			{
				width += 15;
			}

			return width;
		}

		public static class Styles
		{
			static Styles()
			{
				selfPatch = new GUIStyle(EditorStyles.label);
				selfPatch.normal.background = ColorPalette.unityBackgroundLight.GetPixel();
				selfPatch.padding = new RectOffset(1, 0, -1, 0);
				selfPatch.margin = new RectOffset(0, 0, 0, 0);
			}

			public static readonly GUIStyle selfPatch;

			public static readonly float targetSize = 7;
		}
	}
}