﻿using System;
using System.Reflection;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Ludiq
{
	[Inspector(typeof(Type))]
	public sealed class TypeInspector : Inspector
	{
		public TypeInspector(Metadata metadata) : base(metadata) { }

		public bool hideRoot => metadata.HasAttribute<InspectorTypeHideRootAttribute>();

		public override void Initialize()
		{
			base.Initialize();

			typeFilter = metadata.GetAttribute<TypeFilter>() ?? TypeFilter.Any;

			metadata.valueChanged += previousValue => typeTree.type = (Type) metadata.value;
		}

		private TypeFilter typeFilter;
		private TypeTree typeTree = new TypeTree();

		private Func<IFuzzyOptionTree> GetOptions(TypeTree tree)
		{
			return () =>
			{
				return new TypeOptionTree(Codebase.types, typeFilter);
			};
		}

		protected override float GetHeight(float width, GUIContent label)
		{
			return HeightWithLabel(metadata, width, EditorGUIUtility.singleLineHeight, label) * Math.Max(typeTree.recursiveNodeCount - (hideRoot ? 1 : 0), 0);
		}

		public override float GetAdaptiveWidth()
		{
			return LudiqGUI.GetTypeFieldAdaptiveWidth((Type)metadata.value) + LudiqStyles.typeTreeIndentation * Math.Max(typeTree.recursiveDepth - (hideRoot ? 1 : 0) - 1, 0);
		}

		protected override void OnGUI(Rect position, GUIContent label)
		{
			position = BeginBlock(metadata, position, label);

			var fieldPosition = new Rect
			(
				position.x,
				position.y,
				position.width,
				EditorGUIUtility.singleLineHeight
			);

			LudiqGUI.TypeTreeField(ref fieldPosition, GUIContent.none, typeTree, !hideRoot, GetOptions);

			if (EndBlock(metadata))
			{
				Type newType = null;

				try
				{
					newType = typeTree.CreateType();
				}
				catch (Exception e)
				{
					Debug.Log(e);
				}

				if (newType != null)
				{
					metadata.RecordUndo();
					metadata.value = newType;
				}
			}
		}
	}
}