﻿using System.Threading;

namespace Ludiq
{
	public interface ITaskRunner
	{
		void Run(Task task);
		void Report(Task task);
		bool runsCurrentThread { get; }
	}
}
