using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using UnityEngine;
using UnityEngine.Analytics;
using UnityObject = UnityEngine.Object;

namespace Ludiq
{
	public static class Codebase
	{
		static Codebase()
		{
			using (ProfilingUtility.SampleBlock("Codebase initialization"))
			{
				_assemblies = new List<Assembly>();
				_runtimeAssemblies = new List<Assembly>();
				_editorAssemblies = new List<Assembly>();
				_ludiqAssemblies = new List<Assembly>();
				_ludiqRuntimeAssemblies = new List<Assembly>();
				_ludiqEditorAssemblies = new List<Assembly>();

				_types = new List<Type>();
				_runtimeTypes = new List<Type>();
				_editorTypes = new List<Type>();
				_ludiqTypes = new List<Type>();
				_ludiqRuntimeTypes = new List<Type>();
				_ludiqEditorTypes = new List<Type>();

				foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
				{
					_assemblies.Add(assembly);

					var isRuntimeAssembly = IsRuntimeAssembly(assembly);
					var isEditorAssembly = IsEditorDependentAssembly(assembly);
					var isLudiqAssembly = IsLudiqRuntimeDependentAssembly(assembly) || IsLudiqEditorDependentAssembly(assembly);
					var isLudiqEditorAssembly = IsLudiqEditorDependentAssembly(assembly);
					var isLudiqRuntimeAssembly = IsLudiqRuntimeDependentAssembly(assembly) && !IsLudiqEditorDependentAssembly(assembly);

					if (isRuntimeAssembly)
					{
						_runtimeAssemblies.Add(assembly);
					}

					if (isEditorAssembly)
					{
						_editorAssemblies.Add(assembly);
					}

					if (isLudiqAssembly)
					{
						_ludiqAssemblies.Add(assembly);
					}

					if (isLudiqEditorAssembly)
					{
						_ludiqEditorAssemblies.Add(assembly);
					}

					if (isLudiqRuntimeAssembly)
					{
						_ludiqRuntimeAssemblies.Add(assembly);
					}
					
					foreach (var type in assembly.GetTypesSafely())
					{
						_types.Add(type);

						RuntimeCodebase.PrewarmTypeDeserialization(type);

						if (isRuntimeAssembly)
						{
							_runtimeTypes.Add(type);
						}

						if (isEditorAssembly)
						{
							_editorTypes.Add(type);
						}

						if (isLudiqAssembly)
						{
							_ludiqTypes.Add(type);
						}

						if (isLudiqEditorAssembly)
						{
							_ludiqEditorTypes.Add(type);
						}

						if (isLudiqRuntimeAssembly)
						{
							_ludiqRuntimeTypes.Add(type);
						}
					}
				}

				assemblies = _assemblies.AsReadOnly();
				runtimeAssemblies = _runtimeAssemblies.AsReadOnly();
				editorAssemblies = _editorAssemblies.AsReadOnly();
				ludiqAssemblies = _ludiqAssemblies.AsReadOnly();
				ludiqRuntimeAssemblies = _ludiqRuntimeAssemblies.AsReadOnly();
				ludiqEditorAssemblies = _ludiqEditorAssemblies.AsReadOnly();

				types = _types.AsReadOnly();
				runtimeTypes = _runtimeTypes.AsReadOnly();
				editorTypes = _editorTypes.AsReadOnly();
				ludiqTypes = _ludiqTypes.AsReadOnly();
				ludiqRuntimeTypes = _ludiqRuntimeTypes.AsReadOnly();
				ludiqEditorTypes = _ludiqEditorTypes.AsReadOnly();
			}
		}

		private static readonly List<Assembly> _assemblies;
		private static readonly List<Assembly> _runtimeAssemblies;
		private static readonly List<Assembly> _editorAssemblies;
		private static readonly List<Assembly> _ludiqAssemblies;
		private static readonly List<Assembly> _ludiqRuntimeAssemblies;
		private static readonly List<Assembly> _ludiqEditorAssemblies;
		private static readonly List<Type> _types;
		private static readonly List<Type> _runtimeTypes;
		private static readonly List<Type> _editorTypes;
		private static readonly List<Type> _ludiqTypes;
		private static readonly List<Type> _ludiqRuntimeTypes;
		private static readonly List<Type> _ludiqEditorTypes;
		
		#region Serialization
		
		public static string SerializeType(Type type)
		{
			return RuntimeCodebase.SerializeType(type);
		}

		public static bool TryDeserializeRootType(string typeName, out Type type)
		{
			return RuntimeCodebase.TryDeserializeRootType(typeName, out type);
		}

		public static Type DeserializeRootType(string typeName)
		{
			return RuntimeCodebase.DeserializeRootType(typeName);
		}
		
		public static bool TryDeserializeType(string typeName, out Type type, Type parentType, string parentMethodName)
		{
			return RuntimeCodebase.TryDeserializeType(typeName, out type, parentType, parentMethodName);
		}

		public static Type DeserializeType(string typeName, Type parentType, string parentMethodName)
		{
			return RuntimeCodebase.DeserializeType(typeName, parentType, parentMethodName);
		}
		
		private const char memberDataSeparator = ';';

		public static string SerializeMember(Member member)
		{
			// Format:  
			// targetType;name;paramType1;paramType2
			//
			// Examples: 
			// UnityEngine.Transform;rotate;UnityEngine.Vector3
			// System.Collections.Generic.List`1;Add;`T
			// UnityEngine.GameObject;Instantiate;`T;bool

			var sb = new StringBuilder();
			sb.Append(SerializeType(member.targetType));
			sb.Append(memberDataSeparator);
			sb.Append(member.name);

			if (member.parameterTypes != null)
			{
				sb.Append(memberDataSeparator);

				for (int i = 0; i < member.parameterTypes.Length; i++)
				{
					sb.Append(SerializeType(member.parameterTypes[i]));

					if (i < member.parameterTypes.Length - 1)
					{
						sb.Append(memberDataSeparator);
					}
				}
			}

			return sb.ToString();
		}
		
		public static Member DeserializeMember(string memberData)
		{
			try
			{
				Ensure.That(nameof(memberData)).IsNotNullOrEmpty(memberData);

				var parts = memberData.Split(memberDataSeparator);

				if (parts.Length < 2)
				{
					throw new SerializationException("Malformed member data string.");
				}

				var targetType = DeserializeRootType(parts[0]);
				var name = parts[1];
				Type[] parameterTypes;

				if (parts.Length == 2)
				{
					parameterTypes = null;
				}
				else if (parts.Length == 3 && string.IsNullOrEmpty(parts[2]))
				{
					parameterTypes = Empty<Type>.array;
				}
				else
				{
					parameterTypes = new Type[parts.Length - 2];

					for (int i = 2; i < parts.Length; i++)
					{
						parameterTypes[i - 2] = DeserializeType(parts[i], targetType, name);
					}
				}

				return new Member(targetType, name, parameterTypes);
			}
			catch (Exception ex)
			{
				throw new SerializationException($"Unable to find member: '{memberData}'.", ex);
			}
		}

		#endregion
		
		// NETUP: IReadOnlyCollection

		public static ReadOnlyCollection<Assembly> assemblies { get; private set; }

		public static ReadOnlyCollection<Assembly> runtimeAssemblies { get; private set; }

		public static ReadOnlyCollection<Assembly> editorAssemblies { get; private set; }

		public static ReadOnlyCollection<Assembly> ludiqAssemblies { get; private set; }

		public static ReadOnlyCollection<Assembly> ludiqRuntimeAssemblies { get; private set; }

		public static ReadOnlyCollection<Assembly> ludiqEditorAssemblies { get; private set; }

		public static ReadOnlyCollection<Assembly> settingsAssemblies { get; private set; }

		public static ReadOnlyCollection<Type> types { get; private set; }

		public static ReadOnlyCollection<Type> runtimeTypes { get; private set; }

		public static ReadOnlyCollection<Type> editorTypes { get; private set; }

		public static ReadOnlyCollection<Type> ludiqTypes { get; private set; }

		public static ReadOnlyCollection<Type> ludiqRuntimeTypes { get; private set; }

		public static ReadOnlyCollection<Type> ludiqEditorTypes { get; private set; }
		
		private static bool IsEditorAssembly(AssemblyName assemblyName)
		{
			var name = assemblyName.Name;

			return
				name == "Assembly-CSharp-Editor" ||
				name == "Assembly-CSharp-Editor-firstpass" ||
				name == "UnityEditor";
		}

		private static bool IsUserAssembly(AssemblyName assemblyName)
		{
			var name = assemblyName.Name;
			
			return
				name == "Assembly-CSharp" ||
				name == "Assembly-CSharp-firstpass";
		}

		private static bool IsUserAssembly(Assembly assembly)
		{
			return IsUserAssembly(assembly.GetName());
		}

		private static bool IsEditorAssembly(Assembly assembly)
		{
			if (Attribute.IsDefined(assembly, typeof(AssemblyIsEditorAssembly)))
			{
				return true;
			}

			return IsEditorAssembly(assembly.GetName());
		}

		private static bool IsRuntimeAssembly(Assembly assembly)
		{
			// User assemblies refer to the editor when they include
			// a using UnityEditor / #if UNITY_EDITOR, but they should still
			// be considered runtime.
			return IsUserAssembly(assembly) || !IsEditorDependentAssembly(assembly);
		}

		private static bool IsEditorDependentAssembly(Assembly assembly)
		{
			if (IsEditorAssembly(assembly))
			{
				return true;
			}

			foreach (var dependency in assembly.GetReferencedAssemblies())
			{
				if (IsEditorAssembly(dependency))
				{
					return true;
				}
			}

			return false;
		}
		
		private static bool IsLudiqRuntimeDependentAssembly(Assembly assembly)
		{
			if (assembly.GetName().Name == "Ludiq.Core.Runtime")
			{
				return true;
			}

			foreach (var dependency in assembly.GetReferencedAssemblies())
			{
				if (dependency.Name == "Ludiq.Core.Runtime")
				{
					return true;
				}
			}

			return false;
		}

		private static bool IsLudiqEditorDependentAssembly(Assembly assembly)
		{
			if (assembly.GetName().Name == "Ludiq.Core.Editor")
			{
				return true;
			}

			foreach (var dependency in assembly.GetReferencedAssemblies())
			{
				if (dependency.Name == "Ludiq.Core.Editor")
				{
					return true;
				}
			}

			return false;
		}

		public static bool IsEditorType(Type type)
		{
			var rootNamespace = type.RootNamespace();

			return IsEditorAssembly(type.Assembly) ||
				   rootNamespace == "UnityEditor" ||
				   rootNamespace == "UnityEditorInternal";
		}

		public static bool IsInternalType(Type type)
		{
			var rootNamespace = type.RootNamespace();

			return rootNamespace == "UnityEngineInternal" ||
				   rootNamespace == "UnityEditorInternal";
		}

		public static bool IsRuntimeType(Type type)
		{
			return !IsEditorType(type) && !IsInternalType(type);
		}

		private static string RootNamespace(this Type type)
		{
			return type.Namespace?.PartBefore('.');
		}

		public static CodebaseSubset Subset(IEnumerable<Type> types, MemberFilter memberFilter, TypeFilter memberTypeFilter = null)
		{
			return new CodebaseSubset(types, memberFilter, memberTypeFilter);
		}

		public static CodebaseSubset Subset(IEnumerable<Type> typeSet, TypeFilter typeFilter, MemberFilter memberFilter, TypeFilter memberTypeFilter = null)
		{
			return new CodebaseSubset(typeSet, typeFilter, memberFilter, memberTypeFilter);
		}
	}
}