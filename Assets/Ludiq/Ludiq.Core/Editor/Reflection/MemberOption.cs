﻿namespace Ludiq
{
	[FuzzyOption(typeof(Member))]
	public sealed class MemberOption : DocumentedOption<Member>
	{
		public MemberOption(Member member) : this(member, ActionDirection.Any, false) { }

		public MemberOption(Member member, ActionDirection direction, bool expectingBoolean) : base(FuzzyOptionMode.Leaf)
		{
			Ensure.That(nameof(member)).IsNotNull(member);

			value = member;

			documentation = member.info.Documentation();

			getIcon = () => member.pseudoDeclaringType.Icon();

			if (member.isPseudoInherited)
			{
				style = FuzzyWindow.Styles.optionWithIconDim;
			}

			if (member.isInvocable)
			{
				label = $"{member.info.DisplayName(direction, expectingBoolean)} ({member.methodBase.DisplayParameterString()})";
			}
			else
			{
				label = member.info.DisplayName(direction, expectingBoolean);
			}
		}

		public string Haystack(ActionDirection direction, bool expectingBoolean)
		{
			return MemberNameWithTargetType(direction, expectingBoolean);
		}

		private string MemberNameWithTargetType(ActionDirection direction, bool expectingBoolean)
		{
			return $"{value.targetType.DisplayName()}{(LudiqCore.Configuration.humanNaming ? ": " : ".")}{value.info.DisplayName(direction, expectingBoolean)}";
		}

		public string SearchResultLabel(string query, ActionDirection direction, bool expectingBoolean)
		{
			var label = SearchUtility.HighlightQuery(Haystack(direction, expectingBoolean), query);

			if (value.isInvocable)
			{
				label += $" ({value.methodBase.DisplayParameterString()})";
			}

			return label;
		}
	}
}