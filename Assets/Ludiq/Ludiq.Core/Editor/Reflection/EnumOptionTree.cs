﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Ludiq
{
	public class EnumOptionTree : FuzzyOptionTree
	{
		public EnumOptionTree(Type enumType) : base(new GUIContent(enumType.HumanName()))
		{
			Ensure.That(nameof(enumType)).IsNotNull(enumType);

			enums = Enum.GetValues(enumType).Cast<Enum>().ToList();
		}

		private readonly List<Enum> enums;

		public override IEnumerable<IFuzzyOption> Root()
		{
			return enums.Select(x => new EnumOption(x));
		}

		public override IEnumerable<IFuzzyOption> Children(IFuzzyOption parent)
		{
			return Enumerable.Empty<IFuzzyOption>();
		}

		public override IEnumerable<IFuzzyOption> SearchableChildren(IFuzzyOption parent)
		{
			return Enumerable.Empty<IFuzzyOption>();
		}

		public static EnumOptionTree For<T>()
		{
			return new EnumOptionTree(typeof(T));
		}
	}
}