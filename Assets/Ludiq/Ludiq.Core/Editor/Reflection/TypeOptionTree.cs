﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;

namespace Ludiq
{
	public class TypeOptionTree : FuzzyOptionTree
	{
		public enum RootMode
		{
			Types,
			Namespaces
		}

		private TypeOptionTree() : base(new GUIContent("Type")) { }

		public TypeOptionTree(IEnumerable<Type> types) : this()
		{
			Ensure.That(nameof(types)).IsNotNull(types);
			this.types = types.ToHashSet();
		}

		public TypeOptionTree(IEnumerable<Type> typeSet, TypeFilter filter) : this()
		{
			Ensure.That(nameof(typeSet)).IsNotNull(typeSet);
			Ensure.That(nameof(filter)).IsNotNull(filter);
			this.typeSet = typeSet;
			this.filter = filter;
		}

		private readonly IEnumerable<Type> typeSet;

		private readonly IEnumerable<TypeOption> typeOptions;

		private readonly TypeFilter filter;

		private HashSet<Type> types;

		public RootMode rootMode { get; set; } = RootMode.Namespaces;

		public override void Prewarm()
		{
			base.Prewarm();

			if (types == null)
			{
				types = typeSet.Where(filter.Configured().ValidateType).ToHashSet();
			}

			groupEnums = !types.All(t => t.IsEnum);
		}

		#region Configuration

		public bool groupEnums { get; set; } = true;

		public bool surfaceCommonTypes { get; set; } = true;

		#endregion

		#region Hierarchy

		private readonly FuzzyGroup enumsGroup = new FuzzyGroup("(Enums)", typeof(Enum).Icon());

		public override IEnumerable<IFuzzyOption> Root()
		{
			if (rootMode == RootMode.Namespaces)
			{
				if (surfaceCommonTypes)
				{
					foreach (var type in EditorTypeUtility.commonTypes)
					{
						if (types.Contains(type))
						{
							yield return new TypeOption(type, FuzzyOptionMode.Leaf);
						}
					}
				}

				foreach (var @namespace in types.Where(t => !(groupEnums && t.IsEnum))
												.Select(t => t.Namespace().Root)
												.Distinct()
												.OrderBy(ns => ns.DisplayName(false)))
				{
					yield return new NamespaceOption(@namespace, FuzzyOptionMode.Branch);
				}

				if (groupEnums && types.Any(t => t.IsEnum))
				{
					yield return new FuzzyGroupOption(enumsGroup);
				}
			}
			else if (rootMode == RootMode.Types)
			{
				foreach (var type in types)
				{
					yield return new TypeOption(type, FuzzyOptionMode.Leaf);
				}
			}
			else
			{
				throw new UnexpectedEnumValueException<RootMode>(rootMode);
			}
		}

		private IEnumerable<IFuzzyOption> Children(IFuzzyOption parent, bool ordered)
		{
			if (parent is NamespaceOption namespaceOption)
			{
				var @namespace = namespaceOption.value;

				if (!@namespace.IsGlobal)
				{
					var childNamespaces = types.Where(t => !(groupEnums && t.IsEnum))
														.SelectMany(t => t.Namespace().AndAncestors())
														.Distinct()
														.Where(ns => ns.Parent == @namespace);

					if (ordered)
					{
						childNamespaces = childNamespaces.OrderBy(ns => ns.DisplayName(false));
					}

					foreach (var childNamespace in childNamespaces)
					{
						yield return new NamespaceOption(childNamespace, FuzzyOptionMode.Branch);
					}
				}

				var childTypes = types.Where(t => t.Namespace() == @namespace && !(groupEnums && t.IsEnum));

				if (ordered)
				{
					childTypes = childTypes.OrderBy(t => t.DisplayName());
				}

				foreach (var type in childTypes)
				{
					yield return new TypeOption(type, FuzzyOptionMode.Leaf);
				}
			}
			else if (parent == enumsGroup)
			{
				var childTypes = types.Where(t => t.IsEnum);

				if (ordered)
				{
					childTypes = childTypes.OrderBy(t => t.DisplayName());
				}

				foreach (var type in childTypes)
				{
					yield return new TypeOption(type, FuzzyOptionMode.Leaf);
				}
			}
		}

		public override IEnumerable<IFuzzyOption> Children(IFuzzyOption parent)
		{
			return Children(parent, true);
		}

		public override IEnumerable<IFuzzyOption> SearchableChildren(IFuzzyOption parent)
		{
			foreach (var child in Children(parent, false))
			{
				if (child is TypeOption)
				{
					yield return child;
				}

				foreach (var grandchild in SearchableChildren(child))
				{
					yield return grandchild;
				}
			}
		}

		#endregion

		#region Search

		public override bool searchable { get; } = true;

		public override IEnumerable<ISearchResult<IFuzzyOption>> SearchResults(string query, IFuzzyOption parent, CancellationToken cancellation)
		{
			var children = parent != null
				? SearchableChildren(parent)
				: types.Select(t => (IFuzzyOption)new TypeOption(t, FuzzyOptionMode.Leaf));

			return children.OrderableSearchFilter(query, x => x.haystack, cancellation);
		}

		#endregion
	}
}