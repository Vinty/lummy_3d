using System;

namespace Ludiq
{
	[FuzzyOption(typeof(Enum))]
	public class EnumOption : DocumentedOption<Enum>
	{
		public EnumOption(Enum @enum) : base(FuzzyOptionMode.Leaf)
		{
			value = @enum;
			label = @enum.HumanName();
			getIcon = @enum.Icon;
			documentation = @enum.Documentation();
			zoom = true;
		}
	}
}