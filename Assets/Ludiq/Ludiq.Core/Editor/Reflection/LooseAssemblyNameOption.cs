namespace Ludiq
{
	[FuzzyOption(typeof(LooseAssemblyName))]
	public class LooseAssemblyNameOption : FuzzyOption<LooseAssemblyName>
	{
		public LooseAssemblyNameOption(LooseAssemblyName looseAssemblyName) : base(FuzzyOptionMode.Leaf)
		{
			value = looseAssemblyName;
			label = value.name;
		}
	}
}