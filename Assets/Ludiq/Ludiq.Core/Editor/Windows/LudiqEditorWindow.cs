﻿using System;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;

namespace Ludiq
{
	public abstract class LudiqEditorWindow : EditorWindow, ISerializationCallbackReceiver, IHasCustomMenu
	{
		protected virtual Event e => Event.current;

		[SerializeField, DoNotSerialize, FormerlySerializedAs("_data")] // Serialize with Unity, but not with FullSerializer.
		protected FullSerializationData _fullData;

		protected Vector2 scroll;

		void ISerializationCallbackReceiver.OnBeforeSerialize()
		{
			// Ignore the FullSerializer callback, but still catch the Unity callback
			if (Serialization.isFullSerializing)
			{
				return;
			}

			Serialization.isUnitySerializing = true;
			
			// Starting in Unity 2018.3.0b7 apparently, the editor window tries to serialize
			// its title content and along the way, its image, which becomes an invalid reference.
			// But instead of setting it to actual null, it sets it to an invalid Unity Object,
			// which the UnityObjectConverter complains about and can't reliably detect off
			// the main thread.
			var titleImage = titleContent.image;
			titleContent.image = null;

			try
			{
				OnBeforeSerialize();
				_fullData = this.FullSerialize(true);
			}
			catch (Exception ex)
			{
				// Don't abort the whole serialization thread because this one object failed
				Debug.LogError($"Failed to serialize editor window.\n{ex}", this);
			}

			titleContent.image = titleImage;

			Serialization.isUnitySerializing = false;
		}

		void ISerializationCallbackReceiver.OnAfterDeserialize()
		{
			// Ignore the FullSerializer callback, but still catch the Unity callback
			if (Serialization.isFullSerializing)
			{
				return;
			}

			Serialization.isUnitySerializing = true;

			try
			{
				object @this = this;
				_fullData.FullDeserializeInto(ref @this, true);
				OnAfterDeserialize();
			}
			catch (Exception ex)
			{
				// Don't abort the whole deserialization thread because this one object failed
				Debug.LogError($"Failed to deserialize editor window.\n{ex}", this);
			}

			Serialization.isUnitySerializing = false;
		}

		protected virtual void OnBeforeSerialize() { }

		protected virtual void OnAfterDeserialize() { }

		protected virtual void OnEnable()
		{
			// Manual handlers have to be used over magic methods because
			// magic methods don't get triggered when the window is out of focus
			EditorApplicationUtility.onSelectionChange += _OnSelectionChange;
			EditorApplicationUtility.onProjectChange += _OnProjectChange;
			EditorApplicationUtility.onHierarchyChange += _OnHierarchyChange;
			EditorApplicationUtility.onModeChange += _OnModeChange;
			EditorApplicationUtility.onEnterPlayMode += _OnEnterPlayMode;
			EditorApplicationUtility.onExitPlayMode += _OnExitPlayMode;
			EditorApplicationUtility.onEnterEditMode += _OnEnterEditMode;
			EditorApplicationUtility.onExitEditMode += _OnExitEditMode;
			EditorApplicationUtility.onUndoRedo += _OnUndoRedo;
		}

		protected virtual void OnDisable()
		{
			EditorApplicationUtility.onSelectionChange -= _OnSelectionChange;
			EditorApplicationUtility.onProjectChange -= _OnProjectChange;
			EditorApplicationUtility.onHierarchyChange -= _OnHierarchyChange;
			EditorApplicationUtility.onModeChange -= _OnModeChange;
			EditorApplicationUtility.onEnterPlayMode -= _OnEnterPlayMode;
			EditorApplicationUtility.onExitPlayMode -= _OnExitPlayMode;
			EditorApplicationUtility.onEnterEditMode -= _OnEnterEditMode;
			EditorApplicationUtility.onExitEditMode -= _OnExitEditMode;
			EditorApplicationUtility.onUndoRedo -= _OnUndoRedo;
		}

		protected virtual void _OnSelectionChange()
		{

		}

		protected virtual void _OnProjectChange()
		{

		}

		protected virtual void _OnHierarchyChange()
		{

		}

		protected virtual void _OnModeChange()
		{

		}

		protected virtual void _OnEnterPlayMode()
		{
		}

		protected virtual void _OnExitPlayMode()
		{
		}

		protected virtual void _OnEnterEditMode()
		{

		}

		protected virtual void _OnExitEditMode()
		{
		}

		protected virtual void _OnUndoRedo()
		{

		}

		protected virtual void Update()
		{
			// Position isn't reliable in GUI calls due to layouting, so cache it here
			reliablePosition = position;
		}

		protected virtual void OnGUI()
		{

		}

		public Rect reliablePosition { get; private set; }

		public void AddItemsToMenu(GenericMenu menu)
		{
			menu.AddItem(new GUIContent("Show Data..."), false, () => { _fullData.ShowString(ToString()); });
		}

		public override string ToString()
		{
			return this.ToSafeString();
		}
	}
}