﻿using System.Diagnostics;
using UnityEditor;
using UnityEngine;

namespace Ludiq
{
	public class SetupCompletePage : Page
	{
		public SetupCompletePage(Product product)
		{
			Ensure.That(nameof(product)).IsNotNull(product);

			title = "Setup Complete";
			shortTitle = "Finish";
			icon = LudiqCore.Resources.LoadIcon("Icons/Windows/SetupWizard/SetupCompletePage.png");
			
			learnContent = new GUIContent("Learn", LudiqCore.Resources.LoadIcon("Icons/Windows/SetupWizard/LearnButton.png")[IconSize.Medium], "Browse tutorials, examples and manuals.");
			communityContent = new GUIContent("Community", LudiqCore.Resources.LoadIcon("Icons/Windows/SetupWizard/CommunityButton.png")[IconSize.Medium], "Connect online for questions and feedback.");
			configurationContent = new GUIContent("Configuration", LudiqCore.Resources.LoadIcon("Icons/Windows/SetupWizard/ConfigurationButton.png")[IconSize.Medium], "Customize your project settings and preferences.");
			completeContent = new GUIContent("Start", LudiqCore.Resources.LoadIcon("Icons/Windows/SetupWizard/CompleteButton.png")[IconSize.Medium], $"Close the wizard and start using {product.name}!");

			this.product = product;
		}

		private readonly Product product;

		protected override void OnShow()
		{
			base.OnShow();

			foreach (var plugin in product.plugins.ResolveDependencies())
			{
				plugin.configuration.projectSetupCompleted = true;
				plugin.configuration.editorSetupCompleted = true;
				plugin.configuration.Save();
			}

			AssetDatabase.SaveAssets();

			// Run the gizmo disabler. It's an expansive operation,
			// so we don't do it on every assembly reload, but this way at least
			// we make sure that the gizmos will be properly disabled on install.
			AnnotationDisabler.DisableGizmos();
		}

		private readonly GUIContent learnContent;
		private readonly GUIContent communityContent;
		private readonly GUIContent configurationContent;
		private readonly GUIContent completeContent;
		
		protected override void OnContentGUI()
		{
			GUILayout.BeginVertical(Styles.background, GUILayout.ExpandHeight(true));

			LudiqGUI.FlexibleSpace();
			GUILayout.Label($"{product.name} has successfully been setup.", LudiqStyles.centeredLabel);
			LudiqGUI.FlexibleSpace();

			var hasLearn = !string.IsNullOrEmpty(product.learnUrl);

			var hasCommunity = !string.IsNullOrEmpty(product.communityUrl);

			LudiqGUI.BeginHorizontal();

			LudiqGUI.FlexibleSpace();

			EditorGUI.BeginDisabledGroup(!hasLearn);

			if (LudiqGUI.BigButtonLayout(learnContent))
			{
				Process.Start(product.learnUrl);
			}

			EditorGUI.EndDisabledGroup();

			LudiqGUI.Space(Styles.spaceBetweenButtons);

			EditorGUI.BeginDisabledGroup(!hasCommunity);

			if (LudiqGUI.BigButtonLayout(communityContent))
			{
				Process.Start(product.communityUrl);
			}

			EditorGUI.EndDisabledGroup();

			LudiqGUI.FlexibleSpace();

			LudiqGUI.EndHorizontal();


			LudiqGUI.Space(Styles.spaceBetweenButtons);

			
			LudiqGUI.BeginHorizontal();

			LudiqGUI.FlexibleSpace();
			
			if (LudiqGUI.BigButtonLayout(configurationContent))
			{
				product.configurationPanel.Show();
			}
			
			LudiqGUI.Space(Styles.spaceBetweenButtons);
			
			if (LudiqGUI.BigButtonLayout(completeContent))
			{
				Complete();
			}
			
			LudiqGUI.FlexibleSpace();

			LudiqGUI.EndHorizontal();
			
			LudiqGUI.FlexibleSpace();

			LudiqGUI.EndVertical();
		}

		public static class Styles
		{
			static Styles()
			{
				background = new GUIStyle(LudiqStyles.windowBackground);
				background.padding = new RectOffset(10, 10, 10, 10);
			}

			public static readonly GUIStyle background;
			public static readonly float spaceBetweenButtons = 12;
		}
	}
}