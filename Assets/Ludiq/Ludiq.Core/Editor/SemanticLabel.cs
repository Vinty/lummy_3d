﻿namespace Ludiq
{
	public enum SemanticLabel
	{
		Unknown,
		Alpha,
		Beta,
		ReleaseCandidate,
		Final
	}
}
