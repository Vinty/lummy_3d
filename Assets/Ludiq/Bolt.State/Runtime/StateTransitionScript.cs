﻿namespace Bolt
{
	public abstract class StateTransitionScript : IStateTransitionScript
	{
		public StateTransitionScript(IStateScript source, IStateScript destination)
		{
			this.source = source;
			this.destination = destination;
		}

		public IStateScript source { get; }
		public IStateScript destination { get; }

		public abstract void Branch();
		public abstract void BeforeEnter();
		public abstract void OnEnter();
		public abstract void BeforeExit();
		public abstract void OnExit();
	}
}
