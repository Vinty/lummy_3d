﻿using Ludiq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bolt
{
	public abstract class StateScript : IStateScript
	{
		public StateScript(bool canReenter)
		{
			this.canReenter = canReenter;
		}

		public bool canReenter { get; }
		public bool isActive { get; protected set; }
		public List<IStateTransitionScript> incomingTransitionScripts { get; } = new List<IStateTransitionScript>();
		public List<IStateTransitionScript> outgoingTransitionScripts { get; } = new List<IStateTransitionScript>();
		
		public virtual void OnEnter()
		{
			if (isActive && !canReenter) // Prevent re-entry from AnyStateScript
			{
				return;
			}

			isActive = true;

			foreach (var transitionScript in outgoingTransitionScripts)
			{
				transitionScript.BeforeEnter();
			}

			OnEnterImplementation();

			foreach (var transitionScript in outgoingTransitionScripts)
			{
				transitionScript.OnEnter();
			}
		}

		public virtual void OnExit()
		{
			if (!isActive)
			{
				return;
			}

			foreach (var transitionScript in outgoingTransitionScripts)
			{
				transitionScript.BeforeExit();
			}

			OnExitImplementation();

			isActive = false;
			
			foreach (var transitionScript in outgoingTransitionScripts)
			{
				transitionScript.OnExit();
			}
		}

		protected virtual void OnEnterImplementation() {}
		protected virtual void OnExitImplementation() {}
		public virtual void OnBranchTo(IStateScript destination) {}
	}
}
