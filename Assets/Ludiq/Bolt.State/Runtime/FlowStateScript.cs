﻿namespace Bolt
{
	public class FlowStateScript : StateScript
	{
		private readonly FlowGraphScript graphScript;

		public FlowStateScript(FlowGraphScript graphScript, bool canReenter) : base(canReenter)
		{
			this.graphScript = graphScript;
			graphScript.parent = this;
		}

		public FlowGraphData graphData
		{
			get => graphScript.graphData;
			set => graphScript.graphData = value;
		}

		protected override void OnEnterImplementation()
		{
			graphScript.StartListening();
			EventBus.Trigger(new EventHook(StateEventHooks.OnEnterState, graphScript));
		}

		protected override void OnExitImplementation()
		{
			EventBus.Trigger(new EventHook(StateEventHooks.OnExitState, graphScript));
			graphScript.StopListening();
		}
	}
}
