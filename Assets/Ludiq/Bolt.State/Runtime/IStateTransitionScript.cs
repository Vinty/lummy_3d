﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ludiq;

namespace Bolt
{
	public interface IStateTransitionScript
	{
		IStateScript source { get; }
		IStateScript destination { get; }

		void Branch();
		void BeforeEnter();
		void OnEnter();
		void BeforeExit();
		void OnExit();
	}
}
