using Ludiq;
using UnityEngine;

namespace Bolt
{
	[AddComponentMenu("Bolt/State Machine")]
	[RequireComponent(typeof(Variables))]
	[DisableAnnotation]
	public sealed class StateMachine : EventMachine<StateGraph, StateMacro>
	{
		protected override void OnEnable()
		{
			if (hasGraph)
			{
				using (var flow = Flow.New(reference))
				{
					graph.Start(flow);
				}
			}
		}
		
		protected override void Start()
		{
			// Start event doesn't make sense in State Machines
		}

		protected override void OnDisable()
		{
			if (destroyedDuringSwap)
			{
				return;
			}

			if (hasGraph)
			{
				using (var flow = Flow.New(reference))
				{
					graph.Stop(flow);
				}
			}
		}

		[ContextMenu("Show Data...")]
		protected override void ShowData()
		{
			base.ShowData();
		}
	}
}