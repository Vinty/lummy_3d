﻿using Ludiq;

namespace Bolt
{
	public interface IStateTransition : IGraphElementWithDebugData, IConnection<IState, IState>, IStateLifecycle
	{
		void Branch(Flow flow);
		void BeforeEnter(Flow flow);
		void BeforeExit(Flow flow);
	}
}