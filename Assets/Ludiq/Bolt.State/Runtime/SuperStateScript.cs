﻿namespace Bolt
{
	public class SuperStateScript : StateScript
	{
		private readonly StateGraphScript graphScript;

		public SuperStateScript(StateGraphScript graphScript, bool canReenter) : base(canReenter) 
		{
			this.graphScript = graphScript;
		}

		public StateGraphData graphData
		{
			get => graphScript.graphData;
			set => graphScript.graphData = value;
		}

		protected override void OnEnterImplementation()
		{
			graphScript.Start();
		}

		protected override void OnExitImplementation()
		{
			graphScript.Stop();
		}
	}
}
