﻿using Ludiq;
using UnityEngine;

namespace Bolt
{
	[SerializationVersion("A")]
	[TypeIcon(typeof(FlowGraph))]
	public sealed class FlowState : NesterState<FlowGraph, FlowMacro>, IGraphEventListener
	{
		public FlowState() { }

		public FlowState(FlowMacro macro) : base(macro) { } 



		#region Lifecycle

		protected override void OnEnterImplementation(Flow flow)
		{
			if (flow.stack.TryEnterParentElement(this))
			{
				nest.graph.StartListening(flow.stack);
				flow.stack.TriggerEventHandler(hook => hook == StateEventHooks.OnEnterState, new EmptyEventArgs(), parent => parent is SuperUnit, false);
				flow.stack.ExitParentElement();
			}
		}
		
		protected override void OnExitImplementation(Flow flow)
		{
			if (flow.stack.TryEnterParentElement(this))
			{
				flow.stack.TriggerEventHandler(hook => hook == StateEventHooks.OnExitState, new EmptyEventArgs(), parent => parent is SuperUnit, false);
				nest.graph.StopListening(flow.stack);
				flow.stack.ExitParentElement();
			}
		}

		public void StartListening(GraphStack stack)
		{
			if (stack.TryEnterParentElement(this))
			{
				nest.graph.StartListening(stack);
				stack.ExitParentElement();
			}

			foreach (var outgoingTransition in outgoingTransitionsNoAlloc)
			{
				(outgoingTransition as IGraphEventListener)?.StartListening(stack);
			}
		}

		public void StopListening(GraphStack stack)
		{
			foreach (var outgoingTransition in outgoingTransitionsNoAlloc)
			{
				(outgoingTransition as IGraphEventListener)?.StopListening(stack);
			}

			if (stack.TryEnterParentElement(this))
			{
				nest.graph.StopListening(stack);
				stack.ExitParentElement();
			}
		}
		
		public bool IsListening(GraphPointer pointer)
		{
			return pointer.GetElementData<Data>(this).isActive;
		}

		#endregion
	}
}
