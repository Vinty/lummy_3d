﻿using System;
using Ludiq;
using UnityEngine;

namespace Bolt
{
	[SerializationVersion("A")]
	public sealed class FlowStateTransition : NesterStateTransition<FlowGraph, FlowMacro>, IGraphEventListener
	{
		public FlowStateTransition() : base() { }

		public FlowStateTransition(IState source, IState destination) : base(source, destination)
		{
			if (!source.canBeSource)
			{
				throw new InvalidOperationException("Source state cannot emit transitions.");
			}

			if (!destination.canBeDestination)
			{
				throw new InvalidOperationException("Destination state cannot receive transitions.");
			}
		}
		


		#region Lifecycle
		
		public override void BeforeEnter(Flow flow)
		{
			// Start listening for the transition's events
			// before entering the state in case OnEnterState
			// actually instantly triggers a transition via event
			// http://support.ludiq.io/topics/261-event-timing-issue/
			
			if (flow.stack.TryEnterParentElement(this))
			{
				nest.graph?.StartListening(flow.stack);
				flow.stack.ExitParentElement();
			}
		}

		public override void OnEnter(Flow flow)
		{
			if (flow.stack.TryEnterParentElement(this))
			{
				flow.stack.TriggerEventHandler(hook => hook == StateEventHooks.OnEnterState, new EmptyEventArgs(), parent => parent is SuperUnit, false);
				flow.stack.ExitParentElement();
			}
		}

		public override void BeforeExit(Flow flow)
		{
			
		}

		public override void OnExit(Flow flow)
		{
			if (flow.stack.TryEnterParentElement(this))
			{
				flow.stack.TriggerEventHandler(hook => hook == StateEventHooks.OnExitState, new EmptyEventArgs(), parent => parent is SuperUnit, false);
				nest.graph.StopListening(flow.stack);
				flow.stack.ExitParentElement();
			}
		}
		
		public void StartListening(GraphStack stack)
		{
			if (stack.TryEnterParentElement(this))
			{
				nest.graph.StartListening(stack);
				stack.ExitParentElement();
			}
		}

		public void StopListening(GraphStack stack)
		{
			if (stack.TryEnterParentElement(this))
			{
				nest.graph.StopListening(stack);
				stack.ExitParentElement();
			}
		}
		
		public bool IsListening(GraphPointer pointer)
		{
			return pointer.GetElementData<State.Data>(source).isActive;
		}

		#endregion
	}
}