﻿using System.Collections.Generic;
using Ludiq;

namespace Bolt
{
	/// <summary>
	/// A special state that can trigger transitions to other states,
	/// no matter which state is currently active. This state cannot receive
	/// transitions.
	/// </summary>
	public sealed class AnyState : State
	{
		[DoNotSerialize]
		public override bool canToggleStart => false;

		[DoNotSerialize]
		public override bool canBeDestination => false;

		public AnyState() : base()
		{
			isStart = true;
		}

		public override void OnExit(Flow flow)
		{
			// Don't actually ever exit this state.
		}

		public override void OnBranchTo(Flow flow, IState destination)
		{
			// Before entering the destination state,
			// exit all other recursively connected states.

			using (var recursion = Recursion.New(1, true))
			{
				ExitRecursive(this, flow, recursion, destination);
			}
		}

		private static void ExitRecursive(IState state, Flow flow, Recursion recursion, IState except)
		{
			if (!recursion.TryEnter(state))
			{
				return;
			}

			foreach (var outgoingTransition in state.outgoingTransitions)
			{
				var outgoingState = outgoingTransition.destination;
				
				if (outgoingState != except)
				{
					outgoingState.OnExit(flow);
				}

				ExitRecursive(outgoingState, flow, recursion, except);
			}

			foreach (var incomingTransition in state.incomingTransitions)
			{
				var incomingState = incomingTransition.source;
			
				if (incomingState != except)
				{
					incomingState.OnExit(flow);
				}

				ExitRecursive(incomingState, flow, recursion, except);
			}

			recursion.Exit(state);
		}
	}
}