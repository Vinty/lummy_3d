﻿using System.Collections.Generic;
using Ludiq;
using UnityEngine;

namespace Bolt
{
	public interface IState : IGraphElementWithDebugData, IStateLifecycle, IGraphElementWithData
	{
		new StateGraph graph { get; }

		bool isStart { get; set; }
		
		bool canToggleStart { get; }

		bool canBeSource { get; }

		bool canBeDestination { get; }

		void OnBranchTo(Flow flow, IState destination);

		IEnumerable<IStateTransition> outgoingTransitions { get; }

		IEnumerable<IStateTransition> incomingTransitions { get; }

		IEnumerable<IStateTransition> transitions { get; }

		#region Widget

		Vector2 position { get; set; }

		float width { get; set; }

		#endregion
	}
}