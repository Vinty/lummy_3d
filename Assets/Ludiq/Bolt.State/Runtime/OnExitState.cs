﻿using Ludiq;

namespace Bolt
{
	/// <summary>
	/// Called in flow graphs nested in state graphs before the parent state node is exited.
	/// </summary>
	[UnitCategory("Events/State")]
	public class OnExitState : ManualEventUnit<EmptyEventArgs>
	{
		public override string hookName => StateEventHooks.OnExitState;
	}
}