﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bolt
{
	public sealed class FlowStateTransitionScript : StateTransitionScript
	{
		private FlowGraphScript graphScript;

		public FlowStateTransitionScript(FlowGraphScript graphScript, IStateScript source, IStateScript destination)
			: base(source, destination)
		{
			this.graphScript = graphScript;
			graphScript.parent = this;
		}



		public FlowGraphData graphData
		{
			get => graphScript.graphData;
			set => graphScript.graphData = value;
		}



		public override void Branch()
		{
			source.OnExit();
			source.OnBranchTo(destination);
			destination.OnEnter();
		}

		public override void BeforeEnter()
		{
			graphScript.StartListening();
		}

		public override void OnEnter()
		{
			EventBus.Trigger(new EventHook(StateEventHooks.OnEnterState, graphScript));
		}

		public override void BeforeExit()
		{
		}

		public override void OnExit()
		{
			EventBus.Trigger(new EventHook(StateEventHooks.OnExitState, graphScript));
			graphScript.StopListening();
		}
	}
}
