﻿using Ludiq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bolt
{
	public abstract class StateGraphScript : GraphScript<StateGraph, StateGraphData, StateGraphScript>
	{
		protected List<IStateScript> stateScripts = new List<IStateScript>();
		protected HashSet<IStateScript> startingStateScripts = new HashSet<IStateScript>();

		public StateGraphScript(IMachineScript machineScript) : base(machineScript) {}

		public void Start()
		{
			foreach (var state in startingStateScripts)
			{
				state.OnEnter();
			}
		}

		public void Stop()
		{
			foreach (var stateScript in stateScripts)
			{
				if (stateScript.isActive)
				{
					stateScript.OnExit();
				}
			}
		}
	}
}
