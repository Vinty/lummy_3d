﻿namespace Bolt
{
	public interface IStateLifecycle
	{
		void OnEnter(Flow flow);
		void OnExit(Flow flow);
	}
}