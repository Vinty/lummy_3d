﻿using Ludiq;
using UnityEngine;

namespace Bolt
{
	public sealed class StateMacro : Macro<StateGraph>
	{
		[ContextMenu("Show Data...")]
		protected override void ShowData()
		{
			base.ShowData();
		}
	}
}