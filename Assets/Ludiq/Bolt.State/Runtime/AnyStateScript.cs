﻿using Ludiq;

namespace Bolt
{
	public class AnyStateScript : StateScript
	{
		public AnyStateScript() : base(false) { }

		public override void OnExit()
		{
			// Don't actually ever exit this state.
		}

		public override void OnBranchTo(IStateScript destination)
		{
			// Before entering the destination state,
			// exit all other recursively connected states.

			using (var recursion = Recursion.New(1, true))
			{
				ExitRecursive(this, recursion, destination);
			}
		}

		private static void ExitRecursive(IStateScript state, Recursion recursion, IStateScript except)
		{
			if (!recursion.TryEnter(state))
			{
				return;
			}

			foreach (var outgoingTransition in state.outgoingTransitionScripts)
			{
				var outgoingState = outgoingTransition.destination;
				
				if (outgoingState != except)
				{
					outgoingState.OnExit();
				}

				ExitRecursive(outgoingState, recursion, except);
			}

			foreach (var incomingTransition in state.incomingTransitionScripts)
			{
				var incomingState = incomingTransition.source;
			
				if (incomingState != except)
				{
					incomingState.OnExit();
				}

				ExitRecursive(incomingState, recursion, except);
			}

			recursion.Exit(state);
		}
	}
}
