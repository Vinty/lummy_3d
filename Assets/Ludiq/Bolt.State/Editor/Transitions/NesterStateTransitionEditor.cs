﻿using Ludiq;
using UnityEditor;
using UnityEngine;

namespace Bolt
{
	[Editor(typeof(INesterStateTransition))]
	public class NesterStateTransitionEditor : StateTransitionEditor
	{
		public NesterStateTransitionEditor(Metadata metadata) : base(metadata) { }

		private Metadata nestMetadata => metadata[nameof(INesterStateTransition.nest)];

		private Metadata graphMetadata => nestMetadata[nameof(IGraphNest.graph)];
		
		protected override GraphReference headerReference => reference.ChildReference((INesterStateTransition)metadata.value, false);

		protected override Metadata headerTitleMetadata => graphMetadata[nameof(IGraph.title)];

		protected override Metadata headerSummaryMetadata => graphMetadata[nameof(IGraph.summary)];

		protected override float GetInspectorHeight(float width)
		{
			var height = 0f;

			height += base.GetInspectorHeight(width);
			height += EditorGUIUtility.standardVerticalSpacing;
			height += LudiqGUI.GetEditorHeight(this, nestMetadata, width);

			return height;
		}

		protected override void OnInspectorGUI(Rect position)
		{
			var inspectorHeight = base.GetInspectorHeight(position.width);
			var nestHeight = LudiqGUI.GetEditorHeight(this, nestMetadata, position.width);

			y = position.y;
			base.OnInspectorGUI(position.VerticalSection(ref y, inspectorHeight));
			y += EditorGUIUtility.standardVerticalSpacing;
			LudiqGUI.Editor(nestMetadata, position.VerticalSection(ref y, nestHeight));
		}
	}
}