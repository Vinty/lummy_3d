﻿using Ludiq;
using Ludiq.CodeDom;
using System.Collections.Generic;

namespace Bolt
{
	public abstract class StateTransitionGenerator<TStateTransition> : IStateGraphMemberGenerator
		where TStateTransition : class, IStateTransition
	{
		public StateTransitionGenerator(TStateTransition transition)
		{
			this.transition = transition;
			originalMemberName = (transition.Descriptor<IDescriptorWithTitleInformation>().Title().RemoveNonAlphanumeric() + "Transition").FirstCharacterToLower();
		}

		protected TStateTransition transition;
		protected string originalMemberName;

		public virtual void DeclareGraphMemberNames(GraphClassGenerationContext context)
		{
			var transitionScriptMemberName = context.DeclareMemberName(transition, originalMemberName);
			context.AliasMemberName(transition, "transitionScript", transitionScriptMemberName);
		}

		public abstract IEnumerable<CodeCompositeTypeMember> GenerateGraphMembers(GraphClassGenerationContext context);
		public abstract IEnumerable<CodeStatement> GenerateConstructorStatements(StateGraphMethodGenerationContext context);
		public abstract IEnumerable<CodeStatement> GenerateGraphDataPropertySetterStatements(StateGraphMethodGenerationContext context);
	}
}
