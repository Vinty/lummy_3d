﻿using Ludiq;
using Ludiq.CodeDom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bolt.States
{
	[Generator(typeof(FlowState))]
	public class FlowStateGenerator : StateGenerator<FlowState>
	{
		public FlowStateGenerator(FlowState state) : base(state) {}

		public override void DeclareGraphMemberNames(GraphClassGenerationContext context)
		{
			base.DeclareGraphMemberNames(context);

			var generatedChildGraph = context.generationSystem.GenerateGraph(state);

			context.requiredMachineEvents.AddRange(generatedChildGraph.graphScript.requiredMachineEvents);
		}

		public override IEnumerable<CodeCompositeTypeMember> GenerateGraphMembers(GraphClassGenerationContext context)
		{
			yield return new CodeFieldMember(CodeMemberModifiers.Private, CodeFactory.TypeRef(typeof(FlowStateScript)), context.ExpectMemberName(state, originalMemberName));
		}

		public override IEnumerable<CodeStatement> GenerateConstructorStatements(StateGraphMethodGenerationContext context)
		{
			var generatedChildGraph = context.graphClassContext.generationSystem.GenerateGraph(state);
			var childGraphScriptClassName = generatedChildGraph.graphScript.classDeclaration.Name;
			var stateField = context.InstanceField(state, originalMemberName);

			yield return stateField.Assign(CodeFactory.TypeRef(typeof(FlowStateScript)).ObjectCreate(
				CodeFactory.TypeRef(childGraphScriptClassName).ObjectCreate(CodeFactory.VarRef(context.argumentLocals["machineScript"].Name)),
				CodeFactory.Primitive(state.canReenter)
			)).Statement();

			yield return CodeFactory.ThisRef.Field("stateScripts").Method("Add").Invoke(stateField).Statement();

			if (state.isStart)
			{
				yield return CodeFactory.ThisRef.Field("startingStateScripts").Method("Add").Invoke(stateField).Statement();
			}
		}

		public override IEnumerable<CodeStatement> GenerateGraphDataPropertySetterStatements(StateGraphMethodGenerationContext context)
		{
			var generatedChildGraph = context.graphClassContext.generationSystem.GenerateGraph(state);
			var childGraphScriptClassName = generatedChildGraph.graphScript.classDeclaration.Name;

			yield return context.InstanceField(state, originalMemberName).Field("graphData").Assign(
				CodeFactory.VarRef("value").Method("CreateChildGraphData").Invoke(
					CodeFactory.VarRef("value").Field("definition").Field("elements")
						.Index(CodeFactory.TypeRef(typeof(Guid)).ObjectCreate(CodeFactory.Primitive(state.guid.ToString())))
						.Cast(CodeFactory.TypeRef(typeof(FlowState)))
				).Cast(CodeFactory.TypeRef(typeof(FlowGraphData)))
			).Statement();
		}
	}
}
