﻿using Ludiq;

namespace Bolt
{
	[Inspector(typeof(IStateTransition))]
	public class StateTransitionInspector : GraphElementInspector<StateGraphContext>
	{
		public StateTransitionInspector(Metadata metadata) : base(metadata) { }
	}
}