﻿using Ludiq;
using Ludiq.CodeDom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bolt.States
{
	[Generator(typeof(AnyState))]
	public class AnyStateGenerator : StateGenerator<AnyState>
	{
		public AnyStateGenerator(AnyState state) : base(state) {}

		public override IEnumerable<CodeCompositeTypeMember> GenerateGraphMembers(GraphClassGenerationContext context)
		{
			yield return new CodeFieldMember(CodeMemberModifiers.Private, CodeFactory.TypeRef(typeof(AnyStateScript)), context.ExpectMemberName(state, originalMemberName));
		}

		public override IEnumerable<CodeStatement> GenerateConstructorStatements(StateGraphMethodGenerationContext context)
		{
			var stateField = context.InstanceField(state, originalMemberName);

			yield return stateField.Assign(CodeFactory.TypeRef(typeof(AnyStateScript)).ObjectCreate()).Statement();

			yield return CodeFactory.ThisRef.Field("stateScripts").Method("Add").Invoke(stateField).Statement();

			if (state.isStart)
			{
				yield return CodeFactory.ThisRef.Field("startingStateScripts").Method("Add").Invoke(stateField).Statement();
			}
		}

		public override IEnumerable<CodeStatement> GenerateGraphDataPropertySetterStatements(StateGraphMethodGenerationContext context)
		{
			return Enumerable.Empty<CodeStatement>();
		}
	}
}
