﻿using System;
using System.Collections.Generic;
using Ludiq;

namespace Bolt
{
	public abstract class NesterStateWidget<TNesterState> : StateWidget<TNesterState> 
		where TNesterState : class, INesterState
	{
		protected NesterStateWidget(StateCanvas canvas, TNesterState state) : base(canvas, state) { }
		
		protected override IEnumerable<DropdownOption> individualContextOptions
		{
			get
			{
				var childReference = reference.ChildReference(state, false);

				if (childReference != null)
				{
					yield return new DropdownOption((Action)(() => window.reference = childReference), "Open");
					yield return new DropdownOption((Action)(() => GraphWindow.OpenTab(childReference)), "Open in new window");
					yield return new DropdownSeparator();
				}

				foreach (var baseOption in base.individualContextOptions)
				{
					yield return baseOption;
				}
			}
		}

		protected override void OnDoubleClick()
		{
			if (state.graph.zoom == 1)
			{
				var childReference = reference.ChildReference(state, false);

				if (childReference != null)
				{
					if (e.ctrlOrCmd)
					{
						GraphWindow.OpenTab(childReference);
					}
					else
					{
						window.reference = childReference;
					}
				}

				e.Use();
			}
			else
			{
				base.OnDoubleClick();
			}
		}
	}
}