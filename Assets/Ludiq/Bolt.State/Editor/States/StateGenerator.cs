﻿using Ludiq;
using Ludiq.CodeDom;
using System.Collections.Generic;

namespace Bolt
{
	public abstract class StateGenerator<TState> : IStateGraphMemberGenerator
		where TState : class, IState
	{
		public StateGenerator(TState state)
		{
			this.state = state;
			originalMemberName = (state.Descriptor<IDescriptorWithTitleInformation>().Title().RemoveNonAlphanumeric() + "State").FirstCharacterToLower();
		}

		protected TState state;
		protected string originalMemberName;

		public virtual void DeclareGraphMemberNames(GraphClassGenerationContext context)
		{
			var stateScriptMemberName = context.DeclareMemberName(state, originalMemberName);
			context.AliasMemberName(state, "stateScript", stateScriptMemberName);
		}

		public abstract IEnumerable<CodeCompositeTypeMember> GenerateGraphMembers(GraphClassGenerationContext context);
		public abstract IEnumerable<CodeStatement> GenerateConstructorStatements(StateGraphMethodGenerationContext context);
		public abstract IEnumerable<CodeStatement> GenerateGraphDataPropertySetterStatements(StateGraphMethodGenerationContext context);
	}
}
