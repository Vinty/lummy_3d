﻿using Ludiq;
using Ludiq.CodeDom;
using System.Collections.Generic;

namespace Bolt
{
	public interface IStateGraphMemberGenerator : IGenerator
	{
		void DeclareGraphMemberNames(GraphClassGenerationContext context);
		IEnumerable<CodeCompositeTypeMember> GenerateGraphMembers(GraphClassGenerationContext context);
		IEnumerable<CodeStatement> GenerateConstructorStatements(StateGraphMethodGenerationContext context);
		IEnumerable<CodeStatement> GenerateGraphDataPropertySetterStatements(StateGraphMethodGenerationContext context);
	}
}
