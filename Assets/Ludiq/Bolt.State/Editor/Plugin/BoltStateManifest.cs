﻿using DG.Tweening;
using Ludiq;

namespace Bolt
{
	[Plugin(BoltState.ID)]
	public sealed class BoltStateManifest : PluginManifest
	{
		private BoltStateManifest(BoltState plugin) : base(plugin) { }

		public override string name => "Bolt State";
		public override string author => "Ludiq";
		public override string description => "State-machine based visual scripting.";
		public override SemanticVersion version => "2.0.0a3";
	}
}