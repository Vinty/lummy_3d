using System.Collections.Generic;
using Ludiq;

namespace Bolt
{
	[Plugin(ID)]
	[PluginDependency(LudiqCore.ID)]
	[PluginDependency(LudiqGraphs.ID)]
	[PluginDependency(BoltCore.ID)]
	[Product(BoltProduct.ID)]
	[PluginRuntimeAssembly(ID + ".Runtime")]
	public sealed class BoltState : Plugin
	{
		public const string ID = "Bolt.State";

		public BoltState() : base()
		{
			instance = this;
		}

		public static BoltState instance { get; private set; }
		
		public static BoltStateManifest Manifest => (BoltStateManifest)instance.manifest;
		public static BoltStateConfiguration Configuration => (BoltStateConfiguration)instance.configuration;
		public static BoltStateResources Resources => (BoltStateResources)instance.resources;
		public static BoltStateResources.Icons Icons => Resources.icons;

		public const string LegacyRuntimeDllGuid = "dcd2196c4e9166f499793f2007fcda35";
		public const string LegacyEditorDllGuid = "25cf173c22a896d44ae550407b10ed98";

		public override IEnumerable<ScriptReferenceReplacement> scriptReferenceReplacements
		{
			get
			{
				yield return ScriptReferenceReplacement.FromDll<StateMachine>(LegacyRuntimeDllGuid);
				yield return ScriptReferenceReplacement.FromDll<StateMacro>(LegacyRuntimeDllGuid);
			}
		}
	}
}