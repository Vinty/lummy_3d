﻿using System;
using System.Collections.Generic;
using Ludiq;

namespace Bolt
{
	[Plugin(BoltFlow.ID)]
	internal class Changelog_2_0_0a3 : PluginChangelog
	{
		public Changelog_2_0_0a3(Plugin plugin) : base(plugin) { }

		public override SemanticVersion version => "2.0.0a3";
		public override DateTime date => new DateTime(2018, 12, 21);

		public override IEnumerable<string> changes
		{
			get
			{
				yield return "[Added] State Re-Entry Option";
				yield return "[Changed] Any State exit behaviour to be recursive";
			}
		}
	}
}