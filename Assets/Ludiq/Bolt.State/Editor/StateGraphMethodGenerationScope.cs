﻿using Ludiq;
using Ludiq.CodeDom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bolt
{
	public sealed class StateGraphMethodGenerationScope : GraphMethodGenerationScope<StateGraphMethodGenerationScope>
	{
		public StateGraphMethodGenerationScope(StateGraphMethodGenerationScope parentScope, IdentifierPool identifierPool) : base(parentScope, identifierPool) {}
	}
}
