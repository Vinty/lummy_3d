using System;
using System.Collections.Generic;
using System.Linq;
using Ludiq;
using UnityEditor;
using UnityEngine;

namespace Bolt
{
	[Canvas(typeof(StateGraph))]
	public sealed class StateCanvas : Canvas<StateGraph>
	{
		public StateCanvas(StateGraph graph) : base(graph) { }
		

		#region View

		protected override bool shouldEdgePan => base.shouldEdgePan || isCreatingTransition;

		#endregion


		#region Dragging

		protected override void RegisterSnappingSets(SnappingSystem snapping)
		{
			base.RegisterSnappingSets(snapping);

			snapping.AddSet("LeftEdges", Axis2.Vertical);
			snapping.AddSet("RightEdges", Axis2.Vertical);
			snapping.AddSet("TopEdges", Axis2.Horizontal);
			snapping.AddSet("BottomEdges", Axis2.Horizontal);

			snapping.AddMatch("LeftEdges", "LeftEdges");
			snapping.AddMatch("RightEdges", "RightEdges");
			snapping.AddMatch("TopEdges", "TopEdges");
			snapping.AddMatch("BottomEdges", "BottomEdges");
		}

		#endregion


		#region Drawing

		protected override void DrawBackground()
		{
			base.DrawBackground();

			if (isCreatingTransition)
			{
				var startRect = this.Widget(transitionSource).position;
				var end = mousePosition;

				Edge startEdge, endEdge;

				GraphGUI.GetConnectionEdge
				(
					startRect.center,
					end,
					out startEdge,
					out endEdge
				);

				var start = startRect.GetEdgeCenter(startEdge);

				GraphGUI.DrawConnectionArrow(Color.white, start, end, startEdge, endEdge);
			}
		}

		#endregion



		#region Clipboard

		public override void ShrinkCopyGroup(HashSet<IGraphElement> copyGroup)
		{
			copyGroup.RemoveWhere(element =>
			{
				if (element is IStateTransition)
				{
					var transition = (IStateTransition)element;

					if (!copyGroup.Contains(transition.source) ||
						!copyGroup.Contains(transition.destination))
					{
						return true;
					}
				}

				return false;
			});
		}

		#endregion


		



		#region Context

		protected override void HandleLowPriorityInput()
		{
			base.HandleLowPriorityInput();
	
			HandleElementCreation();
		}

		private void HandleElementCreation()
		{
			if (isMouseOver && e.IsKeyDown(KeyCode.Space))
			{
				delayCall += () =>
				{
					var _mousePosition = mousePosition;

					LudiqGUI.Dropdown
					(
						e.mousePosition,
						delegate(object _action)
						{
							delayCall += () =>
							{
								if (_action is Action action)
								{
									action.Invoke();
								}
								else if (_action is Action<Vector2> positionedAction)
								{
									positionedAction.Invoke(_mousePosition);
								}
							};
						},
						new[] {
							new DropdownOption((Action<Vector2>)CreateFlowState, "Create Flow State"),
							new DropdownOption((Action<Vector2>)CreateSuperState, "Create Super State"),
							new DropdownOption((Action<Vector2>)CreateAnyState, "Create Any State"),
						},
						null
					);
				};
			}
		}

		protected override void OnContext()
		{
			if (isCreatingTransition)
			{
				CancelTransition();
			}
			else
			{
				base.OnContext();
			}
		}

		protected override IEnumerable<DropdownOption> GetContextOptions()
		{
			yield return new DropdownOption((Action<Vector2>)CreateFlowState, "Create Flow State");
			yield return new DropdownOption((Action<Vector2>)CreateSuperState, "Create Super State");
			yield return new DropdownOption((Action<Vector2>)CreateAnyState, "Create Any State");

			foreach (var baseOption in base.GetContextOptions())
			{
				yield return baseOption;
			}
		}

		private void CreateFlowState(Vector2 position)
		{
			var flowState = FlowStateFactory.WithEnterUpdateExit();

			if (!graph.states.Any())
			{
				flowState.isStart = true;
				flowState.nest.embed.title = "Start";
			}

			AddState(flowState, position);
		}

		private void CreateSuperState(Vector2 position)
		{
			var superState = SuperStateFactory.WithStart();

			if (!graph.states.Any())
			{
				superState.isStart = true;
				superState.nest.embed.title = "Start";
			}

			AddState(superState, position);
		}

		private void CreateAnyState(Vector2 position)
		{
			AddState(new AnyState(), position);
		}

		public void AddState(IState state, Vector2 position)
		{
			UndoUtility.RecordEditedObject("Create State");
			state.position = position;
			graph.states.Add(state);
			state.position -= this.Widget(state).position.size / 2;
			state.position = state.position.PixelPerfect();
			this.Widget(state).Reposition();
			selection.Select(state);
			GUI.changed = true;
		}

		#endregion



		#region Lifecycle

		public override void Close()
		{
			base.Close();

			CancelTransition();
		}

		protected override void HandleHighPriorityInput()
		{
			if (isCreatingTransition)
			{
				if (e.IsMouseDrag(MouseButton.Left))
				{
					// Priority over lasso
					e.Use();
				}
				else if (e.IsKeyDown(KeyCode.Escape))
				{
					CancelTransition();
					e.Use();
				}
				if (e.IsMouseDown(MouseButton.Left) || e.IsMouseUp(MouseButton.Left))
				{
					CompleteTransitionToNewState();
					e.Use();
				}
			}

			base.HandleHighPriorityInput();
		}

		public void CompleteTransitionToNewState()
		{
			var startRect = this.Widget(transitionSource).position;
			var end = mousePosition;
			
			GraphGUI.GetConnectionEdge
			(
				startRect.center,
				end,
				out var startEdge,
				out var endEdge
			);

			var destination = FlowStateFactory.WithEnterUpdateExit();
			graph.states.Add(destination);
			
			Vector2 offset;

			var size = this.Widget(destination).position.size;

			switch (endEdge)
			{
				case Edge.Left:
					offset = new Vector2(0, -size.y / 2);
					break;
				case Edge.Right:
					offset = new Vector2(-size.x, -size.y / 2);
					break;
				case Edge.Top:
					offset = new Vector2(-size.x / 2, 0);
					break;
				case Edge.Bottom:
					offset = new Vector2(-size.x / 2, -size.y);
					break;
				default:
					throw new UnexpectedEnumValueException<Edge>(endEdge);
			}

			destination.position = mousePosition + offset;

			destination.position = destination.position.PixelPerfect();

			CompleteTransition(destination);
		}

		#endregion



		#region Drag & Drop

		public override bool AcceptsDragAndDrop()
		{
			return DragAndDropUtility.Is<FlowMacro>() || DragAndDropUtility.Is<StateMacro>();
		}

		public override void PerformDragAndDrop()
		{
			if (DragAndDropUtility.Is<FlowMacro>())
			{
				var flowMacro = DragAndDropUtility.Get<FlowMacro>();
				var flowState = new FlowState(flowMacro);
				AddState(flowState, DragAndDropUtility.position);
			}
			else if (DragAndDropUtility.Is<StateMacro>())
			{
				var asset = DragAndDropUtility.Get<StateMacro>();
				var superState = new SuperState(asset);
				AddState(superState, DragAndDropUtility.position);
			}
		}

		public override void DrawDragAndDropPreview()
		{
			var tooltipPosition = DragAndDropUtility.position + GraphGUI.Styles.tooltipCursorOffset;

			if (DragAndDropUtility.Is<FlowMacro>())
			{
				GraphGUI.DrawTooltip(tooltipPosition, DragAndDropUtility.Get<FlowMacro>().name, typeof(FlowMacro).Icon());
			}
			else if (DragAndDropUtility.Is<StateMacro>())
			{
				GraphGUI.DrawTooltip(tooltipPosition, DragAndDropUtility.Get<StateMacro>().name, typeof(StateMacro).Icon());
			}
		}

		#endregion


		#region Transition Creation
		
		public IState transitionSource { get; set; }

		public bool isCreatingTransition => transitionSource != null;

		public void StartTransition(IState source)
		{
			transitionSource = source;
			window.Focus();
		}

		public void CompleteTransition(IState destination)
		{
			UndoUtility.RecordEditedObject("Create State Transition");

			var transition = FlowStateTransitionFactory.WithDefaultTrigger(transitionSource, destination);
			graph.transitions.Add(transition);
			transitionSource = null;
			this.Widget(transition).BringToFront();
			selection.Select(transition);
			GUI.changed = true;
		}

		public void CancelTransition()
		{
			transitionSource = null;
		}

		#endregion


		
		#region Collapsing

		public override bool CanCollapse(IEnumerable<IGraphElement> elements)
		{
			return elements.Any(e => e is IState || e is GraphGroup);
		}
		
		protected override void FilterCollapseGroup(HashSet<IGraphElement> @group)
		{
			base.FilterCollapseGroup(@group);

			group.RemoveWhere(e => e is IStateTransition);
		}
		
		public override void CollapseToEmbed(CollapseRequest request)
		{
			var graph = CollapseToGraph(request, out var incomingTransitions, out var outgoingTransitions);
			var superState = new SuperState();
			superState.nest.SwitchToEmbed(graph);
			AddCollapsedState(request, superState, incomingTransitions, outgoingTransitions);
		}

		public override IMacro CollapseToMacro(CollapseRequest request)
		{
			var graph = CollapseToGraph(request, out var incomingTransitions, out var outgoingTransitions);
			var macro = ScriptableObject.CreateInstance<StateMacro>();
			macro.graph = graph;
			var superState = new SuperState();
			superState.nest.SwitchToMacro(macro);
			AddCollapsedState(request, superState, incomingTransitions, outgoingTransitions);
			return macro;
		}

		private StateGraph CollapseToGraph(CollapseRequest request, out HashSet<IStateTransition> externalIncomingTransitions, out HashSet<IStateTransition> externalOutgoingTransitions)
		{
			var graph = StateGraphFactory.Blank();
			graph.title = request.title;
			graph.summary = request.summary;

			var externalElements = request.elements;
			var externalElementsToInclude = new HashSet<IGraphElement>(externalElements);

			externalIncomingTransitions = new HashSet<IStateTransition>();
			externalOutgoingTransitions = new HashSet<IStateTransition>();

			foreach (var element in externalElements)
			{
				if (element is IState state)
				{
					foreach (var transition in state.incomingTransitions)
					{
						if (externalElements.Contains(transition.source))
						{
							externalElementsToInclude.Add(transition);
						}
						else
						{
							externalIncomingTransitions.Add(transition);
						}
					}

					foreach (var transition in state.outgoingTransitions)
					{
						if (externalElements.Contains(transition.destination))
						{
							externalElementsToInclude.Add(transition);
						}
						else
						{
							externalOutgoingTransitions.Add(transition);
						}
					}
				}
			}
			
			var internalElements = externalElementsToInclude.CloneViaSerializationPolicy().ToList();
			
			graph.elements.AddRangeByDependencies(internalElements);
			
			return graph;
		}

		private void AddCollapsedState(CollapseRequest request, SuperState superState, HashSet<IStateTransition> incomingTransitions, HashSet<IStateTransition> outgoingTransitions)
		{
			var area = request.masterGroup != null ? request.masterGroup.position : GraphGUI.CalculateArea(request.elements.Select(this.Widget));

			AddState(superState, area.center);

			foreach (var incomingTransition in incomingTransitions)
			{
				if (incomingTransition is FlowStateTransition incomingFlowStateTransition)
				{
					var transition = new FlowStateTransition(incomingTransition.source, superState);
					transition.nest.CopyFrom(incomingFlowStateTransition.nest);
					graph.transitions.Add(transition);
				}
			}

			foreach (var outgoingTransition in outgoingTransitions)
			{
				if (outgoingTransition is FlowStateTransition outgoingFlowStateTransition)
				{
					var transition = new FlowStateTransition(superState, outgoingTransition.destination);
					transition.nest.CopyFrom(outgoingFlowStateTransition.nest);
					graph.transitions.Add(transition);
				}
			}

			foreach (var element in request.elements)
			{
				graph.elements.Remove(element);
			}

			if (request.masterGroup != null)
			{
				graph.elements.Remove(request.masterGroup);
			}
			
			Cache();
			var superStateWidget = this.Widget(superState);
			var superUnitPosition = superStateWidget.position;
			superUnitPosition.x = area.x + (area.width - superUnitPosition.width) / 2;
			superUnitPosition.y = area.y + (area.height - superUnitPosition.height) / 2;
			superStateWidget.position = superUnitPosition;
			superStateWidget.Reposition();

			var internalReference = reference.ChildReference(superState, true);
			var internalContext = internalReference.Context<StateGraphContext>();
			var internalCanvas = internalContext.canvas;
			var internalGraph = internalContext.graph;

			internalContext.BeginEdit();
			{
				internalCanvas.Cache();

				foreach (var widget in internalCanvas.elementWidgets)
				{
					widget.position = new Rect(widget.position.position - area.center, widget.position.size);
					widget.Reposition();
				}
				
				internalCanvas.Cache();
			}
			internalContext.EndEdit();
		}

		#endregion
	}
}