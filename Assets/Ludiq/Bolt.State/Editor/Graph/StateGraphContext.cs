using System.Collections.Generic;
using Ludiq;

namespace Bolt
{
	[GraphContext(typeof(StateGraph))]
	public class StateGraphContext : GraphContext<StateGraph, StateCanvas>
	{
		public StateGraphContext(GraphReference reference) : base(reference) { }
		
		public override string windowTitle => "State Graph";
	}
}