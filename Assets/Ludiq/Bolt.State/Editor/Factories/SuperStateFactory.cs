﻿using Ludiq;

namespace Bolt
{
	[GraphFactory(typeof(SuperState))]
	public class SuperStateFactory : GraphFactory<SuperState, StateGraph>
	{
		public SuperStateFactory(SuperState parent) : base(parent) { }

		public override StateGraph DefaultGraph()
		{
			return StateGraphFactory.WithStart();
		}

		public static SuperState WithStart()
		{
			var superState = new SuperState();
			superState.nest.source = GraphSource.Embed;
			superState.nest.embed = StateGraphFactory.WithStart();
			return superState;
		}
	}
}
