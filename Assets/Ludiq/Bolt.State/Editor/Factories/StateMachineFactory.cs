﻿using Ludiq;

namespace Bolt
{
	[GraphFactory(typeof(StateMachine))]
	public class StateMachineFactory : GraphFactory<StateMachine, StateGraph>
	{
		public StateMachineFactory(StateMachine parent) : base(parent) { }

		public override StateGraph DefaultGraph()
		{
			return StateGraphFactory.WithStart();
		}
	}
}
