﻿using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Ludiq;

namespace Bolt
{
	public sealed class StateGraphMethodGenerationContext : GraphMethodGenerationContext<StateGraphMethodGenerationContext, StateGraphMethodGenerationScope>
	{
		public StateGraphMethodGenerationContext(GraphClassGenerationContext graphClassContext, bool coroutine)
			: base(graphClassContext, coroutine)
		{
		}

		protected override StateGraphMethodGenerationScope CreateScope(IdentifierPool identifierPool)
		{
			return new StateGraphMethodGenerationScope(currentScope, identifierPool);
		}
	}
}
