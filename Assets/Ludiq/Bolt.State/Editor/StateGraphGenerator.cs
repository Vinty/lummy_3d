﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ludiq.CodeDom;
using System.Collections;
using Ludiq;

namespace Bolt
{
	[Generator(typeof(StateGraph))]
	public class StateGraphGenerator : GraphGenerator<StateGraph>
	{
		public StateGraphGenerator(StateGraph graph) : base(graph)
		{
		}

		protected override Type graphScriptType => typeof(StateGraphScript);
		protected override Type machineScriptType => typeof(StateMachineScript);

		protected override void PopulateGraphScript(GraphClassGenerationContext context)
		{
			using (ProfilingUtility.SampleBlock("Populate state graph script"))
			{
				context.usings.AddRange(new[] {
					CodeFactory.Using("System"),
					CodeFactory.Using("System.Collections"),
					CodeFactory.Using("System.Collections.Generic"),
					CodeFactory.Using("System.Linq"),
					CodeFactory.Using("Bolt"),
					CodeFactory.Using("Ludiq"),
					CodeFactory.Using("UnityEngine")
				});

				var states = graph.states.ToList();
				states.Sort((a, b) => {
					int result = a.GetType().Name.CompareTo(b.GetType().Name);
					if (result != 0) return result;
					return a.guid.CompareTo(b.guid);
				});

				var transitions = graph.transitions.ToList();
				transitions.Sort((a, b) => {
					int result = a.GetType().Name.CompareTo(b.GetType().Name);
					if (result != 0) return result;
					return a.guid.CompareTo(b.guid);
				});

				var stateGenerators = states.Select(state => state.Generator<IStateGraphMemberGenerator>()).ToList();
				var transitionGenerators = transitions.Select(transition => transition.Generator<IStateGraphMemberGenerator>()).ToList();

				foreach (var generator in stateGenerators)
				{
					generator.DeclareGraphMemberNames(context);
				}
				foreach (var generator in transitionGenerators)
				{
					generator.DeclareGraphMemberNames(context);
				}

				foreach (var generator in stateGenerators)
				{
					context.classDeclaration.Members.AddRange(generator.GenerateGraphMembers(context));
				}
				foreach (var generator in transitionGenerators)
				{
					context.classDeclaration.Members.AddRange(generator.GenerateGraphMembers(context));
				}

				context.classDeclaration.Members.Add(GenerateConstructor(context, stateGenerators, transitionGenerators));
				context.classDeclaration.Members.Add(GenerateGraphDataProperty(context, stateGenerators, transitionGenerators));
			}
		}

		private CodeConstructorMember GenerateConstructor(GraphClassGenerationContext context, List<IStateGraphMemberGenerator> stateGenerators, List<IStateGraphMemberGenerator> transitionGenerators)
		{
			return context.AddImplementationMember(new StateGraphMethodGenerationContext(context, false).GenerateConstructor(
				CodeMemberModifiers.Public,
				new[] {
					new CodeParameterDeclaration(CodeFactory.TypeRef(typeof(IMachineScript)), "machineScript")
				},
				new CodeConstructorInitializer(CodeConstructorInitializer.InitializerKind.Base, new[] {
					CodeFactory.VarRef("machineScript")
				}),
				methodGenerationContext => Enumerable.Concat(
					stateGenerators.SelectMany(generator => generator.GenerateConstructorStatements(methodGenerationContext)),
					transitionGenerators.SelectMany(generator => generator.GenerateConstructorStatements(methodGenerationContext)))));
		}

		private CodePropertyMember GenerateGraphDataProperty(GraphClassGenerationContext context, List<IStateGraphMemberGenerator> stateGenerators, List<IStateGraphMemberGenerator> transitionGenerators)
		{
			var getter = new CodeUserPropertyAccessor(default(CodeMemberModifiers), new[] { new CodeReturnStatement(CodeFactory.VarRef("_graphData")) });
			var setter = new StateGraphMethodGenerationContext(context, false).GeneratePropertyAccessor(
				default(CodeMemberModifiers),
				Enumerable.Empty<CodeParameterDeclaration>(),
				methodGenerationContext => CodeFactory.ThisRef.Field("_graphData").Assign(CodeFactory.VarRef("value")).Statement().Yield().Concat(Enumerable.Concat(
					stateGenerators.SelectMany(generator => generator.GenerateGraphDataPropertySetterStatements(methodGenerationContext)),
					transitionGenerators.SelectMany(generator => generator.GenerateGraphDataPropertySetterStatements(methodGenerationContext)))));
			return context.AddImplementationMember(new CodePropertyMember(CodeMemberModifiers.Public | CodeMemberModifiers.Override, CodeFactory.TypeRef(typeof(StateGraphData)), "graphData", getter, setter));
		}

		protected override void PopulateMachineScript(GraphClassGenerationContext context, IEnumerable<string> requiredMachineEvents)
		{
			using (ProfilingUtility.SampleBlock("Populate state machine script"))
			{
				context.usings.AddRange(new[] {
					CodeFactory.Using("System"),
					CodeFactory.Using("Ludiq"),
				});

				context.classDeclaration.Members.Add(new CodeConstructorMember(
					CodeMemberModifiers.Public,
					Enumerable.Empty<CodeParameterDeclaration>(),
					new[] {
						CodeFactory.ThisRef.Field("graphScript").Assign(context.graphScriptTypeReference.ObjectCreate(CodeFactory.ThisRef)).Statement()
					}));

				context.GenerateMachineEvents(requiredMachineEvents);
			}
		}
	}
}
