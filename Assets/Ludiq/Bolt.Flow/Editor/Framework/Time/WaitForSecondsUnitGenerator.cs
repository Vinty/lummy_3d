﻿using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ludiq;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(WaitForSecondsUnit))]
	public class WaitForSecondsUnitGenerator : UnitGenerator<WaitForSecondsUnit>
	{
		public WaitForSecondsUnitGenerator(WaitForSecondsUnit unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			if (controlInput == unit.enter)
			{
				var unscaledTimeExpression = unit.unscaledTime.GenerateExpression(context, typeof(bool));
				var waitForRealtimeYield = new CodeYieldReturnStatement(CodeFactory.TypeRef(typeof(WaitForSecondsRealtime)).ObjectCreate(unit.seconds.GenerateExpression(context, typeof(float))));
				var waitForSecondsYield = new CodeYieldReturnStatement(CodeFactory.TypeRef(typeof(WaitForSeconds)).ObjectCreate(unit.seconds.GenerateExpression(context, typeof(float))));

				if (unscaledTimeExpression is CodePrimitiveExpression unscaledTimeBoolPrimitive)
				{
					yield return (bool)unscaledTimeBoolPrimitive.Value ? waitForRealtimeYield : waitForSecondsYield;
				}
				else
				{
					yield return new CodeIfStatement(unscaledTimeExpression, new[] { waitForRealtimeYield }, new[] { waitForSecondsYield });
				}
			}
			else
			{
				throw new NotImplementedException();
			}
		}

		public override IEnumerable<CodeStatement> GenerateControlInputSuccessor(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			if (controlInput == unit.enter)
			{
				return unit.exit.GenerateStatements(context);
			}
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			throw new NotImplementedException();
		}
	}
}
