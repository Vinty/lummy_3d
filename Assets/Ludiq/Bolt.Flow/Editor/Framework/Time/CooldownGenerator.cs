﻿using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ludiq;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(Cooldown))]
	public class CooldownGenerator : UnitGenerator<Cooldown>, IListeningUnitGenerator
	{
		public CooldownGenerator(Cooldown unit) : base(unit) 
		{
			var upperCamelCaseName = BoltFlowNameUtility.UnitTitle(unit.GetType(), false);
			var lowerCamelCaseName = upperCamelCaseName.FirstCharacterToLower();
			remainingMemberOriginalName = lowerCamelCaseName + "Remaining";
			durationMemberOriginalName = lowerCamelCaseName + "Duration";
			unscaledMemberOriginalName = lowerCamelCaseName + "Unscaled";
			updateHandlerMemberOriginalName = lowerCamelCaseName + "UpdateHook";
			resetMethodOriginalName = upperCamelCaseName + "Reset";
			updateMethodOriginalName = upperCamelCaseName + "Update";
		}

		private string remainingMemberOriginalName { get; }
		private string durationMemberOriginalName { get; }
		private string unscaledMemberOriginalName { get; }
		private string updateHandlerMemberOriginalName { get; }
		private string resetMethodOriginalName { get; }
		private string updateMethodOriginalName { get; }

		public override void DeclareGraphMemberNames(GraphClassGenerationContext context)
		{
			base.DeclareGraphMemberNames(context);

			context.requiredMachineEvents.Add(EventHooks.Update);

			context.DeclareMemberName(unit, remainingMemberOriginalName);
			context.DeclareMemberName(unit, durationMemberOriginalName);
			context.DeclareMemberName(unit, unscaledMemberOriginalName);
			context.DeclareMemberName(unit, updateHandlerMemberOriginalName);
			context.DeclareMemberName(unit, resetMethodOriginalName);
			context.DeclareMemberName(unit, updateMethodOriginalName);
		}

		public override IEnumerable<CodeCompositeTypeMember> GenerateMembers(GraphClassGenerationContext context)
		{
			foreach (var member in base.GenerateMembers(context)) yield return member;

			yield return new CodeFieldMember(CodeMemberModifiers.Private, CodeFactory.TypeRef(typeof(float)), context.ExpectMemberName(unit, remainingMemberOriginalName));
			yield return new CodeFieldMember(CodeMemberModifiers.Private, CodeFactory.TypeRef(typeof(float)), context.ExpectMemberName(unit, durationMemberOriginalName));
			yield return new CodeFieldMember(CodeMemberModifiers.Private, CodeFactory.TypeRef(typeof(bool)), context.ExpectMemberName(unit, unscaledMemberOriginalName));
			yield return context.AddImplementationMember(new CodeFieldMember(CodeMemberModifiers.Private, CodeFactory.TypeRef(typeof(Action<EmptyEventArgs>)), context.ExpectMemberName(unit, updateHandlerMemberOriginalName)));
			yield return new FlowMethodGenerationContext(context, false).GenerateMethod(
				CodeMemberModifiers.Public,
				CodeFactory.TypeRef(typeof(void)),
				context.ExpectMemberName(unit, resetMethodOriginalName),
				Enumerable.Empty<CodeParameterDeclaration>(),
				GenerateResetStatements);
			yield return new FlowMethodGenerationContext(context, false).GenerateMethod(
				CodeMemberModifiers.Public,
				CodeFactory.TypeRef(typeof(void)),
				context.ExpectMemberName(unit, updateMethodOriginalName),
				Enumerable.Empty<CodeParameterDeclaration>(),
				GenerateUpdateStatements);
		}

		public IEnumerable<CodeStatement> GenerateStartListeningStatements(FlowMethodGenerationContext context)
		{
			var graphClassContext = context.graphClassContext;

			yield return context.InstanceField(unit, updateHandlerMemberOriginalName).Assign(
				CodeFactory.Lambda(
					new CodeParameterDeclaration[] { new CodeParameterDeclaration(CodeFactory.VarType, "args") },
					context.InstanceMethod(unit, updateMethodOriginalName).Invoke())
			).Statement();
			yield return CodeFactory.TypeRef(typeof(EventBus)).Expression().Method("Register").Invoke(
				CodeFactory.TypeRef(typeof(EventHook)).ObjectCreate(
					CodeFactory.TypeRef(typeof(EventHooks)).Expression().Field("Update"),
					CodeFactory.ThisRef.Field("machineScript")),
				context.InstanceField(unit, updateHandlerMemberOriginalName)
			).Statement();
		}

		public IEnumerable<CodeStatement> GenerateStopListeningStatements(FlowMethodGenerationContext context)
		{
			var graphClassContext = context.graphClassContext;
			yield return CodeFactory.TypeRef(typeof(EventBus)).Expression().Method("Unregister").Invoke(
				CodeFactory.TypeRef(typeof(EventHook)).ObjectCreate(
					CodeFactory.TypeRef(typeof(EventHooks)).Expression().Field("Update"),
					CodeFactory.ThisRef.Field("machineScript")),
				context.InstanceField(unit, updateHandlerMemberOriginalName)
			).Statement();
			yield return context.InstanceField(unit, updateHandlerMemberOriginalName).Assign(CodeFactory.Primitive(null)).Statement();
		}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			if (controlInput == unit.enter)
			{
				var isReadyExpression = context.InstanceField(unit, remainingMemberOriginalName).LessThanOrEqual(CodeFactory.Primitive(0));
				var ifTrueStatements = new List<CodeStatement>() { context.InstanceMethod(unit, resetMethodOriginalName).Invoke().Statement() };
				ifTrueStatements.AddRange(unit.exitReady.GenerateScopedStatements(context));

				var ifFalseStatements = unit.exitNotReady.GenerateScopedStatements(context).ToList();

				yield return new CodeIfStatement(isReadyExpression, ifTrueStatements, ifFalseStatements);
			}
			else if (controlInput == unit.reset)
			{
				yield return context.InstanceMethod(unit, updateMethodOriginalName).Invoke().Statement();
			}
			else
			{
				throw new NotImplementedException();
			}
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.remainingSeconds)
			{
				return context.InstanceField(unit, remainingMemberOriginalName);
			}
			else if (valueOutput == unit.remainingRatio)
			{
				return CodeFactory.TypeRef(typeof(Mathf)).Expression().Method("Clamp01").Invoke(
					context.InstanceField(unit, remainingMemberOriginalName).Divide(context.InstanceField(unit, durationMemberOriginalName)));
			}
			throw new NotImplementedException();
		}

		private IEnumerable<CodeStatement> GenerateResetStatements(FlowMethodGenerationContext context)
		{
			yield return context.InstanceField(unit, durationMemberOriginalName).Assign(unit.duration.GenerateExpression(context, typeof(float))).Statement();
			yield return context.InstanceField(unit, remainingMemberOriginalName).Assign(context.InstanceField(unit, durationMemberOriginalName)).Statement();
			yield return context.InstanceField(unit, unscaledMemberOriginalName).Assign(unit.unscaledTime.GenerateExpression(context, typeof(bool))).Statement();
		}

		private IEnumerable<CodeStatement> GenerateUpdateStatements(FlowMethodGenerationContext context)
		{
			yield return new CodeIfStatement(
				context.InstanceField(unit, remainingMemberOriginalName).GreaterThan(CodeFactory.Primitive(0)),
				GenerateUpdateInnerStatements(context).ToList());
		}

		private IEnumerable<CodeStatement> GenerateUpdateInnerStatements(FlowMethodGenerationContext context)
		{
			yield return context.InstanceField(unit, remainingMemberOriginalName).Assign(
				CodeFactory.TypeRef(typeof(Mathf)).Expression().Method("Max").Invoke(
					context.InstanceField(unit, remainingMemberOriginalName).Subtract(
						CodeFactory.Conditional(context.InstanceField(unit, unscaledMemberOriginalName),
							CodeFactory.TypeRef(typeof(Time)).Expression().Field("unscaledDeltaTime"),
							CodeFactory.TypeRef(typeof(Time)).Expression().Field("deltaTime"))),
					CodeFactory.Primitive(0))
			).Statement();

			foreach (var statement in unit.tick.GenerateStatements(context))
			{
				yield return statement;
			}

			yield return new CodeIfStatement(context.InstanceField(unit, remainingMemberOriginalName).LessThanOrEqual(CodeFactory.Primitive(0)), unit.becameReady.GenerateScopedStatements(context));
		}
	}
}
