﻿using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ludiq;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(WaitUntilUnit))]
	public class WaitUntilUnitGenerator : UnitGenerator<WaitUntilUnit>
	{
		public WaitUntilUnitGenerator(WaitUntilUnit unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			if (controlInput == unit.enter)
			{
				yield return new CodeYieldReturnStatement(CodeFactory.TypeRef(typeof(WaitUntil)).ObjectCreate(
					CodeFactory.Lambda(new CodeParameterDeclaration[] {}, unit.condition.GenerateExpression(context, typeof(bool)))));
			}
			else
			{
				throw new NotImplementedException();
			}
		}

		public override IEnumerable<CodeStatement> GenerateControlInputSuccessor(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			if (controlInput == unit.enter)
			{
				return unit.exit.GenerateStatements(context);
			}
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			throw new NotImplementedException();
		}
	}
}
