﻿using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ludiq;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(Timer))]
	public class TimerGenerator : UnitGenerator<Timer>, IListeningUnitGenerator
	{
		public TimerGenerator(Timer unit) : base(unit) 
		{
			var upperCamelCaseName = BoltFlowNameUtility.UnitTitle(unit.GetType(), false);
			var lowerCamelCaseName = upperCamelCaseName.FirstCharacterToLower();
			elapsedMemberOriginalName = lowerCamelCaseName + "Elapsed";
			durationMemberOriginalName = lowerCamelCaseName + "Duration";
			activeMemberOriginalName = lowerCamelCaseName + "Active";
			pausedMemberOriginalName = lowerCamelCaseName + "Paused";
			unscaledMemberOriginalName = lowerCamelCaseName + "Unscaled";
			updateHandlerMemberOriginalName = lowerCamelCaseName + "UpdateHook";

			startMethodOriginalName = upperCamelCaseName + "Start";
			updateMethodOriginalName = upperCamelCaseName + "Update";
		}

		private string elapsedMemberOriginalName { get; }
		private string durationMemberOriginalName { get; }
		private string activeMemberOriginalName { get; }
		private string pausedMemberOriginalName { get; }
		private string unscaledMemberOriginalName { get; }
		private string updateHandlerMemberOriginalName { get; }

		private string startMethodOriginalName { get; }
		private string updateMethodOriginalName { get; }

		public override void DeclareGraphMemberNames(GraphClassGenerationContext context)
		{
			base.DeclareGraphMemberNames(context);

			context.requiredMachineEvents.Add(EventHooks.Update);

			context.DeclareMemberName(unit, elapsedMemberOriginalName);
			context.DeclareMemberName(unit, durationMemberOriginalName);
			context.DeclareMemberName(unit, activeMemberOriginalName);
			context.DeclareMemberName(unit, pausedMemberOriginalName);
			context.DeclareMemberName(unit, unscaledMemberOriginalName);
			context.DeclareMemberName(unit, updateHandlerMemberOriginalName);

			context.DeclareMemberName(unit, startMethodOriginalName);
			context.DeclareMemberName(unit, updateMethodOriginalName);
		}

		public override IEnumerable<CodeCompositeTypeMember> GenerateMembers(GraphClassGenerationContext context)
		{
			foreach (var member in base.GenerateMembers(context)) yield return member;

			yield return new CodeFieldMember(CodeMemberModifiers.Private, CodeFactory.TypeRef(typeof(float)), context.ExpectMemberName(unit, elapsedMemberOriginalName));
			yield return new CodeFieldMember(CodeMemberModifiers.Private, CodeFactory.TypeRef(typeof(float)), context.ExpectMemberName(unit, durationMemberOriginalName));
			yield return new CodeFieldMember(CodeMemberModifiers.Private, CodeFactory.TypeRef(typeof(bool)), context.ExpectMemberName(unit, activeMemberOriginalName));
			yield return new CodeFieldMember(CodeMemberModifiers.Private, CodeFactory.TypeRef(typeof(bool)), context.ExpectMemberName(unit, pausedMemberOriginalName));
			yield return new CodeFieldMember(CodeMemberModifiers.Private, CodeFactory.TypeRef(typeof(bool)), context.ExpectMemberName(unit, unscaledMemberOriginalName));
			yield return context.AddImplementationMember(new CodeFieldMember(CodeMemberModifiers.Private, CodeFactory.TypeRef(typeof(Action<EmptyEventArgs>)), context.ExpectMemberName(unit, updateHandlerMemberOriginalName)));
			yield return new FlowMethodGenerationContext(context, false).GenerateMethod(
				CodeMemberModifiers.Public,
				CodeFactory.TypeRef(typeof(void)),
				context.ExpectMemberName(unit, startMethodOriginalName),
				Enumerable.Empty<CodeParameterDeclaration>(),
				GenerateStartStatements);
			yield return new FlowMethodGenerationContext(context, false).GenerateMethod(
				CodeMemberModifiers.Public,
				CodeFactory.TypeRef(typeof(void)),
				context.ExpectMemberName(unit, updateMethodOriginalName),
				Enumerable.Empty<CodeParameterDeclaration>(),
				GenerateUpdateStatements);
		}

		public IEnumerable<CodeStatement> GenerateStartListeningStatements(FlowMethodGenerationContext context)
		{
			var graphClassContext = context.graphClassContext;

			yield return context.InstanceField(unit, updateHandlerMemberOriginalName).Assign(
				CodeFactory.Lambda(
					new CodeParameterDeclaration[] { new CodeParameterDeclaration(CodeFactory.VarType, "args") },
					context.InstanceMethod(unit, updateMethodOriginalName).Invoke())
			).Statement();
			yield return CodeFactory.TypeRef(typeof(EventBus)).Expression().Method("Register").Invoke(
				CodeFactory.TypeRef(typeof(EventHook)).ObjectCreate(
					CodeFactory.TypeRef(typeof(EventHooks)).Expression().Field("Update"),
					CodeFactory.ThisRef.Field("machineScript")),
				context.InstanceField(unit, updateHandlerMemberOriginalName)
			).Statement();
		}

		public IEnumerable<CodeStatement> GenerateStopListeningStatements(FlowMethodGenerationContext context)
		{
			var graphClassContext = context.graphClassContext;
			yield return CodeFactory.TypeRef(typeof(EventBus)).Expression().Method("Unregister").Invoke(
				CodeFactory.TypeRef(typeof(EventHook)).ObjectCreate(
					CodeFactory.TypeRef(typeof(EventHooks)).Expression().Field("Update"),
					CodeFactory.ThisRef.Field("machineScript")),
				context.InstanceField(unit, updateHandlerMemberOriginalName)
			).Statement();
			yield return context.InstanceField(unit, updateHandlerMemberOriginalName).Assign(CodeFactory.Primitive(null)).Statement();
		}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			if (controlInput == unit.start)
			{
				yield return context.InstanceMethod(unit, startMethodOriginalName).Invoke().Statement();

				foreach (var statement in unit.started.GenerateStatements(context)) yield return statement;
			}
			else if (controlInput == unit.pause)
			{
				yield return context.InstanceField(unit, pausedMemberOriginalName).Assign(CodeFactory.Primitive(true)).Statement();
			}
			else if (controlInput == unit.resume)
			{
				yield return context.InstanceField(unit, pausedMemberOriginalName).Assign(CodeFactory.Primitive(false)).Statement();
			}
			else if (controlInput == unit.toggle)
			{
				yield return new CodeIfStatement(
					context.InstanceField(unit, activeMemberOriginalName).LogicalNot(),
					context.InstanceMethod(unit, startMethodOriginalName).Invoke().Statement().Yield().Concat(unit.started.GenerateScopedStatements(context)),
					context.InstanceField(unit, pausedMemberOriginalName).Assign(context.InstanceField(unit, pausedMemberOriginalName).LogicalNot()).Statement().Yield());
			}
			else
			{
				throw new NotImplementedException();
			}
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.elapsedSeconds)
			{
				return context.InstanceField(unit, elapsedMemberOriginalName);
			}
			else if (valueOutput == unit.elapsedRatio)
			{
				return CodeFactory.TypeRef(typeof(Mathf)).Expression().Method("Clamp01").Invoke(
					context.InstanceField(unit, elapsedMemberOriginalName).Divide(context.InstanceField(unit, durationMemberOriginalName)));
			}
			else if (valueOutput == unit.remainingSeconds)
			{
				return CodeFactory.TypeRef(typeof(Mathf)).Expression().Method("Max").Invoke(
					CodeFactory.Primitive(0),
					context.InstanceField(unit, durationMemberOriginalName).Subtract(context.InstanceField(unit, elapsedMemberOriginalName)));
			}
			else if (valueOutput == unit.remainingRatio)
			{
				return CodeFactory.TypeRef(typeof(Mathf)).Expression().Method("Clamp01").Invoke(
					context.InstanceField(unit, durationMemberOriginalName)
						.Subtract(context.InstanceField(unit, elapsedMemberOriginalName))
						.Divide(context.InstanceField(unit, durationMemberOriginalName)));
			}
			throw new NotImplementedException();
		}

		private IEnumerable<CodeStatement> GenerateStartStatements(FlowMethodGenerationContext context)
		{
			yield return context.InstanceField(unit, elapsedMemberOriginalName).Assign(CodeFactory.Primitive(0)).Statement();
			yield return context.InstanceField(unit, durationMemberOriginalName).Assign(unit.duration.GenerateExpression(context, typeof(float))).Statement();
			yield return context.InstanceField(unit, activeMemberOriginalName).Assign(CodeFactory.Primitive(true)).Statement();
			yield return context.InstanceField(unit, pausedMemberOriginalName).Assign(CodeFactory.Primitive(false)).Statement();
			yield return context.InstanceField(unit, unscaledMemberOriginalName).Assign(unit.unscaledTime.GenerateExpression(context, typeof(bool))).Statement();
		}

		private IEnumerable<CodeStatement> GenerateUpdateStatements(FlowMethodGenerationContext context)
		{
			yield return new CodeIfStatement(
				context.InstanceField(unit, activeMemberOriginalName).LogicalAnd(context.InstanceField(unit, pausedMemberOriginalName).LogicalNot()),
				GenerateUpdateInnerStatements(context).ToList());
		}

		private IEnumerable<CodeStatement> GenerateUpdateInnerStatements(FlowMethodGenerationContext context)
		{
			yield return context.InstanceField(unit, elapsedMemberOriginalName).Assign(
				CodeFactory.TypeRef(typeof(Mathf)).Expression().Method("Min").Invoke(
					context.InstanceField(unit, elapsedMemberOriginalName).Add(
						CodeFactory.Conditional(context.InstanceField(unit, unscaledMemberOriginalName),
							CodeFactory.TypeRef(typeof(Time)).Expression().Field("unscaledDeltaTime"),
							CodeFactory.TypeRef(typeof(Time)).Expression().Field("deltaTime"))),
					context.InstanceField(unit, durationMemberOriginalName))
			).Statement();

			foreach (var statement in unit.tick.GenerateStatements(context))
			{
				yield return statement;
			}

			yield return new CodeIfStatement(
				context.InstanceField(unit, elapsedMemberOriginalName).GreaterThanOrEqual(context.InstanceField(unit, durationMemberOriginalName)),
				context.InstanceField(unit, activeMemberOriginalName).Assign(CodeFactory.Primitive(false)).Statement().Yield().Concat(unit.completed.GenerateScopedStatements(context)));
		}
	}
}
