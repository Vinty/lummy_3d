﻿using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ludiq;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(WaitForFlow))]
	public class WaitForFlowGenerator : UnitGenerator<WaitForFlow>
	{
		public WaitForFlowGenerator(WaitForFlow unit) : base(unit)
		{
			var upperCamelCaseName = BoltFlowNameUtility.UnitTitle(unit.GetType(), false);
			var lowerCamelCaseName = BoltFlowNameUtility.UnitTitle(unit.GetType(), false).FirstCharacterToLower();
			activatedInputsMemberOriginalName = lowerCamelCaseName + "ActivatedInputs";
			checkActivatedMethodOriginalName = upperCamelCaseName + "IsActivated";
		}

		private string activatedInputsMemberOriginalName { get; }
		private string checkActivatedMethodOriginalName { get; }

		public override void DeclareGraphMemberNames(GraphClassGenerationContext context)
		{
			base.DeclareGraphMemberNames(context);

			context.DeclareMemberName(unit, activatedInputsMemberOriginalName);
			context.DeclareMemberName(unit, checkActivatedMethodOriginalName);
		}

		public override IEnumerable<CodeCompositeTypeMember> GenerateMembers(GraphClassGenerationContext context)
		{
			foreach (var member in base.GenerateMembers(context)) yield return member;

			yield return new CodeFieldMember(CodeMemberModifiers.Public,
				CodeFactory.ArrayTypeRef(CodeFactory.TypeRef(typeof(bool))), context.ExpectMemberName(unit, activatedInputsMemberOriginalName),
				CodeFactory.ArrayTypeRef(CodeFactory.TypeRef(typeof(bool))).ArrayOfSize(CodeFactory.Primitive(unit.awaitedInputs.Count)));
			yield return new FlowMethodGenerationContext(context, false).GenerateMethod(
				CodeMemberModifiers.Public,
				CodeFactory.TypeRef(typeof(bool)),
				context.ExpectMemberName(unit, checkActivatedMethodOriginalName),
				Enumerable.Empty<CodeParameterDeclaration>(),				
				GenerateCheckActivatedStatements);
		}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			if (controlInput == unit.reset)
			{							
				foreach (var statement in GenerateResetStatements(context)) yield return statement;
			}
			else if (unit.awaitedInputs.Contains(controlInput))
			{
				yield return context.InstanceField(unit, activatedInputsMemberOriginalName).Index(
					CodeFactory.Primitive(unit.awaitedInputs.IndexOf(controlInput))
				).Assign(CodeFactory.Primitive(false)).Statement();

				yield return new CodeIfStatement(
					context.InstanceMethod(unit, checkActivatedMethodOriginalName).Invoke(), 
					GenerateEnterActivatedStatements(context).ToList());
			} 
			else
			{
				throw new NotImplementedException();
			}
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			throw new NotImplementedException();
		}

		public IEnumerable<CodeStatement> GenerateResetStatements(FlowMethodGenerationContext context)
		{
			var indexLocal = context.currentScope.DeclareLocal(CodeFactory.IntType, "index", CodeFactory.Primitive(0));

			yield return new CodeForStatement(indexLocal,
				CodeFactory.VarRef(indexLocal.Name).LessThan(context.InstanceField(unit, activatedInputsMemberOriginalName).Field("Length")),
				CodeFactory.PostIncrement(CodeFactory.VarRef(indexLocal.Name)),
				new[] {
					context.InstanceField(unit, activatedInputsMemberOriginalName).Index(CodeFactory.VarRef(indexLocal.Name)).Assign(CodeFactory.Primitive(false)).Statement()
				});
		}

		public IEnumerable<CodeStatement> GenerateEnterActivatedStatements(FlowMethodGenerationContext context)
		{
			if (unit.resetOnExit)
			{
				foreach (var statement in GenerateResetStatements(context)) yield return statement;
			}

			foreach (var next in unit.exit.GenerateScopedStatements(context)) yield return next;
		}

		private IEnumerable<CodeStatement> GenerateCheckActivatedStatements(FlowMethodGenerationContext context)
		{
			var indexLocal = context.currentScope.DeclareLocal(CodeFactory.IntType, "index", CodeFactory.Primitive(0));

			yield return new CodeForStatement(indexLocal,
				CodeFactory.VarRef(indexLocal.Name).LessThan(context.InstanceField(unit, activatedInputsMemberOriginalName).Field("Length")),
				CodeFactory.PostIncrement(CodeFactory.VarRef(indexLocal.Name)),
				new[] {
					new CodeIfStatement(
						context.InstanceField(unit, activatedInputsMemberOriginalName).Index(CodeFactory.VarRef(indexLocal.Name)), 
						new [] {
							new CodeReturnStatement(CodeFactory.Primitive(false)) 
						})
				});

			yield return new CodeReturnStatement(CodeFactory.Primitive(true));
		}
	}
}
