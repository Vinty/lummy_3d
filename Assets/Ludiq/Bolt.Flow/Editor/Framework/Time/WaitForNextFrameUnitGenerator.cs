﻿using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ludiq;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(WaitForNextFrameUnit))]
	public class WaitForNextFrameUnitGenerator : UnitGenerator<WaitForNextFrameUnit>
	{
		public WaitForNextFrameUnitGenerator(WaitForNextFrameUnit unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			if (controlInput == unit.enter)
			{
				yield return new CodeYieldReturnStatement(CodeFactory.Primitive(null));
			}
			else
			{
				throw new NotImplementedException();
			}
		}

		public override IEnumerable<CodeStatement> GenerateControlInputSuccessor(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			if (controlInput == unit.enter)
			{
				return unit.exit.GenerateStatements(context);
			}
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			throw new NotImplementedException();
		}
	}
}
