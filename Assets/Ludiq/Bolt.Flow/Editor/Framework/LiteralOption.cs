﻿using System;
using Ludiq;

namespace Bolt
{
	[FuzzyOption(typeof(Literal))]
	public class LiteralOption : UnitOption<Literal>
	{
		public LiteralOption() : base() { }

		public LiteralOption(Literal unit) : base(unit)
		{

		}

		public Type literalType { get; private set; }
		
		protected override void FillSerializable()
		{
			literalType = unit.type;

			base.FillSerializable();
		}
		
		public override Type sourceType => literalType;

		protected override string Label(bool human)
		{
			return "Literal";
		}

		protected override string Haystack(bool human)
		{
			return unit.type.SelectedName(human) + " Literal";
		}

		protected override string Key()
		{
			return $"{literalType.FullName}@literal";
		}

		public override void Deserialize(UnitOptionData data)
		{
			base.Deserialize(data);

			literalType = Codebase.DeserializeRootType(data.tag1);
		}

		public override UnitOptionData Serialize()
		{
			var row = base.Serialize();

			row.tag1 = Codebase.SerializeType(literalType);

			return row;
		}
	}
}