﻿using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using Ludiq;

namespace Bolt
{
	[Generator(typeof(UnitValuePortProxy))]
	public class UnitValuePortProxyGenerator : UnitGenerator<UnitValuePortProxy>
	{
		public UnitValuePortProxyGenerator(UnitValuePortProxy unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			throw new NotSupportedException();
		}

		public override IEnumerable<CodeStatement> GenerateControlInputSuccessor(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			throw new NotSupportedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.output)
			{
				return unit.input.GenerateExpression(context);
			}
			else
			{
				throw new NotSupportedException();
			}
		}
	}
}