﻿using System.Collections.Generic;
using System.Linq;
using Ludiq;

namespace Bolt
{
	[Widget(typeof(IUnitPortProxy))]
	public class UnitPortProxyWidget : UnitWidget<IUnitPortProxy>
	{
		public UnitPortProxyWidget(FlowCanvas canvas, IUnitPortProxy unit) : base(canvas, unit) { }

		protected override void CacheItemFirstTime()
		{
			base.CacheItemFirstTime();

			CacheTarget();
		}

		public override void OnElementsChanged()
		{
			base.OnElementsChanged();
			
			CacheTarget();
		}

		protected override void CacheDescription()
		{
			base.CacheDescription();

			CacheTarget();
		}
		
		protected virtual void CacheTarget()
		{
			Reposition();

			var resolvedPorts = unit.target.ResolveProxyTargets().ToArray();

			if (resolvedPorts.Length == 0)
			{
				titleContent.text = "(Missing)";
			}
			else if (resolvedPorts.Length > 1)
			{
				titleContent.text = "(Multiple)";
			}
			else if (resolvedPorts.Length == 1)
			{
				var port = resolvedPorts[0];
				titleContent.text = UnitPortDescription.ResolveDisplayLabel(port);
				var portDescription = port.Description<UnitPortDescription>();
				var unitDescription = port.unit.Description<UnitDescription>();
				titleContent.tooltip = $"Proxies port {portDescription.label} on {unitDescription.shortTitle}";
				icon = unitDescription.icon;
			}
		}
	}
}