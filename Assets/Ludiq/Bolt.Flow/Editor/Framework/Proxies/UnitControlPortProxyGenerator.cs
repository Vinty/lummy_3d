﻿using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using Ludiq;

namespace Bolt
{
	[Generator(typeof(UnitControlPortProxy))]
	public class UnitControlPortProxyGenerator : UnitGenerator<UnitControlPortProxy>
	{
		public UnitControlPortProxyGenerator(UnitControlPortProxy unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			if (controlInput == unit.input)
			{
				return Enumerable.Empty<CodeStatement>();
			}
			else
			{
				throw new NotSupportedException();
			}
		}

		public override IEnumerable<CodeStatement> GenerateControlInputSuccessor(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			if (controlInput == unit.input)
			{
				foreach (var next in unit.output.GenerateStatements(context)) yield return next;
			}
			else
			{
				throw new NotSupportedException();
			}
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			throw new NotSupportedException();
		}
	}
}