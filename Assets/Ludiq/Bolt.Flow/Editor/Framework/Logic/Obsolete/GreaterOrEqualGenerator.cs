﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(GreaterOrEqual))]
	public class GreaterOrEqualGenerator : UnitGenerator<GreaterOrEqual>
	{ 
		public GreaterOrEqualGenerator(GreaterOrEqual unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{			
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.comparison)
			{
				if (unit.numeric)
				{
					return unit.a.GenerateExpression(context, typeof(float)).Method("GreaterThanOrApproximately").Invoke(unit.b.GenerateExpression(context, typeof(float)));
				}
				else
				{
					return OperatorGeneratorUtility.GenerateGenericBinaryOperatorExpression(context, BinaryOperator.GreaterThanOrEqual, unit.a, unit.b).Cast(CodeFactory.TypeRef(typeof(bool)));
				}
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
