﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(NotEqual))]
	public class NotEqualGenerator : UnitGenerator<NotEqual>
	{ 
		public NotEqualGenerator(NotEqual unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{			
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.comparison)
			{
				if (unit.numeric)
				{
					return CodeFactory.TypeRef(typeof(Mathf)).Expression().Method("Approximately").Invoke(
						unit.a.GenerateExpression(context, typeof(float)),
						unit.b.GenerateExpression(context, typeof(float))).LogicalNot();
				}
				else
				{
					return OperatorGeneratorUtility.GenerateGenericBinaryOperatorExpression(context, BinaryOperator.Inequality, unit.a, unit.b).Cast(CodeFactory.TypeRef(typeof(bool)));
				}
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
