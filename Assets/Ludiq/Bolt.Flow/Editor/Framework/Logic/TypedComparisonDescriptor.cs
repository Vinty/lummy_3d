﻿using Ludiq;

namespace Bolt
{
	[Descriptor(typeof(TypedComparison))]
	public class TypedComparisonDescriptor : UnitDescriptor<TypedComparison>
	{
		public TypedComparisonDescriptor(TypedComparison unit) : base(unit) { }
		
		protected override string DefinedTitle()
		{
			return $"Compare {unit.type.HumanName()}";
		}

		protected override string DefinedShortTitle()
		{
			return "Compare";
		}

		protected override string DefinedSurtitle()
		{
			return unit.type.DisplayName();
		}

		protected override string DefinedSummary()
		{
			return "Compares two " + unit.type.DisplayName() + " inputs together.";
		}
	}
}