﻿using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(ExclusiveOr))]
	public class ExclusiveOrGenerator : UnitGenerator<ExclusiveOr>
	{ 
		public ExclusiveOrGenerator(ExclusiveOr unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{			
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.result)
			{
				return unit.a.GenerateExpression(context, typeof(bool)).BitwiseXor(unit.b.GenerateExpression(context, typeof(bool)));
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
