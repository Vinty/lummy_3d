﻿using System;
using System.Collections.Generic;
using Ludiq;

namespace Bolt
{
	[FuzzyOption(typeof(TypedComparison))]
	public class TypedComparisonOption : UnitOption<TypedComparison>, IOperatorUnitOption
	{
		public TypedComparisonOption() : base() { }

		public TypedComparisonOption(TypedComparison unit) : base(unit) { }

		public Type comparisonType { get; private set; }

		public OperatorCategory operatorCategory => OperatorCategory.Comparison;
		
		protected override void FillSerializable()
		{
			comparisonType = unit.type;
			base.FillSerializable();
		}
		
		public override Type sourceType => comparisonType;

		protected override string Label(bool human) => "Compare";

		protected override string Haystack(bool human) => $"Compare ({comparisonType.DisplayName()})";

		public override string SearchResultLabel(string query)
		{
			return $"{SearchUtility.HighlightQuery("Compare", query)} ({comparisonType.DisplayName()})";
		}

		protected override string Key() => $"{comparisonType.FullName}@comparison";

		public override void Deserialize(UnitOptionData data)
		{
			base.Deserialize(data);

			comparisonType = Codebase.DeserializeRootType(data.tag1);
		}

		public override UnitOptionData Serialize()
		{
			var row = base.Serialize();

			row.tag1 = Codebase.SerializeType(comparisonType);

			return row;
		}
	}
}