﻿using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(TypedComparison))]
	public class TypedComparisonGenerator : UnitGenerator<TypedComparison>
	{ 
		public TypedComparisonGenerator(TypedComparison unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			var expressionA = unit.a.GenerateExpression(context, unit.type);
			var expressionB = unit.b.GenerateExpression(context, unit.type);

			if (valueOutput == unit.aLessThanB) return expressionA.LessThan(expressionB);
			else if (valueOutput == unit.aLessThanOrEqualToB) return expressionA.LessThanOrEqual(expressionB);
			else if (valueOutput == unit.aEqualToB) return expressionA.Equal(expressionB);
			else if (valueOutput == unit.aNotEqualToB) return expressionA.NotEqual(expressionB);
			else if (valueOutput == unit.aGreaterThanOrEqualToB) return expressionA.GreaterThanOrEqual(expressionB);
			else if (valueOutput == unit.aGreatherThanB) return expressionA.GreaterThan(expressionB);

			throw new NotImplementedException();
		}
	}
}
