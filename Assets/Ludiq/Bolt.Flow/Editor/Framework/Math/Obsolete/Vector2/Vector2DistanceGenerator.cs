﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(Vector2Distance))]
	public class Vector2DistanceGenerator : UnitGenerator<Vector2Distance>
	{ 
		public Vector2DistanceGenerator(Vector2Distance unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{			
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.distance)
			{
				return CodeFactory.TypeRef(typeof(Vector2)).Expression().Method("Distance").Invoke(
					unit.a.GenerateExpression(context, typeof(Vector2)),
					unit.b.GenerateExpression(context, typeof(Vector2)));
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
