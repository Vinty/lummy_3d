﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(Vector2Average))]
	public class Vector2AverageGenerator : UnitGenerator<Vector2Average>
	{ 
		public Vector2AverageGenerator(Vector2Average unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput) => throw new NotImplementedException();

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.average)
			{
				var sumExpression = (CodeExpression) null;
				foreach(var input in unit.multiInputs)
				{
					var inputExpression = input.GenerateExpression(context, typeof(Vector2));
					sumExpression = sumExpression != null ? sumExpression.Add(inputExpression) : inputExpression;
				}
				return sumExpression.Divide(CodeFactory.Primitive((float) unit.inputCount));
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
