﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(Vector2Minimum))]
	public class Vector2MinimumGenerator : UnitGenerator<Vector2Minimum>
	{ 
		public Vector2MinimumGenerator(Vector2Minimum unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{			
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.minimum)			
			{
				List<CodeExpression> inputExpressions = new List<CodeExpression>();
				foreach(var input in unit.multiInputs)
				{
					inputExpressions.Add(input.GenerateExpression(context, typeof(Vector2)));
				}

				return CodeFactory.TypeRef(typeof(MathUtility)).Expression().Method("Min").Invoke(inputExpressions);
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
