﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(Vector2Angle))]
	public class Vector2AngleGenerator : UnitGenerator<Vector2Angle>
	{ 
		public Vector2AngleGenerator(Vector2Angle unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{			
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.angle)
			{
				return CodeFactory.TypeRef(typeof(Vector2)).Expression().Method("Angle").Invoke(
					unit.a.GenerateExpression(context, typeof(Vector2)),
					unit.b.GenerateExpression(context, typeof(Vector2)));
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
