﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(Vector2Multiply))]
	public class Vector2MultiplyGenerator : UnitGenerator<Vector2Multiply>
	{ 
		public Vector2MultiplyGenerator(Vector2Multiply unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{			
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.product)
			{
				return unit.a.GenerateExpression(context, typeof(Vector2)).Method("Multiply").Invoke(unit.b.GenerateExpression(context, typeof(Vector2)));
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
