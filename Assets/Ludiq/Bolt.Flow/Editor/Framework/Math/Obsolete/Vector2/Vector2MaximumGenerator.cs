﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(Vector2Maximum))]
	public class Vector2MaximumGenerator : UnitGenerator<Vector2Maximum>
	{ 
		public Vector2MaximumGenerator(Vector2Maximum unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{			
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.maximum)			
			{
				List<CodeExpression> inputExpressions = new List<CodeExpression>();
				foreach(var input in unit.multiInputs)
				{
					inputExpressions.Add(input.GenerateExpression(context, typeof(Vector2)));
				}

				return CodeFactory.TypeRef(typeof(MathUtility)).Expression().Method("Max").Invoke(inputExpressions);
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
