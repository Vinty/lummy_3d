﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(Vector3Project))]
	public class Vector3ProjectGenerator : UnitGenerator<Vector3Project>
	{ 
		public Vector3ProjectGenerator(Vector3Project unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{			
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.projection)
			{
				return CodeFactory.TypeRef(typeof(Vector3)).Expression().Method("Project").Invoke(
					unit.a.GenerateExpression(context, typeof(Vector3)),
					unit.b.GenerateExpression(context, typeof(Vector3)));
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
