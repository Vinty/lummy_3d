﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(Vector3Angle))]
	public class Vector3AngleGenerator : UnitGenerator<Vector3Angle>
	{ 
		public Vector3AngleGenerator(Vector3Angle unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{			
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.angle)
			{
				return CodeFactory.TypeRef(typeof(Vector3)).Expression().Method("Angle").Invoke(
					unit.a.GenerateExpression(context, typeof(Vector3)),
					unit.b.GenerateExpression(context, typeof(Vector3)));
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
