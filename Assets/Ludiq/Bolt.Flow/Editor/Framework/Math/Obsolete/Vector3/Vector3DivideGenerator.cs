﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(Vector3Divide))]
	public class Vector3DivideGenerator : UnitGenerator<Vector3Divide>
	{ 
		public Vector3DivideGenerator(Vector3Divide unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{			
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.quotient)
			{
				return unit.dividend.GenerateExpression(context, typeof(Vector3)).Method("Divide").Invoke(unit.divisor.GenerateExpression(context, typeof(Vector3)));
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
