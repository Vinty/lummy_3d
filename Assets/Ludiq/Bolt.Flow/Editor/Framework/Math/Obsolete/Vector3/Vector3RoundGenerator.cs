﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(Vector3Round))]
	public class Vector3RoundGenerator : UnitGenerator<Vector3Round>
	{ 
		public Vector3RoundGenerator(Vector3Round unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput) => throw new NotImplementedException();

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.output)
			{
				string roundMethod;
				switch (unit.rounding)
				{
					case Round<Vector3, Vector3>.Rounding.Floor: roundMethod = "Floor"; break;
					case Round<Vector3, Vector3>.Rounding.AwayFromZero: roundMethod = "Round"; break;
					case Round<Vector3, Vector3>.Rounding.Ceiling: roundMethod = "Ceil"; break;
					default: throw new UnexpectedEnumValueException<Round<Vector3, Vector3>.Rounding>(unit.rounding);
				}

				return unit.input.GenerateExpression(context, typeof(Vector3)).Method(roundMethod).Invoke();
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
