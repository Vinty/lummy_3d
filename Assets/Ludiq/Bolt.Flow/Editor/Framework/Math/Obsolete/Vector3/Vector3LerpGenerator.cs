﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(Vector3Lerp))]
	public class Vector3LerpGenerator : UnitGenerator<Vector3Lerp>
	{ 
		public Vector3LerpGenerator(Vector3Lerp unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{			
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.interpolation)
			{
				return CodeFactory.TypeRef(typeof(Vector3)).Expression().Method("Lerp").Invoke(
					unit.a.GenerateExpression(context, typeof(Vector3)),
					unit.b.GenerateExpression(context, typeof(Vector3)),
					unit.t.GenerateExpression(context, typeof(float)));
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
