﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(Vector3MoveTowards))]
	public class Vector3MoveTowardsGenerator : UnitGenerator<Vector3MoveTowards>
	{ 
		public Vector3MoveTowardsGenerator(Vector3MoveTowards unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput) => throw new NotImplementedException();

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.result)
			{
				var maxDeltaExpression = unit.maxDelta.GenerateExpression(context, typeof(float));
				if (unit.perSecond)
				{
					maxDeltaExpression.Multiply(CodeFactory.TypeRef(typeof(Time)).Expression().Field("deltaTime"));
				}

				return CodeFactory.TypeRef(typeof(Vector3)).Expression().Method("MoveTowards").Invoke(
					unit.current.GenerateExpression(context, typeof(Vector3)),
					unit.target.GenerateExpression(context, typeof(Vector3)),
					maxDeltaExpression);
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
