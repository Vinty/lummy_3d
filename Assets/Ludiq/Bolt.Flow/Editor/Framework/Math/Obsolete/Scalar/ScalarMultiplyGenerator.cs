﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Bolt
{
	[Generator(typeof(ScalarMultiply))]
	public class ScalarMultiplyGenerator : UnitGenerator<ScalarMultiply>
	{ 
		public ScalarMultiplyGenerator(ScalarMultiply unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput) => throw new NotImplementedException();

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.product)
			{
				return unit.a.GenerateExpression(context, typeof(float)).Multiply(unit.b.GenerateExpression(context, typeof(float)));
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
