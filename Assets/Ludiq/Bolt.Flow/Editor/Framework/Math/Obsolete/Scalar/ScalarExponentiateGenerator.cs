﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(ScalarExponentiate))]
	public class ScalarExponentiateGenerator : UnitGenerator<ScalarExponentiate>
	{ 
		public ScalarExponentiateGenerator(ScalarExponentiate unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{			
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.power)			
			{
				return CodeFactory.TypeRef(typeof(Mathf)).Expression().Method("Pow").Invoke(
					unit.@base.GenerateExpression(context, typeof(float)),
					unit.exponent.GenerateExpression(context, typeof(float)));
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
