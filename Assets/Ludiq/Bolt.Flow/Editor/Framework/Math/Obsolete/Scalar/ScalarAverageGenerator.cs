﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(ScalarAverage))]
	public class ScalarAverageGenerator : UnitGenerator<ScalarAverage>
	{ 
		public ScalarAverageGenerator(ScalarAverage unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput) => throw new NotImplementedException();

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.average)
			{
				var sumExpression = (CodeExpression) null;
				foreach(var input in unit.multiInputs)
				{
					var inputExpression = input.GenerateExpression(context, typeof(float));
					sumExpression = sumExpression != null ? sumExpression.Add(inputExpression) : inputExpression;
				}
				return sumExpression.Divide(CodeFactory.Primitive((float) unit.inputCount));
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
