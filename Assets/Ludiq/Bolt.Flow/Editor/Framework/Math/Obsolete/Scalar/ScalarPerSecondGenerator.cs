﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(ScalarPerSecond))]
	public class ScalarPerSecondGenerator : UnitGenerator<ScalarPerSecond>
	{ 
		public ScalarPerSecondGenerator(ScalarPerSecond unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput) => throw new NotImplementedException();

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.output)
			{
				return unit.input.GenerateExpression(context, typeof(float)).Multiply(CodeFactory.TypeRef(typeof(Time)).Expression().Field("deltaTime"));
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
