﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(ScalarRoot))]
	public class ScalarRootGenerator : UnitGenerator<ScalarRoot>
	{ 
		public ScalarRootGenerator(ScalarRoot unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput) => throw new NotImplementedException();

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.root)
			{
				var degreeExpression = unit.degree.GenerateExpression(context, typeof(float));
				if (degreeExpression is CodePrimitiveExpression degreePrimitive)
				{
					if ((float) degreePrimitive.Value == 2)
					{
						return CodeFactory.TypeRef(typeof(Mathf)).Expression().Method("Sqrt").Invoke(unit.radicand.GenerateExpression(context));
					}
				}

				return CodeFactory.TypeRef(typeof(Mathf)).Expression().Method("Pow").Invoke(unit.radicand.GenerateExpression(context, typeof(float)), CodeFactory.Primitive(1f).Divide(degreeExpression));
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
