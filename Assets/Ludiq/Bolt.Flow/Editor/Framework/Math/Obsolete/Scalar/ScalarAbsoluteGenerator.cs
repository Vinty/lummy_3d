﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(ScalarAbsolute))]
	public class ScalarAbsoluteGenerator : UnitGenerator<ScalarAbsolute>
	{ 
		public ScalarAbsoluteGenerator(ScalarAbsolute unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput) => throw new NotImplementedException();

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.output)
			{
				return CodeFactory.TypeRef(typeof(Mathf)).Expression().Method("Abs").Invoke(unit.input.GenerateExpression(context, typeof(float)));
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
