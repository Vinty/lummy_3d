﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(GenericModulo))]
	public class GenericModuloGenerator : UnitGenerator<GenericModulo>
	{ 
		public GenericModuloGenerator(GenericModulo unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{			
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.remainder)
			{
				return OperatorGeneratorUtility.GenerateGenericBinaryOperatorExpression(context, BinaryOperator.Modulo, unit.dividend, unit.divisor);
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
