﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(GenericSubtract))]
	public class GenericSubtractGenerator : UnitGenerator<GenericSubtract>
	{ 
		public GenericSubtractGenerator(GenericSubtract unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{			
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.difference)
			{
				return OperatorGeneratorUtility.GenerateGenericBinaryOperatorExpression(context, BinaryOperator.Subtraction, unit.minuend, unit.subtrahend);
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
