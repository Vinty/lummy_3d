﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(GenericMultiply))]
	public class GenericMultiplyGenerator : UnitGenerator<GenericMultiply>
	{ 
		public GenericMultiplyGenerator(GenericMultiply unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{			
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.product)
			{
				return OperatorGeneratorUtility.GenerateGenericBinaryOperatorExpression(context, BinaryOperator.Multiplication, unit.a, unit.b);
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
