﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(Vector4Sum))]
	public class Vector4SumGenerator : UnitGenerator<Vector4Sum>
	{ 
		public Vector4SumGenerator(Vector4Sum unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput) => throw new NotImplementedException();

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.sum)
			{
				var sumExpression = (CodeExpression) null;
				foreach(var input in unit.multiInputs)
				{
					var inputExpression = input.GenerateExpression(context, typeof(Vector4));
					sumExpression = sumExpression != null ? sumExpression.Add(inputExpression) : inputExpression;
				}
				return sumExpression;
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
