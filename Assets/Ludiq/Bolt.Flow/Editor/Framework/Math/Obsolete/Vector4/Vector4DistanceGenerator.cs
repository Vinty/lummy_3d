﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(Vector4Distance))]
	public class Vector4DistanceGenerator : UnitGenerator<Vector4Distance>
	{ 
		public Vector4DistanceGenerator(Vector4Distance unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{			
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.distance)
			{
				return CodeFactory.TypeRef(typeof(Vector4)).Expression().Method("Distance").Invoke(
					unit.a.GenerateExpression(context, typeof(Vector4)),
					unit.b.GenerateExpression(context, typeof(Vector4)));
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
