﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(Vector4Multiply))]
	public class Vector4MultiplyGenerator : UnitGenerator<Vector4Multiply>
	{ 
		public Vector4MultiplyGenerator(Vector4Multiply unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{			
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.product)
			{
				return unit.a.GenerateExpression(context, typeof(Vector4)).Method("Multiply").Invoke(unit.b.GenerateExpression(context, typeof(Vector4)));
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
