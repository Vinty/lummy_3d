﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(Vector4Minimum))]
	public class Vector4MinimumGenerator : UnitGenerator<Vector4Minimum>
	{ 
		public Vector4MinimumGenerator(Vector4Minimum unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{			
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.minimum)			
			{
				List<CodeExpression> inputExpressions = new List<CodeExpression>();
				foreach(var input in unit.multiInputs)
				{
					inputExpressions.Add(input.GenerateExpression(context, typeof(Vector4)));
				}

				return CodeFactory.TypeRef(typeof(MathUtility)).Expression().Method("Min").Invoke(inputExpressions);
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
