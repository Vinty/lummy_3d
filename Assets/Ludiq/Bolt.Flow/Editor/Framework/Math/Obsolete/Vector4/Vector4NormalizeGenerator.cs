﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(Vector4Normalize))]
	public class Vector4NormalizeGenerator : UnitGenerator<Vector4Normalize>
	{ 
		public Vector4NormalizeGenerator(Vector4Normalize unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{			
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.output)
			{
				return unit.input.GenerateExpression(context, typeof(Vector4)).Field("normalized");
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
