﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(Vector4Subtract))]
	public class Vector4SubtractGenerator : UnitGenerator<Vector4Subtract>
	{ 
		public Vector4SubtractGenerator(Vector4Subtract unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{			
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.difference)
			{
				return unit.minuend.GenerateExpression(context, typeof(Vector4)).Subtract(unit.subtrahend.GenerateExpression(context, typeof(Vector4)));
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
