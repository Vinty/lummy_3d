﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(Vector4Round))]
	public class Vector4RoundGenerator : UnitGenerator<Vector4Round>
	{ 
		public Vector4RoundGenerator(Vector4Round unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput) => throw new NotImplementedException();

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.output)
			{
				string roundMethod;
				switch (unit.rounding)
				{
					case Round<Vector4, Vector4>.Rounding.Floor: roundMethod = "Floor"; break;
					case Round<Vector4, Vector4>.Rounding.AwayFromZero: roundMethod = "Round"; break;
					case Round<Vector4, Vector4>.Rounding.Ceiling: roundMethod = "Ceil"; break;
					default: throw new UnexpectedEnumValueException<Round<Vector4, Vector4>.Rounding>(unit.rounding);
				}

				return unit.input.GenerateExpression(context, typeof(Vector4)).Method(roundMethod).Invoke();
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
