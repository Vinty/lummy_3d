﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(Vector4Lerp))]
	public class Vector4LerpGenerator : UnitGenerator<Vector4Lerp>
	{ 
		public Vector4LerpGenerator(Vector4Lerp unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{			
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.interpolation)
			{
				return CodeFactory.TypeRef(typeof(Vector4)).Expression().Method("Lerp").Invoke(
					unit.a.GenerateExpression(context, typeof(Vector4)),
					unit.b.GenerateExpression(context, typeof(Vector4)),
					unit.t.GenerateExpression(context, typeof(float)));
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
