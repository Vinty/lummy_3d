﻿#pragma warning disable 618

using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(Vector4Modulo))]
	public class Vector4ModuloGenerator : UnitGenerator<Vector4Modulo>
	{ 
		public Vector4ModuloGenerator(Vector4Modulo unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{			
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.remainder)
			{
				return unit.dividend.GenerateExpression(context, typeof(Vector4)).Method("Modulo").Invoke(unit.divisor.GenerateExpression(context, typeof(Vector4)));
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
