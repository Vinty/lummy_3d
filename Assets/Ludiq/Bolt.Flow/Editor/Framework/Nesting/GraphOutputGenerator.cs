﻿using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Bolt
{
	[Generator(typeof(GraphOutput))]
	public class GraphOutputGenerator : UnitGenerator<GraphOutput>
	{ 
		public GraphOutputGenerator(GraphOutput unit) : base(unit) {}

		public override void DeclareGraphMemberNames(GraphClassGenerationContext context)
		{
			base.DeclareGraphMemberNames(context);

			if (context.generationSystem.graphGenerationMode == GraphGenerationMode.Preview)
			{
				foreach (var valueInput in unit.valueInputs)
				{
					context.DeclareMemberName(unit, valueInput.key.RemoveNonAlphanumeric());
				}
			}
		}

		public override IEnumerable<CodeCompositeTypeMember> GenerateMembers(GraphClassGenerationContext context)
		{
			foreach (var member in base.GenerateMembers(context)) yield return member;

			if (context.generationSystem.graphGenerationMode == GraphGenerationMode.Preview)
			{
				foreach (var valueInput in unit.valueInputs)
				{
					if (valueInput.hasValidConnection)
					{
						var getter = new FlowMethodGenerationContext(context, false).GeneratePropertyAccessor(
							default(CodeMemberModifiers),
							Enumerable.Empty<CodeParameterDeclaration>(),
							methodContext => GenerateEnterStatements(methodContext, valueInput));
						var property = new CodePropertyMember(
							CodeMemberModifiers.Public,
							CodeFactory.TypeRef(valueInput.type),
							valueInput.key.RemoveNonAlphanumeric(),
							getter,
							null);
						property.Comments.Add(new CodeComment("(Inlined)"));

						yield return property;
					}
				}
			}
		}

		public IEnumerable<CodeStatement> GenerateEnterStatements(FlowMethodGenerationContext context, ValueInput valueInput)
		{
			var source = valueInput.connection.source;

			foreach (var statement in source.GenerateValueInputDependencies(context))
			{
				yield return statement;
			}

			foreach (var statement in source.unit.Generator<IUnitGenerator>().GenerateValueOutputInit(context, source))
			{
				yield return statement;
			}

			yield return new CodeReturnStatement(valueInput.GenerateExpression(context));
		}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			if (unit.controlInputs.Contains(controlInput))
			{
				if (context.canExitGraph)
				{
					var parent = (SuperUnit)context.currentParentElement;
					var controlOutput = parent.controlOutputs[controlInput.key];
					if (controlOutput.hasValidConnection)
					{
						context.ExitParentElement();

						foreach (var child in controlOutput.GenerateStatements(context)) yield return child;

						context.EnterParentElement(parent);
					}
					else
					{
						yield break;
					}
				}
				else
				{
					yield return new CodeCommentStatement(new CodeComment(controlInput.key.RemoveNonAlphanumeric(), CodeComment.CommentStyle.Block));
				}
			}
			else
			{
				throw new InvalidOperationException();
			}
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			throw new NotImplementedException();
		}
	}
}
