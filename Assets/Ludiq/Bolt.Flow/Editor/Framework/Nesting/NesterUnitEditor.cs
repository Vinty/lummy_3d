﻿using Ludiq;
using UnityEditor;
using UnityEngine;

namespace Bolt
{
	[Editor(typeof(INesterUnit))]
	public class NesterUnitEditor : UnitEditor
	{
		public NesterUnitEditor(Metadata metadata) : base(metadata) { }

		protected INesterUnit nesterUnit => (INesterUnit)metadata.value;

		private Metadata nestMetadata => metadata[nameof(INesterUnit.nest)];

		private Metadata graphMetadata => nestMetadata[nameof(IGraphNest.graph)];
		
		protected GraphReference childReference => reference.ChildReference(nesterUnit, false);

		protected IGraphContext childContext => childReference?.Context();

		protected override GraphReference headerReference => childReference;
		
		protected override Metadata headerTitleMetadata => graphMetadata[nameof(IGraph.title)];

		protected override Metadata headerSummaryMetadata => graphMetadata[nameof(IGraph.summary)];

		protected override float GetInspectorHeight(float width)
		{
			var height = 0f;
			 
			height += base.GetInspectorHeight(width);
			height += EditorGUIUtility.standardVerticalSpacing;
			height += LudiqGUI.GetEditorHeight(this, nestMetadata, width);

			if (nesterUnit.childGraph != null)
			{
				var childContext = this.childContext;
				childContext.BeginEdit();
				height += EditorGUIUtility.standardVerticalSpacing * 2;
				height += LudiqGUI.GetInspectorHeight(this, graphMetadata, width, GUIContent.none);
				childContext.EndEdit();
			}

			return height;
		}

		protected override void OnInspectorGUI(Rect position)
		{
			var inspectorHeight = base.GetInspectorHeight(position.width);
			var nestHeight = LudiqGUI.GetEditorHeight(this, nestMetadata, position.width);

			y = position.y;
			base.OnInspectorGUI(position.VerticalSection(ref y, inspectorHeight));
			y += EditorGUIUtility.standardVerticalSpacing;
			LudiqGUI.Editor(nestMetadata, position.VerticalSection(ref y, nestHeight));

			if (nesterUnit.childGraph != null)
			{
				var childContext = this.childContext;
				childContext.BeginEdit();
				y += EditorGUIUtility.standardVerticalSpacing * 2;
				var graphHeight = LudiqGUI.GetInspectorHeight(this, graphMetadata, position.width);
				LudiqGUI.Inspector(graphMetadata, position.VerticalSection(ref y, graphHeight), GUIContent.none);
				childContext.EndEdit();
			}
		}
	}
}