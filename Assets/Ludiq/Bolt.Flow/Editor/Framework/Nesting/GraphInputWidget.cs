using System;
using Ludiq;
using System.Linq;

namespace Bolt
{
	[Widget(typeof(GraphInput))]
	public sealed class GraphInputWidget : UnitWidget<GraphInput>
	{
		public GraphInputWidget(FlowCanvas canvas, GraphInput unit) : base(canvas, unit) { }

		protected override NodeColorMix baseColor => NodeColorMix.TealReadable;
		
		protected override void OnDoubleClick()
		{
			if (unit.graph.zoom == 1)
			{
				var parentReference = reference.ParentReference(false);

				if (parentReference != null)
				{
					if (e.ctrlOrCmd)
					{
						GraphWindow.OpenTab(parentReference);
					}
					else
					{
						window.reference = parentReference;
					}
				}

				e.Use();
			}
			else
			{
				base.OnDoubleClick();
			}
		}

		public override bool CanCreateCompatiblePort(IUnitPort port)
		{
			return port is ValueInput || port is ControlInput;
		}

		public override IUnitPort CreateCompatiblePort(ref IUnitPort port)
		{
			UndoUtility.RecordEditedObject("Create Compatible Port");
			var definition = graph.AddCompatibleDefinition(ref port);
			var compatiblePort = unit.FindPortByKey(definition.key);
			return compatiblePort;
		}
	}
}