﻿using System;
using System.Linq;
using Ludiq;
using UnityEditor;
using UnityEngine;

namespace Bolt
{
	[Widget(typeof(SuperUnit))]
	public class SuperUnitWidget : NesterUnitWidget<SuperUnit>, IDragAndDropHandler
	{
		public SuperUnitWidget(FlowCanvas canvas, SuperUnit unit) : base(canvas, unit) { }

		protected override NodeColorMix baseColor
		{
			get
			{
				// TODO: Move to descriptor for optimization
				using (var recursion = Recursion.New(1))
				{
					if (unit.nest.graph?.GetUnitsRecursive(recursion).OfType<IEventUnit>().Any() ?? false)
					{
						return NodeColor.Green;
					}
				}

				return base.baseColor;
			}
		}

		public DragAndDropVisualMode dragAndDropVisualMode => DragAndDropVisualMode.Generic;

		public bool AcceptsDragAndDrop()
		{
			return DragAndDropUtility.Is<FlowMacro>();
		}

		public void PerformDragAndDrop()
		{
			UndoUtility.RecordEditedObject("Drag & Drop Macro");
			unit.nest.source = GraphSource.Macro;
			unit.nest.macro = DragAndDropUtility.Get<FlowMacro>();
			unit.nest.embed = null;
			unit.Define();
			GUI.changed = true;
		}

		public void UpdateDragAndDrop()
		{

		}

		public void DrawDragAndDropPreview()
		{
			GraphGUI.DrawTooltip(new Vector2(edgePosition.x, outerPosition.yMax), "Replace with: " + DragAndDropUtility.Get<FlowMacro>().name, typeof(FlowMacro).Icon());
		}

		public void ExitDragAndDrop()
		{

		}

		public override bool CanCreateCompatiblePort(IUnitPort port)
		{
			return unit.nest.graph != null;
		}

		public override IUnitPort CreateCompatiblePort(ref IUnitPort port)
		{
			var childGraph = unit.nest.graph;
			var childReference = reference.ChildReference(unit, true);
			var childContext = childReference.Context();
			childContext.BeginEdit();
			UndoUtility.RecordEditedObject("Create Compatible Port");
			var definition = childGraph.AddExternalCompatibleDefinition(ref port);
			var compatiblePort = unit.FindPortByKey(definition.key);
			childContext.EndEdit();
			return compatiblePort;
		}
	}
}