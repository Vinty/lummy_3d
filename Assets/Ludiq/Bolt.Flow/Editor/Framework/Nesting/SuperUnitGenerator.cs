﻿using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Bolt
{
	[Generator(typeof(SuperUnit))]
	public class SuperUnitGenerator : UnitGenerator<SuperUnit>, IListeningUnitGenerator
	{ 
		public SuperUnitGenerator(SuperUnit unit) : base(unit)
		{
		}

		public override void DeclareGraphMemberNames(GraphClassGenerationContext context)
		{
			base.DeclareGraphMemberNames(context);

			var generatedChildGraph = context.generationSystem.GenerateGraph(unit);
			var childGraphScriptClassName = generatedChildGraph.graphScript.classDeclaration.Name;
			var childGraphScriptMemberName = context.DeclareMemberName(unit, childGraphScriptClassName.FirstCharacterToLower());
			context.AliasMemberName(unit, "childGraphScript", childGraphScriptMemberName);

			context.requiredMachineEvents.AddRange(generatedChildGraph.graphScript.requiredMachineEvents);
		}

		public override IEnumerable<CodeCompositeTypeMember> GenerateMembers(GraphClassGenerationContext context)
		{
			foreach (var member in base.GenerateMembers(context)) yield return member;

			var generatedChildGraph = context.generationSystem.GenerateGraph(unit);
			var childGraphScriptClassName = generatedChildGraph.graphScript.classDeclaration.Name;

			yield return context.AddImplementationMember(new CodeFieldMember(CodeMemberModifiers.Private, CodeFactory.TypeRef(childGraphScriptClassName), context.ExpectMemberName(unit, "childGraphScript")));
		}

		public override IEnumerable<CodeStatement> GenerateConstructorStatements(FlowMethodGenerationContext context)
		{
			var generatedChildGraph = context.graphClassContext.generationSystem.GenerateGraph(unit);
			var childGraphScriptClassName = generatedChildGraph.graphScript.classDeclaration.Name;

			yield return context.InstanceField(unit, "childGraphScript").Assign(
				CodeFactory.TypeRef(childGraphScriptClassName).ObjectCreate(CodeFactory.VarRef(context.argumentLocals["machineScript"].Name))
			).Statement();
			yield return context.InstanceField(unit, "childGraphScript").Field("parent").Assign(CodeFactory.ThisRef).Statement();
		}

		public override IEnumerable<CodeStatement> GenerateDeserializerStatements(FlowMethodGenerationContext context)
		{
			var generatedChildGraph = context.graphClassContext.generationSystem.GenerateGraph(unit);
			var childGraphScriptClassName = generatedChildGraph.graphScript.classDeclaration.Name;

			yield return context.InstanceField(unit, "childGraphScript").Field("graphData").Assign(
				CodeFactory.VarRef("value").Method("CreateChildGraphData").Invoke(
					CodeFactory.VarRef("value").Field("definition").Field("units")
						.Index(CodeFactory.TypeRef(typeof(Guid)).ObjectCreate(CodeFactory.Primitive(unit.guid.ToString())))
						.Cast(CodeFactory.TypeRef(typeof(SuperUnit)))
				).Cast(CodeFactory.TypeRef(typeof(FlowGraphData)))
			).Statement();
		}

		public IEnumerable<CodeStatement> GenerateStartListeningStatements(FlowMethodGenerationContext context)
		{
			var generatedChildGraph = context.graphClassContext.generationSystem.GenerateGraph(unit);
			yield return context.InstanceField(unit, "childGraphScript").Method("StartListening").Invoke().Statement();
		}

		public IEnumerable<CodeStatement> GenerateStopListeningStatements(FlowMethodGenerationContext context)
		{
			var generatedChildGraph = context.graphClassContext.generationSystem.GenerateGraph(unit);
			yield return context.InstanceField(unit, "childGraphScript").Method("StopListening").Invoke().Statement();
		}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			if (unit.controlInputs.Contains(controlInput))
			{
				yield break;
			}
			else
			{
				throw new InvalidOperationException();
			}
		}

		public override IEnumerable<CodeStatement> GenerateControlInputSuccessor(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			if (unit.controlInputs.Contains(controlInput))
			{
				foreach (var nestedGraphUnit in unit.nest.graph.units)
				{
					if (nestedGraphUnit is GraphInput)
					{						
						int top = context.GetParentElementStackTop();

						context.EnterParentElement(unit);

						foreach (var statement in nestedGraphUnit.controlOutputs[controlInput.key].GenerateStatements(context))
						{
							yield return statement;
						}

						context.RestoreParentElementStackTop(top);

						break;
					}
				}
			}
			else
			{
				throw new InvalidOperationException();
			}
		}

		public override IEnumerable<CodeStatement> GenerateValueOutputInit(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (unit.valueOutputs.Contains(valueOutput))
			{
				foreach (var nestedGraphUnit in unit.nest.graph.units)
				{
					if (nestedGraphUnit is GraphOutput)
					{
						int top = context.GetParentElementStackTop();

						context.EnterParentElement(unit);

						foreach (var statement in nestedGraphUnit.valueInputs[valueOutput.key].GenerateValueInputDependencies(context))
						{
							yield return statement;
						}

						context.RestoreParentElementStackTop(top);

						break;
					}
				}
			}
			else
			{
				throw new InvalidOperationException();
			}
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (unit.valueOutputs.Contains(valueOutput))
			{
				foreach (var nestedGraphUnit in unit.nest.graph.units)
				{
					if (nestedGraphUnit is GraphOutput)
					{
						int top = context.GetParentElementStackTop();

						context.EnterParentElement(unit);

						var result = nestedGraphUnit.valueInputs[valueOutput.key].GenerateExpression(context);

						context.RestoreParentElementStackTop(top);

						return result;
					}
				}
			}

			throw new NotImplementedException();
		}
	}
}
