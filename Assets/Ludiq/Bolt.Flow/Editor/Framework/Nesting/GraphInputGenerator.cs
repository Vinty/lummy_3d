﻿using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Bolt
{
	[Generator(typeof(GraphInput))]
	public class GraphInputGenerator : UnitGenerator<GraphInput>
	{ 
		public GraphInputGenerator(GraphInput unit) : base(unit) {}

		public override void DeclareGraphMemberNames(GraphClassGenerationContext context)
		{
			base.DeclareGraphMemberNames(context);

			if (context.generationSystem.graphGenerationMode == GraphGenerationMode.Preview)
			{
				foreach (var controlOutput in unit.controlOutputs)
				{
					context.DeclareMemberName(unit, controlOutput.key.RemoveNonAlphanumeric());
				}
			}
		}

		public override IEnumerable<CodeCompositeTypeMember> GenerateMembers(GraphClassGenerationContext context)
		{
			foreach (var member in base.GenerateMembers(context)) yield return member;

			if (context.generationSystem.graphGenerationMode == GraphGenerationMode.Preview)
			{
				foreach (var controlOutput in unit.controlOutputs)
				{
					var method = new FlowMethodGenerationContext(context, false).GenerateMethod(
						CodeMemberModifiers.Public,
						CodeFactory.TypeRef(typeof(void)),
						context.ExpectMemberName(unit, controlOutput.key.RemoveNonAlphanumeric()),
						Enumerable.Empty<CodeParameterDeclaration>(),
						methodContext => GenerateEnterStatements(methodContext, controlOutput));
					method.Comments.Add(new CodeComment("(Inlined)"));

					yield return method;
				}
			}
		}

		public IEnumerable<CodeStatement> GenerateEnterStatements(FlowMethodGenerationContext context, ControlOutput controlOutput)
		{
			foreach (var next in controlOutput.GenerateStatements(context)) yield return next;
		}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			throw new NotImplementedException();
		}

		public override IEnumerable<CodeStatement> GenerateValueOutputInit(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (unit.valueOutputs.Contains(valueOutput))
			{
				if (context.canExitGraph)
				{
					var parent = (SuperUnit)context.currentParentElement;
					var valueInput = parent.valueInputs[valueOutput.key];
					if (valueInput.hasValidConnection)
					{
						context.ExitParentElement();

						foreach (var dependency in valueInput.GenerateValueInputDependencies(context))
						{
							yield return dependency;
						}

						context.EnterParentElement(parent);
					}
				}
			}
			else
			{
				throw new InvalidOperationException();
			}
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (unit.valueOutputs.Contains(valueOutput))
			{
				if (context.canExitGraph)
				{
					var parent = (SuperUnit)context.currentParentElement;
					var valueInput = parent.valueInputs[valueOutput.key];

					if (valueInput.hasValidConnection)
					{
						context.ExitParentElement();

						var result = valueInput.GenerateExpression(context);

						context.EnterParentElement(parent);
						return result;
					}
					else
					{
						throw new InvalidOperationException();
					}
				}
				else
				{
					return new CodeCommentExpression(new CodeComment(valueOutput.key.RemoveNonAlphanumeric(), CodeComment.CommentStyle.Block));
				}
			}
			else
			{
				throw new InvalidOperationException();
			}
		}
	}
}
