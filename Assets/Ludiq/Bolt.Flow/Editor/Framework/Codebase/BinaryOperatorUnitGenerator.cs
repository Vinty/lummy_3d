﻿using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Bolt
{
	[Generator(typeof(BinaryOperatorUnit))]
	public class BinaryOperatorUnitGenerator : UnitGenerator<BinaryOperatorUnit>
	{ 
		public BinaryOperatorUnitGenerator(BinaryOperatorUnit unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{			
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.result)			
			{
				return unit.a.GenerateExpression(context, unit.leftType).BinaryOp(OperatorGeneratorUtility.binaryOperatorToCode[unit.@operator], unit.b.GenerateExpression(context, unit.rightType));
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
