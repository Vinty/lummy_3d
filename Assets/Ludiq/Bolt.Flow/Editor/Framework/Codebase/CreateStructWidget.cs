﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ludiq;
using UnityEditor;
using UnityEngine;

namespace Bolt
{
	[Widget(typeof(CreateStruct))]
	public sealed class CreateStructWidget : UnitWidget<CreateStruct>
	{
		public CreateStructWidget(FlowCanvas canvas, CreateStruct unit) : base(canvas, unit) { }

		protected override bool showHeaderAddon => true;

		public override bool foregroundRequiresInput => true;

		protected override float GetHeaderAddonWidth()
		{
			return metadata.Inspector().GetAdaptiveWidth();
		}

		protected override float GetHeaderAddonHeight(float width)
		{
			return LudiqGUI.GetInspectorHeight(null, metadata, width, GUIContent.none);
		}
		
		protected override void DrawHeaderAddon()
		{
			using (LudiqGUIUtility.labelWidth.Override(Math.Max(75, GetHeaderAddonWidth()))) // For reflected inspectors / custom property drawers
			using (Inspector.adaptiveWidth.Override(true))
			{
				EditorGUI.BeginChangeCheck();
				
				LudiqGUI.Inspector(metadata, headerAddonPosition, GUIContent.none);

				if (EditorGUI.EndChangeCheck())
				{
					unit.Define();
					Reposition();
				}
			}
		}
	}
}