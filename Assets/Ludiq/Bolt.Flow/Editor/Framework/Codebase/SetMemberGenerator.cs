﻿using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Bolt
{
	[Generator(typeof(SetMember))]
	public class SetMemberGenerator : UnitGenerator<SetMember>
	{ 
		public SetMemberGenerator(SetMember unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			if (controlInput == unit.assign)
			{
				var targetExpression = unit.member.GenerateTargetExpression(context, unit.target);
				var assignmentSourceExpression = unit.input.GenerateExpression(context);

				if (unit.member.requiresTarget)
				{
					if (!targetExpression.IsValidAssignmentTerm() || unit.targetType.IsValueType)
					{
						var targetLocal = context.currentScope.DeclareValuePortLocal(CodeFactory.VarType, unit.target, targetExpression);

						yield return targetLocal;

						targetExpression = CodeFactory.VarRef(targetLocal.Name);
					}
				}

				if (unit.output.hasValidConnection)
				{
					var outputLocal = context.currentScope.DeclareValuePortLocal(CodeFactory.VarType, unit.output, assignmentSourceExpression);

					yield return outputLocal;

					assignmentSourceExpression = CodeFactory.VarRef(outputLocal.Name);
				}

				yield return unit.member.GenerateSet(context, targetExpression, assignmentSourceExpression).Statement();

			}
			else
			{			
				throw new NotImplementedException();
			}			
		}

		public override IEnumerable<CodeStatement> GenerateControlInputSuccessor(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			if (controlInput == unit.assign)
			{
				return unit.assigned.GenerateStatements(context);
			}
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.output)
			{
				return CodeFactory.VarRef(context.currentScope.ExpectValuePortLocal(unit.output));
			}
			else if (valueOutput == unit.targetOutput)
			{
				if (context.currentScope.TryGetValuePortLocal(unit.target, out var local))
				{
					return CodeFactory.VarRef(local);
				}
				else
				{
					return unit.member.GenerateTargetExpression(context, unit.target);
				}
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
