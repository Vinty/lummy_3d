﻿using System;
using System.Collections.Generic;
using Ludiq;
using UnityEditor;
using UnityEngine;

namespace Bolt
{
	public interface IMemberUnitOption : IUnitOption
	{
		Type targetType { get; }
		Member member { get; }
		Member pseudoDeclarer { get; }
	}

	public abstract class MemberUnitOption<TMemberUnit> : UnitOption<TMemberUnit>, IMemberUnitOption where TMemberUnit : MemberUnit
	{
		protected MemberUnitOption() : base() { }

		protected MemberUnitOption(TMemberUnit unit) : base(unit)
		{

		}

		private Member _member;

		private Member _pseudoDeclarer;

		public Member member
		{
			get => _member ?? unit.member;
			set => _member = value;
		}

		public Member pseudoDeclarer
		{
			get => _pseudoDeclarer ?? member.ToPseudoDeclarer();
			set => _pseudoDeclarer = value;
		}

		public bool isPseudoInherited => member == pseudoDeclarer;
		
		protected abstract ActionDirection direction { get; }

		public Type targetType { get; private set; }
		
		public override Type sourceType => targetType;

		protected override Type genericParentType => targetType;
		
		protected override GUIStyle Style()
		{
			if (unit.member.isPseudoInherited)
			{
				return FuzzyWindow.Styles.optionWithIconDim;
			}

			return base.Style();
		}

		protected override string Label(bool human)
		{
			return unit.member.info.SelectedName(human, direction);
		}

		protected override string Key()
		{
			return $"{member.ToUniqueString()}@{direction.ToString().ToLower()}";
		}

		protected override int Order()
		{
			if (member.isConstructor)
			{
				return 0;
			}

			return base.Order();
		}

		protected override string Haystack(bool human)
		{
			return $"{targetType.SelectedName(human)}{( human ? ": " : ".")}{Label(human)}";
		}

		protected override void FillSerializable()
		{
			targetType = unit.member.targetType;
			member = unit.member;
			pseudoDeclarer = member.ToPseudoDeclarer();

			base.FillSerializable();
		}

		public override void Deserialize(UnitOptionData data)
		{
			base.Deserialize(data);

			targetType = Codebase.DeserializeRootType(data.tag1);

			if (!string.IsNullOrEmpty(data.tag2))
			{
				member = Codebase.DeserializeMember(data.tag2);
			}

			if (!string.IsNullOrEmpty(data.tag3))
			{
				pseudoDeclarer = Codebase.DeserializeMember(data.tag3);
			}
		}

		public override UnitOptionData Serialize()
		{
			var row = base.Serialize();

			row.tag1 = Codebase.SerializeType(targetType);
			row.tag2 = Codebase.SerializeMember(member);
			row.tag3 = Codebase.SerializeMember(pseudoDeclarer);

			return row;
		}

		public override void OnPopulate()
		{
			// Members are late-reflected to speed up loading and search
			// We only reflect them when we're just about to populate their node
			// By doing it in OnPopulate instead of on-demand later, we ensure 
			// any error will be gracefully catched and shown as a warning by
			// the fuzzy window

			member.EnsureReflected();

			if (member == pseudoDeclarer)
			{
				pseudoDeclarer = member;
			}
			else
			{
				pseudoDeclarer.EnsureReflected();
			}

			base.OnPopulate();
		}
	}
}