﻿using System.Linq;
using Ludiq;

namespace Bolt
{
	[Descriptor(typeof(UnaryOperatorUnit))]
	public class UnaryOperatorUnitDescriptor : UnitDescriptor<UnaryOperatorUnit>
	{
		public UnaryOperatorUnitDescriptor(UnaryOperatorUnit unit) : base(unit) { }

		protected override string DefaultTitle() => $"{OperatorUtility.Verb(unit.@operator)} {unit.type.DisplayName()}";
		protected override string DefinedTitle() => DefaultTitle();

		protected override string DefaultShortTitle() => $"{OperatorUtility.Verb(unit.@operator)}";
		protected override string DefinedShortTitle() => DefaultShortTitle();

		protected override string DefaultSurtitle() => unit.type.DisplayName();
		protected override string DefinedSurtitle() => DefaultSurtitle();

		protected override string DefaultSummary() => string.Format(unit.handler.GetDescriptionFormat(unit.type), $"the given {unit.type.DisplayName()}");
		protected override string DefinedSummary() => DefaultSummary();

		protected override EditorTexture DefaultIcon()
		{
			var resource = BoltFlow.Icons.Operator(unit.@operator);

			if (resource != null)
			{
				return resource;
			}

			return base.DefaultIcon();
		}

		protected override EditorTexture DefinedIcon() => DefaultIcon();

		protected override void DefinedPort(IUnitPort port, UnitPortDescription description)
		{
			base.DefinedPort(port, description);

			if (port == unit.output)
			{
				description.label = $"{unit.handler.fancySymbol}A";
			}
		}
	}
}
