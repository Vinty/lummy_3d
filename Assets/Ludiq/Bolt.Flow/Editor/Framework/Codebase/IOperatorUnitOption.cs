﻿using System;
using Ludiq;

namespace Bolt
{
	public interface IOperatorUnitOption : IUnitOption
	{
		OperatorCategory operatorCategory { get; }
	}
}
