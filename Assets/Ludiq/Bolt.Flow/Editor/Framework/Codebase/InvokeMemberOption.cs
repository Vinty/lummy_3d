﻿using Ludiq;

namespace Bolt
{
	[FuzzyOption(typeof(InvokeMember))]
	public class InvokeMemberOption : MemberUnitOption<InvokeMember>
	{
		public InvokeMemberOption() : base() { }

		public InvokeMemberOption(InvokeMember unit) : base(unit) { }

		protected override ActionDirection direction => ActionDirection.Any;

		protected override string genericParentMethodName => member.name;

		public override string SearchResultLabel(string query)
		{
			return base.SearchResultLabel(query) + LudiqGUIUtility.DimString($" ({unit.member.methodBase.DisplayParameterString()})");
		}

		protected override string Label(bool human)
		{
			return base.Label(human) + LudiqGUIUtility.DimString($" ({unit.member.methodBase.SelectedParameterString(human)})");
		}

		protected override string Haystack(bool human)
		{
			if (!human && member.isConstructor)
			{
				return base.Label(human);
			}
			else
			{
				return $"{targetType.SelectedName(human)}{(human ? ": " : ".")}{base.Label(human)}";
			}
		}
	}
}