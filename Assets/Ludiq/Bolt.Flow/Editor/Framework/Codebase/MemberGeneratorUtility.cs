﻿using Ludiq;
using Ludiq.CodeDom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bolt
{
	public static class MemberGeneratorUtility
	{
		public static CodeExpression GenerateTargetExpression(this Member member, FlowMethodGenerationContext context, ValueInput target)
		{
			var targetExpression = member.requiresTarget
				? target.GenerateExpression(context)
				: CodeFactory.TypeRef(member.targetType).Expression();

			if (targetExpression is CodePrimitiveExpression targetPrimitive && targetPrimitive.Value == null)
			{
				throw new InvalidOperationException($"value used for target cannot be null");
			}

			return targetExpression;
		}

		public static CodeExpression GenerateGet(this Member member, FlowMethodGenerationContext context, CodeExpression targetExpression)
		{
			if (!member.isGettable)
			{
				throw new InvalidOperationException("member used for GetMember unit must be gettable");
			}

			context.graphClassContext.usings.Add(new CodeUsingImport(member.targetType.Namespace));
			if (member.isExtension)
			{
				context.graphClassContext.usings.Add(new CodeUsingImport(member.methodInfo.DeclaringType.Namespace));
			}

			if (member.source == Member.Source.Constructor)
			{
				return CodeFactory.TypeRef(member.type).ObjectCreate();
			}
			else
			{
				if (targetExpression is CodePrimitiveExpression targetPrimitive && targetPrimitive.Value == null)
				{
					throw new InvalidOperationException($"value used for target cannot be null");
				}

				switch(member.source)
				{
					case Member.Source.Field: return targetExpression.Field(member.name);
					case Member.Source.Property: return targetExpression.Field(member.name);
					case Member.Source.Method: return targetExpression.Method(member.name).Invoke();
					default: throw new NotImplementedException();
				}
			}
		}

		public static CodeExpression GenerateSet(this Member member, FlowMethodGenerationContext context, CodeExpression targetExpression, CodeExpression assignmentSourceExpression)
		{
			if (!member.isSettable)
			{
				throw new InvalidOperationException("member used for SetMember unit must be settable");
			}

			context.graphClassContext.usings.Add(new CodeUsingImport(member.targetType.Namespace));
			if (member.isExtension)
			{
				context.graphClassContext.usings.Add(new CodeUsingImport(member.methodInfo.DeclaringType.Namespace));
			}

			switch(member.source)
			{
				case Member.Source.Field:
				case Member.Source.Property:
					return targetExpression.Field(member.name).Assign(assignmentSourceExpression);
				default: throw new NotImplementedException();
			}
		}

		public static readonly Dictionary<string, Func<CodeExpression[], CodeExpression>> specialMethodRewriters = new Dictionary<string, Func<CodeExpression[], CodeExpression>>
		{
			{"op_Addition", a => a[0].Add(a[1])},
			{"op_Subtraction", a => a[0].Subtract(a[1])},
			{"op_Multiply", a => a[0].Multiply(a[1])},
			{"op_Division", a => a[0].Divide(a[1])},
			{"op_Modulus", a => a[0].Modulo(a[1])},
			{"op_ExclusiveOr", a => a[0].BitwiseXor(a[1])},
			{"op_BitwiseAnd", a => a[0].BitwiseAnd(a[1])},
			{"op_BitwiseOr", a => a[0].BitwiseOr(a[1])},
			{"op_LogicalAnd", a => a[0].LogicalAnd(a[1])},
			{"op_LogicalOr", a => a[0].LogicalOr(a[1])},
			{"op_Assign", a => a[0].Assign(a[1])},
			{"op_LeftShift", a => a[0].BitwiseShiftLeft(a[1])},
			{"op_RightShift", a => a[0].BitwiseShiftRight(a[1])},
			{"op_Equality", a => a[0].Equal(a[1])},
			{"op_GreaterThan", a => a[0].GreaterThan(a[1])},
			{"op_LessThan", a => a[0].LessThan(a[1])},
			{"op_Inequality", a => a[0].NotEqual(a[1])},
			{"op_GreaterThanOrEqual", a => a[0].GreaterThanOrEqual(a[1])},
			{"op_LessThanOrEqual", a => a[0].LessThanOrEqual(a[1])},
			{"op_MultiplicationAssignment", a => a[0].MultiplyAssign(a[1])},
			{"op_SubtractionAssignment", a => a[0].SubtractAssign(a[1])},
			{"op_ExclusiveOrAssignment", a => a[0].BitwiseXorAssign(a[1])},
			{"op_LeftShiftAssignment", a => a[0].BitwiseShiftLeftAssign(a[1])},
			{"op_RightShiftAssignment", a => a[0].BitwiseShiftRightAssign(a[1])},
			{"op_ModulusAssignment", a => a[0].ModuloAssign(a[1])},
			{"op_AdditionAssignment", a => a[0].AddAssign(a[1])},
			{"op_BitwiseAndAssignment", a => a[0].BitwiseAndAssign(a[1])},
			{"op_BitwiseOrAssignment", a => a[0].BitwiseOrAssign(a[1])},
			{"op_DivisionAssignment", a => a[0].DivideAssign(a[1])},
			{"op_Decrement", a => a[0].PreDecrement()},
			{"op_Increment", a => a[0].PreIncrement()},
			{"op_UnaryNegation", a => a[0].Negative()},
			{"op_UnaryPlus", a => a[0].Positive()},
			{"op_OnesComplement", a => a[0].BitwiseNot()},
		};

		public static CodeExpression RewriteSpecialMethodExpression(string name, CodeExpression[] arguments)
		{
			return specialMethodRewriters[name](arguments);
		}
	}
}
