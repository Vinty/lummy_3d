﻿using System.Linq;
using Ludiq;

namespace Bolt
{
	[Descriptor(typeof(BinaryOperatorUnit))]
	public class BinaryOperatorUnitDescriptor : UnitDescriptor<BinaryOperatorUnit>
	{
		public BinaryOperatorUnitDescriptor(BinaryOperatorUnit unit) : base(unit) { }

		private string CompactSignature()
		{
			return $"({unit.leftType.DisplayName()}, {unit.rightType.DisplayName()})";
		}
		
		protected override string DefinedTitle() => $"{OperatorUtility.Verb(unit.@operator)} {CompactSignature()}";
		
		protected override string DefinedShortTitle() => OperatorUtility.Verb(unit.@operator);
		
		protected override string DefinedSurtitle() => CompactSignature();

		protected override string DefaultSummary()
		{
			return string.Format(
				unit.handler.GetDescriptionFormat(unit.leftType, unit.rightType),
				unit.leftType == unit.rightType
					? $"two {unit.leftType.DisplayName()} inputs"
					: $"one {unit.leftType.DisplayName()} and one {unit.rightType.DisplayName()}");
		}

		protected override string DefinedSummary() => DefaultSummary();

		protected override EditorTexture DefaultIcon()
		{
			var resource = BoltFlow.Icons.Operator(unit.@operator);

			if (resource != null)
			{
				return resource;
			}

			return base.DefaultIcon();
		}

		protected override EditorTexture DefinedIcon() => DefaultIcon();

		protected override void DefinedPort(IUnitPort port, UnitPortDescription description)
		{
			base.DefinedPort(port, description);

			if (port == unit.result)
			{
				description.label = $"A {unit.handler.fancySymbol} B";
			}
		}
	}
}
