﻿using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Bolt
{
	[Generator(typeof(InvokeMember))]
	public class InvokeMemberGenerator : UnitGenerator<InvokeMember>
	{ 
		public InvokeMemberGenerator(InvokeMember unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			if (controlInput == unit.enter)
			{
				foreach (var statement in DeclareArgumentLocals(context))
				{
					yield return statement;
				}

				var expression = GenerateInvokeExpression(context);

				if (unit.member.isGettable && unit.result.hasValidConnection)
				{
					yield return context.currentScope.DeclareValuePortLocal(CodeFactory.VarType, unit.result, expression);
				}
				else
				{
					yield return expression.Statement();
				}
			}
			else
			{			
				throw new NotImplementedException();
			}
		}

		public override IEnumerable<CodeStatement> GenerateControlInputSuccessor(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			if (controlInput == unit.enter)
			{
				return unit.exit.GenerateStatements(context);
			}
			throw new NotImplementedException();
		}

		private IEnumerable<CodeStatement> DeclareArgumentLocals(FlowMethodGenerationContext context)
		{
			int parameterIndex = 0;

			var member = unit.member;

			foreach (var parameterInfo in member.methodBase.GetParametersWithoutThis())
			{
				if (parameterInfo.ParameterType.IsByRef)
				{
					var referenceRemovedParameterType = parameterInfo.ParameterType.GetElementType();
					var initializer = parameterInfo.IsOut
						? CodeFactory.TypeRef(referenceRemovedParameterType).DefaultValue()
						: unit.inputParameters[parameterIndex].GenerateExpression(context, referenceRemovedParameterType);
					
					yield return context.currentScope.DeclareValuePortLocal(CodeFactory.TypeRef(referenceRemovedParameterType), unit.outputParameters[parameterIndex], initializer);
				}
				else if (member.operatorCategory == OperatorCategory.Incrementation)
				{
					yield return context.currentScope.DeclareValuePortLocal(CodeFactory.TypeRef(parameterInfo.ParameterType), unit.inputParameters[parameterIndex], unit.inputParameters[parameterIndex].GenerateExpression(context, parameterInfo.ParameterType));
				}

				parameterIndex++;
			}
		}

		public override IEnumerable<CodeStatement> GenerateValueOutputInit(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.result)
			{
				if (!context.currentScope.HasValuePortLocal(valueOutput))
				{
					foreach (var statement in DeclareArgumentLocals(context))
					{
						yield return statement;
					}
				}
			}
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.result)
			{
				if (context.currentScope.TryGetValuePortLocal(valueOutput, out string local))
				{
					return CodeFactory.VarRef(local);
				}
				else
				{
					return GenerateInvokeExpression(context);
				}
			}
			else if (valueOutput == unit.targetOutput)
			{
				return unit.target.GenerateExpression(context);
			}
			else if (unit.outputParameters.ContainsValue(valueOutput))
			{
				return CodeFactory.VarRef(context.currentScope.ExpectValuePortLocal(valueOutput));
			}
			else
			{
				throw new NotImplementedException();
			}
		}

		public CodeExpression GenerateInvokeExpression(FlowMethodGenerationContext context)
		{
			var member = unit.member;

			if (!member.isInvocable)
			{
				throw new InvalidOperationException("member used for InvokeMember unit must be invocable.");
			}

			context.graphClassContext.usings.Add(new CodeUsingImport(member.targetType.Namespace));

			if (member.isExtension)
			{
				context.graphClassContext.usings.Add(new CodeUsingImport(member.methodInfo.DeclaringType.Namespace));
			}

			var argumentExpressions = member.methodBase.GetParametersWithoutThis().Select((parameterInfo, index) => GenerateArgumentExpression(context, parameterInfo, index));

			switch(member.source)
			{
				case Member.Source.Method:
				{
					var targetExpression = member.requiresTarget
						? unit.target.GenerateExpression(context)
						: CodeFactory.TypeRef(member.targetType).Expression();
					
					if (targetExpression is CodePrimitiveExpression targetPrimitive && targetPrimitive.Value == null)
					{
						throw new InvalidOperationException($"value used for {unit} target cannot be null");
					}

					if (TrySimplifyConcat(member, argumentExpressions, out var simplifiedConcatExpression))
					{
						return simplifiedConcatExpression;
					}

					if (member.isOperator)
					{
						return MemberGeneratorUtility.RewriteSpecialMethodExpression(member.name, argumentExpressions.ToArray());
					}
				
					return targetExpression.Method(member.name).Invoke(argumentExpressions);
				}
				case Member.Source.Constructor: return CodeFactory.TypeRef(member.type).ObjectCreate(argumentExpressions);
				default: throw new NotImplementedException();
			}
		}
		
		public CodeExpression GenerateArgumentExpression(FlowMethodGenerationContext context, ParameterInfo parameterInfo, int index)
		{
			if (parameterInfo.IsOut)
			{
				return CodeFactory.OutArgument(CodeFactory.VarRef(context.currentScope.ExpectValuePortLocal(unit.outputParameters[index])));
			}
			else if (parameterInfo.ParameterType.IsByRef)
			{
				return CodeFactory.RefArgument(CodeFactory.VarRef(context.currentScope.ExpectValuePortLocal(unit.outputParameters[index])));
			}
			else
			{
				var valueInput = unit.inputParameters[index];

				if (context.currentScope.TryGetValuePortLocal(valueInput, out string local))
				{
					return CodeFactory.VarRef(local);
				}
				else
				{
					return valueInput.GenerateExpression(context);
				}
			}
		}

		public bool TrySimplifyConcat(Member member, IEnumerable<CodeExpression> argumentExpressions, out CodeExpression simplifiedExpression)
		{
			if (!member.requiresTarget && member.targetType == typeof(string) && member.name == "Concat")
			{
				var methodBase = member.methodBase;
				var parameters = methodBase.GetParameters();

				var isParamsArrayOverload = (parameters.Length == 1 && parameters[0].HasAttribute(typeof(ParamArrayAttribute), false));					
				var isStringOverload = (!isParamsArrayOverload && parameters.All(x => x.ParameterType == typeof(string))) || (isParamsArrayOverload && parameters[0].ParameterType == typeof(string[]));
				var isObjectOverload = (!isParamsArrayOverload && parameters.All(x => x.ParameterType == typeof(object))) || (isParamsArrayOverload && parameters[0].ParameterType == typeof(object[]));

				if (isStringOverload || isObjectOverload)
				{
					if (isObjectOverload && !argumentExpressions.All(expression => expression is CodePrimitiveExpression primitive && primitive.Value is string))
					{
						simplifiedExpression = null;
						return false;
					}

					CodeExpression resultExpression = null;
					CodePrimitiveExpression intermediatePrimitive = null;

					foreach (var argumentExpression in argumentExpressions)
					{
						bool added = false;

						if (argumentExpression is CodePrimitiveExpression argumentPrimitive)
						{
							if (argumentPrimitive.Value is string parameterValue)
							{
								intermediatePrimitive = intermediatePrimitive != null ? CodeFactory.Primitive((string)intermediatePrimitive.Value + parameterValue) : argumentPrimitive;
								added = true;
							}
						}
						else 
						{
							if (argumentExpression is CodeBinaryOperatorExpression binaryOperatorExpression)
							{
								if (binaryOperatorExpression.Operator == CodeBinaryOperatorType.Add)
								{
									if (binaryOperatorExpression.Left is CodePrimitiveExpression leftPrimitive)
									{
										if (leftPrimitive.Value is string leftValue)
										{
											intermediatePrimitive = intermediatePrimitive != null ? CodeFactory.Primitive((string)intermediatePrimitive.Value + leftValue) : leftPrimitive;
											resultExpression = resultExpression != null ? (CodeExpression)resultExpression.Add(intermediatePrimitive) : intermediatePrimitive;
											intermediatePrimitive = null;
													
											resultExpression = resultExpression.Add(binaryOperatorExpression.Right);
										}
									}
									else if (binaryOperatorExpression.Right is CodePrimitiveExpression rightPrimitive)
									{
										if (rightPrimitive.Value is string rightValue)
										{
											if (intermediatePrimitive != null)
											{
												resultExpression = resultExpression != null ? (CodeExpression)resultExpression.Add(intermediatePrimitive) : intermediatePrimitive;
												intermediatePrimitive = null;
											}

											resultExpression = resultExpression != null ? (CodeExpression)resultExpression.Add(binaryOperatorExpression.Left) : binaryOperatorExpression.Left;

											intermediatePrimitive = rightPrimitive;
											added = true;
										}
									}
								}
							}
						}
								
						if (!added)
						{
							if (intermediatePrimitive != null)
							{
								resultExpression = resultExpression != null ? (CodeExpression)resultExpression.Add(intermediatePrimitive) : intermediatePrimitive;
								intermediatePrimitive = null;
							}

							resultExpression = resultExpression != null ? resultExpression.Add(argumentExpression) : argumentExpression;
						}
					}

					if (intermediatePrimitive != null)
					{
						resultExpression = resultExpression != null ? (CodeExpression)resultExpression.Add(intermediatePrimitive) : intermediatePrimitive;
						intermediatePrimitive = null;
					}

					simplifiedExpression = resultExpression;
					return true;
				}
			}

			simplifiedExpression = null;
			return false;
		}
	}
}
