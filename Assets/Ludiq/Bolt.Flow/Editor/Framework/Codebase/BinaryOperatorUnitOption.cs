﻿using Ludiq;
using System;

namespace Bolt
{
	[FuzzyOption(typeof(BinaryOperatorUnit))]
	public class BinaryOperatorUnitOption : UnitOption<BinaryOperatorUnit>, IOperatorUnitOption
	{
		public BinaryOperatorUnitOption() : base() { }

		public BinaryOperatorUnitOption(BinaryOperatorUnit unit) : base(unit) { }

		public BinaryOperator @operator { get; private set; }
		public Type leftType { get; private set; }
		public Type rightType { get; private set; }
		public Type resultType { get; private set; }
		public OperatorCategory operatorCategory { get; private set; }

		public int uncategorizedSortRank => (int) @operator * 200 + (leftType == rightType ? 1 : 50);
		
		protected override void FillSerializable()
		{
			@operator = unit.@operator;
			leftType = unit.leftType;
			rightType = unit.rightType;
			resultType = unit.resultType;
			operatorCategory = @operator.GetOperatorCategory();
			base.FillSerializable();
		}
		
		public override Type sourceType => resultType;

		protected override string Label(bool human)
		{
			if (unit.leftType == unit.rightType)
			{
				return OperatorUtility.Verb(unit.@operator);
			}

			return Haystack(human);
		}

		protected override string Haystack(bool human) => $"{OperatorUtility.Verb(@operator)} ({leftType.SelectedName(human)}, {rightType.SelectedName(human)})";

		public override string SearchResultLabel(string query)
		{
			return $"{SearchUtility.HighlightQuery(OperatorUtility.Verb(@operator), query)} ({leftType.DisplayName()}, {rightType.DisplayName()})";
		}

		protected override string Key() => $"{@operator.ToString()}({leftType.FullName},{rightType.FullName})@operator";

		public override void Deserialize(UnitOptionData data)
		{
			base.Deserialize(data);

			@operator = (BinaryOperator)Enum.Parse(typeof(BinaryOperator), data.tag1);
			leftType = Codebase.DeserializeRootType(data.tag2);
			rightType = Codebase.DeserializeRootType(data.tag3);

			if (@operator.GetHandler().TryGetResultType(leftType, rightType, out var resultType))
			{
				this.resultType = resultType;
			}

			operatorCategory = @operator.GetOperatorCategory();
		}

		public override UnitOptionData Serialize()
		{
			var row = base.Serialize();

			row.tag1 = @operator.ToString();
			row.tag2 = Codebase.SerializeType(leftType);
			row.tag3 = Codebase.SerializeType(rightType);

			return row;
		}
	}
}