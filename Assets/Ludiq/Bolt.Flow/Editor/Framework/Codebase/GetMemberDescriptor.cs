﻿using Ludiq;

namespace Bolt
{
	[Descriptor(typeof(GetMember))]
	public class GetMemberDescriptor : MemberUnitDescriptor<GetMember>
	{
		public GetMemberDescriptor(GetMember unit) : base(unit) { }

		protected override ActionDirection direction => ActionDirection.Get;
		
		protected override string DefinedShortTitle()
		{
			// Hide Get for more compactness
			return unit.member.info.DisplayName(ActionDirection.Any);
		}

		protected override void DefinedPort(IUnitPort port, UnitPortDescription description)
		{
			base.DefinedPort(port, description);

			if (port == unit.value)
			{
				description.summary = unit.member.info.Summary();
			}
			else if (port == unit.target)
			{
				description.primaryAxes = Axes2.Both;
			}
		}
	}
}