﻿using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Bolt
{
	[Generator(typeof(Expose))]
	public class ExposeGenerator : UnitGenerator<Expose>
	{ 
		public ExposeGenerator(Expose unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{	
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (unit.members.TryGetValue(valueOutput, out var member))
			{
				return member.GenerateGet(context, member.GenerateTargetExpression(context, unit.target));
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
