﻿using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Bolt
{
	[Generator(typeof(CreateStruct))]
	public class CreateStructGenerator : UnitGenerator<CreateStruct>
	{ 
		public CreateStructGenerator(CreateStruct unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{	
			if (controlInput == unit.enter)
			{
				return Enumerable.Empty<CodeStatement>();
			}
			else
			{
				throw new NotImplementedException();
			}
		}

		public override IEnumerable<CodeStatement> GenerateControlInputSuccessor(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			if (controlInput == unit.enter)
			{
				return unit.exit.GenerateStatements(context);
			}
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.output)
			{
				return CodeFactory.TypeRef(unit.type).ObjectCreate();
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
