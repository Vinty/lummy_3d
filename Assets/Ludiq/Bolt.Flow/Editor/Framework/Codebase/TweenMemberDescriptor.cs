﻿using Ludiq;

namespace Bolt
{
	[Descriptor(typeof(TweenMember))]
	public class TweenMemberDescriptor : MemberUnitDescriptor<TweenMember>
	{
		public TweenMemberDescriptor(TweenMember unit) : base(unit) { }

		protected override ActionDirection direction => ActionDirection.Any;

		protected override string Name()
		{
			return $"Tween {unit.member.info.DisplayName()}";
		}

		protected override void DefinedPort(IUnitPort port, UnitPortDescription description)
		{
			base.DefinedPort(port, description);

			if (port == unit.target)
			{
				description.primaryAxes = Axes2.Both;
			}
		}
	}
}