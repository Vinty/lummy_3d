﻿using Ludiq;
using System;

namespace Bolt
{
	[FuzzyOption(typeof(UnaryOperatorUnit))]
	public class UnaryOperatorUnitOption : UnitOption<UnaryOperatorUnit>, IOperatorUnitOption
	{
		public UnaryOperatorUnitOption() : base() { }

		public UnaryOperatorUnitOption(UnaryOperatorUnit unit) : base(unit) { }

		public UnaryOperator @operator { get; private set; }
		public Type type { get; private set; }
		public OperatorCategory operatorCategory { get; private set; }

		protected override void FillSerializable()
		{
			@operator = unit.@operator;
			type = unit.type;
			operatorCategory = unit.@operator.GetOperatorCategory();
			base.FillSerializable();
		}
		
		public override Type sourceType => type;

		protected override string Label(bool human) => $"{OperatorUtility.Verb(@operator)}";

		protected override string Haystack(bool human) => $"{OperatorUtility.Verb(unit.@operator)} ({type.SelectedName(human)})";

		public override string SearchResultLabel(string query)
		{
			return $"{SearchUtility.HighlightQuery(OperatorUtility.Verb(unit.@operator), query)} ({type.DisplayName()})";
		}

		protected override string Key() => $"{@operator.ToString()}({type.FullName})@operator";

		public override void Deserialize(UnitOptionData data)
		{
			base.Deserialize(data);

			@operator = (UnaryOperator)Enum.Parse(typeof(UnaryOperator), data.tag1);
			type = Codebase.DeserializeRootType(data.tag2);
			operatorCategory = unit.@operator.GetOperatorCategory();
		}

		public override UnitOptionData Serialize()
		{
			var row = base.Serialize();

			row.tag1 = @operator.ToString();
			row.tag2 = Codebase.SerializeType(type);

			return row;
		}
	}
}