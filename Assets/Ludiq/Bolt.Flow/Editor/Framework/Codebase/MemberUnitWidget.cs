﻿using System;
using System.Collections.Generic;
using Ludiq;
using UnityEditor;
using UnityEngine;

namespace Bolt
{
	[Widget(typeof(MemberUnit))]
	public sealed class MemberUnitWidget : UnitWidget<MemberUnit>
	{
		public MemberUnitWidget(FlowCanvas canvas, MemberUnit unit) : base(canvas, unit) { }

		protected override bool showHeaderAddon => unit.targetType.ContainsGenericParameters;

		public override bool foregroundRequiresInput => true;

		private Metadata targetTypeMetadata => metadata[nameof(MemberUnit.targetType)];

		protected override IEnumerable<DropdownOption> individualContextOptions
		{
			get
			{
				yield return new DropdownOption((Action)OverloadUnit, "Overloads...");

				foreach (var baseOption in base.individualContextOptions)
				{
					yield return baseOption;
				}
			}
		}

		protected override float GetHeaderAddonWidth()
		{
			return targetTypeMetadata.Inspector().GetAdaptiveWidth();
		}

		protected override float GetHeaderAddonHeight(float width)
		{
			return LudiqGUI.GetInspectorHeight(null, targetTypeMetadata, width, GUIContent.none);
		}
		
		protected override void DrawHeaderAddon()
		{
			using (LudiqGUIUtility.labelWidth.Override(Math.Max(75, GetHeaderAddonWidth()))) // For reflected inspectors / custom property drawers
			using (Inspector.adaptiveWidth.Override(true))
			{
				EditorGUI.BeginChangeCheck();
				
				LudiqGUI.Inspector(targetTypeMetadata, headerAddonPosition, GUIContent.none);

				if (EditorGUI.EndChangeCheck())
				{
					unit.Define();
					Reposition();
				}
			}
		}

		private void OverloadUnit()
		{
			ReplaceUnit("Overloads", overloadMember: unit.member);
		}
	}
}