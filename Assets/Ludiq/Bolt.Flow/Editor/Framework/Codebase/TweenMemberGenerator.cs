﻿using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using DG.Tweening;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(TweenMember))]
	public class TweenMemberGenerator : UnitGenerator<TweenMember>
	{ 
		public TweenMemberGenerator(TweenMember unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{			
			if (controlInput == unit.enter)
			{
				if (!TweenMember.tweenFactoriesByType.ContainsKey(unit.member.type))
				{
					throw new NotSupportedException($"Member of type {unit.member.type} does not support tweening.");
				}

				context.graphClassContext.usings.Add(new CodeUsingImport("DG.Tweening"));
				
				var targetExpression = unit.member.GenerateTargetExpression(context, unit.target);

				if (targetExpression != null && !targetExpression.IsValidAssignmentTerm())
				{
					var targetLocal = context.currentScope.DeclareValuePortLocal(CodeFactory.VarType, unit.target, targetExpression);

					yield return targetLocal;

					targetExpression = CodeFactory.VarRef(targetLocal.Name);
				}

				CodeExpression tweenExpression = CodeFactory.TypeRef(typeof(DOTween)).Expression().Method("To").Invoke(
					CodeFactory.Lambda(Enumerable.Empty<CodeParameterDeclaration>(), unit.member.GenerateGet(context, targetExpression)),
					CodeFactory.Lambda(new[] { new CodeParameterDeclaration(CodeFactory.VarType, "x") }, unit.member.GenerateSet(context, targetExpression, CodeFactory.VarRef("x"))),
					unit.value.GenerateExpression(context, unit.member.type),
					unit.duration.GenerateExpression(context, typeof(float))
				);

				switch (unit.easingMode)
				{
					case TweenMember.EasingMode.Predefined: tweenExpression = tweenExpression.Method("SetEase").Invoke(unit.ease.GenerateExpression(context, typeof(Ease))); break;
					case TweenMember.EasingMode.AnimationCurve: tweenExpression = tweenExpression.Method("SetEase").Invoke(unit.ease.GenerateExpression(context, typeof(AnimationCurve))); break;
					default: throw new UnexpectedEnumValueException<TweenMember.EasingMode>(unit.easingMode);
				}

				CodeVariableDeclarationStatement tweenDeclaration = null;

				if (unit.advanced)
				{
					tweenExpression = tweenExpression
						.Method("SetLoops").Invoke(unit.loops.GenerateExpression(context, typeof(int)), unit.loopType.GenerateExpression(context, typeof(LoopType)))
						.Method("SetRelative").Invoke(unit.relative.GenerateExpression(context, typeof(bool)))
						.Method("SetAutoKill").Invoke(unit.autokill.GenerateExpression(context, typeof(bool)))
						.Method("SetRecyclable").Invoke(unit.recyclable.GenerateExpression(context, typeof(bool)))
						.Method("SetUpdate").Invoke(unit.updateType.GenerateExpression(context, typeof(UpdateType)));

					var idExpression = unit.id.GenerateExpression(context, typeof(string));

					if (idExpression is CodePrimitiveExpression idPrimitive && idPrimitive.Value is string idString)
					{
						if (!string.IsNullOrEmpty(idString))
						{
							tweenExpression = tweenExpression.Method("SetId").Invoke(idExpression);
						}
					}
					else
					{
						var idDeclaration = context.currentScope.DeclareLocal(CodeFactory.VarType, "id", idExpression);
						tweenDeclaration = context.currentScope.DeclareLocal(CodeFactory.VarType, "tween", tweenExpression);

						yield return idDeclaration;
						yield return tweenDeclaration;

						tweenExpression = CodeFactory.VarRef(tweenDeclaration.Name);

						yield return new CodeIfStatement(
							CodeFactory.TypeRef(typeof(string)).Expression().Method("IsNullOrEmpty").Invoke(CodeFactory.VarRef(idDeclaration.Name)).LogicalNot(),
							new [] {
								tweenExpression.Method("SetId").Invoke(CodeFactory.VarRef(idDeclaration.Name)).Statement()
							});
					}
				}

				var needsCallbackBinding = unit.onComplete.hasValidConnection;

				if (unit.advanced)
				{
					needsCallbackBinding = needsCallbackBinding
						|| unit.onKill.hasValidConnection
						|| unit.onPlay.hasValidConnection
						|| unit.onPause.hasValidConnection
						|| unit.onRewind.hasValidConnection
						|| unit.onStart.hasValidConnection
						|| unit.onStepComplete.hasValidConnection
						|| unit.update.hasValidConnection;
				}

				if (unit.tween.hasValidConnection || needsCallbackBinding)
				{
					if (tweenDeclaration != null)
					{
						context.currentScope.AliasValuePortLocal(unit.tween, "tween");
					}
					else
					{
						tweenDeclaration = context.currentScope.DeclareValuePortLocal(CodeFactory.VarType, unit.tween, tweenExpression);

						yield return tweenDeclaration;

						tweenExpression = CodeFactory.VarRef(tweenDeclaration.Name);
					}
				}
				else
				{
					yield return tweenExpression.Statement();				
				}

				if (unit.onComplete.hasValidConnection)
				{
					tweenExpression = tweenExpression.Method("OnComplete").Invoke(new CodeLambdaExpression(Enumerable.Empty<CodeParameterDeclaration>(), unit.onComplete.GenerateStatements(context)));
				}

				if (unit.advanced)
				{
					if (unit.onKill.hasValidConnection)
					{
						tweenExpression = tweenExpression.Method("OnKill").Invoke(new CodeLambdaExpression(Enumerable.Empty<CodeParameterDeclaration>(), unit.onKill.GenerateStatements(context)));
					}

					if (unit.onPlay.hasValidConnection)
					{
						tweenExpression = tweenExpression.Method("OnPlay").Invoke(new CodeLambdaExpression(Enumerable.Empty<CodeParameterDeclaration>(), unit.onPlay.GenerateStatements(context)));
					}

					if (unit.onPause.hasValidConnection)
					{
						tweenExpression = tweenExpression.Method("OnPause").Invoke(new CodeLambdaExpression(Enumerable.Empty<CodeParameterDeclaration>(), unit.onPause.GenerateStatements(context)));
					}

					if (unit.onRewind.hasValidConnection)
					{
						tweenExpression = tweenExpression.Method("OnRewind").Invoke(new CodeLambdaExpression(Enumerable.Empty<CodeParameterDeclaration>(), unit.onRewind.GenerateStatements(context)));
					}

					if (unit.onStart.hasValidConnection)
					{
						tweenExpression = tweenExpression.Method("OnStart").Invoke(new CodeLambdaExpression(Enumerable.Empty<CodeParameterDeclaration>(), unit.onStart.GenerateStatements(context)));
					}

					if (unit.onStepComplete.hasValidConnection)
					{
						tweenExpression = tweenExpression.Method("OnStepComplete").Invoke(new CodeLambdaExpression(Enumerable.Empty<CodeParameterDeclaration>(), unit.onStepComplete.GenerateStatements(context)));
					}

					if (unit.update.hasValidConnection)
					{
						tweenExpression = tweenExpression.Method("OnUpdate").Invoke(new CodeLambdaExpression(Enumerable.Empty<CodeParameterDeclaration>(), unit.update.GenerateStatements(context)));
					}
				}

				if (needsCallbackBinding)
				{
					yield return tweenExpression.Statement();				
				}
			}
			else
			{
				throw new NotImplementedException();
			}
		}

		public override IEnumerable<CodeStatement> GenerateControlInputSuccessor(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			if (controlInput == unit.enter)
			{
				foreach (var next in unit.exit.GenerateStatements(context)) yield return next;
			}
			else
			{
				throw new NotImplementedException();
			}
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.tween)
			{
				return CodeFactory.VarRef(context.currentScope.ExpectValuePortLocal(valueOutput));
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}
}
