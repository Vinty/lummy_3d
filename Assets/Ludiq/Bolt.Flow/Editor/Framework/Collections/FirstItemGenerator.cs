﻿using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Bolt
{
	[Generator(typeof(FirstItem))]
	public class FirstItemGenerator : UnitGenerator<FirstItem>
	{
		public FirstItemGenerator(FirstItem unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.firstItem)
			{
				var sourceType = unit.collection.GetSourceType();
				if (typeof(IList).IsAssignableFrom(sourceType))
				{
					return unit.collection.GenerateExpression(context, typeof(IList)).Index(CodeFactory.Primitive(0));
				}
				else
				{
					return unit.collection.GenerateExpression(context, typeof(IEnumerable)).Method("First").Invoke();
				}
			}
			throw new NotImplementedException();
		}
	}
}
