﻿using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using Ludiq;

namespace Bolt
{
	[Generator(typeof(DictionaryContainsKey))]
	public class DictionaryContainsKeyGenerator : UnitGenerator<DictionaryContainsKey>
	{
		public DictionaryContainsKeyGenerator(DictionaryContainsKey unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.contains)
			{
				return unit.dictionary.GenerateExpression(context, typeof(IDictionary)).Method("Contains").Invoke(unit.key.GenerateExpression(context, typeof(object)));
			}
			throw new NotImplementedException();
		}
	}
}
