﻿using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using Ludiq;

namespace Bolt
{
	[Generator(typeof(GetDictionaryItem))]
	public class GetDictionaryItemGenerator : UnitGenerator<GetDictionaryItem>
	{
		public GetDictionaryItemGenerator(GetDictionaryItem unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.value)
			{
				return unit.dictionary.GenerateExpression(context, typeof(IDictionary)).Index(unit.key.GenerateExpression(context, typeof(int)));
			}
			throw new NotImplementedException();
		}
	}
}
