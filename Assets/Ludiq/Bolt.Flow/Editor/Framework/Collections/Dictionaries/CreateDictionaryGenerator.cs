﻿using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using Ludiq;

namespace Bolt
{
	[Generator(typeof(CreateDictionary))]
	public class CreateDictionaryGenerator : UnitGenerator<CreateDictionary>
	{
		public CreateDictionaryGenerator(CreateDictionary unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.dictionary)
			{
				return CodeFactory.ObjectCreate(CodeFactory.TypeRef(typeof(Dictionary<object, object>)));
			}
			throw new NotImplementedException();
		}
	}
}
