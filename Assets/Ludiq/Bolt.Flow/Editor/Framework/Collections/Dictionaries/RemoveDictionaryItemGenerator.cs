﻿using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using Ludiq;

namespace Bolt
{
	[Generator(typeof(RemoveDictionaryItem))]
	public class RemoveDictionaryItemGenerator : UnitGenerator<RemoveDictionaryItem>
	{
		public RemoveDictionaryItemGenerator(RemoveDictionaryItem unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			if (controlInput == unit.enter) 
			{
				var dictionaryLocal = context.currentScope.DeclareValuePortLocal(CodeFactory.VarType, unit.dictionaryOutput, unit.dictionaryInput.GenerateExpression(context, typeof(IDictionary)));

				yield return dictionaryLocal;
				yield return CodeFactory.VarRef(dictionaryLocal.Name).Method("Remove").Invoke(unit.key.GenerateExpression(context)).Statement();
			}
			else
			{
				throw new NotImplementedException();
			}
		}

		public override IEnumerable<CodeStatement> GenerateControlInputSuccessor(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			if (controlInput == unit.enter)
			{
				return unit.exit.GenerateStatements(context);
			}
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.dictionaryOutput)
			{
				return CodeFactory.VarRef(context.currentScope.ExpectValuePortLocal(unit.dictionaryOutput));
			}
			throw new NotImplementedException();
		}
	}
}
