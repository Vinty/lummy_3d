﻿using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using Ludiq;

namespace Bolt
{
	[Generator(typeof(InsertListItem))]
	public class InsertListItemGenerator : UnitGenerator<InsertListItem>
	{
		public InsertListItemGenerator(InsertListItem unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			if (controlInput == unit.enter) 
			{
				yield return context.currentScope.DeclareValuePortLocal(CodeFactory.VarType, unit.listOutput,
					CodeFactory.TypeRef(typeof(ListUtility)).Expression().Method("Insert").Invoke(
						unit.listInput.GenerateExpression(context, typeof(IList)),
						unit.index.GenerateExpression(context, typeof(int)),
						unit.item.GenerateExpression(context)));
			}
			else
			{
				throw new NotImplementedException();
			}
		}

		public override IEnumerable<CodeStatement> GenerateControlInputSuccessor(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			if (controlInput == unit.enter)
			{
				return unit.exit.GenerateStatements(context);
			}
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.listOutput)
			{
				return CodeFactory.VarRef(context.currentScope.ExpectValuePortLocal(unit.listOutput));
			}
			throw new NotImplementedException();
		}
	}
}
