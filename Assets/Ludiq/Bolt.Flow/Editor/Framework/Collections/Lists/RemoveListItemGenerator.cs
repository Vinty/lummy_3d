﻿using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using Ludiq;

namespace Bolt
{
	[Generator(typeof(RemoveListItem))]
	public class RemoveListItemGenerator : UnitGenerator<RemoveListItem>
	{
		public RemoveListItemGenerator(RemoveListItem unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			if (controlInput == unit.enter) 
			{
				yield return context.currentScope.DeclareValuePortLocal(CodeFactory.VarType, unit.listOutput,
					CodeFactory.TypeRef(typeof(ListUtility)).Expression().Method("Remove").Invoke(
						unit.listInput.GenerateExpression(context, typeof(IList)),
						unit.item.GenerateExpression(context)));
			}
			else
			{
				throw new NotImplementedException();
			}
		}

		public override IEnumerable<CodeStatement> GenerateControlInputSuccessor(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			if (controlInput == unit.enter)
			{
				return unit.exit.GenerateStatements(context);
			}
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.listOutput)
			{
				return CodeFactory.VarRef(context.currentScope.ExpectValuePortLocal(unit.listOutput));
			}
			throw new NotImplementedException();
		}
	}
}
