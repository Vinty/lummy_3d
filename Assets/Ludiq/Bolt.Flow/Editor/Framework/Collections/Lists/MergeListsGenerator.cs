﻿using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using Ludiq;

namespace Bolt
{
	[Generator(typeof(MergeLists))]
	public class MergeListsGenerator : UnitGenerator<MergeLists>
	{
		public MergeListsGenerator(MergeLists unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			throw new NotImplementedException();
		}

		public override IEnumerable<CodeStatement> GenerateValueOutputInit(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.list)
			{
				var listLocal = context.currentScope.DeclareValuePortLocal(CodeFactory.VarType, unit.list, CodeFactory.ObjectCreate(CodeFactory.TypeRef(typeof(List<object>))));
				yield return listLocal;

				foreach (var input in unit.multiInputs)
				{
					CodeFactory.VarRef(listLocal.Name).Method("AddRange").Invoke(input.GenerateExpression(context, typeof(IEnumerable)));
				}
			}
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.list)
			{
				return CodeFactory.VarRef(context.currentScope.ExpectValuePortLocal(unit.list));
			}
			throw new NotImplementedException();
		}
	}
}
