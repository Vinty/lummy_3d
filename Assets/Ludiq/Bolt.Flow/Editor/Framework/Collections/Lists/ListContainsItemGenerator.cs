﻿using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using Ludiq;

namespace Bolt
{
	[Generator(typeof(ListContainsItem))]
	public class ListContainsItemGenerator : UnitGenerator<ListContainsItem>
	{
		public ListContainsItemGenerator(ListContainsItem unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.contains)
			{
				return unit.list.GenerateExpression(context, typeof(IList)).Method("Contains").Invoke(unit.item.GenerateExpression(context, typeof(int)));
			}
			throw new NotImplementedException();
		}
	}
}
