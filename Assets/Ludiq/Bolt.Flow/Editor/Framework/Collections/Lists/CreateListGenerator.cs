﻿using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using Ludiq;

namespace Bolt
{
	[Generator(typeof(CreateList))]
	public class CreateListGenerator : UnitGenerator<CreateList>
	{
		public CreateListGenerator(CreateList unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.list)
			{
				var collectionInitializerItems = new List<IEnumerable<CodeExpression>>();
				foreach (var input in unit.multiInputs)
				{
					collectionInitializerItems.Add(new[] { input.GenerateExpression(context) });
				}
				return CodeFactory.ObjectCreate(CodeFactory.TypeRef(typeof(List<object>)), Enumerable.Empty<CodeExpression>(), collectionInitializerItems);
			}
			throw new NotImplementedException();
		}
	}
}
