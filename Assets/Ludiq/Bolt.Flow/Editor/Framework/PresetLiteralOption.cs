﻿using System;
using System.Collections.Generic;
using Ludiq;
using UnityEngine;
using UnityObject = UnityEngine.Object;

namespace Bolt
{
	public class PresetLiteralOption : LiteralOption
	{
		[Obsolete(Serialization.ConstructorWarning)]
		public PresetLiteralOption() : base() { }

		public PresetLiteralOption(Literal unit) : base(unit)
		{
		}

		private string NamedValue(bool human)
		{
			if (unit.value is UnityObject uo && !uo.IsUnityNull())
			{
				return UnityAPI.Await(() => uo.name);
			}
			else if (unit.type == typeof(string))
			{
				return "\"" + (string)unit.value + "\"";
			}
			else if (unit.type.IsNumeric())
			{
				return unit.value.ToString();
			}
			else
			{
				var namedValue = unit.value.ToString();

				if (human)
				{
					namedValue = namedValue.Prettify();
				}

				return namedValue;
			}
		}

		protected override string Label(bool human)
		{
			return NamedValue(human) + LudiqGUIUtility.DimString($" ({unit.type.SelectedName(human)} Literal)");
		}

		protected override string Haystack(bool human)
		{
			return NamedValue(human);
		}

		public override string SearchResultLabel(string query)
		{
			return base.SearchResultLabel(query) + LudiqGUIUtility.DimString($" ({unit.type.DisplayName()} Literal)");
		}

		public override EditorTexture Icon()
		{
			if (unit.value is UnityObject uo && !uo.IsUnityNull())
			{
				return uo.Icon();
			}

			return base.Icon();
		}

		protected override string Key()
		{
			return $"{literalType.FullName}@preset-literal#{unit.value?.GetHashCode() ?? 0}";
		}
	}
}