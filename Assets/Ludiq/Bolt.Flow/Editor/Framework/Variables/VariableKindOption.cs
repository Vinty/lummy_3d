﻿using Ludiq;

namespace Bolt
{
	[FuzzyOption(typeof(VariableKind))]
	public class VariableKindOption : DocumentedOption<VariableKind>
	{
		public VariableKindOption(VariableKind kind) : base(FuzzyOptionMode.Branch)
		{
			value = kind;
			label = kind.HumanName();
			getIcon = () => BoltCore.Icons.VariableKind(kind);
			documentation = kind.Documentation();
			zoom = true;
		}
	}
}