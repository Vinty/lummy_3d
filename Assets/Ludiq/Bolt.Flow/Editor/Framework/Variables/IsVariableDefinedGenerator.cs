﻿using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using Ludiq;

namespace Bolt
{
	[Generator(typeof(IsVariableDefined))]
	class IsVariableDefinedGenerator : UnitGenerator<IsVariableDefined>
	{
		public IsVariableDefinedGenerator(IsVariableDefined unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.isVariableDefined)
			{
				CodeExpression variables;
				switch (unit.kind)
				{			
					case VariableKind.Flow:
						variables = CodeFactory.VarRef(context.RequireFlowVariablesLocal());
						break;
					case VariableKind.Graph:
						variables = CodeFactory.ThisRef.Field("graphData").Field("variables");
						break;
					case VariableKind.Object:
						variables = CodeFactory.TypeRef(typeof(Variables)).Expression().Method("Object").Invoke(unit.@object.GenerateExpression(context));
						break;
					case VariableKind.Scene:
						variables = CodeFactory.TypeRef(typeof(Variables)).Expression().Method("Scene").Invoke(CodeFactory.ThisRef.Field("gameObject").Field("scene"));
						break;
					case VariableKind.Application:
						variables = CodeFactory.TypeRef(typeof(Variables)).Expression().Field("Application");
						break;
					case VariableKind.Saved:
						variables = CodeFactory.TypeRef(typeof(Variables)).Expression().Field("Saved");
						break;
					default:
						throw new UnexpectedEnumValueException<VariableKind>(unit.kind);
				}

				return variables.Method("IsDefined").Invoke(unit.name.GenerateExpression(context, typeof(string)));
			}
			throw new NotImplementedException();
		}
	}
}