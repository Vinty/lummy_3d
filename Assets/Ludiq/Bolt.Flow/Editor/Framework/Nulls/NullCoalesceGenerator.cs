﻿using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;
using UnityObject = UnityEngine.Object;

namespace Bolt
{
	[Generator(typeof(NullCoalesce))]
	public class NullCoalesceGenerator : UnitGenerator<NullCoalesce>
	{ 
		public NullCoalesceGenerator(NullCoalesce unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{			
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.result)
			{
				return unit.input.GenerateExpression(context).Method("UnityNullCoalesce").Invoke(unit.fallback.GenerateExpression(context));
			}
			throw new NotImplementedException();
		}
	}
}
