﻿using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;
using UnityObject = UnityEngine.Object;

namespace Bolt
{
	[Generator(typeof(NullCheck))]
	public class NullCheckGenerator : UnitGenerator<NullCheck>
	{ 
		public NullCheckGenerator(NullCheck unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{			
			if (controlInput == unit.enter)
			{
				var conditionExpression = unit.input.GenerateExpression(context).Method("IsUnityNull").Invoke();
				var ifTrueStatements = unit.ifNull.GenerateStatements(context).ToList();
				var ifFalseStatements = unit.ifNotNull.GenerateStatements(context).ToList();

				if (unit.ifNull.hasValidConnection && unit.ifNotNull.hasValidConnection && unit.ifNull.connection.destination == unit.ifNotNull.connection.destination)
				{
					foreach (var statement in ifTrueStatements) yield return statement;
				}
				else if (ifTrueStatements.Any())
				{
					yield return new CodeIfStatement(conditionExpression, ifTrueStatements, ifFalseStatements);
				}
				else if (ifFalseStatements.Any())
				{
					yield return new CodeIfStatement(conditionExpression.LogicalNot(), ifFalseStatements);
				}
			}
			else
			{
				throw new NotImplementedException();
			}
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			throw new NotImplementedException();
		}
	}
}
