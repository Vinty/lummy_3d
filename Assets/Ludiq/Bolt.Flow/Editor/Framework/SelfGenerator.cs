﻿using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;

namespace Bolt
{
	[Generator(typeof(Self))]
	class SelfGenerator : UnitGenerator<Self>
	{
		public SelfGenerator(Self unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.self)
			{
				return CodeFactory.ThisRef.Field("gameObject");
			}
			throw new NotImplementedException();
		}
	}
}