﻿using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ludiq;

namespace Bolt
{
	[Generator(typeof(SelectOnInteger))]
	class SelectOnIntegerGenerator : SelectUnitGenerator<SelectOnInteger, int>
	{
		public SelectOnIntegerGenerator(SelectOnInteger unit) : base(unit) {}
	}
}
