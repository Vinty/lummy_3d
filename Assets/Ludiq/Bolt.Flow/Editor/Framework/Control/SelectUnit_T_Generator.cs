﻿using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ludiq;

namespace Bolt
{
	public abstract class SelectUnitGenerator<TUnit, TSelector> : UnitGenerator<SelectUnit<TSelector>>
		where TUnit : SelectUnit<TSelector>
	{
		public SelectUnitGenerator(SelectUnit<TSelector> unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			throw new NotImplementedException();
		}

		public override IEnumerable<CodeStatement> GenerateValueOutputInit(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.selection)
			{
				var selectionLocal = context.currentScope.DeclareValuePortLocal(CodeFactory.ObjectType, unit.selection);

				yield return selectionLocal;

				var cases = new List<CodeStatement>();
				foreach (var branch in unit.branches)
				{
					cases.Add(new CodeCaseStatement(branch.Key.Generator<IConstantGenerator>().GenerateExpression(), new[]
					{
						CodeFactory.VarRef(selectionLocal.Name).Assign(branch.Value.GenerateExpression(context)).Statement()
					}));
				}
				cases.Add(new CodeDefaultStatement(new[]
				{
					CodeFactory.VarRef(selectionLocal.Name).Assign(unit.@default.GenerateExpression(context)).Statement()
				}));

				yield return new CodeSwitchStatement(unit.selector.GenerateExpression(context), cases);
			}
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.selection)
			{
				return CodeFactory.VarRef(context.currentScope.ExpectValuePortLocal(valueOutput));
			}

			throw new NotImplementedException();
		}
	}
}
