﻿using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ludiq;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(Once))]
	public class OnceGenerator : UnitGenerator<Once>
	{
		public OnceGenerator(Once unit) : base(unit)
		{
			var lowerCamelCaseName = BoltFlowNameUtility.UnitTitle(unit.GetType(), false).FirstCharacterToLower();
			executedMemberOriginalName = lowerCamelCaseName + "Executed"; 
		}

		private string executedMemberOriginalName { get; }

		public override void DeclareGraphMemberNames(GraphClassGenerationContext context)
		{
			base.DeclareGraphMemberNames(context);

			context.DeclareMemberName(unit, executedMemberOriginalName);
		}

		public override IEnumerable<CodeCompositeTypeMember> GenerateMembers(GraphClassGenerationContext context)
		{
			foreach (var member in base.GenerateMembers(context)) yield return member;

			yield return new CodeFieldMember(CodeMemberModifiers.Public, CodeFactory.BoolType, context.ExpectMemberName(unit, executedMemberOriginalName), CodeFactory.Primitive(false));
		}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			var executedMemberRef = context.InstanceField(unit, executedMemberOriginalName);
			if (controlInput == unit.enter)
			{
				var onceStatements = unit.once.GenerateScopedStatements(context).ToList();
				onceStatements.Add(CodeFactory.Assign(executedMemberRef, CodeFactory.Primitive(true)).Statement());

				if (onceStatements.Any())
				{
					yield return new CodeIfStatement(executedMemberRef.LogicalNot(), onceStatements);
				}
			}
			else if (controlInput == unit.reset)
			{
				yield return CodeFactory.Assign(executedMemberRef, CodeFactory.Primitive(false)).Statement();
			}
			else
			{
				throw new NotImplementedException();
			}
		}

		public override IEnumerable<CodeStatement> GenerateControlInputSuccessor(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			if (controlInput == unit.enter)
			{
				return unit.after.GenerateStatements(context);
			}
			else if (controlInput == unit.reset)
			{
				return Enumerable.Empty<CodeStatement>();
			}
			else
			{
				throw new NotImplementedException();
			}
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			throw new NotImplementedException();
		}
	}
}
