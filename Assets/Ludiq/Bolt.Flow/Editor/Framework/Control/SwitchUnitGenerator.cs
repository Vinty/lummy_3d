﻿using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ludiq;

namespace Bolt
{
	public abstract class SwitchUnitGenerator<TUnit, TSelector> : UnitGenerator<SwitchUnit<TSelector>>
		where TUnit : SwitchUnit<TSelector>
	{
		public SwitchUnitGenerator(SwitchUnit<TSelector> unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			if (controlInput == unit.enter)
			{
				var cases = new List<CodeStatement>();
				foreach (var branch in unit.branches.OrderBy(x => x.Key))
				{
					cases.Add(new CodeCaseStatement(branch.Key.Generator<IConstantGenerator>().GenerateExpression(), branch.Value.GenerateScopedStatements(context)));
				}
				cases.Add(new CodeDefaultStatement(unit.@default.GenerateScopedStatements(context)));

				yield return new CodeSwitchStatement(unit.selector.GenerateExpression(context), cases);
			}
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			throw new NotImplementedException();
		}
	}
}
