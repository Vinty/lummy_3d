﻿using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bolt
{
	[Generator(typeof(Branch))]
	class BranchGenerator : UnitGenerator<Branch>
	{
		public BranchGenerator(Branch unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			if (controlInput == unit.enter)
			{
				var conditionExpression = unit.condition.GenerateExpression(context);
				var ifTrueStatements = unit.ifTrue.GenerateScopedStatements(context).ToList();
				var ifFalseStatements = unit.ifFalse.GenerateScopedStatements(context).ToList();

				if (unit.ifTrue.hasValidConnection && unit.ifFalse.hasValidConnection && unit.ifTrue.connection.destination == unit.ifFalse.connection.destination)
				{
					foreach (var statement in ifTrueStatements) yield return statement;
				}
				else if (ifTrueStatements.Any())
				{
					yield return new CodeIfStatement(conditionExpression, ifTrueStatements, ifFalseStatements);
				}
				else if (ifFalseStatements.Any())
				{
					yield return new CodeIfStatement(conditionExpression.LogicalNot(), ifFalseStatements);
				}
			}
			else
			{
				throw new NotImplementedException();
			}
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			throw new NotImplementedException();
		}
	}
}
