﻿using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ludiq;

namespace Bolt
{
	[Generator(typeof(SelectOnEnum))]
	class SelectOnEnumGenerator : UnitGenerator<SelectOnEnum>
	{
		public SelectOnEnumGenerator(SelectOnEnum unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			throw new NotImplementedException();
		}

		public override IEnumerable<CodeStatement> GenerateValueOutputInit(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.selection)
			{
				var selectorLocal = context.currentScope.DeclareLocal(CodeFactory.VarType, "selector", unit.selector.GenerateExpression(context));
				var selectionLocal = context.currentScope.DeclareValuePortLocal(CodeFactory.ObjectType, unit.selection);

				yield return selectorLocal;
				yield return selectionLocal;

				var cases = new List<CodeStatement>();
				foreach (var branch in unit.branches.OrderBy(x => x.Key))
				{
					cases.Add(new CodeCaseStatement(branch.Key.Generator<IConstantGenerator>().GenerateExpression(), new[]
					{
						CodeFactory.VarRef(selectionLocal.Name).Assign(branch.Value.GenerateExpression(context)).Statement()
					}));
				}
				cases.Add(new CodeDefaultStatement(new[]
				{
					new CodeThrowStatement(CodeFactory.TypeRef(typeof(UnexpectedEnumValueException<>).MakeGenericType(unit.enumType)).ObjectCreate(CodeFactory.VarRef(selectorLocal.Name))) 
				}));

				yield return new CodeSwitchStatement(CodeFactory.VarRef(selectorLocal.Name), cases);
			}
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.selection)
			{
				return CodeFactory.VarRef(context.currentScope.ExpectValuePortLocal(valueOutput));
			}

			throw new NotImplementedException();
		}
	}
}
