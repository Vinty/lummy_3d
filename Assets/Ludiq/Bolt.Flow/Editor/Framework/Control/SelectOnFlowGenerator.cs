﻿using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ludiq;

namespace Bolt
{
	[Generator(typeof(SelectOnFlow))]
	class SelectOnFlowGenerator : UnitGenerator<SelectOnFlow>
	{
		public SelectOnFlowGenerator(SelectOnFlow unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			if (unit.branches.TryGetValue(controlInput, out var valueInput))
			{
				if (context.currentScope.TryGetValuePortLocal(unit.selection, out var selectionLocalName))
				{
					yield return CodeFactory.VarRef(selectionLocalName).Assign(valueInput.GenerateExpression(context)).Statement();
				}
				else
				{
					var selectionLocal = context.currentScope.DeclareValuePortLocal(CodeFactory.ObjectType, unit.selection, valueInput.GenerateExpression(context));
					yield return selectionLocal;
				}
			}
			else
			{
				throw new NotImplementedException();
			}
		}

		public override IEnumerable<CodeStatement> GenerateControlInputSuccessor(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			if (unit.branches.ContainsKey(controlInput))
			{
				return unit.exit.GenerateStatements(context);
			}
			throw new NotImplementedException();
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			if (valueOutput == unit.selection)
			{
				return CodeFactory.VarRef(context.currentScope.ExpectValuePortLocal(valueOutput));
			}

			throw new NotImplementedException();
		}
	}
}
