﻿using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ludiq;

namespace Bolt
{
	[Generator(typeof(SelectOnString))]
	class SelectOnStringGenerator : SelectUnitGenerator<SelectOnString, string>
	{
		public SelectOnStringGenerator(SelectOnString unit) : base(unit) {}
	}
}
