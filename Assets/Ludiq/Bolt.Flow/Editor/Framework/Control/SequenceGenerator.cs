﻿using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ludiq;

namespace Bolt
{
	[Generator(typeof(Sequence))]
	public class SequenceGenerator : UnitGenerator<Sequence>
	{
		public SequenceGenerator(Sequence unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			if (controlInput == unit.enter)
			{
				foreach (var output in unit.multiOutputs)
				{
					foreach (var statement in output.GenerateStatements(context))
					{
						yield return statement;
					}
				}
			}
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			throw new NotImplementedException();
		}
	}
}
