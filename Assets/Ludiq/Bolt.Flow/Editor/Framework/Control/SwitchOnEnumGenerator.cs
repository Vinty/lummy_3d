﻿using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ludiq;

namespace Bolt
{
	[Generator(typeof(SwitchOnEnum))]
	class SwitchOnEnumGenerator : UnitGenerator<SwitchOnEnum>
	{
		public SwitchOnEnumGenerator(SwitchOnEnum unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			if (controlInput == unit.enter)
			{
				var cases = new List<CodeStatement>();
				foreach (var branch in unit.branches.OrderBy(x => x.Key))
				{
					cases.Add(new CodeCaseStatement(branch.Key.Generator<IConstantGenerator>().GenerateExpression(), branch.Value.GenerateScopedStatements(context)));
				}

				yield return new CodeSwitchStatement(unit.@enum.GenerateExpression(context), cases);
			}
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			throw new NotImplementedException();
		}
	}
}
