﻿using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ludiq;

namespace Bolt
{
	[Generator(typeof(ToggleValue))]
	class ToggleValueGenerator : UnitGenerator<ToggleValue>
	{
		public ToggleValueGenerator(ToggleValue unit) : base(unit)
		{
			var lowerCamelCaseName = BoltFlowNameUtility.UnitTitle(unit.GetType(), false).FirstCharacterToLower();
			isOnMemberOriginalName = lowerCamelCaseName + "IsOn";
		}

		private string isOnMemberOriginalName { get; }

		public override void DeclareGraphMemberNames(GraphClassGenerationContext context)
		{
			base.DeclareGraphMemberNames(context);

			context.DeclareMemberName(unit, isOnMemberOriginalName);
		}

		public override IEnumerable<CodeCompositeTypeMember> GenerateMembers(GraphClassGenerationContext context)
		{
			foreach (var member in base.GenerateMembers(context)) yield return member;

			yield return new CodeFieldMember(CodeMemberModifiers.Public, CodeFactory.BoolType, context.ExpectMemberName(unit, isOnMemberOriginalName), CodeFactory.Primitive(unit.startOn));
		}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			var isOnMemberRef = context.InstanceField(unit, isOnMemberOriginalName);
			if (controlInput == unit.turnOn)
			{
				var turnedOnStatements = unit.turnedOn.GenerateScopedStatements(context).ToList();
				if (turnedOnStatements.Any())
				{
					yield return new CodeIfStatement(isOnMemberRef, turnedOnStatements);
				}
				yield return CodeFactory.Assign(isOnMemberRef, CodeFactory.Primitive(true)).Statement();
			}
			else if (controlInput == unit.turnOff)
			{
				var turnedOffStatements = unit.turnedOff.GenerateScopedStatements(context).ToList();
				if (turnedOffStatements.Any())
				{
					yield return new CodeIfStatement(isOnMemberRef.LogicalNot(), turnedOffStatements);
				}
				yield return CodeFactory.Assign(isOnMemberRef, CodeFactory.Primitive(false)).Statement();
			}
			else if (controlInput == unit.toggle)
			{
				var turnedOnStatements = unit.turnedOn.GenerateScopedStatements(context).ToList();
				var turnedOffStatements = unit.turnedOff.GenerateScopedStatements(context).ToList();
				if (unit.turnedOn.hasValidConnection && unit.turnedOff.hasValidConnection && unit.turnedOn.connection.destination == unit.turnedOff.connection.destination)
				{
					foreach (var statement in turnedOnStatements) yield return statement;
				}
				else if (turnedOnStatements.Any())
				{
					yield return new CodeIfStatement(isOnMemberRef, turnedOnStatements, turnedOffStatements);
				}
				else if (turnedOffStatements.Any())
				{
					yield return new CodeIfStatement(isOnMemberRef.LogicalNot(), turnedOffStatements);
				}
				yield return CodeFactory.Assign(isOnMemberRef, isOnMemberRef.LogicalNot()).Statement();
			}
			else
			{
				throw new NotImplementedException();
			}
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			var isOnMemberRef = context.InstanceField(unit, isOnMemberOriginalName);
			if (valueOutput == unit.isOn)
			{
				return isOnMemberRef;
			}
			else if (valueOutput == unit.value)
			{
				var onValueExpression = unit.onValue.GenerateExpression(context).Cast(CodeFactory.TypeRef(unit.value.type));
				var offValueExpression = unit.offValue.GenerateExpression(context).Cast(CodeFactory.TypeRef(unit.value.type));
				return CodeFactory.Conditional(isOnMemberRef, onValueExpression, offValueExpression);
			}
			throw new NotImplementedException();
		}
	}
}
