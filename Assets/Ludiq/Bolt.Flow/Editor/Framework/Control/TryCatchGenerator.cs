﻿using Ludiq;
using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bolt
{
	[Generator(typeof(TryCatch))]
	class TryCatchGenerator : UnitGenerator<TryCatch>
	{
		public TryCatchGenerator(TryCatch unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			if (context.coroutine)
			{
				throw new NotSupportedException("Coroutines cannot catch exceptions.");
			}

			if (controlInput == unit.enter)
			{
				var tryStatements = unit.@try.GenerateScopedStatements(context).ToList();
				var catchStatements = unit.@try.GenerateScopedStatements(context).ToList();
				var finallyStatements = unit.@try.GenerateScopedStatements(context).ToList();
				var caughtExceptionLocal = context.currentScope.DeclareLocal(CodeFactory.TypeRef(unit.exceptionType), "e");
				var catchClause = new CodeCatchClause(caughtExceptionLocal.Name, caughtExceptionLocal.Type, catchStatements);

				yield return new CodeTryStatement(tryStatements, new[] { catchClause }, finallyStatements);
			}
			else
			{
				throw new NotImplementedException();
			}
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			throw new NotImplementedException();
		}
	}
}
