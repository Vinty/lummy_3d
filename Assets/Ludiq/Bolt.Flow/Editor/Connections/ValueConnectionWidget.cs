﻿using System;
using Ludiq;
using UnityEditor;
using UnityEngine;

namespace Bolt
{
	[Widget(typeof(ValueConnection))]
	public sealed class ValueConnectionWidget : UnitConnectionWidget<ValueConnection>
	{
		public ValueConnectionWidget(FlowCanvas canvas, ValueConnection connection) : base(canvas, connection) { }
		
		private new ValueConnection.DebugData connectionDebugData => GetDebugData<ValueConnection.DebugData>();
		
		public override Axis2 axis => Axis2.Horizontal;
		
		#region Drawing

		public override Color color => DetermineColor(connection.source.type, connection.destination.type);

		protected override bool colorIfActive => !BoltFlow.Configuration.animateControlConnections || !BoltFlow.Configuration.animateValueConnections;

		public override void DrawForeground()
		{
			base.DrawForeground();

			if (BoltFlow.Configuration.showConnectionValues)
			{
				var showLastValue = EditorApplication.isPlaying && connectionDebugData.assignedLastValue;
				var showPredictedvalue = BoltFlow.Configuration.predictConnectionValues && !EditorApplication.isPlaying && Flow.CanPredict(connection.source, reference);

				if (showLastValue || showPredictedvalue)
				{
					var previousIconSize = EditorGUIUtility.GetIconSize();
					EditorGUIUtility.SetIconSize(new Vector2(IconSize.Small, IconSize.Small));

					object value;

					if (showLastValue)
					{
						value = connectionDebugData.lastValue;
					}
					else // if (showPredictedvalue)
					{
						value = Flow.Predict(connection.source, reference);
					}

					var label = new GUIContent(value.ToShortString(), Icons.Type(value?.GetType())?[IconSize.Small]);
					var labelSize = Styles.prediction.CalcSize(label);
					var labelPosition = new Rect(position.position - labelSize / 2, labelSize);

					BeginDim();

					GUI.Label(labelPosition, label, Styles.prediction);

					EndDim();

					EditorGUIUtility.SetIconSize(previousIconSize);
				}
			}
		}

		public static Color DetermineColor(Type source, Type destination)
		{
			if (destination == typeof(object))
			{
				return source.Color();
			}

			return destination.Color();
		}

		#endregion



		#region Droplets

		protected override bool showDroplets => BoltFlow.Configuration.animateValueConnections;

		protected override Vector2 GetDropletSize()
		{
			return LudiqGraphs.Icons.droplet[1].PointSize();
		}

		protected override void DrawDroplet(Rect position)
		{
			GUI.DrawTexture(position, LudiqGraphs.Icons.droplet[1]);
		}

		#endregion



		private static class Styles
		{
			static Styles()
			{
				prediction = new GUIStyle(EditorStyles.label);
				prediction.normal.textColor = Color.white;
				prediction.fontSize = 9;
				prediction.normal.background = new Color(0, 0, 0, 0.25f).GetPixel();
				prediction.padding = new RectOffset(4, 6, 3, 3);
				prediction.margin = new RectOffset(0, 0, 0, 0);
				prediction.alignment = TextAnchor.MiddleCenter;
			}

			public static readonly GUIStyle prediction;
		}
	}
}
