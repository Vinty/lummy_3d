﻿using Ludiq;
using UnityEngine;

namespace Bolt
{
	[Widget(typeof(ControlConnection))]
	public sealed class ControlConnectionWidget : UnitConnectionWidget<ControlConnection>
	{
		public ControlConnectionWidget(FlowCanvas canvas, ControlConnection connection) : base(canvas, connection) { }

		public override Axis2 axis => connection.graph.controlAxis;



		#region Drawing

		public override Color color => Color.white;

		protected override bool colorIfActive => !BoltFlow.Configuration.animateControlConnections || !BoltFlow.Configuration.animateValueConnections;

		#endregion



		#region Droplets

		protected override bool showDroplets => BoltFlow.Configuration.animateControlConnections;

		protected override Vector2 GetDropletSize()
		{
			return LudiqGraphs.Icons.droplet[1].PointSize();
		}

		protected override void DrawDroplet(Rect position)
		{
			GUI.DrawTexture(position, LudiqGraphs.Icons.droplet[1]);
		}

		#endregion
	}
}
