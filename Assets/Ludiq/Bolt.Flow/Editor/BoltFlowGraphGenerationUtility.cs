﻿using Ludiq;
using Ludiq.CodeDom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bolt
{
	public static class BoltFlowGraphGenerationUtility
	{
		public static void GenerateMachineEvents(this GraphClassGenerationContext context, IEnumerable<string> requiredMachineEvents)
		{
			foreach (var requiredMachineEvent in requiredMachineEvents)
			{
				context.classDeclaration.Members.Add(new CodeMethodMember(
					CodeMemberModifiers.Protected | CodeMemberModifiers.Virtual,
					CodeFactory.TypeRef(typeof(void)),
					requiredMachineEvent,
					Enumerable.Empty<CodeParameterDeclaration>(),
					new[] {
						CodeFactory.ThisRef.Method("TriggerEvent").Invoke(CodeFactory.TypeRef(typeof(EventHooks)).Expression().Field(requiredMachineEvent)).Statement()
					}));
			}
		}
	}
}
