﻿using Ludiq;

namespace Bolt
{
	public sealed class UnitDescription : GraphElementDescription
	{
		public bool showTitle { get; set; }
		public string shortTitle { get; set; }
		public string surtitle { get; set; }
		public string subtitle { get; set; }
		public EditorTexture[] statusIcons { get; set; }
	}
}