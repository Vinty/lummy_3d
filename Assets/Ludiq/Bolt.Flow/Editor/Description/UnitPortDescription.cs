﻿using System;
using System.Collections.Generic;
using Ludiq;

namespace Bolt
{
	public sealed class UnitPortDescription : IDescription
	{
		private string _label;

		private bool _showLabel = true;
		
		public string fallbackLabel { get; set; }

		public string label
		{
			get => _label ?? fallbackLabel;
			set => _label = value;
		}

		public bool showLabel
		{
			get => _showLabel;
			set => _showLabel = value;
		}

		string IDescription.title => label;

		public string summary { get; set; }

		public EditorTexture icon { get; set; }

		public Func<Metadata, Metadata> getMetadata { get; set; }

		public Axes2 primaryAxes { get; set; }

		public Dictionary<Axis2, int> ordersPerAxis { get; } = new Dictionary<Axis2, int>();

		public int GetOrder(Axis2 axis)
		{
			if (!ordersPerAxis.TryGetValue(axis, out var order))
			{
				return int.MaxValue;
			}
			else
			{
				return order;
			}
		}

		public void SetOrder(int order, Axes2 axes)
		{
			foreach (var axis in axes.Split())
			{
				ordersPerAxis[axis] = order;
			}
		}

		public void CopyFrom(UnitPortDescription other)
		{
			_label = other._label;
			_showLabel = other._showLabel;
			summary = other.summary;
			icon = other.icon ?? icon;
			getMetadata = other.getMetadata ?? getMetadata;
			primaryAxes = other.primaryAxes;

			foreach (var kvp in other.ordersPerAxis)
			{
				ordersPerAxis[kvp.Key] = kvp.Value;
			}
		}

		public static string ResolveDisplayLabel(IUnitPort port)
		{
			Ensure.That(nameof(port)).IsNotNull(port);

			var portDescription = port.Description<UnitPortDescription>();

			if (portDescription.showLabel || port.unit == null)
			{
				return portDescription.label;
			}
			
			var unitDescription = port.unit.Description<UnitDescription>();

			return unitDescription.shortTitle;
		}
	}
}