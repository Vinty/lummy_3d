﻿using System.Collections.Generic;
using Ludiq;

namespace Bolt
{
	[Plugin(BoltFlow.ID)]
	public sealed class BoltFlowConfiguration : PluginConfiguration
	{
		private BoltFlowConfiguration(BoltFlow plugin) : base(plugin) { }

		public override string header => "Flow Graphs";

		/// <summary>
		/// The default control axis for newly created flow graphs.
		/// </summary>
		[ProjectSetting]
		public Axis2 defaultControlAxis { get; set; } = Axis2.Vertical;

		/// <summary>
		/// Whether predictive debugging should warn about null value inputs.
		/// Note that in some cases, this setting may report false positives.
		/// </summary>
		[EditorPref]
		public bool predictPotentialNullReferences { get; set; } = true;

		/// <summary>
		/// Whether predictive debugging should warn about missing components.
		/// Note that in some cases, this setting may report false positives.
		/// </summary>
		[EditorPref]
		public bool predictPotentialMissingComponents { get; set; } = true;
		
		/// <summary>
		/// Whether values should be shown on flow graph connections.
		/// </summary>
		[EditorPref]
		public bool showConnectionValues { get; set; } = true;

		/// <summary>
		/// Whether predictable values should be shown on flow graph connections.
		/// </summary>
		[EditorPref]
		public bool predictConnectionValues { get; set; } = false;
		
		/// <summary>
		/// Determines under which condition proxy connections should be revealed.
		/// </summary>
		[EditorPref]
		public RevealCondition revealProxies { get; set; } = RevealCondition.OnHoverOrWithAlt;
		
		/// <summary>
		/// Determines under which condition internal unit relations should be revealed.
		/// </summary>
		[EditorPref]
		public RevealCondition revealRelations { get; set; } = RevealCondition.OnHoverWithAlt;

		/// <summary>
		/// Whether active control connections should show a droplet animation.
		/// </summary>
		[EditorPref]
		public bool animateControlConnections { get; set; } = true;

		/// <summary>
		/// Whether active value connections should show a droplet animation.
		/// </summary>
		[EditorPref]
		public bool animateValueConnections { get; set; } = true;

		/// <summary>
		/// When active, right-clicking a flow graph will skip the context menu 
		/// and instantly open the fuzzy finder. To open the context menu, hold shift.
		/// </summary>
		[EditorPref]
		public bool skipContextMenu { get; set; } = false;

		[ProjectSetting(visible = false, resettable = false)]
		public HashSet<string> favoriteUnitOptions { get; set; } = new HashSet<string>();
	}
}