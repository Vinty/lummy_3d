using System.Collections.Generic;
using Ludiq;

namespace Bolt
{
	[Plugin(ID)]
	[PluginDependency(LudiqCore.ID)]
	[PluginDependency(LudiqGraphs.ID)]
	[PluginDependency(BoltCore.ID)]
	[Product(BoltProduct.ID)]
	[PluginRuntimeAssembly(ID + ".Runtime")]
	public sealed class BoltFlow : Plugin
	{
		public override IEnumerable<Page> SetupWizardPages()
		{
			yield return new ExtractorPage();
		}

		public BoltFlow()
		{
			instance = this;
		}

		public static BoltFlow instance { get; private set; }

		public const string ID = "Bolt.Flow";

		public static BoltFlowManifest Manifest => (BoltFlowManifest)instance.manifest;
		public static BoltFlowConfiguration Configuration => (BoltFlowConfiguration)instance.configuration;
		public static BoltFlowResources Resources => (BoltFlowResources)instance.resources;
		public static BoltFlowResources.Icons Icons => Resources.icons;
		public static BoltFlowPaths Paths => (BoltFlowPaths)instance.paths;

		public const string LegacyRuntimeDllGuid = "a040fb66244a7f54289914d98ea4ef7d";
		public const string LegacyEditorDllGuid = "6cb65bfc2ee1c854ca1382175f3aba91";

		public override IEnumerable<ScriptReferenceReplacement> scriptReferenceReplacements
		{
			get
			{
				yield return ScriptReferenceReplacement.FromDll<FlowMachine>(LegacyRuntimeDllGuid);
				yield return ScriptReferenceReplacement.FromDll<FlowMacro>(LegacyRuntimeDllGuid);
			}
		}

		public override IEnumerable<string> tips
		{
			get
			{
				yield return "Hold Ctrl (Cmd on Mac) while dragging a new connection to create a proxy.";
				yield return "Dragging a connection to an input or output unit will automatically create the port definition.";
			}
		}
	}
}
