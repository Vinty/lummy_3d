﻿using System;
using System.Collections.Generic;
using Ludiq;

namespace Bolt
{
	[Plugin(BoltFlow.ID)]
	internal class Changelog_2_0_0a1 : PluginChangelog
	{
		public Changelog_2_0_0a1(Plugin plugin) : base(plugin) { }

		public override SemanticVersion version => "2.0.0a1";
		public override DateTime date => new DateTime(2018, 11, 07);

		public override IEnumerable<string> changes
		{
			get
			{
				yield return "[Added] Overloads Context Menu Item";
				yield return "[Added] Fuzzy Finder Hotkey (Space)";
				yield return "[Added] Numeric Negation Operators";
				yield return "[Added] OnParticleTrigger Event";
			}
		}
	}

	[Plugin(BoltFlow.ID)]
	internal class Changelog_2_0_0a2 : PluginChangelog
	{
		public Changelog_2_0_0a2(Plugin plugin) : base(plugin) { }

		public override SemanticVersion version => "2.0.0a2";
		public override DateTime date => new DateTime(2018, 11, 30);

		public override IEnumerable<string> changes
		{
			get
			{
				yield return "[Added] Vertical Flow";
				yield return "[Added] Compact Units";
				yield return "[Added] Port Proxies";
				yield return "[Added] Breakpoints";
				yield return "[Added] Smart Carry";
				yield return "[Added] Drag Port to Input / Output";
				yield return "[Added] C# Preview for Super Units";
				yield return "[Added] Default Flow Macro Presets";
				yield return "[Added] Keyboard shortcut to reveal Proxies and Relations (Alt)";
				yield return "[Added] Double-click Graph Input or Output to go to parent graph";
				yield return "[Fixed] Implicitly typed variables in generated C# for input events";
				yield return "[Fixed] Various formatting issues in generated C#";
				yield return "[Fixed] Generated C# wrapping not updating when resizing preview window";
				yield return "[Fixed] Added a time-out recovery code for C# preview generation";
				yield return "[Fixed] Error in generated C# for Once unit";
			}
		}
	}

	[Plugin(BoltFlow.ID)]
	internal class Changelog_2_0_0a3 : PluginChangelog
	{
		public Changelog_2_0_0a3(Plugin plugin) : base(plugin) { }

		public override SemanticVersion version => "2.0.0a3";
		public override DateTime date => new DateTime(2018, 12, 21);

		public override IEnumerable<string> changes
		{
			get
			{
				yield return "[Added] Tweening (powered by DOTween)";
				yield return "[Added] Smart Contextual options";
				yield return "[Added] Strongly-Typed Operators";
				yield return "[Added] Bitwise Operators";
				yield return "[Added] Literal Creation from the fuzzy finder";
				yield return "[Added] Unit port definition options for Allows Null, Null Means Self and Primary";
				yield return "[Added] C#-like copy-by-value semantics for value types";
				yield return "[Added] Default true value for Branch condition";
				yield return "[Added] Nested graph inspector to super unit inspector";
				yield return "[Added] Port to Super Unit promotion";
				yield return "[Changed] Hide Get verb label on unit titles for compactness";
				yield return "[Obsoleted] All previous operator units";
				yield return "[Fixed] Port proxy carry behaviour";
				yield return "[Fixed] MSBuild path detection";
			}
		}
	}
}