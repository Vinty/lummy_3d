﻿using System.Collections.Generic;
using Ludiq;
using UnityEngine;

namespace Bolt
{
	[Plugin(BoltFlow.ID)]
	public sealed class BoltFlowResources : PluginResources
	{
		private BoltFlowResources(BoltFlow plugin) : base(plugin) 
		{
			icons = new Icons(this); 
		}

		public Icons icons { get; private set; }
		

		public override void LateInitialize()
		{
			base.LateInitialize();

			icons.Load();
		}

		public class Icons
		{
			public Icons(BoltFlowResources resources)
			{
				this.resources = resources;
			}

			private readonly Dictionary<UnitCategory, EditorTexture> unitCategoryIcons = new Dictionary<UnitCategory, EditorTexture>();
			private readonly Dictionary<string, EditorTexture> operatorIcons = new Dictionary<string, EditorTexture>();

			private readonly BoltFlowResources resources;

			public EditorTexture graph { get; private set; }
			public EditorTexture unit { get; private set; }
			public EditorTexture flowMacro { get; private set; }
			public EditorTexture unitCategory { get; private set; }

			public EditorTexture controlPortHorizontalConnected { get; private set; }
			public EditorTexture controlPortHorizontalUnconnected { get; private set; }
			public EditorTexture controlPortVerticalConnected { get; private set; }
			public EditorTexture controlPortVerticalUnconnected { get; private set; }
			public EditorTexture valuePortConnected { get; private set; }
			public EditorTexture valuePortUnconnected { get; private set; }
			public EditorTexture invalidPortConnected { get; private set; }
			public EditorTexture invalidPortUnconnected { get; private set; }
			public Dictionary<Edge, EditorTexture> unitPortProxies { get; } = new Dictionary<Edge, EditorTexture>();

			public EditorTexture coroutine { get; private set; }

			public EditorTexture breakpoint { get; private set; }

			public EditorTexture valuesCommand { get; private set; }

			public void Load()
			{
				graph = typeof(FlowGraph).Icon();
				unit = typeof(IUnit).Icon();
				flowMacro = resources.LoadIcon("Icons/FlowMacro.png");
				unitCategory = resources.LoadIcon("Icons/UnitCategory.png");

				var portResolutions = EditorTexture.UnitResolutions;
				var portOptions = CreateTextureOptions.PixelPerfect;

				controlPortHorizontalConnected = resources.LoadTexture("Ports/ControlPortHorizontalConnected.png", portResolutions, portOptions);
				controlPortHorizontalUnconnected = resources.LoadTexture("Ports/ControlPortHorizontalUnconnected.png", portResolutions, portOptions);
				controlPortVerticalConnected = resources.LoadTexture("Ports/ControlPortVerticalConnected.png", portResolutions, portOptions);
				controlPortVerticalUnconnected = resources.LoadTexture("Ports/ControlPortVerticalUnconnected.png", portResolutions, portOptions);
				valuePortConnected = resources.LoadTexture("Ports/ValuePortConnected.png", portResolutions, portOptions);
				valuePortUnconnected = resources.LoadTexture("Ports/ValuePortUnconnected.png", portResolutions, portOptions);
				invalidPortConnected = resources.LoadTexture("Ports/InvalidPortConnected.png", portResolutions, portOptions);
				invalidPortUnconnected = resources.LoadTexture("Ports/InvalidPortUnconnected.png", portResolutions, portOptions);
				
				unitPortProxies[Edge.Top] = resources.LoadTexture("Ports/UnitPortProxyTop.png", portResolutions, portOptions);
				unitPortProxies[Edge.Bottom] = resources.LoadTexture("Ports/UnitPortProxyBottom.png", portResolutions, portOptions);
				unitPortProxies[Edge.Left] = resources.LoadTexture("Ports/UnitPortProxyLeft.png", portResolutions, portOptions);
				unitPortProxies[Edge.Right] = resources.LoadTexture("Ports/UnitPortProxyRight.png", portResolutions, portOptions);

				coroutine = resources.LoadIcon("Icons/Coroutine.png");

				breakpoint = resources.LoadIcon("Icons/Breakpoint.png");

				valuesCommand = resources.LoadTexture("Icons/Commands/Values.png", EditorTexture.UnitResolutions, CreateTextureOptions.PixelPerfect);
			}

			public EditorTexture UnitCategory(UnitCategory category)
			{
				if (category == null)
				{
					return unitCategory;
				}

				if (!unitCategoryIcons.ContainsKey(category))
				{
					var path = $"Icons/Unit Categories/{category.fullName}.png";

					unitCategoryIcons.Add(category, LoadSharedIcon(path, false) ?? unitCategory);
				}
				 
				return unitCategoryIcons[category];
			}

			public EditorTexture Operator(UnaryOperator @operator) => Operator(OperatorUtility.Verb(@operator).Filter(whitespace: false));
			public EditorTexture Operator(BinaryOperator @operator) => Operator(OperatorUtility.Verb(@operator).Filter(whitespace: false));

			public EditorTexture Operator(string operatorName)
			{
				if (!operatorIcons.ContainsKey(operatorName))
				{
					EditorTexture icon = null;

					var path = $"Icons/Operators/{operatorName}.png";
					icon = LoadSharedIcon(path, false);

					operatorIcons.Add(operatorName, icon);
				}

				return operatorIcons[operatorName];
			}
		}
	}
}