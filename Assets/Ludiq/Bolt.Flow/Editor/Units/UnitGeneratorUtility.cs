﻿using Ludiq;
using Ludiq.CodeDom;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Bolt
{
	public static class UnitGeneratorUtility
	{
		public static IEnumerable<CodeStatement> GenerateStatements(this ControlOutput controlOutput, FlowMethodGenerationContext context, bool bind = true)
		{
			if (controlOutput.hasValidConnection)
			{
				var controlDestination = controlOutput.connection.destination;

				foreach (var statement in controlDestination.GenerateValueInputDependencies(context, bind))
				{
					yield return statement;
				}

				var unit = controlDestination.unit;
				var unitGenerator = unit.Generator<IUnitGenerator>();

				var breakpointStatements = unit.GenerateBreakpointStatements(context);
				var bodyStatements = unitGenerator.GenerateControlInputBody(context, controlDestination);

				if (bind)
				{
					breakpointStatements = breakpointStatements.Bind(context, unit);
					bodyStatements = bodyStatements.Bind(context, unit);
				}

				foreach (var statement in breakpointStatements)
				{
					yield return statement;
				}

				foreach (var statement in bodyStatements)
				{
					yield return statement;
				}

				var successorStatements = unitGenerator.GenerateControlInputSuccessor(context, controlDestination);

				foreach (var statement in successorStatements)
				{
					yield return statement;
				}
			}
		}		

		public static IEnumerable<CodeStatement> GenerateScopedStatements(this ControlOutput controlOutput, FlowMethodGenerationContext context, bool bind = true)
		{
			context.EnterScope();

			foreach (var statement in controlOutput.GenerateStatements(context, bind))
			{
				yield return statement;
			}

			context.ExitScope();
		}

		private static IEnumerable<CodeStatement> GenerateBreakpointStatements(this IUnit unit, FlowMethodGenerationContext context)
		{
			if (unit.breakpoint)
			{
				var descriptor = unit.Descriptor<IUnitDescriptor>();
				var surtitle = descriptor.Surtitle();
				var title = descriptor.Title();
				var information = unit.GetType().ToString()
					+ (!string.IsNullOrEmpty(surtitle) ? ": " + surtitle : "")
					+ (!string.IsNullOrEmpty(title) ? ": " + title : "");

				var breakpointCheck = new CodeIfStatement(
					CodeFactory.TypeRef(typeof(Application)).Expression().Field("isEditor"),
					new CodeStatement[] {
						new CodeThrowStatement(CodeFactory.TypeRef(typeof(Breakpoint)).ObjectCreate(CodeFactory.Primitive(information)))
					});

				yield return breakpointCheck;
			}
		}

		public static IEnumerable<CodeStatement> GenerateValueInputDependencies(this IUnitPort destination, FlowMethodGenerationContext context, bool bind = true)
		{
			var relations = destination.unit.relations.WithDestination(destination);

			foreach (var relation in relations)
			{
				if (relation.source is ValueInput valueInput)
				{
					if (valueInput.hasValidConnection)
					{
						var valueSource = valueInput.connection.source;

						foreach (var statement in valueSource.GenerateValueInputDependencies(context, bind))
						{
							yield return statement;
						}

						var valueSourceUnit = valueSource.unit;

						var breakpointStatements = valueSourceUnit.GenerateBreakpointStatements(context); 
						var initStatements = valueSourceUnit.Generator<IUnitGenerator>().GenerateValueOutputInit(context, valueSource);

						if (bind)
						{
							breakpointStatements = breakpointStatements.Bind(context, valueSourceUnit);
							initStatements = initStatements.Bind(context, valueSourceUnit);
						}

						foreach (var statement in breakpointStatements)
						{
							yield return statement;
						}

						foreach (var statement in initStatements)
						{
							yield return statement;
						}
					}
				}
			}
		}

		public static CodeExpression GenerateConvertedExpression(this CodeExpression expression, Type sourceType, Type destinationType)
		{
			if (expression is CodePrimitiveExpression primitive)
			{
				if (primitive.Value == null)
				{
					if (!destinationType.IsValueType || Nullable.GetUnderlyingType(destinationType) != null)
					{
						return expression;
					}
					else
					{
						throw new InvalidConversionException($"Cannot convert null literal to value type '{destinationType}'.");
					}
				}
				else
				{
					return CodeFactory.Primitive(primitive.Value.ConvertTo(destinationType));
				}
			}

			var conversionType = ConversionUtility.GetRequiredConversion(sourceType, destinationType);

			switch (conversionType)
			{
				case ConversionUtility.ConversionType.Impossible:
					throw new InvalidConversionException($"Cannot convert from '{sourceType}' to '{destinationType}'.");

				case ConversionUtility.ConversionType.Identity:
				case ConversionUtility.ConversionType.Upcast:
				case ConversionUtility.ConversionType.NumericImplicit:
				case ConversionUtility.ConversionType.UserDefinedImplicit:
				case ConversionUtility.ConversionType.UserDefinedThenNumericImplicit:
					return expression;

				case ConversionUtility.ConversionType.Downcast:
					return expression.Method("ConvertTo", CodeFactory.TypeRef(destinationType)).Invoke();

				case ConversionUtility.ConversionType.NumericExplicit: 
				case ConversionUtility.ConversionType.UserDefinedExplicit:
				case ConversionUtility.ConversionType.UserDefinedThenNumericExplicit:
					return expression.Cast(CodeFactory.TypeRef(destinationType));

				case ConversionUtility.ConversionType.UnityHierarchy:
					if (destinationType == typeof(GameObject) && typeof(Component).IsAssignableFrom(sourceType))
					{
						return expression.Field("gameObject");
					}
					else if (typeof(Component).IsAssignableFrom(destinationType))
					{
						if (sourceType == typeof(GameObject) || typeof(Component).IsAssignableFrom(sourceType))
						{
							if (destinationType == typeof(Transform))
							{
								return expression.Field("transform");
							}
							else
							{
								return expression.Method("GetComponent", CodeFactory.TypeRef(destinationType)).Invoke();
							}
						}
					}
					return expression.Method("ConvertTo", CodeFactory.TypeRef(destinationType)).Invoke();
					//throw new InvalidConversionException($"Cannot convert from '{sourceType}' to '{destinationType}'.");

				case ConversionUtility.ConversionType.EnumerableToArray:
					return expression.Method("OfType", CodeFactory.TypeRef(destinationType.GetElementType())).Invoke().Method("ToArray").Invoke();
				case ConversionUtility.ConversionType.EnumerableToList:
					return expression.Method("OfType", CodeFactory.TypeRef(destinationType.GetElementType())).Invoke().Method("ToList").Invoke();
				case ConversionUtility.ConversionType.ToString:
					return expression.Method("ToString").Invoke();
				default:
					throw new UnexpectedEnumValueException<ConversionUtility.ConversionType>(conversionType);
			}
		}

		public static Type GetSourceType(this ValueInput valueInput)
		{
			if (valueInput.hasValidConnection)
			{
				return valueInput.connection.source.type;
			}
			else if (valueInput.hasDefaultValue)
			{
				var defaultValue = valueInput.unit.defaultValues[valueInput.key];
				if (defaultValue == null)
				{
					if (valueInput.nullMeansSelf)
					{
						return typeof(GameObject);
					}
					else
					{
						return valueInput.type;
					}
				}
				else
				{
					return defaultValue.GetType();
				}
			}
			else
			{
				return valueInput.type;
			}
		}

		public static CodeExpression GenerateSourceExpression(this ValueInput valueInput, FlowMethodGenerationContext context)
		{
			if (valueInput.hasValidConnection)
			{
				var source = valueInput.connection.source;
				return source.unit.Generator<IUnitGenerator>().GenerateValueOutputExpression(context, source);
			}
			else if (valueInput.hasDefaultValue)
			{
				var unit = valueInput.unit;
				var defaultValue = unit.defaultValues[valueInput.key];

				if (defaultValue == null)
				{
					if (valueInput.nullMeansSelf)
					{
						return CodeFactory.ThisRef.Field("gameObject");
					}
					else
					{
						return CodeFactory.Primitive(null);
					}
				}
				else
				{
					var unitGenerator = unit.Generator<IUnitGenerator>();

					if (unitGenerator.defaultValueMembers.TryGetValue(valueInput.key, out var defaultValueMember))
					{
						return context.InstanceField(unit, defaultValueMember);
					}
					else
					{
						return defaultValue.Generator<IConstantGenerator>().GenerateExpression();
					}
				}
			}
			else
			{
				throw new Exception("value input \"" + valueInput.key + "\" is not connected");
			}
		}

		public static CodeExpression GenerateExpression(this ValueInput valueInput, FlowMethodGenerationContext context, bool bind = true)
		{
			return GenerateExpression(valueInput, context, valueInput.type, bind);
		}

		public static CodeExpression GenerateExpression(this ValueInput valueInput, FlowMethodGenerationContext context, Type destinationType, bool bind = true)
		{
			var result = valueInput.GenerateSourceExpression(context).GenerateConvertedExpression(valueInput.GetSourceType(), destinationType);

			if (bind)
			{
				result.Bind(context, valueInput.unit);

				if (valueInput.hasValidConnection)
				{
					result.Bind(context, valueInput.connection.source.unit);
				}
			}

			return result;
		}
	}
}
