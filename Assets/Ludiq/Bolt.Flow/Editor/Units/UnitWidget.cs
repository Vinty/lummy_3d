﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ludiq;
using UnityEditor;
using UnityEngine;

namespace Bolt
{
	[Widget(typeof(IUnit))]
	public class UnitWidget<TUnit> : NodeWidget<FlowCanvas, TUnit>, IUnitWidget where TUnit : class, IUnit
	{
		public UnitWidget(FlowCanvas canvas, TUnit unit) : base(canvas, unit)
		{
			unit.onPortsChanged += CacheDefinition;
			unit.onPortsChanged += SubWidgetsChanged;
		}

		public override void Dispose()
		{
			base.Dispose();

			unit.onPortsChanged -= CacheDefinition;
			unit.onPortsChanged -= SubWidgetsChanged;
		}

		public override IEnumerable<IWidget> subWidgets => unit.ports.Select(port => canvas.Widget(port));

		#region Model

		protected TUnit unit => element;

		IUnit IUnitWidget.unit => unit;

		protected new FlowGraph graph => (FlowGraph)base.graph;

		protected IUnitDebugData unitDebugData => GetDebugData<IUnitDebugData>();

		private new UnitDescription description => (UnitDescription)base.description;

		private new UnitAnalysis analysis => (UnitAnalysis)base.analysis;

		protected readonly List<IUnitPortWidget> ports = new List<IUnitPortWidget>();

		private readonly List<string> settingNames = new List<string>();

		protected IEnumerable<Metadata> settings
		{
			get
			{
				foreach (var settingName in settingNames)
				{
					yield return metadata[settingName];
				}
			}
		}

		protected override void CacheItemFirstTime()
		{
			base.CacheItemFirstTime();

			CacheDefinition();
		}

		protected virtual void CacheDefinition()
		{
			ports.Clear();
			ports.AddRange(unit.ports.Select(port => canvas.Widget<IUnitPortWidget>(port)));

			Reposition();
		}

		protected override void CacheDescription()
		{
			base.CacheDescription();

			titleContent.text = description.shortTitle;
			titleContent.tooltip = description.summary;
			surtitleContent.text = description.surtitle;
			subtitleContent.text = description.subtitle;
			icon = description.icon;

			Reposition();
		}

		protected override void CacheMetadata()
		{
			settingNames.Clear();

			settingNames.AddRange(metadata.valueType
				.GetMembers()
				.Where(mi => mi.HasAttribute<UnitHeaderInspectableAttribute>())
				.OrderBy(mi => mi.GetAttributes<Attribute>().OfType<IInspectableAttribute>().FirstOrDefault()?.order ?? int.MaxValue)
				.ThenBy(mi => mi.MetadataToken)
				.Select(mi => mi.Name));

			lock (settingLabelsContents)
			{
				settingLabelsContents.Clear();

				foreach (var setting in settings)
				{
					var settingLabel = setting.GetAttribute<UnitHeaderInspectableAttribute>().label;

					GUIContent settingContent;

					if (string.IsNullOrEmpty(settingLabel))
					{
						settingContent = null;
					}
					else
					{
						settingContent = new GUIContent(settingLabel);
					}

					settingLabelsContents.Add(setting, settingContent);
				}
			}

			Reposition();
		}

		public virtual Inspector GetPortInspector(IUnitPort port, Metadata metadata)
		{
			return metadata.Inspector();
		}

		#endregion


		#region Lifecycle

		public override bool foregroundRequiresInput => showSettings || unit.valueInputs.Any(vip => vip.hasDefaultValue);

		public override void HandleInput()
		{
			if (canvas.isCreatingConnection)
			{
				if (e.IsMouseDown(MouseButton.Left))
				{
					var source = canvas.connectionSource;
					
					IUnitPort destination;

					if (CanCreateCompatiblePort(canvas.connectionSource))
					{
						destination = CreateCompatiblePort(ref source);
					}
					else
					{
						destination = source.CompatiblePort(unit);
					}

					if (destination != null)
					{
						canvas.CompleteConnection(source, destination);
					}

					e.Use();
				}
				else if (e.IsMouseDown(MouseButton.Right))
				{
					canvas.CancelConnection();
					e.Use();
				}
			}

			base.HandleInput();
		}

		public virtual bool CanCreateCompatiblePort(IUnitPort port) => false;

		public virtual IUnitPort CreateCompatiblePort(ref IUnitPort port) => throw new NotSupportedException();

		#endregion


		#region Contents

		protected readonly GUIContent titleContent = new GUIContent();

		protected readonly GUIContent surtitleContent = new GUIContent();

		protected readonly GUIContent subtitleContent = new GUIContent();

		protected EditorTexture icon; 

		protected readonly Dictionary<Metadata, GUIContent> settingLabelsContents = new Dictionary<Metadata, GUIContent>();

		#endregion


		#region Positioning

		public override IEnumerable<IWidget> positionDependers => ports;

		protected Rect _position;

		protected Rect _edgePosition;

		public override Rect position
		{
			get => _position;
			set => unit.position = value.position;
		}

		public override Rect edgePosition => _edgePosition;

		public Rect titlePosition { get; private set; }

		public Rect surtitlePosition { get; private set; }

		public Rect subtitlePosition { get; private set; }

		public Rect headerIconPosition { get; private set; }

		public List<Rect> statusIconsPositions { get; private set; } = new List<Rect>();

		public Dictionary<Metadata, Rect> settingsPositions { get; } = new Dictionary<Metadata, Rect>();

		public Rect headerAddonPosition { get; private set; }

		public Rect headerPosition { get; private set; }

		public Rect? secondaryHorizontalPortsPosition { get; private set; }

		public Rect surroundPosition { get; private set; }

		public override void CachePosition()
		{
			// Enumerable Caching
			
			var verticalInputs = new List<IUnitPortWidget>();
			var verticalOutputs = new List<IUnitPortWidget>();
			var primaryHorizontalInputs = new List<IUnitPortWidget>();
			var primaryHorizontalOutputs = new List<IUnitPortWidget>();
			var secondaryHorizontalPorts = new List<IUnitPortWidget>();
			var secondaryHorizontalInputs = new List<IUnitPortWidget>();
			var secondaryHorizontalOutputs = new List<IUnitPortWidget>();

			var graphAxis = unit.graph.controlAxis;

			foreach (var portWidget in ports.OrderBy(pw => pw.port.Description<UnitPortDescription>().GetOrder(pw.axis)))
			{
				var port = portWidget.port;
				var isPrimary = port.Description<UnitPortDescription>().primaryAxes.Contains(graphAxis);
				var isHorizontal = portWidget.axis == Axis2.Horizontal;
				var isVertical = portWidget.axis == Axis2.Vertical;
				var isInput = port is IUnitInputPort;
				var isOutput = port is IUnitOutputPort;
				
				if (isHorizontal)
				{
					if (isPrimary)
					{
						if (isInput)
						{
							primaryHorizontalInputs.Add(portWidget);
						}
						else if (isOutput)
						{
							primaryHorizontalOutputs.Add(portWidget);
						}
					}
					else
					{
						secondaryHorizontalPorts.Add(portWidget);

						if (isInput)
						{
							secondaryHorizontalInputs.Add(portWidget);
						}
						else if (isOutput)
						{
							secondaryHorizontalOutputs.Add(portWidget);
						}
					}
				}
				else if (isVertical)
				{
					if (isInput)
					{
						verticalInputs.Add(portWidget);
					}
					else if (isOutput)
					{
						verticalOutputs.Add(portWidget);
					}
				}
			}
			
			foreach (var verticalInput in verticalInputs)
			{
				verticalInput.forceShowLabel = verticalInputs.Count > 1;
			}
			
			foreach (var verticalOutput in verticalOutputs)
			{
				verticalOutput.forceShowLabel = verticalOutputs.Count > 1;
			}

			foreach (var port in ports)
			{
				port.CacheSize();
			}

			var showHeaderContent = showSurtitle || showTitle || showSubtitle || showSettings || showHeaderAddon || showStatusIcons;
			var showHeaderIcon = this.showHeaderIcon && (primaryHorizontalInputs.Count == 0 || !showHeaderContent);
			var showHeader = showHeaderIcon || showHeaderContent;
			var headerIconSize = showHeaderContent ? Styles.headerIconSizeWithContent : Styles.headerIconSizeWithoutContent;

			// Width

			var secondaryHorizontalInputsWidth = 0f;
			var secondaryHorizontalOutputsWidth = 0f;
			var verticalInputsWidth = 0f;
			var verticalOutputsWidth = 0f;

			foreach (var secondaryHorizontalInput in secondaryHorizontalInputs)
			{
				secondaryHorizontalInputsWidth = Mathf.Max(secondaryHorizontalInputsWidth, secondaryHorizontalInput.outerSize.x);
			}

			foreach (var secondaryHorizontalOutput in secondaryHorizontalOutputs)
			{
				secondaryHorizontalOutputsWidth = Mathf.Max(secondaryHorizontalOutputsWidth, secondaryHorizontalOutput.outerSize.x);
			}

			foreach (var verticalInput in verticalInputs)
			{
				verticalInputsWidth += verticalInput.outerSize.x;
			}

			foreach (var verticalOutput in verticalOutputs)
			{
				verticalOutputsWidth += verticalOutput.outerSize.x;
			}

			var secondaryHorizontalPortsWidth = secondaryHorizontalInputsWidth + secondaryHorizontalOutputsWidth;

			var verticalPortsWidth = Mathf.Max(verticalInputsWidth, verticalOutputsWidth);

			var headerIconWidth = 0f;

			if (showHeaderIcon)
			{
				headerIconWidth += headerIconSize;
			}

			if (showHeaderIcon && showHeaderContent)
			{
				headerIconWidth += Styles.spaceAfterHeaderIcon;
			}
			
			var headerIdentifierWidth = 0f;

			var maxHeaderIdentifierWidth = Styles.maxHeaderIdentifierWidth;

			var headerAddonWidth = 0f;

			if (showHeaderAddon)
			{
				headerAddonWidth = GetHeaderAddonWidth();
				maxHeaderIdentifierWidth = Math.Max(maxHeaderIdentifierWidth, headerAddonWidth);
			}

			var titleWidth = 0f;

			if (showTitle)
			{
				titleWidth = Mathf.Min(maxHeaderIdentifierWidth, Styles.title.CalcSize(titleContent).x);
				headerIdentifierWidth = Mathf.Max(headerIdentifierWidth, titleWidth);
			}
			
			var surtitleWidth = 0f;

			if (showSurtitle)
			{
				surtitleWidth = Mathf.Min(maxHeaderIdentifierWidth, Styles.surtitle.CalcSize(surtitleContent).x);
				headerIdentifierWidth = Mathf.Max(headerIdentifierWidth, surtitleWidth);
			}

			var subtitleWidth = 0f;

			if (showSubtitle)
			{
				subtitleWidth = Mathf.Min(maxHeaderIdentifierWidth, Styles.subtitle.CalcSize(subtitleContent).x);
				headerIdentifierWidth = Mathf.Max(headerIdentifierWidth, subtitleWidth);
			}

			var statusIconsWidth = 0f;

			if (showStatusIcons)
			{
				var statusIconsColumns = Mathf.Ceil((float)description.statusIcons.Length / Styles.iconsPerColumn);
				statusIconsWidth = statusIconsColumns * Styles.statusIconSize + ((statusIconsColumns - 1) * Styles.iconsSpacing);
			}

			settingsPositions.Clear();

			var settingsWidth = 0f;

			if (showSettings)
			{
				foreach (var setting in settings)
				{
					var settingWidth = 0f;

					var settingLabelContent = settingLabelsContents[setting];

					if (settingLabelContent != null)
					{
						settingWidth += Styles.settingLabel.CalcSize(settingLabelContent).x;
					}

					settingWidth += setting.Inspector().GetAdaptiveWidth();

					settingWidth = Mathf.Min(settingWidth, Styles.maxSettingsWidth);

					settingsPositions.Add(setting, new Rect(0, 0, settingWidth, 0));

					settingsWidth = Mathf.Max(settingsWidth, settingWidth);
				}
			}
			
			var headerWidth = headerIconWidth + Mathf.Max(headerIdentifierWidth + statusIconsWidth, Mathf.Max(settingsWidth, headerAddonWidth));
			
			if (showHeader)
			{
				headerWidth += Styles.headerPadding.left;
				headerWidth += Styles.headerPadding.right;
			}

			var primaryHorizontalInputsWidth = 0f;
			var primaryHorizontalOutputsWidth = 0f;

			foreach (var primaryHorizontalInput in primaryHorizontalInputs)
			{
				primaryHorizontalInputsWidth = Mathf.Max(primaryHorizontalInputsWidth, primaryHorizontalInput.outerSize.x);
			}

			foreach (var primaryHorizontalOutput in primaryHorizontalOutputs)
			{
				primaryHorizontalOutputsWidth = Mathf.Max(primaryHorizontalOutputsWidth, primaryHorizontalOutput.outerSize.x);
			}

			var edgeWidth = Mathf.Max(headerWidth + primaryHorizontalInputsWidth + primaryHorizontalOutputsWidth, Mathf.Max(secondaryHorizontalPortsWidth, verticalPortsWidth));

			if (secondaryHorizontalPortsWidth < edgeWidth)
			{
				var inputsWeight = secondaryHorizontalInputsWidth / secondaryHorizontalPortsWidth;
				var outputsWeight = secondaryHorizontalOutputsWidth / secondaryHorizontalPortsWidth;

				secondaryHorizontalInputsWidth = inputsWeight * edgeWidth;
				secondaryHorizontalOutputsWidth = outputsWeight * edgeWidth;
			}

			// Height

			var height = 0f;

			var verticalInputsHeight = 0f;

			foreach (var verticalInput in verticalInputs)
			{
				verticalInputsHeight = Mathf.Max(verticalInputsHeight, verticalInput.outerSize.y);
			}

			height += verticalInputsHeight;

			if (verticalInputsHeight > 0)
			{
				height += Styles.spaceAfterVerticalInputs;
			}
			
			var headerHeight = 0f;
			var headerContentHeight = 0f;
			
			if (showHeader)
			{
				headerHeight += Styles.headerPadding.top;
			}

			var headerIconHeight = 0f;

			if (showHeaderIcon)
			{
				headerIconHeight = headerIconSize;
			}
			else
			{
				headerIconPosition = Rect.zero;
			}

			var surtitleHeight = 0f;

			if (showSurtitle)
			{
				surtitleHeight = Styles.surtitle.CalcHeight(surtitleContent, headerIdentifierWidth);

				headerContentHeight += surtitleHeight;

				headerContentHeight += Styles.spaceAfterSurtitle;
			}

			var titleHeight = 0f;

			if (showTitle)
			{
				titleHeight = Styles.title.CalcHeight(titleContent, headerIdentifierWidth);

				headerContentHeight += titleHeight;
			}

			var subtitleHeight = 0f;

			if (showSubtitle)
			{
				headerContentHeight += Styles.spaceBeforeSubtitle;

				subtitleHeight = Styles.subtitle.CalcHeight(subtitleContent, headerIdentifierWidth);

				headerContentHeight += subtitleHeight;
			}

			var settingsHeight = 0f;

			if (showSettings)
			{
				headerContentHeight += Styles.spaceBeforeSettings;

				foreach (var setting in settings)
				{
					var settingPosition = settingsPositions[setting];

					using (LudiqGUIUtility.currentInspectorWidth.Override(settingPosition.width))
					{
						settingPosition.height = LudiqGUI.GetInspectorHeight(null, setting, settingPosition.width, settingLabelsContents[setting] ?? GUIContent.none);

						settingsPositions[setting] = settingPosition;

						settingsHeight += settingPosition.height;

						settingsHeight += Styles.spaceBetweenSettings;
					}
				}

				settingsHeight -= Styles.spaceBetweenSettings;

				headerContentHeight += settingsHeight;

				headerContentHeight += Styles.spaceAfterSettings;
			}

			var headerAddonHeight = 0f;

			if (showHeaderAddon)
			{
				headerAddonHeight = GetHeaderAddonHeight(headerAddonWidth);
				
				headerContentHeight += headerAddonHeight;
			}
			
			headerHeight += Mathf.Max(headerContentHeight, headerIconHeight);

			if (showHeader)
			{
				headerHeight += Styles.headerPadding.bottom;
			}

			var primaryHorizontalInputsHeight = 0f;
			
			foreach (var primaryHorizontalInput in primaryHorizontalInputs)
			{
				primaryHorizontalInputsHeight += primaryHorizontalInput.outerSize.y;
			}

			var primaryHorizontalOutputsHeight = 0f;
			
			foreach (var primaryHorizontalOutput in primaryHorizontalOutputs)
			{
				primaryHorizontalOutputsHeight += primaryHorizontalOutput.outerSize.y;
			}

			var primaryHorizontalPortsHeight = Mathf.Max(primaryHorizontalInputsHeight, primaryHorizontalOutputsHeight);
			
			var headerAndPrimaryHorizontalPortsHeight = Mathf.Max(primaryHorizontalPortsHeight, headerHeight);
			
			height += headerAndPrimaryHorizontalPortsHeight;
			
			var secondaryHorizontalInputsHeight = 0f;
			
			foreach (var secondaryHorizontalInput in secondaryHorizontalInputs)
			{
				secondaryHorizontalInputsHeight += secondaryHorizontalInput.outerSize.y;
			}

			var secondaryHorizontalOutputsHeight = 0f;
			
			foreach (var secondaryHorizontalOutput in secondaryHorizontalOutputs)
			{
				secondaryHorizontalOutputsHeight += secondaryHorizontalOutput.outerSize.y;
			}

			var secondaryHorizontalPortsHeight = Mathf.Max(secondaryHorizontalInputsHeight, secondaryHorizontalOutputsHeight);

			height += secondaryHorizontalPortsHeight;

			var verticalOutputsHeight = 0f;

			foreach (var verticalOutput in verticalOutputs)
			{
				verticalOutputsHeight = Mathf.Max(verticalOutputsHeight, verticalOutput.outerSize.y);
			}

			height += verticalOutputsHeight;

			if (secondaryHorizontalPorts.Count > 0 && verticalOutputsHeight == 0)
			{
				height += Styles.spaceAfterSecondaryHorizontalPorts;
			}
			
			if (verticalOutputsHeight > 0)
			{
				height += Styles.spaceBeforeVerticalOutputs;
			}
			
			var edgeHeight = height;




			// Positioning

			var edgeOrigin = unit.position;
			var edgeX = edgeOrigin.x;
			var edgeY = edgeOrigin.y;

			var x = edgeX;
			var y = edgeY;

			if (verticalInputs.Count > 0)
			{
				var verticalInputsStartX = edgeX + (edgeWidth - verticalInputsWidth) / 2;
				var verticalInputsX = verticalInputsStartX;

				for (int i = 0; i < verticalInputs.Count; i++)
				{
					var verticalInput = verticalInputs[i];
					var isFirst = i == 0;
					var isLast = i == verticalInputs.Count - 1;

					var verticalInputWidth = verticalInput.outerSize.x;
					var verticalInputHeight = verticalInput.outerSize.y;

					verticalInput.outerPosition = new Rect
					(
						verticalInputsX,
						y,
						verticalInputWidth,
						verticalInputHeight
					);
					
					verticalInputsX += verticalInputWidth;

					var drawnEdges = Edges.None;

					if (!isFirst)
					{
						drawnEdges |= Edges.Left;
					}

					if (!isLast)
					{
						drawnEdges |= Edges.Right;
					}

					verticalInput.drawnEdges = drawnEdges;
				}

				y += verticalInputsHeight;

				if (verticalInputsHeight > 0)
				{
					y += Styles.spaceAfterVerticalInputs;
				}
			}

			var yAfterVerticalInputs = y;
			
			var centerHeaderX = true;

			float headerX;

			if (centerHeaderX)
			{
				headerX = edgeX + (edgeWidth - headerWidth) / 2;
			
				if (headerX < edgeX + primaryHorizontalInputsWidth || 
				    headerX + headerWidth > edgeX + edgeWidth - primaryHorizontalOutputsWidth)
				{
					headerX = edgeX + primaryHorizontalInputsWidth + (edgeWidth - primaryHorizontalInputsWidth - primaryHorizontalOutputsWidth - headerWidth) / 2;
				}
			}
			else
			{
				headerX = edgeX + primaryHorizontalInputsWidth;
			}
			
			var headerY = yAfterVerticalInputs;
			
			var centerHeaderY = true;

			if (centerHeaderY)
			{
				headerY += (headerAndPrimaryHorizontalPortsHeight - headerHeight) / 2;
			}

			x = headerX;
			y = headerY;

			if (showHeader)
			{
				x += Styles.headerPadding.left;
				y += Styles.headerPadding.top;
			}
			
			if (showHeaderIcon)
			{
				headerIconPosition = new Rect
				(
					x,
					y,
					headerIconSize,
					headerIconSize
				);

				x += headerIconPosition.width;
			}
			else
			{
				headerIconPosition = Rect.zero;
			}
			
			if (showHeaderIcon && showHeaderContent)
			{
				x += Styles.spaceAfterHeaderIcon;
			}
			
			if (showSurtitle)
			{
				surtitlePosition = new Rect
				(
					x,
					y,
					surtitleWidth,
					surtitleHeight
				);
				
				y += surtitleHeight;
				
				y += Styles.spaceAfterSurtitle;
			}
			
			if (showTitle)
			{
				titlePosition = new Rect
				(
					x,
					y,
					titleWidth,
					titleHeight
				);
				
				y += titleHeight;
			}
			
			if (showSubtitle)
			{
				y += Styles.spaceBeforeSubtitle;
				
				subtitlePosition = new Rect
				(
					x,
					y,
					subtitleWidth,
					subtitleHeight
				);
				
				y += subtitleHeight;
			}

			statusIconsPositions.Clear();

			if (showStatusIcons)
			{
				var iconRow = 0;
				var iconCol = 0;

				for (int i = 0; i < description.statusIcons.Length; i++)
				{
					var iconPosition = new Rect
					(
						edgeX + edgeWidth - Styles.headerPadding.right - ((iconCol + 1) * Styles.statusIconSize) - ((iconCol) * Styles.iconsSpacing),
						edgeY + Styles.headerPadding.top + (iconRow * (Styles.statusIconSize + Styles.iconsSpacing)),
						Styles.statusIconSize,
						Styles.statusIconSize
					);

					statusIconsPositions.Add(iconPosition);

					iconRow++;

					if (iconRow % Styles.iconsPerColumn == 0)
					{
						iconCol++;
						iconRow = 0;
					}
				}
			}
			
			if (showSettings)
			{
				y += Styles.spaceBeforeSettings;

				foreach (var setting in settings)
				{
					var settingPosition = settingsPositions[setting];
					settingPosition.x = x;
					settingPosition.y = y;
					settingsPositions[setting] = settingPosition;
					y += settingPosition.height;
					y += Styles.spaceBetweenSettings;
				}
				
				y -= Styles.spaceBetweenSettings;
				y += Styles.spaceAfterSettings;
			}

			if (showHeaderAddon)
			{
				headerAddonPosition = new Rect
				(
					x,
					y,
					headerAddonWidth,
					headerAddonHeight
				);
				
				y += headerAddonHeight;
			}

			headerPosition = new Rect
			(
				headerX,
				headerY,
				headerWidth,
				headerHeight
			);

			y = yAfterVerticalInputs;
			
			for (int i = 0; i < primaryHorizontalInputs.Count; i++)
			{
				var primaryHorizontalInput = primaryHorizontalInputs[i];
				var isFirst = i == 0;
				var isLast = i == primaryHorizontalInputs.Count - 1;

				var primaryHorizontalInputWidth = primaryHorizontalInputsWidth;
				var primaryHorizontalInputHeight = primaryHorizontalInput.outerSize.y;

				primaryHorizontalInput.outerPosition = new Rect
				(
					edgeX,
					y,
					primaryHorizontalInputWidth,
					primaryHorizontalInputHeight
				);
				
				y += primaryHorizontalInputHeight;

				var drawnEdges = Edges.None;

				if (!isFirst)
				{
					drawnEdges |= Edges.Top;
				}

				if (!isLast)
				{
					drawnEdges |= Edges.Bottom;
				}

				primaryHorizontalInput.drawnEdges = drawnEdges;
			}

			y = yAfterVerticalInputs;
			
			for (int i = 0; i < primaryHorizontalOutputs.Count; i++)
			{
				var primaryHorizontalOutput = primaryHorizontalOutputs[i];
				var isFirst = i == 0;
				var isLast = i == primaryHorizontalOutputs.Count - 1;

				var primaryHorizontalOutputWidth = primaryHorizontalOutputsWidth;
				var primaryHorizontalOutputHeight = primaryHorizontalOutput.outerSize.y;

				primaryHorizontalOutput.outerPosition = new Rect
				(
					edgeX + edgeWidth - primaryHorizontalOutputWidth,
					y,
					primaryHorizontalOutputWidth,
					primaryHorizontalOutputHeight
				);
				
				y += primaryHorizontalOutputHeight;

				var drawnEdges = Edges.None;

				if (!isFirst)
				{
					drawnEdges |= Edges.Top;
				}

				if (!isLast)
				{
					drawnEdges |= Edges.Bottom;
				}

				primaryHorizontalOutput.drawnEdges = drawnEdges;
			}

			if (primaryHorizontalInputs.Count == 1 && primaryHorizontalInputsHeight < headerAndPrimaryHorizontalPortsHeight)
			{
				var primaryHorizontalInput = primaryHorizontalInputs[0];
				var primaryHorizontalInputPosition = primaryHorizontalInput.outerPosition;
				primaryHorizontalInputPosition.height = headerAndPrimaryHorizontalPortsHeight;
				primaryHorizontalInput.outerPosition = primaryHorizontalInputPosition;
			}

			if (primaryHorizontalOutputs.Count == 1 && primaryHorizontalOutputsHeight < headerAndPrimaryHorizontalPortsHeight)
			{
				var primaryHorizontalOutput = primaryHorizontalOutputs[0];
				var primaryHorizontalOutputPosition = primaryHorizontalOutput.outerPosition;
				primaryHorizontalOutputPosition.height = headerAndPrimaryHorizontalPortsHeight;
				primaryHorizontalOutput.outerPosition = primaryHorizontalOutputPosition;
			}

			y = yAfterVerticalInputs + headerAndPrimaryHorizontalPortsHeight;

			secondaryHorizontalPortsPosition = null;

			if (secondaryHorizontalPorts.Count > 0)
			{
				var secondaryHorizontalPortStartY = y;
				
				for (int i = 0; i < secondaryHorizontalInputs.Count; i++)
				{
					var secondaryHorizontalInput = secondaryHorizontalInputs[i];
					var isFirst = i == 0;
					var isLast = i == secondaryHorizontalInputs.Count - 1;

					var secondaryHorizontalInputWidth = secondaryHorizontalInputsWidth;
					var secondaryHorizontalInputHeight = secondaryHorizontalInput.outerSize.y;

					secondaryHorizontalInput.outerPosition = new Rect
					(
						edgeX,
						y,
						secondaryHorizontalInputWidth,
						secondaryHorizontalInputHeight
					);
					
					secondaryHorizontalPortsPosition = secondaryHorizontalPortsPosition.Encompass(secondaryHorizontalInput.outerPosition);
					
					y += secondaryHorizontalInputHeight;

					var drawnEdges = Edges.None;

					if (!isFirst)
					{
						drawnEdges |= Edges.Top;
					}

					if (!isLast)
					{
						drawnEdges |= Edges.Bottom;
					}

					secondaryHorizontalInput.drawnEdges = drawnEdges;
				}

				y = secondaryHorizontalPortStartY;
				
				for (int i = 0; i < secondaryHorizontalOutputs.Count; i++)
				{
					var secondaryHorizontalOutput = secondaryHorizontalOutputs[i];
					var isFirst = i == 0;
					var isLast = i == secondaryHorizontalOutputs.Count - 1;

					var secondaryHorizontalOutputWidth = secondaryHorizontalOutputsWidth;
					var secondaryHorizontalOutputHeight = secondaryHorizontalOutput.outerSize.y;

					secondaryHorizontalOutput.outerPosition = new Rect
					(
						edgeX + edgeWidth - secondaryHorizontalOutputWidth,
						y,
						secondaryHorizontalOutputWidth,
						secondaryHorizontalOutputHeight
					);
					
					secondaryHorizontalPortsPosition = secondaryHorizontalPortsPosition.Encompass(secondaryHorizontalOutput.outerPosition);
					
					y += secondaryHorizontalOutputHeight;

					var drawnEdges = Edges.None;

					if (!isFirst)
					{
						drawnEdges |= Edges.Top;
					}

					if (!isLast)
					{
						drawnEdges |= Edges.Bottom;
					}

					secondaryHorizontalOutput.drawnEdges = drawnEdges;
				}
				
				y = secondaryHorizontalPortStartY + secondaryHorizontalPortsHeight;
			}
			
			if (secondaryHorizontalPorts.Count > 0 && verticalOutputsHeight == 0)
			{
				y += Styles.spaceAfterSecondaryHorizontalPorts;
			}
			
			if (verticalOutputs.Count > 0)
			{
				var verticalOutputsStartX = edgeX + (edgeWidth - verticalOutputsWidth) / 2;
				var verticalOutputsX = verticalOutputsStartX;

				if (verticalOutputsHeight > 0)
				{
					y += Styles.spaceBeforeVerticalOutputs;
				}
				
				for (int i = 0; i < verticalOutputs.Count; i++)
				{
					var verticalOutput = verticalOutputs[i];
					var isFirst = i == 0;
					var isLast = i == verticalOutputs.Count - 1;

					var verticalOutputWidth = verticalOutput.outerSize.x;
					var verticalOutputHeight = verticalOutput.outerSize.y;

					verticalOutput.outerPosition = new Rect
					(
						verticalOutputsX,
						y,
						verticalOutputWidth,
						verticalOutputHeight
					);
					
					verticalOutputsX += verticalOutputWidth;

					var drawnEdges = Edges.None;

					if (!isFirst)
					{
						drawnEdges |= Edges.Left;
					}

					if (!isLast)
					{
						drawnEdges |= Edges.Right;
					}

					verticalOutput.drawnEdges = drawnEdges;
				}
				
				y += verticalOutputsHeight;
			}
			
			_position = new Rect
			(
				edgeX,
				edgeY,
				edgeWidth,
				edgeHeight
			);

			_edgePosition = _position;

			var isOneLiner = 
				primaryHorizontalInputs.Count <= 1 &&
				primaryHorizontalOutputs.Count == 1 &&
				secondaryHorizontalPorts.Count == 0 &&
				!showSubtitle &&
				!showSurtitle && 
				verticalInputsHeight == 0 &&
				verticalOutputsHeight == 0;

			if (isOneLiner)
			{
				_edgePosition.y += 2;
				_edgePosition.height -= 4;
			}

			surroundPosition = Styles.surroundPadding.Add(position);
		}

		protected virtual float GetHeaderAddonWidth()
		{
			return 0;
		}

		protected virtual float GetHeaderAddonHeight(float width)
		{
			return 0;
		}
		
		protected override Rect EdgeToInnerPosition(Rect position)
		{
			return position;
		}

		protected override Rect InnerToEdgePosition(Rect position)
		{
			return position;
		}

		#endregion


		#region Drawing

		protected virtual NodeColorMix baseColor => NodeColor.Gray;

		protected override NodeColorMix color
		{
			get
			{
				if (unitDebugData.runtimeException != null)
				{
					return NodeColor.Red;
				}

				var color = baseColor;

				if (analysis.warnings.Count > 0)
				{
					var mostSevereWarning = Warning.MostSevereLevel(analysis.warnings);

					switch (mostSevereWarning)
					{
						case WarningLevel.Error:
							color = NodeColor.Red;

							break;

						case WarningLevel.Severe:
							color = NodeColor.Orange;

							break;

						case WarningLevel.Caution:
							color = NodeColor.Yellow;

							break;
					}
				}

				if (EditorApplication.isPaused)
				{
					if (EditorTimeBinding.frame == unitDebugData.lastInvokeFrame)
					{
						return NodeColor.Blue;
					}
				}
				else
				{
					var mix = color;

					mix.blue = GraphGUI.GetActivity(unitDebugData.lastInvokeFrame, unitDebugData.lastInvokeTime);

					return mix;
				}

				return color;
			}
		}

		protected override NodeShape shape => NodeShape.Square;

		protected virtual bool showHeaderIcon => icon != null;

		protected virtual bool showTitle => description.showTitle && !string.IsNullOrEmpty(titleContent.text);

		protected virtual bool showSurtitle => false && !string.IsNullOrEmpty(surtitleContent.text);

		protected virtual bool showSubtitle => false && !string.IsNullOrEmpty(subtitleContent.text);

		protected virtual bool showStatusIcons => description.statusIcons.Length > 0;

		protected virtual bool showSettings => settingNames.Count > 0;

		protected virtual bool showHeaderAddon => false;

		protected override bool dim
		{
			get
			{
				var dim = LudiqGraphs.Configuration.dimInactiveNodes && !analysis.isEntered;

				if (isMouseOver || isSelected)
				{
					dim = false;
				}

				if (LudiqGraphs.Configuration.dimIncompatibleNodes && canvas.isCreatingConnection)
				{
					dim = !CanCreateCompatiblePort(canvas.connectionSource) && !unit.ports.Any(p => canvas.connectionSource == p || canvas.connectionSource.CanValidlyConnectTo(p));
				}

				return dim;
			}
		}

		public override void DrawForeground()
		{
			BeginDim();

			BeginBreakpointDim();
			
			base.DrawForeground();
			
			if (BoltFlow.Configuration.revealRelations.IsMet(isMouseOver, isSelected, e.alt))
			{
				DrawRelations();
			}
			else
			{
				DrawBackgrounds();
			}

			if (showHeaderIcon)
			{
				DrawIcon();
			}

			if (showSurtitle)
			{
				DrawSurtitle();
			}

			if (showTitle)
			{
				DrawTitle();
			}

			if (showSubtitle)
			{
				DrawSubtitle();
			}

			if (showStatusIcons)
			{
				DrawIcons();
			}

			if (showSettings)
			{
				DrawSettings();
			}

			if (showHeaderAddon)
			{
				DrawHeaderAddon();
			}

			EndBreakpointDim();

			EndDim();

			DrawSurround();
		}

		protected void DrawIcon()
		{
			GUI.DrawTexture(headerIconPosition, icon[(int)headerIconPosition.width]);
		}

		protected void DrawTitle()
		{
			GUI.Label(titlePosition.FixLabelRect(canvas.zoom), titleContent, invertForeground ? Styles.titleInverted : Styles.title);
		}

		protected void DrawSurtitle()
		{
			GUI.Label(surtitlePosition.FixLabelRect(canvas.zoom), surtitleContent, invertForeground ? Styles.surtitleInverted : Styles.surtitle);
		}

		protected void DrawSubtitle()
		{
			GUI.Label(subtitlePosition.FixLabelRect(canvas.zoom), subtitleContent, invertForeground ? Styles.subtitleInverted : Styles.subtitle);
		}

		protected void DrawIcons()
		{
			for (int i = 0; i < description.statusIcons.Length; i++)
			{
				var icon = description.statusIcons[i];
				var position = statusIconsPositions[i];

				GUI.DrawTexture(position, icon[(int)position.width]);
			}
		}

		private void DrawSettings()
		{
			if (canvas.zoom < FlowCanvas.inspectorZoomThreshold)
			{
				return;
			}

			EditorGUI.BeginDisabledGroup(!e.IsRepaint && isMouseThrough && !isMouseOver);

			EditorGUI.BeginChangeCheck();

			foreach (var setting in settings)
			{
				DrawSetting(setting);
			}

			if (EditorGUI.EndChangeCheck())
			{
				unit.Define();
				Reposition();
			}

			EditorGUI.EndDisabledGroup();
		}

		protected void DrawSetting(Metadata setting)
		{
			var settingPosition = settingsPositions[setting];

			using (LudiqGUIUtility.currentInspectorWidth.Override(settingPosition.width))
			using (Inspector.expandTooltip.Override(false))
			{
				var label = settingLabelsContents[setting];

				if (label == null)
				{
					LudiqGUI.Inspector(setting, settingPosition, GUIContent.none);
				}
				else
				{
					using (Inspector.defaultLabelStyle.Override(Styles.settingLabel))
					using (LudiqGUIUtility.labelWidth.Override(Styles.settingLabel.CalcSize(label).x))
					{
						LudiqGUI.Inspector(setting, settingPosition, label);
					}
				}
			}
		}

		protected virtual void DrawHeaderAddon() { }

		protected void DrawRelations()
		{
			foreach (var relation in unit.relations)
			{
				var source = ports.Single(pw => pw.port == relation.source);
				var destination = ports.Single(pw => pw.port == relation.destination);

				var sourcePosition = source.handlePosition.center;
				var destinationPosition = destination.handlePosition.center;

				var sourceTangent = sourcePosition;
				var destinationTangent = destinationPosition;

				if (relation.source is IUnitInputPort &&
				    relation.destination is IUnitInputPort)
				{
					//startTangent -= new Vector2(20, 0);
					destinationTangent -= new Vector2(32, 0);
				}
				else
				{
					switch (source.axis)
					{
						case Axis2.Horizontal:
							sourceTangent += new Vector2(innerPosition.width / 2, 0);
							break;

						case Axis2.Vertical:
							sourceTangent += new Vector2(0, innerPosition.height / 2);
							break;

						default: throw new UnexpectedEnumValueException<Axis2>(source.axis);
					}

					switch (destination.axis)
					{
						case Axis2.Horizontal:
							destinationTangent += new Vector2(-innerPosition.width / 2, 0);
							break;

						case Axis2.Vertical:
							destinationTangent += new Vector2(0, -innerPosition.height / 2);
							break;

						default: throw new UnexpectedEnumValueException<Axis2>(destination.axis);
					}
				}

				Handles.DrawBezier
				(
					sourcePosition,
					destinationPosition,
					sourceTangent,
					destinationTangent,
					new Color(1, 1, 1, 0.5f),
					null,
					3
				);
			}
		}
		
		protected void DrawBackgrounds()
		{
			if (e.IsRepaint)
			{
				//Styles.background.Draw(headerPosition, false, false, false, false);

				if (secondaryHorizontalPortsPosition != null)
				{
					Styles.background.Draw(secondaryHorizontalPortsPosition.Value, false, false, false, false);
				}
			}
		}

		public void DrawSurround()
		{
			if (canvas.isCreatingConnection && isMouseOver && CanCreateCompatiblePort(canvas.connectionSource))
			{
				if (e.controlType == EventType.Repaint)
				{
					Styles.surround.Draw(surroundPosition, false, false, false, false);
				}
			}
		}

		protected void BeginBreakpointDim()
		{
			if (unit.breakpoint)
			{
				LudiqGUI.color.BeginOverride(Color.Lerp(LudiqGUI.color.value, ColorPalette.red, 0.25f));
			}
		}

		protected void EndBreakpointDim()
		{
			if (unit.breakpoint)
			{
				LudiqGUI.color.EndOverride();
			}
		}

		#endregion


		#region Selecting

		public override bool canSelect => true;

		#endregion


		#region Dragging

		public override bool canDrag => true;

		public override void ExpandDragGroup(HashSet<IGraphElement> dragGroup)
		{
			if (LudiqGraphs.Configuration.carrying != e.ctrlOrCmd)
			{
				foreach (var port in unit.ports)
				{
					if (port is ValueInput || port is ControlOutput)
					{
						if (port.unit is IUnitPortProxy proxy && port == proxy.target)
						{
							continue;
						}

						foreach (var connectedPort in port.connectedPorts)
						{
							if (dragGroup.Contains(connectedPort.unit))
							{
								continue;
							}

							if (connectedPort.unit is IUnitPortProxy connectedProxy && connectedPort == connectedProxy.target)
							{
								continue;
							}

							dragGroup.Add(connectedPort.unit);
							
							canvas.Widget(connectedPort.unit).ExpandDragGroup(dragGroup);
						}
					}
				}
			}
		}

		public override void RegisterSnappingAnchors(SnappingSystem snapping, SnappingAnchorType type)
		{
			foreach (var portWidget in ports)
			{
				foreach (var connection in portWidget.port.connections)
				{
					var set = connection.guid;
					snapping.AddSet(set, portWidget.axis.Perpendicular());
					snapping.AddMatch(set, set);
					snapping[set].Add(portWidget, portWidget.handlePosition.center, type);
				}
			}
		}

		#endregion


		#region Deleting

		public override bool canDelete => true;

		#endregion


		#region Clipboard

		public override void ExpandCopyGroup(HashSet<IGraphElement> copyGroup)
		{
			copyGroup.UnionWith(unit.connections);
		}

		#endregion


		#region Screenshotting

		public override void ExpandScreenshotGroup(HashSet<IWidget> screenshotGroup)
		{
			base.ExpandScreenshotGroup(screenshotGroup);

			//screenshotGroup.UnionWith(unit.connections);
		}

		#endregion


		#region Context

		protected override IEnumerable<DropdownOption> individualContextOptions
		{
			get
			{
				yield return new DropdownOption((Action)ReplaceUnit, "Replace...");
				
				foreach (var baseOption in base.individualContextOptions)
				{
					yield return baseOption;
				}

				if (unit.breakpoint)
				{
					yield return new DropdownOption((Action)RemoveBreakpoint, "Remove Breakpoint");
				}
				else
				{
					yield return new DropdownOption((Action)AddBreakpoint, "Add Breakpoint");
				}
			}
		}

		private void ReplaceUnit()
		{
			ReplaceUnit("Replace Unit");
		}

		protected void ReplaceUnit(string actionName, Member overloadMember = null)
		{
			var oldUnit = unit;
			var unitPosition = oldUnit.position;
			var preservation = UnitPreservation.Preserve(oldUnit);

			var activatorPosition = new Rect(e.mousePosition, new Vector2(200, 1));
			var options = new UnitOptionTree(new GUIContent(actionName))
			{
				reference = reference,
				overloadMember = overloadMember,
			};
			var selected = overloadMember != null ? overloadMember : null;

			var context = this.context;

			LudiqGUI.FuzzyDropdown
			(
				activatorPosition,
				options,
				selected,
				delegate(object _option)
				{
					var option = (IUnitOption)_option;

					context.BeginEdit();
					UndoUtility.RecordEditedObject(actionName);
					var graph = oldUnit.graph;
					oldUnit.graph.units.Remove(oldUnit);
					var newUnit = option.InstantiateUnit();
					newUnit.guid = Guid.NewGuid();
					newUnit.position = unitPosition;
					graph.units.Add(newUnit);
					preservation.RestoreTo(newUnit);
					option.PreconfigureUnit(newUnit);
					selection.Select(newUnit);
					GUI.changed = true;
					context.EndEdit();
				}
			);
		}

		private void AddBreakpoint()
		{
			UndoUtility.RecordEditedObject("Add Breakpoint");
			unit.breakpoint = true;
			GUI.changed = true;
		}

		private void RemoveBreakpoint()
		{
			UndoUtility.RecordEditedObject("Remove Breakpoint");
			unit.breakpoint = false;
			if (unit.GetException(reference) is Breakpoint breakpoint)
			{
				unit.SetException(reference, null);
			}
			GUI.changed = true;
		}

		#endregion


		public static class Styles
		{
			static Styles()
			{
				title = new GUIStyle(LudiqGraphs.Styles.nodeLabel);
				title.padding = new RectOffset(0, 0, 0, 0);
				title.margin = new RectOffset(0, 0, 0, 0);
				title.fontSize = 12;
				title.alignment = TextAnchor.MiddleLeft;
				title.wordWrap = true;

				surtitle = new GUIStyle(LudiqGraphs.Styles.nodeLabel);
				surtitle.padding = new RectOffset(0, 5, 0, 0);
				surtitle.margin = new RectOffset(0, 0, 0, 0);
				surtitle.fontSize = 10;
				surtitle.alignment = TextAnchor.MiddleLeft;
				surtitle.wordWrap = true;

				subtitle = new GUIStyle(surtitle);
				subtitle.padding.bottom = 2;

				titleInverted = new GUIStyle(title);
				titleInverted.normal.textColor = ColorPalette.unityBackgroundDark;

				surtitleInverted = new GUIStyle(surtitle);
				surtitleInverted.normal.textColor = ColorPalette.unityBackgroundDark;

				subtitleInverted = new GUIStyle(subtitle);
				subtitleInverted.normal.textColor = ColorPalette.unityBackgroundDark;

				settingLabel = new GUIStyle(LudiqGraphs.Styles.nodeLabel);
				settingLabel.padding.left = 0;
				settingLabel.padding.right = 5;
				settingLabel.wordWrap = false;
				settingLabel.clipping = TextClipping.Clip;

				background = new GUIStyle("IN BigTitle");

				surround = new GUIStyle("LightmapEditorSelectedHighlight");
			}

			public static readonly GUIStyle title;

			public static readonly GUIStyle surtitle;

			public static readonly GUIStyle subtitle;

			public static readonly GUIStyle titleInverted;

			public static readonly GUIStyle surtitleInverted;

			public static readonly GUIStyle subtitleInverted;

			public static readonly GUIStyle settingLabel;

			public static readonly GUIStyle background;

			public static readonly RectOffset headerPadding = new RectOffset(6, 6, 6, 6);

			public static readonly float spaceAfterVerticalInputs = 0;

			public static readonly float spaceBeforeVerticalOutputs = 0;

			public static readonly float spaceAfterSecondaryHorizontalPorts = 4;

			public static readonly float spaceBeforeSettings = 4;

			public static readonly float spaceBetweenSettings = 3;

			public static readonly float spaceAfterSettings = 0;

			public static readonly float maxSettingsWidth = 150;

			public static readonly float maxHeaderIdentifierWidth = 150;

			public static readonly float headerIconSizeWithContent = IconSize.Small;

			public static readonly float headerIconSizeWithoutContent = IconSize.Medium;

			public static readonly float statusIconSize = IconSize.Small;

			public static readonly float iconsSpacing = 3;

			public static readonly int iconsPerColumn = 1;

			public static readonly float spaceAfterHeaderIcon = 6;

			public static readonly float spaceAfterSurtitle = 2;

			public static readonly float spaceBeforeSubtitle = 0;

			public static readonly GUIStyle surround;

			public static readonly RectOffset surroundPadding = new RectOffset(3, 3, 2, 2);
		}
	}
}