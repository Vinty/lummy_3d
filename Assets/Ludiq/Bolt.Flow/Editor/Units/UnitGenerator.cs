﻿using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Ludiq;

namespace Bolt
{
	public abstract class UnitGenerator<TUnit> : IUnitGenerator
		where TUnit : class, IUnit
	{
		public UnitGenerator(TUnit unit)
		{
			this.unit = unit;

			var lowerCamelCaseName = BoltFlowNameUtility.UnitTitle(unit.GetType(), false).FirstCharacterToLower();			

			foreach (var valueInput in unit.valueInputs)
			{
				if (!valueInput.hasValidConnection && valueInput.hasDefaultValue)
				{
					var defaultValue = unit.defaultValues[valueInput.key];
					if (defaultValue != null && !defaultValue.HasGenerator<IConstantGenerator>())
					{
						defaultValueMembers[valueInput.key] = lowerCamelCaseName + valueInput.key.FirstCharacterToUpper();
					}
				}
			}
		}

		IUnit IUnitGenerator.unit => unit;

		public TUnit unit { get; }

		public Dictionary<string, string> defaultValueMembers { get; } = new Dictionary<string, string>();

		public virtual void DeclareGraphMemberNames(GraphClassGenerationContext context)
		{
			foreach (var defaultValueMember in defaultValueMembers)
			{
				context.DeclareMemberName(unit, defaultValueMember.Value);
			}
		}

		public virtual IEnumerable<CodeCompositeTypeMember> GenerateMembers(GraphClassGenerationContext context)
		{
			foreach (var defaultValueMember in defaultValueMembers)
			{
				var defaultValueMemberType = unit.defaultValues[defaultValueMember.Key].GetType();

				yield return new CodeFieldMember(CodeMemberModifiers.Public, CodeFactory.TypeRef(defaultValueMemberType), context.ExpectMemberName(unit, defaultValueMember.Value));
			}
		}

		public virtual IEnumerable<CodeStatement> GenerateConstructorStatements(FlowMethodGenerationContext context)
		{
			return Enumerable.Empty<CodeStatement>();
		}

		public virtual IEnumerable<CodeStatement> GenerateDeserializerStatements(FlowMethodGenerationContext context)
		{
			foreach (var defaultValueMember in defaultValueMembers)
			{
				var defaultValueMemberType = unit.defaultValues[defaultValueMember.Key].GetType();

				yield return context.InstanceField(unit, defaultValueMember.Value).Assign(
					CodeFactory.VarRef("value").Field("definition").Field("units")
						.Index(CodeFactory.TypeRef(typeof(Guid)).ObjectCreate(CodeFactory.Primitive(unit.guid.ToString())))
						.Field("defaultValues")
						.Index(CodeFactory.Primitive(defaultValueMember.Key))
						.Cast(CodeFactory.TypeRef(defaultValueMemberType))
				).Statement();
			}
		}

		public abstract IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput);

		public virtual IEnumerable<CodeStatement> GenerateControlInputSuccessor(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			return Enumerable.Empty<CodeStatement>();
		}

		public virtual IEnumerable<CodeStatement> GenerateValueOutputInit(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			return Enumerable.Empty<CodeStatement>();
		}

		public abstract CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput);
	}
}
