﻿using Ludiq;
using Ludiq.CodeDom;
using System.Collections.Generic;

namespace Bolt
{
	public interface IUnitGenerator : IGenerator
	{
		IUnit unit { get; }		
		Dictionary<string, string> defaultValueMembers { get; }

		void DeclareGraphMemberNames(GraphClassGenerationContext context);
		IEnumerable<CodeCompositeTypeMember> GenerateMembers(GraphClassGenerationContext context);
		IEnumerable<CodeStatement> GenerateConstructorStatements(FlowMethodGenerationContext context);
		IEnumerable<CodeStatement> GenerateDeserializerStatements(FlowMethodGenerationContext context);
		IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput);
		IEnumerable<CodeStatement> GenerateControlInputSuccessor(FlowMethodGenerationContext context, ControlInput controlInput);
		IEnumerable<CodeStatement> GenerateValueOutputInit(FlowMethodGenerationContext context, ValueOutput valueOutput);
		CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput);
	}
}
