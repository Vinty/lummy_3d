﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Ludiq;
using UnityEngine;

namespace Bolt
{
	public static class UnitOptions
	{
		internal static readonly object @lock = new object();

		internal static HashSet<IUnitOption> extractedOptions;

		public static bool isExtracted => extractedOptions != null;

		public static IEnumerable<IUnitOption> Subset(IUnitPort connectionSource, UnitOptionFilter filter, GraphReference reference)
		{
			lock (@lock)
			{
				if (!isExtracted)
				{
					return Enumerable.Empty<IUnitOption>();
				}

				var dynamicOptions = UnityAPI.Await(() => UnitOptionsExtractor.GetDynamicOptions().ToHashSet());
				var contextualOptions = UnityAPI.Await(() => UnitOptionsExtractor.GetContextualOptions(connectionSource, filter, reference).ToHashSet());
				
				return LinqUtility.Concat<IUnitOption>(extractedOptions, dynamicOptions, contextualOptions)
					// TODO: .AsParallel()
					.Where((filter ?? UnitOptionFilter.Any).ValidateOption)
					.ToArray();
			}
		}



		#region Duplicates
		
		public static IEnumerable<T> WithoutInheritedDuplicates<T>(this IEnumerable<T> items, Func<T, IFuzzyOption> optionSelector, CancellationToken cancellation)
		{
			// Doing everything we can to avoid reflection here, as it then becomes the main search bottleneck
			
			var _items = items.ToArray();

			var pseudoDeclarers = new HashSet<Member>();
				
			foreach (var item in _items.Cancellable(cancellation))
			{
				var option = optionSelector(item);

				if (option is IMemberUnitOption memberOption)
				{
					if (memberOption.targetType == memberOption.pseudoDeclarer.targetType)
					{
						pseudoDeclarers.Add(memberOption.pseudoDeclarer);
					}
				}
			}

			foreach (var item in _items.Cancellable(cancellation))
			{
				var option = optionSelector(item);

				if (option is IMemberUnitOption memberOption)
				{
					if (pseudoDeclarers.Contains(memberOption.member) || !pseudoDeclarers.Contains(memberOption.pseudoDeclarer))
					{
						yield return item;
					}
				}
				else
				{
					yield return item;
				}
			}
		}

		#endregion
	}
}
