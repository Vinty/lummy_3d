﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Ludiq;
using UnityEngine;
using Task = Ludiq.Task;

namespace Bolt
{
	public sealed class UnitOptionsExtractor : IExtractor
	{
		static UnitOptionsExtractor()
		{
			staticUnitsExtensions = new NonNullableList<Func<IEnumerable<IUnitOption>>>();
			dynamicUnitsExtensions = new NonNullableList<Func<IEnumerable<IUnitOption>>>();
			contextualUnitsExtensions = new NonNullableList<Func<GraphReference, IEnumerable<IUnitOption>>>();
		}

		public string label => "Unit Options";

		public string path => BoltFlow.Paths.unitOptions;

		public int extractPriority => 2;

		public int loadPriority => 1;

		public IExtract ReadExtract(Stream stream)
		{
			using (var reader = new BinaryReader(stream))
			{
				return UnitOptionsExtractFastSerializer.instance.Read(reader);
			}
		}

		public void WriteExtract(IExtract extract, Stream stream)
		{
			using (var writer = new BinaryWriter(stream))
			{
				UnitOptionsExtractFastSerializer.instance.Write((UnitOptionsExtract)extract, writer);
			}
		}

		public IExtract Extract(ExtractionConfiguration configuration)
		{
			var incremental = configuration.mode == ExtractionMode.Fast && UnitOptions.isExtracted;

			var extract = incremental ? ExtractIncremental(configuration) : ExtractFull(configuration);

			if (extract != null)
			{
				Debug.Log($"Unit options extraction succeeded.\n{extract.options.Count} options available in the fuzzy finder.");
			}

			return extract;
		}

		private UnitOptionsExtract ExtractFull(ExtractionConfiguration configuration)
		{
			UnitOptionsExtract extract = null;

			Task.Run("Extracting Unit Options...", 4, extraction =>
			{
				extraction.StartStep("Analyzing Codebase...");
				var codebase = Codebase.Subset(configuration.types, TypeFilter.Any.Configured(), MemberFilter.Any.Configured(), TypeFilter.Any.Configured(false));
				extraction.CompleteStep();
				
				extraction.AllowCancellation();

				extraction.StartStep("Creating Unit Options...");
				var options = GetStaticOptions(codebase).ToHashSet();
				extraction.CompleteStep();
				
				extraction.AllowCancellation();

				extraction.StartStep("Serializing Unit Options...");

				var optionsData = options.SelectTask("Serializing Unit Options...", true, null, option =>
				{
					option.data = option.Serialize();
					return option.data;
				}).ToList();

				extraction.CompleteStep();
				
				extraction.AllowCancellation();

				extraction.StartStep("Applying...");

				lock (UnitOptions.@lock)
				{
					UnitOptions.extractedOptions = options;
				}

				extract = new UnitOptionsExtract();
				extract.options = optionsData;
				extraction.CompleteStep();
			});

			return extract;
		}

		private UnitOptionsExtract ExtractIncremental(ExtractionConfiguration configuration)
		{
			UnitOptionsExtract extract = null;

			Task.Run("Extracting Unit Options...", 6, extraction =>
			{
				lock (UnitOptions.@lock)
				{
					extraction.StartStep("Analyzing Unit Options Changes...");

					AnalyzeIncrementalChanges(configuration, out var outdatedTypes, out var updatedTypes);

					extraction.CompleteStep();
				
					extraction.AllowCancellation();
					
					extraction.StartStep("Removing Outdated Unit Options...");

					if (outdatedTypes.Count > 0)
					{
						UnitOptions.extractedOptions.RemoveWhere(o => outdatedTypes.Contains(o.sourceType));
					}

					extraction.CompleteStep();
						
					if (updatedTypes.Count > 0)
					{
						extraction.StartStep("Analyzing Updated Codebase...");
						var codebase = Codebase.Subset(updatedTypes, TypeFilter.Any.Configured(), MemberFilter.Any.Configured(), TypeFilter.Any.Configured(false));
						extraction.CompleteStep();
				
						extraction.AllowCancellation();
							
						extraction.StartStep("Building Updated Unit Options...");
						var updatedOptions = updatedTypes.SelectManyTask("Building Updated Unit Options...", t => GetIncrementalOptions(codebase, t));
						UnitOptions.extractedOptions.UnionWith(updatedOptions);
						extraction.CompleteStep();
				
						extraction.AllowCancellation();

						extraction.StartStep("Serializing Updated Unit Options...");
						updatedOptions.ForEachTask("Serializing Updated Unit Options...", true, null, option => { option.data = option.Serialize(); });
						extraction.CompleteStep();
					}
				
					extraction.AllowCancellation();
					
					extraction.StartStep("Applying...");

					extract = new UnitOptionsExtract();
					extract.options = UnitOptions.extractedOptions.Select(o => o.data).ToList();

					extraction.CompleteStep();
				}
			});

			return extract;
		}
		
		private void AnalyzeIncrementalChanges(ExtractionConfiguration configuration, out HashSet<Type> outdatedTypes, out HashSet<Type> updatedTypes)
		{
			// We have to analyze both scripts and types, because some types don't have associated MonoScripts.
			// For example, any System type or DLL type might not.

			// Types that were in the previous extraction
			var extractedTypes = UnitOptions.extractedOptions.Select(o => o.sourceType).NotNull().ToHashSet();
			
			// Scripts that were modified since the last extraction
			var modifiedScripts = ScriptUtility.GetModifiedScriptGuids(File.GetLastWriteTimeUtc(path)).ToHashSet();
			
			// Types that are requested from the current extractor configuration
			var requestedTypes = configuration.types.WhereTask("Filtering types...", TypeFilter.Any.Configured().ValidateType);

			// Types of scripts that were modified since the last extraction and are still included in the current extraction
			var modifiedTypes = modifiedScripts.SelectMany(ScriptUtility.GetScriptTypes).ToHashSet();
			modifiedTypes.IntersectWith(requestedTypes);
			
			// Types that are requested for the current extraction but weren't included in the last extraction
			var addedTypes = requestedTypes.Except(extractedTypes).ToHashSet();

			// Types that are no longer requested for the current extraction but were included in the last extraction
			var removedTypes = extractedTypes.Except(requestedTypes).ToHashSet();
			
			// Types that were added or modified
			updatedTypes = new HashSet<Type>();
			updatedTypes.UnionWith(modifiedTypes);
			updatedTypes.UnionWith(addedTypes);

			// Types that were removed or modified
			outdatedTypes = new HashSet<Type>();
			outdatedTypes.UnionWith(modifiedTypes);
			outdatedTypes.UnionWith(removedTypes);
			
			// Debug.Log("Outdated Types:\n" + outdatedTypes.ToLineSeparatedString());
			// Debug.Log("Updated Types:\n" + updatedTypes.ToLineSeparatedString());
		}

		void IExtractor.Load(IExtract extract)
		{
			Load((UnitOptionsExtract) extract);
		}
		
		public void Load(UnitOptionsExtract extract)
		{
			if (extract.options.Count == 0)
			{
				Debug.LogWarning("Unit options are empty.\nTry re-extracting with 'Tools > Bolt > Extractor'.");
				return;
			}

			var options = new HashSet<IUnitOption>();
			var failedOptions = new Dictionary<UnitOptionData, Exception>();
			
			Task.Run("Loading Unit Options...", extract.options.Count, task =>
			{
				foreach (var row in extract.options)
				{
					task.StartStep();

					try
					{
						var option = row.ToOption();

						options.Add(option);
					}
					catch (Exception rowEx)
					{
						failedOptions.Add(row, rowEx);
					}

					task.CompleteStep();
				}
			});

			if (failedOptions.Count > 0)
			{
				var sb = new StringBuilder();

				sb.AppendLine($"{failedOptions.Count} unit options failed to load and were skipped.");
				sb.AppendLine("Try to purge them with 'Tools > Bolt > Extractor'.");
				sb.AppendLine();

				foreach (var failedOption in failedOptions)
				{
					sb.AppendLine(failedOption.Key.key);
				}

				sb.AppendLine();

				foreach (var failedOption in failedOptions)
				{
					sb.AppendLine(failedOption.Key.key + ": ");
					sb.AppendLine(failedOption.Value.ToString());
					sb.AppendLine();
				}

				Debug.LogWarning(sb.ToString());
			}

			lock (UnitOptions.@lock)
			{
				UnitOptions.extractedOptions = options;
			}
		}

		public void Fallback()
		{
			lock (UnitOptions.@lock)
			{
				UnitOptions.extractedOptions = null;
			}
		}
		
		public static HashSet<Type> outdatedTypes { get; } = new HashSet<Type>();
		
		public static HashSet<Type> updatedTypes { get; } = new HashSet<Type>();

		public int Compare(ExtractionConfiguration configuration)
		{
			if (!UnitOptions.isExtracted)
			{
				return 0;
			}

			AnalyzeIncrementalChanges(configuration, out var _outdatedTypes, out var _updatedTypes);

			outdatedTypes.Clear();
			outdatedTypes.UnionWith(_outdatedTypes);

			updatedTypes.Clear();
			updatedTypes.UnionWith(_updatedTypes);

			return outdatedTypes.Count + updatedTypes.Count;
		}


		#region Analysis
		
		private static bool IsSpecialUnitType(Type t)
		{
			return typeof(IUnit).IsAssignableFrom(t) &&
			       t.IsConcrete() &&
			       t.GetDefaultConstructor() != null &&
			       !t.HasAttribute<SpecialUnitAttribute>() &&
			       (EditorPlatformUtility.allowJit || !t.HasAttribute<AotIncompatibleAttribute>()) &&
			       t.GetDefaultConstructor() != null;
		}

		private static bool IsEventUnitType(Type t)
		{
			return typeof(IEventUnit).IsAssignableFrom(t) && t.IsConcrete();
		}

		private static bool IsStandaloneUnitType(Type t)
		{
			return IsSpecialUnitType(t) || IsEventUnitType(t);
		}

		private static IUnitOption InstantiateStandaloneUnitType(Type unitType)
		{
			return ((IUnit)unitType.Instantiate()).Option();
		}

		private static IEnumerable<IUnitOption> GetStaticOptions(CodebaseSubset codebase)
		{
			var options = new ConcurrentBag<IUnitOption>();

			Parallel.Invoke
			(
				() => options.UnionWith(Codebase.ludiqRuntimeTypes.SelectWhereTask("Standalones...", IsStandaloneUnitType, InstantiateStandaloneUnitType)),
				() => options.UnionWith(codebase.types.SelectManyTask("Types...", GetTypeOptions)),
				() => options.UnionWith(codebase.members.SelectManyTask("Members...", GetMemberOptions))
			);

			// Self

			options.Add(new Self().Option());
			
			// Blank Variables

			options.Add(new GetVariableOption(VariableKind.Flow));
			options.Add(new GetVariableOption(VariableKind.Graph));
			options.Add(new GetVariableOption(VariableKind.Object));
			options.Add(new GetVariableOption(VariableKind.Scene));
			options.Add(new GetVariableOption(VariableKind.Application));
			options.Add(new GetVariableOption(VariableKind.Saved));

			options.Add(new SetVariableOption(VariableKind.Flow));
			options.Add(new SetVariableOption(VariableKind.Graph));
			options.Add(new SetVariableOption(VariableKind.Object));
			options.Add(new SetVariableOption(VariableKind.Scene));
			options.Add(new SetVariableOption(VariableKind.Application));
			options.Add(new SetVariableOption(VariableKind.Saved));

			options.Add(new IsVariableDefinedOption(VariableKind.Flow));
			options.Add(new IsVariableDefinedOption(VariableKind.Graph));
			options.Add(new IsVariableDefinedOption(VariableKind.Object));
			options.Add(new IsVariableDefinedOption(VariableKind.Scene));
			options.Add(new IsVariableDefinedOption(VariableKind.Application));
			options.Add(new IsVariableDefinedOption(VariableKind.Saved));

			// Blank Super Unit

			options.Add(SuperUnitFactory.WithInputOutput().Option());

			// Extensions

			foreach (var staticUnitsExtension in staticUnitsExtensions)
			{
				foreach (var extensionStaticUnit in staticUnitsExtension())
				{
					options.Add(extensionStaticUnit);
				}
			}

			return options;
		}

		private static IEnumerable<IUnitOption> GetIncrementalOptions(CodebaseSubset codebase, Type type)
		{
			if (!codebase.ContainsType(type))
			{
				yield break;
			}

			foreach (var typeOption in GetTypeOptions(type))
			{
				yield return typeOption;
			}

			foreach (var member in codebase.members)
			{
				if (member.targetType == type)
				{
					foreach (var memberOption in GetMemberOptions(member))
					{
						yield return memberOption;
					}
				}
			}
		}

		private static IEnumerable<IUnitOption> GetTypeOptions(Type type)
		{
			// Enum operators
			if (type.IsEnum)
			{
				yield return new BinaryOperatorUnit(BinaryOperator.Equality, type, type).Option();
				yield return new BinaryOperatorUnit(BinaryOperator.Inequality, type, type).Option();

				if (type.IsFlagsEnum())
				{
					yield return new UnaryOperatorUnit(UnaryOperator.BitwiseNegation, type).Option();
					yield return new BinaryOperatorUnit(BinaryOperator.And, type, type).Option();
					yield return new BinaryOperatorUnit(BinaryOperator.Or, type, type).Option();
					yield return new BinaryOperatorUnit(BinaryOperator.ExclusiveOr, type, type).Option();
				}
			}
			else 
			{
				// Predefined unary operators
				foreach (var unaryOperatorHandler in OperatorUtility.GetUnaryOperatorHandlers().Where(x => x.@operator.IsExtractable()))
				{
					var @operator = unaryOperatorHandler.@operator;

					if (type == typeof(bool) && @operator == UnaryOperator.BitwiseNegation)
					{
						continue;
					}

					if (unaryOperatorHandler.manualOperatorQueries.Contains(type))
					{
						yield return new UnaryOperatorUnit(@operator, type).Option();
					}
				}

				// Predefined binary operators
				foreach (var binaryOperatorHandler in OperatorUtility.GetBinaryOperatorHandlers().Where(x => x.@operator.IsExtractable()))
				{
					var @operator = binaryOperatorHandler.@operator;

					if (binaryOperatorHandler.manualOperatorQueries.Contains(new BinaryOperatorHandler.OperatorQuery(type, type))
					|| ((@operator == BinaryOperator.Equality || @operator == BinaryOperator.Inequality) && type.IsNullable()))
					{
						yield return new BinaryOperatorUnit(@operator, type, type).Option();
					}
				}
			}

			// Comparison
			if (type.HasAnyComparisonOperator())
			{
				yield return new TypedComparison(type).Option();
			}

			// Struct Initializer
			if (type.IsStruct())
			{
				yield return new CreateStruct(type).Option();
			}
			
			// Literal
			if (type.HasInspector())
			{
				yield return new Literal(type).Option();
			}

			// Expose
			if (!type.IsBasic())
			{
				yield return new Expose(type).Option();
			}
		}

		private static IEnumerable<IUnitOption> GetMemberOptions(Member member)
		{
			// Operators are handled with special operator units
			// that are more elegant than the raw methods
			if (member.isOperator)
			{
				var methodInfo = member.methodInfo;

				if (OperatorUtility.TryGetUnaryByMethodName(methodInfo.Name, out var unaryOperator) && unaryOperator.IsExtractable())
				{
					yield return new UnaryOperatorUnit(unaryOperator, methodInfo.GetParameters()[0].ParameterType).Option();
				}
				else if (OperatorUtility.TryGetBinaryByMethodName(methodInfo.Name, out var binaryOperator) && binaryOperator.IsExtractable())
				{
					var parameters = methodInfo.GetParameters();
					var leftType = parameters[0].ParameterType;
					var rightType = parameters[1].ParameterType;

					if ((binaryOperator != BinaryOperator.Equality && binaryOperator != BinaryOperator.Inequality) || leftType != rightType || !leftType.IsNullable())
					{
						yield return new BinaryOperatorUnit(binaryOperator, leftType, rightType).Option();
					}
				}

				yield break;
			}

			// Conversions are handled automatically by connections
			if (member.isConversion)
			{
				yield break;
			}

			if (member.isAccessor)
			{
				if (member.isPubliclyGettable)
				{
					yield return new GetMember(member).Option();
				}

				if (member.isPubliclySettable)
				{
					yield return new SetMember(member).Option();
				}

				if (!member.declaringType.IsValueType && member.isPubliclyGettable && member.isPubliclySettable && TweenMember.tweenFactoriesByType.ContainsKey(member.type))
				{
					yield return new TweenMember(member).Option();
				}
			}
			else if (member.isPubliclyInvocable)
			{
				yield return new InvokeMember(member).Option();
			}
		}

		internal static IEnumerable<IUnitOption> GetDynamicOptions()
		{
			// Super Units

			var flowMacros = AssetUtility.GetAllAssetsOfType<FlowMacro>().ToArray();

			foreach (var superUnit in flowMacros.Select(flowMacro => new SuperUnit(flowMacro)))
			{
				yield return superUnit.Option();
			}

			// Preset boolean literals.

			yield return new PresetLiteralOption(new Literal(typeof(bool), false));
			yield return new PresetLiteralOption(new Literal(typeof(bool), true));

			// Extensions

			foreach (var dynamicUnitsExtension in dynamicUnitsExtensions)
			{
				foreach (var extensionDynamicUnit in dynamicUnitsExtension())
				{
					yield return extensionDynamicUnit;
				}
			}
		}

		internal static IEnumerable<IUnitOption> GetContextualOptions(IUnitPort connectionSource, UnitOptionFilter filter, GraphReference reference)
		{
			foreach (var variableKind in Enum.GetValues(typeof(VariableKind)).Cast<VariableKind>())
			{
				foreach (var graphVariableName in EditorVariablesUtility.GetVariableNameSuggestions(variableKind, reference))
				{
					yield return new GetVariableOption(variableKind, graphVariableName);
					yield return new SetVariableOption(variableKind, graphVariableName);
					yield return new IsVariableDefinedOption(variableKind, graphVariableName);
				}
			}

			// Extensions

			foreach (var contextualUnitsExtension in contextualUnitsExtensions)
			{
				foreach (var extensionContextualUnitOption in contextualUnitsExtension(reference))
				{
					yield return extensionContextualUnitOption;
				}
			}
		}

		#endregion



		#region Extensions

		public static NonNullableList<Func<IEnumerable<IUnitOption>>> staticUnitsExtensions { get; }

		public static NonNullableList<Func<IEnumerable<IUnitOption>>> dynamicUnitsExtensions { get; }

		public static NonNullableList<Func<GraphReference, IEnumerable<IUnitOption>>> contextualUnitsExtensions { get; }

		#endregion
	}
}