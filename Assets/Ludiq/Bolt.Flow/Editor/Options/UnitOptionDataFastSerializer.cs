﻿using System.IO;
using Ludiq;

namespace Bolt
{
	public class UnitOptionDataFastSerializer : FastSerializer<UnitOptionData>
	{
		public static UnitOptionDataFastSerializer instance { get; } = new UnitOptionDataFastSerializer();

		public override UnitOptionData Instantiate()
		{
			return new UnitOptionData();
		}

		public override void Write(UnitOptionData value, BinaryWriter writer)
		{
			writer.WriteNullable(value.optionType);
			writer.WriteNullable(value.unitType);
			writer.WriteNullable(value.labelHuman);
			writer.WriteNullable(value.labelProgrammer);
			writer.WriteNullable(value.category);
			writer.Write(value.order);
			writer.WriteNullable(value.haystackHuman);
			writer.WriteNullable(value.haystackProgrammer);
			writer.WriteNullable(value.key);
			writer.WriteNullable(value.tag1);
			writer.WriteNullable(value.tag2);
			writer.WriteNullable(value.tag3);

			if (value.unit != null)
			{
				writer.Write(true);
				writer.Write(value.unit.Length);
				writer.Write(value.unit);
			}
			else
			{
				writer.Write(false);
			}
			
			writer.Write(value.controlInputCount);
			writer.Write(value.controlOutputCount);
			
			if (value.valueInputTypes != null)
			{
				writer.Write(true);
				writer.Write(value.valueInputTypes.Length);

				foreach (var valueInputType in value.valueInputTypes)
				{
					writer.WriteNullable(valueInputType);
				}
			}
			else
			{
				writer.Write(false);
			}

			if (value.valueOutputTypes != null)
			{
				writer.Write(true);
				writer.Write(value.valueOutputTypes.Length);

				foreach (var valueOutputType in value.valueOutputTypes)
				{
					writer.WriteNullable(valueOutputType);
				}
			}
			else
			{
				writer.Write(false);
			}
		}

		public override void Read(ref UnitOptionData value, BinaryReader reader)
		{
			value.optionType = reader.ReadNullableString();
			value.unitType = reader.ReadNullableString();
			value.labelHuman = reader.ReadNullableString();
			value.labelProgrammer = reader.ReadNullableString();
			value.category = reader.ReadNullableString();
			value.order = reader.ReadInt32();
			value.haystackHuman = reader.ReadNullableString();
			value.haystackProgrammer = reader.ReadNullableString();
			value.key = reader.ReadNullableString();
			value.tag1 = reader.ReadNullableString();
			value.tag2 = reader.ReadNullableString();
			value.tag3 = reader.ReadNullableString();
			
			if (reader.ReadBoolean())
			{
				value.unit = reader.ReadBytes(reader.ReadInt32());
			}

			value.controlInputCount = reader.ReadInt32();
			value.controlOutputCount = reader.ReadInt32();

			if (reader.ReadBoolean())
			{
				value.valueInputTypes = new string[reader.ReadInt32()];

				for (int i = 0; i < value.valueInputTypes.Length; i++)
				{
					value.valueInputTypes[i] = reader.ReadNullableString();
				}
			}
			
			if (reader.ReadBoolean())
			{
				value.valueOutputTypes = new string[reader.ReadInt32()];

				for (int i = 0; i < value.valueOutputTypes.Length; i++)
				{
					value.valueOutputTypes[i] = reader.ReadNullableString();
				}
			}
		}
	}
}
