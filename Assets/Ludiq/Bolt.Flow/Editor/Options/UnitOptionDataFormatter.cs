﻿using Ludiq.OdinSerializer;
using Bolt;

[assembly: RegisterFormatter(typeof(UnitOptionData))]

namespace Bolt
{
	public class UnitOptionDataFormatter : MinimalBaseFormatter<UnitOptionData>
	{
		private static readonly Serializer<string> StringSerializer = Serializer.Get<string>();
		private static readonly Serializer<int> IntSerializer = Serializer.Get<int>();
		private static readonly Serializer<byte[]> ByteArraySerializer = Serializer.Get<byte[]>();
		private static readonly Serializer<string[]> StringArraySerializer = Serializer.Get<string[]>();

		protected override UnitOptionData GetUninitializedObject()
		{
			return new UnitOptionData();
		}

		protected override void Read(ref UnitOptionData value, IDataReader reader)
		{
			value.optionType = StringSerializer.ReadValue(reader);
			value.unitType = StringSerializer.ReadValue(reader);
			value.labelHuman = StringSerializer.ReadValue(reader);
			value.labelProgrammer = StringSerializer.ReadValue(reader);
			value.category = StringSerializer.ReadValue(reader);
			value.order = IntSerializer.ReadValue(reader);
			value.haystackHuman = StringSerializer.ReadValue(reader);
			value.haystackProgrammer = StringSerializer.ReadValue(reader);
			value.key = StringSerializer.ReadValue(reader);
			value.tag1 = StringSerializer.ReadValue(reader);
			value.tag2 = StringSerializer.ReadValue(reader);
			value.tag3 = StringSerializer.ReadValue(reader);
			value.unit = ByteArraySerializer.ReadValue(reader);
			value.controlInputCount = IntSerializer.ReadValue(reader);
			value.controlOutputCount = IntSerializer.ReadValue(reader);
			value.valueInputTypes = StringArraySerializer.ReadValue(reader);
			value.valueOutputTypes = StringArraySerializer.ReadValue(reader);
		}

		protected override void Write(ref UnitOptionData value, IDataWriter writer)
		{
			StringSerializer.WriteValue(value.optionType, writer);
			StringSerializer.WriteValue(value.unitType, writer);
			StringSerializer.WriteValue(value.labelHuman, writer);
			StringSerializer.WriteValue(value.labelProgrammer, writer);
			StringSerializer.WriteValue(value.category, writer);
			IntSerializer.WriteValue(value.order, writer);
			StringSerializer.WriteValue(value.haystackHuman, writer);
			StringSerializer.WriteValue(value.haystackProgrammer, writer);
			StringSerializer.WriteValue(value.key, writer);
			StringSerializer.WriteValue(value.tag1, writer);
			StringSerializer.WriteValue(value.tag2, writer);
			StringSerializer.WriteValue(value.tag3, writer);
			ByteArraySerializer.WriteValue(value.unit, writer);
			IntSerializer.WriteValue(value.controlInputCount, writer);
			IntSerializer.WriteValue(value.controlOutputCount, writer);
			StringArraySerializer.WriteValue(value.valueInputTypes, writer);
			StringArraySerializer.WriteValue(value.valueOutputTypes, writer);
		}
	}
}