﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Ludiq;
using UnityEngine;

namespace Bolt
{
	public class UnitOptionTree : ExtensibleFuzzyOptionTree
	{
		#region Initialization

		public UnitOptionTree(GUIContent label) : base(label)
		{
			favorites = new Favorites(this);
		}

		public override void Prewarm()
		{
			filter = filter ?? UnitOptionFilter.Any;

			while (!UnitOptions.isExtracted)
			{
				Thread.Sleep(100);
			}

			try
			{
				options = new HashSet<IUnitOption>(UnitOptions.Subset(connectionSource, filter, reference));
			}
			catch (Exception ex)
			{
				Debug.LogError($"Failed to fetch unit options for fuzzy finder (error log below).\nTry re-extracting with 'Tools > Bolt > Extractor'.\n\n{ex}");
				options = new HashSet<IUnitOption>();
			}

			optionsByTypes = new Dictionary<Type, HashSet<IUnitOption>>();

			typesWithMembers = new HashSet<Type>();
			typesWithVariables = new HashSet<Type>();

			foreach (var option in options)
			{
				if (option.sourceType != null)
				{
					AddOptionByType(option.sourceType, option);
				}

				if (option is IMemberUnitOption memberUnitOption && memberUnitOption.targetType != null)
				{
					typesWithMembers.Add(memberUnitOption.targetType);

					var source = memberUnitOption.member.source;

					if (source == Member.Source.Property || source == Member.Source.Field)
					{
						typesWithVariables.Add(memberUnitOption.targetType);
					}
				}
				else if (option is UnaryOperatorUnitOption unaryOperatorUnitOption && unaryOperatorUnitOption.type != null)
				{
					var type = unaryOperatorUnitOption.type;

					if (type != typeof(bool))
					{
						typesWithMembers.Add(type);
					}
				}
				else if (option is BinaryOperatorUnitOption binaryOperatorUnitOption && binaryOperatorUnitOption.leftType != null && binaryOperatorUnitOption.rightType != null)
				{
					var leftType = binaryOperatorUnitOption.leftType;
					var rightType = binaryOperatorUnitOption.rightType;

					if (leftType != typeof(bool) || rightType != typeof(bool))
					{
						typesWithMembers.Add(leftType);
						typesWithMembers.Add(rightType);
					}

					AddOptionByType(leftType, option);
					AddOptionByType(rightType, option);
				}
			}
		}

		private IEnumerable<IUnitOption> GetOptionsByType(Type type)
		{
			if (optionsByTypes.TryGetValue(type, out var result))
			{
				return result;
			}

			return Enumerable.Empty<IUnitOption>();
		}

		private void AddOptionByType(Type type, IUnitOption option)
		{
			if (!optionsByTypes.TryGetValue(type, out var optionsByType))
			{
				optionsByType = new HashSet<IUnitOption>();
				optionsByTypes[type] = optionsByType;
			}

			optionsByType.Add(option);
		}

		private HashSet<IUnitOption> options;

		private Dictionary<Type, HashSet<IUnitOption>> optionsByTypes;

		private HashSet<Type> typesWithMembers;
		private HashSet<Type> typesWithVariables;

		#endregion


		#region Configuration

		public IUnitPort connectionSource { get; set; }
		public UnitOptionFilter filter { get; set; }
		public GraphReference reference { get; set; }
		public bool surfaceCommonTypeLiterals { get; set; }
		public IFuzzyOption[] rootOverride { get; set; }
		public Member overloadMember { get; set; }

		public FlowGraph graph => reference.graph as FlowGraph;
		public GameObject self => reference.self;

		public ActionDirection direction { get; set; } = ActionDirection.Any;

		#endregion


		#region Hierarchy

		private readonly FuzzyGroup enumsGroup = new FuzzyGroup("(Enums)", typeof(Enum).Icon());
		private readonly FuzzyGroup selfGroup = new FuzzyGroup("Self", typeof(GameObject).Icon());

		private IEnumerable<UnitCategory> SpecialCategories()
		{
			yield return new UnitCategory("Codebase");
			yield return new UnitCategory("Events");
			yield return new UnitCategory("Variables");
			yield return new UnitCategory("Math");
			yield return new UnitCategory("Nesting");
			yield return new UnitCategory("Macros");
		}

		private static readonly HashSet<Type> noSurfaceConstructors = new HashSet<Type>()
		{
			typeof(string),
			typeof(object)
		};

		public override IEnumerable<IFuzzyOption> Root()
		{
			if (rootOverride != null && rootOverride.Length > 0)
			{
				foreach (var item in rootOverride)
				{
					yield return item;
				}

				yield break;
			}

			List<Type> selfTypes = null;
			Dictionary<Type, int> selfTypeIndices = null;

			if (overloadMember != null)
			{
				foreach (var member in GetMembers(overloadMember.targetType, true).Where(option => option is IMemberUnitOption memberUnitOption && memberUnitOption.member.name == overloadMember.name))
				{
					yield return member;
				}

				yield break;
			}

			if (self != null)
			{
				selfTypes = SelfTypes().ToList();
				selfTypeIndices = selfTypes.ToValueIndexPairs().ToDictionary();
			}

			if (filter.CompatibleOutputType != null)
			{
				var outputType = filter.CompatibleOutputType;

				var outputTypeLiteral = GetOptionsByType(outputType).FirstOrDefault(option => option is LiteralOption literalOption && literalOption.literalType == outputType);

				if (outputTypeLiteral != null && outputType != typeof(object) && outputType != typeof(bool))
				{
					yield return outputTypeLiteral;
				}

				if (surfaceCommonTypeLiterals)
				{
					foreach (var commonType in EditorTypeUtility.commonTypes)
					{
						if (commonType == outputType)
						{
							continue;
						}

						if (commonType == typeof(bool))
						{
							foreach (var booleanChild in BooleanLiteralChildren())
							{
								yield return booleanChild;
							}

							continue;
						}

						var commonTypeLiteral = GetOptionsByType(commonType).FirstOrDefault(option => option is LiteralOption literalOption && literalOption.literalType == commonType);

						if (commonTypeLiteral != null)
						{
							yield return commonTypeLiteral;
						}
					}
				}

				if (!noSurfaceConstructors.Contains(outputType))
				{
					var outputTypeConstructors = GetOptionsByType(outputType).Where(option => option is InvokeMemberOption invokeMemberOption &&
					                                                                          invokeMemberOption.targetType == outputType &&
					                                                                          invokeMemberOption.member.isConstructor);

					foreach (var outputTypeConstructor in outputTypeConstructors)
					{
						yield return outputTypeConstructor;
					}
				}

				if (outputType == typeof(bool))
				{
					foreach (var logicOperation in CategoryChildren(new UnitCategory("Logic"), true, true))
					{
						yield return logicOperation;
					}
				}

				foreach (var operatorChild in GetOperators(outputType, true, false, true))
				{
					yield return operatorChild;
				}

				Member mirrorMember = null;

				if (connectionSource.unit is SetMember setMember
				    && connectionSource == setMember.input
				    && setMember.member is var setMirrorMember
				    && setMirrorMember.isPubliclyGettable
				    && setMirrorMember.type == outputType)
				{
					mirrorMember = setMirrorMember;
					yield return new GetMemberOption(new GetMember(setMirrorMember));
				}
				else if (connectionSource.unit is TweenMember tweenMember
				         && connectionSource == tweenMember.value
				         && tweenMember.member is var tweenMirrorMember
				         && tweenMirrorMember.isPubliclyGettable
				         && tweenMirrorMember.type == outputType)
				{
					mirrorMember = tweenMirrorMember;
					yield return new GetMemberOption(new GetMember(tweenMirrorMember));
				}


				if (selfTypes != null)
				{
					foreach (var option in options.OfType<GetMemberOption>()
						.Where(option => option.member is var member
						                 && member != mirrorMember
						                 && selfTypes.Contains(member.targetType)
						                 && member.targetType != outputType
						                 && member.requiresTarget
						                 && member.isPubliclySettable
						                 && member.type == outputType
						                 && !member.isPseudoInherited)
						.OrderBy(memberOption => selfTypeIndices[memberOption.member.targetType]))
					{
						yield return option;
					}
				}

				foreach (var member in GetMembers(outputType, true).OfType<IMemberUnitOption>()
					.Where(option =>
						option.member is var member
						&& member != mirrorMember
						&& member.declaringType == outputType
						&& member.isPubliclyGettable
						&& !member.isConstructor
						&& !member.isPseudoInherited)
					.OrderBy(option => option.member.requiresTarget))
				{
					yield return member;
				}

				if (outputType.IsNumeric())
				{
					foreach (var child in MathChildren(true))
					{
						yield return child;
					}
				}
			}

			if (filter.CompatibleInputType != null)
			{
				var inputType = filter.CompatibleInputType;

				if (inputType == typeof(bool))
				{
					yield return options.Single(o => o.UnitIs<Branch>());
					yield return options.Single(o => o.UnitIs<SelectUnit>());
				}

				if (inputType == typeof(bool))
				{
					foreach (var logicOperation in CategoryChildren(new UnitCategory("Logic"), true, true))
					{
						yield return logicOperation;
					}
				}

				foreach (var operatorChild in GetOperators(inputType, true, false, true))
				{
					yield return operatorChild;
				}

				foreach (var expose in GetOptionsByType(inputType).Where(option => option is ExposeOption))
				{
					yield return expose;
				}

				Member mirrorMember = null;

				if (connectionSource.unit is GetMember getMember
				    && connectionSource == getMember.value
				    && getMember.member is var getMirrorMember
				    && getMirrorMember.isPubliclySettable
				    && getMirrorMember.type == inputType)
				{
					mirrorMember = getMirrorMember;

					yield return new SetMemberOption(new SetMember(getMirrorMember));
				}

				if (selfTypes != null)
				{
					foreach (var option in options.OfType<IMemberUnitOption>()
						.Where(option => (option is SetMemberOption || option is TweenMemberOption)
						                 && option.member is var member
						                 && member != mirrorMember
						                 && selfTypes.Contains(member.targetType)
						                 && member.targetType != inputType
						                 && member.requiresTarget
						                 && member.isPubliclySettable
						                 && member.type == inputType
						                 && !member.isPseudoInherited)
						.OrderBy(memberOption => selfTypeIndices[memberOption.member.targetType]))
					{
						yield return option;
					}
				}

				foreach (var member in GetMembers(inputType, true).OfType<IMemberUnitOption>()
					.Where(option => option.member is var member
					                 && member != mirrorMember
					                 && !member.isPseudoInherited)
					.OrderBy(option => option.member.requiresTarget))
				{
					yield return member;
				}

				if (inputType.IsNumeric())
				{
					foreach (var child in MathChildren(true))
					{
						yield return child;
					}
				}

				if (typeof(IEnumerable).IsAssignableFrom(inputType) && (inputType != typeof(string) && inputType != typeof(Transform)))
				{
					foreach (var collectionOperation in CategoryChildren(new UnitCategory("Collections"), false, true))
					{
						yield return collectionOperation;
					}
				}

				if (typeof(IList).IsAssignableFrom(inputType))
				{
					foreach (var listOperation in CategoryChildren(new UnitCategory("Collections/Lists"), true, true))
					{
						yield return listOperation;
					}
				}

				if (typeof(IDictionary).IsAssignableFrom(inputType))
				{
					foreach (var dictionaryOperation in CategoryChildren(new UnitCategory("Collections/Dictionaries"), true, true))
					{
						yield return dictionaryOperation;
					}
				}
			}

			if (selfTypes != null && filter.CompatibleInputType == null && filter.CompatibleOutputType == null)
			{
				foreach (var selfType in selfTypes)
				{
					yield return new TypeOption(selfType, FuzzyOptionMode.Branch);
				}
			}

			foreach (var category in options.Select(option => option.category?.root)
				.NotNull()
				.Concat(SpecialCategories())
				.Distinct()
				.OrderBy(c => c.name))
			{
				yield return new UnitCategoryOption(category);
			}

			foreach (var extensionRootItem in base.Root())
			{
				yield return extensionRootItem;
			}

			if (filter.Self)
			{
				var self = options.FirstOrDefault(option => option.UnitIs<Self>());

				if (self != null)
				{
					yield return self;
				}
			}

			foreach (var unit in CategoryChildren(null, true, true))
			{
				yield return unit;
			}
		}

		private IEnumerable<IFuzzyOption> Children(IFuzzyOption parent, bool ordered)
		{
			if (parent is NamespaceOption namespaceOption)
			{
				return NamespaceChildren(namespaceOption.value, ordered);
			}
			else if (parent is TypeOption typeOption)
			{
				return TypeChildren(typeOption.value, ordered);
			}
			else if (parent.value == enumsGroup)
			{
				return EnumsChildren(ordered);
			}
			else if (parent.value == selfGroup)
			{
				return SelfChildren();
			}
			else if (parent is UnitCategoryOption unitCategoryOption)
			{
				return CategoryChildren(unitCategoryOption.value, true, ordered);
			}
			else if (parent is VariableKindOption variableKindOption)
			{
				return VariableKindChildren(variableKindOption.value, ordered);
			}
			else
			{
				return base.Children(parent);
			}
		}

		private IEnumerable<Type> SelfTypes()
		{
			return typeof(GameObject).Yield().Concat(UnityAPI.Await(() => self.GetComponents<Component>().NotUnityNull().Select(c => c.GetType()))).Where(type => !type.IsSubclassOf(typeof(LudiqBehaviour))).Distinct();
		}

		public override IEnumerable<IFuzzyOption> Children(IFuzzyOption parent)
		{
			return Children(parent, true);
		}

		public override IEnumerable<IFuzzyOption> SearchableChildren(IFuzzyOption parent)
		{
			if (parent is FuzzyWindow.FavoritesRoot favoritesRoot)
			{
				foreach (var favorite in favorites.Where(favorite => CanFavorite(favorite)))
				{
					yield return favorite;
				}

				yield break;
			}

			foreach (var child in Children(parent, false))
			{
				yield return child;

				foreach (var grandchild in SearchableChildren(child))
				{
					yield return grandchild;
				}
			}
		}

		private IEnumerable<IFuzzyOption> SelfChildren()
		{
			yield return new TypeOption(typeof(GameObject), FuzzyOptionMode.Branch);

			// Self components can be null if no script is assigned to them
			// https://support.ludiq.io/forums/5-bolt/topics/817-/
			foreach (var selfComponentType in UnityAPI.Await(() => self.GetComponents<Component>().NotUnityNull().Select(c => c.GetType())))
			{
				yield return new TypeOption(selfComponentType, FuzzyOptionMode.Branch);
			}
		}

		private IEnumerable<IFuzzyOption> CodebaseChildren(bool ordered)
		{
			var rootNamespaces = typesWithMembers.Select(t => t.Namespace().Root).Distinct();

			if (ordered)
			{
				rootNamespaces = rootNamespaces.OrderBy(ns => ns.DisplayName(false));
			}

			foreach (var rootNamespace in rootNamespaces)
			{
				yield return new NamespaceOption(rootNamespace, FuzzyOptionMode.Branch);
			}

			if (filter.Literals && options.Any(option => option is LiteralOption literalOption && literalOption.literalType.IsEnum))
			{
				yield return new FuzzyGroupOption(enumsGroup);
			}
		}

		private IEnumerable<IFuzzyOption> MathChildren(bool ordered)
		{
			return GetMembers(typeof(Mathf), ordered).Where(option => !option.member.requiresTarget);
		}

		private IEnumerable<IFuzzyOption> TimeChildren(bool ordered)
		{
			return GetMembers(typeof(Time), ordered).Where(option => !option.member.requiresTarget);
		}

		private IEnumerable<IFuzzyOption> NestingChildren(bool ordered)
		{
			var nesters = options.Where(option => option.UnitIs<IGraphNesterElement>() && ((IGraphNesterElement)option.unit).nest.macro == null);

			if (ordered)
			{
				nesters = nesters.OrderBy(option => option.label);
			}

			return nesters;
		}

		private IEnumerable<IFuzzyOption> MacroChildren(bool ordered)
		{
			var macroNesters = options.Where(option => option.UnitIs<IGraphNesterElement>() && ((IGraphNesterElement)option.unit).nest.macro != null);

			if (ordered)
			{
				macroNesters = macroNesters.OrderBy(option => option.label);
			}

			return macroNesters;
		}

		private IEnumerable<IFuzzyOption> VariablesChildren()
		{
			yield return new VariableKindOption(VariableKind.Flow);
			yield return new VariableKindOption(VariableKind.Graph);
			yield return new VariableKindOption(VariableKind.Object);
			yield return new VariableKindOption(VariableKind.Scene);
			yield return new VariableKindOption(VariableKind.Application);
			yield return new VariableKindOption(VariableKind.Saved);
		}

		private IEnumerable<IFuzzyOption> VariableKindChildren(VariableKind kind, bool ordered)
		{
			var variableKinds = options.OfType<IUnifiedVariableUnitOption>().Where(option => option.kind == kind);

			if (ordered)
			{
				variableKinds = variableKinds.OrderBy(option => option.name);
			}

			return variableKinds;
		}

		private IEnumerable<IFuzzyOption> NamespaceChildren(Namespace @namespace, bool ordered)
		{
			if (!@namespace.IsGlobal)
			{
				var childNamespaces = typesWithMembers.SelectMany(t => t.Namespace().AndAncestors())
					.Distinct()
					.Where(ns => ns.Parent == @namespace);

				if (ordered)
				{
					childNamespaces = childNamespaces.OrderBy(ns => ns.DisplayName(false));
				}

				foreach (var childNamespace in childNamespaces)
				{
					yield return new NamespaceOption(childNamespace, FuzzyOptionMode.Branch);
				}
			}

			var types = typesWithMembers.Where(t => t.Namespace() == @namespace);

			if (ordered)
			{
				types = types.OrderBy(t => t.DisplayName());
			}

			foreach (var type in types)
			{
				yield return new TypeOption(type, FuzzyOptionMode.Branch);
			}
		}

		private IEnumerable<IFuzzyOption> TypeChildren(Type type, bool ordered)
		{
			var optionsByType = GetOptionsByType(type);

			if (type == typeof(bool))
			{
				foreach (var literal in BooleanLiteralChildren())
				{
					yield return literal;
				}
			}

			if (type != typeof(bool) || !ordered)
			{
				foreach (var literal in optionsByType.Where(option => option is LiteralOption literalOption && literalOption.literalType == type))
				{
					yield return literal;
				}
			}

			if (typesWithVariables.Contains(type))
			{
				foreach (var expose in optionsByType.Where(option => option is ExposeOption exposeOption && exposeOption.exposedType == type))
				{
					yield return expose;
				}
			}

			foreach (var @operator in GetOperators(type, true, true, ordered))
			{
				yield return @operator;
			}

			if (type.IsStruct())
			{
				foreach (var createStruct in optionsByType.Where(option => option is CreateStructOption createStructOption && createStructOption.structType == type))
				{
					yield return createStruct;
				}
			}

			foreach (var member in GetMembers(type, ordered))
			{
				yield return member;
			}
		}

		private IEnumerable<IOperatorUnitOption> GetOperators(Type type, bool leftHand, bool rightHand, bool ordered)
		{
			var operators = GetOperatorsUncategorized(type, leftHand, rightHand, ordered);

			if (ordered)
			{
				operators = operators.OrderBy(option => option.operatorCategory);
			}

			return operators;
		}

		private IEnumerable<IOperatorUnitOption> GetOperatorsUncategorized(Type type, bool leftHand, bool rightHand, bool ordered)
		{
			var optionsByType = GetOptionsByType(type);

			var unaryOperators = optionsByType
				.Where(option => option is UnaryOperatorUnitOption unaryOperatorUnitOption && unaryOperatorUnitOption.sourceType == type);

			if (ordered)
			{
				unaryOperators = unaryOperators.OrderBy(option => ((UnaryOperatorUnitOption)option).@operator);
			}

			foreach (var unaryOperator in unaryOperators)
			{
				yield return (IOperatorUnitOption)unaryOperator;
			}

			var binaryOperators = optionsByType.Where(option => option is BinaryOperatorUnitOption binaryOperatorUnitOption
			                                                    && ((leftHand && binaryOperatorUnitOption.leftType == type) || (rightHand && binaryOperatorUnitOption.rightType == type)));

			if (ordered)
			{
				binaryOperators = binaryOperators.OrderBy(option => ((BinaryOperatorUnitOption)option).uncategorizedSortRank);
			}

			foreach (var binaryOperator in binaryOperators)
			{
				yield return (IOperatorUnitOption)binaryOperator;
			}

			foreach (var comparison in optionsByType.Where(option => option is TypedComparisonOption comparisonOption && comparisonOption.comparisonType == type))
			{
				yield return (IOperatorUnitOption)comparison;
			}
		}

		private IEnumerable<IMemberUnitOption> GetMembers(Type type, bool ordered)
		{
			var optionsByType = GetOptionsByType(type);

			// TODO HACK Super hacky hotfix, just because ParticleSystem.setTime fails to deserialize for some reason
			// and I really want to ship this freaking release before Christmas.
			var members = optionsByType.OfType<IMemberUnitOption>().Where(option => option.targetType == type/* && option.unit.canDefine*/);

			if (ordered)
			{
				members = members.OrderBy(option => LudiqCore.Configuration.groupInheritedMembers && option.member.isPseudoInherited)
					.ThenBy(option => option.order)
					.ThenBy(option => option.label);
			}

			return members;
		}

		private IEnumerable<IFuzzyOption> EnumsChildren(bool ordered)
		{
			var literals = options.Where(option => option is LiteralOption literalOption && literalOption.literalType.IsEnum);

			if (ordered)
			{
				literals = literals.OrderBy(option => option.label);
			}

			return literals;
		}

		private IEnumerable<IFuzzyOption> CategoryChildren(UnitCategory category, bool subCategories, bool ordered)
		{
			if (category != null && subCategories)
			{
				var subCategoryOptions = options.SelectMany(option => option.category == null ? Enumerable.Empty<UnitCategory>() : option.category.AndAncestors())
					.Distinct()
					.Where(c => c.parent == category);
				if (ordered)
				{
					subCategoryOptions = subCategoryOptions.OrderBy(c => c.name);
				}

				foreach (var subCategory in subCategoryOptions)
				{
					yield return new UnitCategoryOption(subCategory);
				}
			}

			if (category != null)
			{
				if (category.fullName == "Logic")
				{
					if (filter.CompatibleInputType == null)
					{
						foreach (var booleanChild in BooleanLiteralChildren())
						{
							yield return booleanChild;
						}
					}
				}
			}

			var units = options.Where(option => option.category == category && !option.unitType.HasAttribute<SpecialUnitAttribute>());

			if (ordered)
			{
				units = units.OrderBy(option => option.order).ThenBy(option => option.label);
			}

			foreach (var unit in units)
			{
				yield return unit;
			}

			if (category != null)
			{
				if (category.root.name == "Events")
				{
					foreach (var eventChild in EventsChildren(category, ordered))
					{
						yield return eventChild;
					}
				}
				else if (category.fullName == "Codebase")
				{
					foreach (var codebaseChild in CodebaseChildren(ordered))
					{
						yield return codebaseChild;
					}
				}
				else if (category.fullName == "Variables")
				{
					foreach (var variableChild in VariablesChildren())
					{
						yield return variableChild;
					}
				}
				else if (category.fullName == "Math")
				{
					yield return new TypeOption(typeof(float), FuzzyOptionMode.Branch);
					yield return new TypeOption(typeof(Vector2), FuzzyOptionMode.Branch);
					yield return new TypeOption(typeof(Vector3), FuzzyOptionMode.Branch);
					yield return new TypeOption(typeof(int), FuzzyOptionMode.Branch);
					yield return new TypeOption(typeof(Vector2Int), FuzzyOptionMode.Branch);
					yield return new TypeOption(typeof(Vector3Int), FuzzyOptionMode.Branch);

					foreach (var mathChild in MathChildren(ordered))
					{
						yield return mathChild;
					}
				}
				else if (category.fullName == "Time")
				{
					foreach (var timeChild in TimeChildren(ordered))
					{
						yield return timeChild;
					}
				}
				else if (category.fullName == "Nesting")
				{
					foreach (var nestingChild in NestingChildren(ordered))
					{
						yield return nestingChild;
					}
				}
				else if (category.fullName == "Macros")
				{
					foreach (var macroChild in MacroChildren(ordered))
					{
						yield return macroChild;
					}
				}
			}
		}

		private IEnumerable<IFuzzyOption> EventsChildren(UnitCategory category, bool ordered)
		{
			var units = options.Where(option => option.UnitIs<IEventUnit>() && option.category == category);

			if (ordered)
			{
				units = units.OrderBy(option => option.order).ThenBy(option => option.label);
			}

			return units;
		}

		private IEnumerable<IFuzzyOption> BooleanLiteralChildren()
		{
			yield return new PresetLiteralOption(new Literal(typeof(bool), true));
			yield return new PresetLiteralOption(new Literal(typeof(bool), false));
		}

		#endregion


		#region Search

		public override bool searchable { get; } = true;

		public override IEnumerable<ISearchResult<IFuzzyOption>> SearchResults(string query, IFuzzyOption parent, CancellationToken cancellation)
		{
			var children = parent != null
				? SearchableChildren(parent)
				: typesWithMembers.Select(t => (IFuzzyOption)new TypeOption(t, FuzzyOptionMode.Branch)).Concat(options);

			return children.OrderableSearchFilter(query, o => o.haystack, cancellation).WithoutInheritedDuplicates(r => r.result, cancellation);
		}

		public override IEnumerable<IFuzzyOption> OrderedSearchResults(string query, IFuzzyOption parent, CancellationToken cancellation)
		{
			if (filter.CompatibleOutputType != null || connectionSource == null)
			{
				var trimmedQuery = query.Trim();

				var outputType = filter.CompatibleOutputType;

				if (outputType == null || outputType.IsNumeric() || surfaceCommonTypeLiterals)
				{
					var numericType = outputType != null && outputType.IsNumeric() ? outputType : typeof(float);
					object numericValue = null;

					try
					{
						numericValue = Convert.ChangeType(trimmedQuery, numericType);
					}
					catch (Exception) { }

					if (numericValue != null)
					{
						yield return new PresetLiteralOption(new Literal(numericType, numericValue));
					}
				}

				if (outputType == null || outputType == typeof(string) || surfaceCommonTypeLiterals)
				{
					if (trimmedQuery.StartsWith('\"'))
					{
						var stringValue = trimmedQuery.Length > 2 && trimmedQuery.EndsWith('\"')
							? trimmedQuery.Substring(1, trimmedQuery.Length - 2)
							: trimmedQuery.Substring(1);
						yield return new PresetLiteralOption(new Literal(typeof(string), stringValue));
					}
				}

				if ((outputType == null || outputType == typeof(char)) && GetOptionsByType(typeof(char)).Any(option => option is LiteralOption literalOption))
				{
					if (trimmedQuery.StartsWith('\''))
					{
						var stringValue = trimmedQuery.Length > 2 && trimmedQuery.EndsWith('\'')
							? trimmedQuery.Substring(1, trimmedQuery.Length - 2)
							: trimmedQuery.Substring(1);
						if (stringValue.Length == 1)
						{
							yield return new PresetLiteralOption(new Literal(typeof(char), stringValue[0]));
						}
					}
				}
			}

			foreach (var baseSearchResult in base.OrderedSearchResults(query, parent, cancellation))
			{
				yield return baseSearchResult;
			}
		}

		#endregion


		#region Favorites

		public override ICollection<IFuzzyOption> favorites { get; }

		public override bool CanFavorite(IFuzzyOption item)
		{
			return (item as IUnitOption)?.favoritable ?? false;
		}

		public override bool UseExplicitRootLabel(IFuzzyOption item)
		{
			if (filter.CompatibleOutputType != null)
			{
				if (item is IMemberUnitOption memberOption
				    && memberOption.member is var member
				    && (member.declaringType != filter.CompatibleOutputType || (filter.CompatibleOutputType == typeof(float) && member.declaringType != typeof(Mathf))))
				{
					return true;
				}
			}

			if (filter.CompatibleInputType != null)
			{
				if (item is IMemberUnitOption memberOption
				    && memberOption.member is var member
				    && (member.declaringType != filter.CompatibleInputType || (filter.CompatibleInputType == typeof(float) && member.declaringType != typeof(Mathf))))
				{
					return true;
				}
			}

			return false;
		}

		public override string ExplicitLabel(IFuzzyOption item)
		{
			return SearchResultLabel(item, null);
		}

		public override void OnFavoritesChange()
		{
			BoltFlow.Configuration.Save();
		}

		private class Favorites : ICollection<IFuzzyOption>
		{
			public Favorites(UnitOptionTree tree)
			{
				this.tree = tree;
			}

			private UnitOptionTree tree { get; }

			private IEnumerable<IUnitOption> options => tree.options.Where(option => BoltFlow.Configuration.favoriteUnitOptions.Contains(option.key));

			public bool IsReadOnly => false;

			public int Count => BoltFlow.Configuration.favoriteUnitOptions.Count;

			public IEnumerator<IFuzzyOption> GetEnumerator()
			{
				foreach (var option in options)
				{
					yield return option;
				}
			}

			IEnumerator IEnumerable.GetEnumerator()
			{
				return GetEnumerator();
			}

			public bool Contains(IFuzzyOption item)
			{
				var option = (IUnitOption)item;

				return BoltFlow.Configuration.favoriteUnitOptions.Contains(option.key);
			}

			public void Add(IFuzzyOption item)
			{
				var option = (IUnitOption)item;

				BoltFlow.Configuration.favoriteUnitOptions.Add(option.key);
			}

			public bool Remove(IFuzzyOption item)
			{
				var option = (IUnitOption)item;

				return BoltFlow.Configuration.favoriteUnitOptions.Remove(option.key);
			}

			public void Clear()
			{
				BoltFlow.Configuration.favoriteUnitOptions.Clear();
			}

			public void CopyTo(IFuzzyOption[] array, int arrayIndex)
			{
				if (array == null)
				{
					throw new ArgumentNullException(nameof(array));
				}

				if (arrayIndex < 0)
				{
					throw new ArgumentOutOfRangeException(nameof(arrayIndex));
				}

				if (array.Length - arrayIndex < Count)
				{
					throw new ArgumentException();
				}

				var i = 0;

				foreach (var item in this)
				{
					array[i + arrayIndex] = item;
					i++;
				}
			}
		}

		#endregion
	}
}