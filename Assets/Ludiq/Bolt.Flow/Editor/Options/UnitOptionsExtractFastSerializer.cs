﻿using System.IO;
using Ludiq;

namespace Bolt
{
	public class UnitOptionsExtractFastSerializer : FastSerializer<UnitOptionsExtract>
	{
		public static UnitOptionsExtractFastSerializer instance { get; } = new UnitOptionsExtractFastSerializer();

		public override UnitOptionsExtract Instantiate()
		{
			return new UnitOptionsExtract();
		}

		public override void Write(UnitOptionsExtract value, BinaryWriter writer)
		{
			writer.Write(value.options.Count);

			foreach (var option in value.options)
			{
				UnitOptionDataFastSerializer.instance.Write(option, writer);
			}
		}

		public override void Read(ref UnitOptionsExtract value, BinaryReader reader)
		{
			var optionsCount = reader.ReadInt32();

			for (int i = 0; i < optionsCount; i++)
			{
				value.options.Add(UnitOptionDataFastSerializer.instance.Read(reader));
			}
		}
	}
}
