﻿using System.Linq;
using Ludiq;

namespace Bolt
{
	[FuzzyOption(typeof(UnitCategory))]
	public class UnitCategoryOption : FuzzyOption<UnitCategory>
	{
		public UnitCategoryOption(UnitCategory category) : base(FuzzyOptionMode.Branch)
		{
			value = category;
			label = category.name.Split('/').Last().Prettify();
			getIcon = () => BoltFlow.Icons.UnitCategory(category);
		}
	}
}