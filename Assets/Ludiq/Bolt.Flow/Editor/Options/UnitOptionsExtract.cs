﻿using System;
using System.Collections.Generic;
using Ludiq;

namespace Bolt
{
	public sealed class UnitOptionsExtract : IExtract
	{
		public List<UnitOptionData> options { get; set; } = new List<UnitOptionData>();
	}
}