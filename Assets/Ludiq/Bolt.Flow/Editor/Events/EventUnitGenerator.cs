﻿using Ludiq;
using Ludiq.CodeDom;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bolt
{
	public abstract class EventUnitGenerator<TUnit, TArgs> : UnitGenerator<TUnit>, IListeningUnitGenerator
		where TUnit : EventUnit<TArgs>
	{
		public EventUnitGenerator(TUnit unit) : base(unit)
		{
			upperCamelCaseName = BoltFlowNameUtility.UnitTitle(unit.GetType(), false);
			lowerCamelCaseName = upperCamelCaseName.FirstCharacterToLower();

			hookMemberOriginalName = lowerCamelCaseName + "Hook";
			handlerMemberOriginalName = lowerCamelCaseName + "Handler";
			triggerMethodOriginalName = upperCamelCaseName;
		}

		protected string upperCamelCaseName { get; }
		protected string lowerCamelCaseName { get; }

		protected string hookMemberOriginalName { get; set; }
		protected string handlerMemberOriginalName { get; set; }
		protected string triggerMethodOriginalName { get; set; }

		protected virtual string argumentLocalName => "args";
		protected abstract string hookName { get; }

		protected virtual bool ShouldDiscardTriggerArgument()
		{
			return typeof(TArgs) == typeof(EmptyEventArgs);
		}

		public override void DeclareGraphMemberNames(GraphClassGenerationContext context)
		{
			base.DeclareGraphMemberNames(context);

			context.DeclareMemberName(unit, hookMemberOriginalName);
			context.DeclareMemberName(unit, handlerMemberOriginalName);
			context.DeclareMemberName(unit, triggerMethodOriginalName);
		}

		public override IEnumerable<CodeCompositeTypeMember> GenerateMembers(GraphClassGenerationContext context)
		{
			foreach (var member in base.GenerateMembers(context)) yield return member;

			context.usings.Add(new CodeUsingImport(typeof(TArgs).Namespace));

			yield return context.AddImplementationMember(new CodeFieldMember(CodeMemberModifiers.Private, CodeFactory.TypeRef(typeof(EventHook)), context.ExpectMemberName(unit, hookMemberOriginalName)));
			yield return context.AddImplementationMember(new CodeFieldMember(CodeMemberModifiers.Private, CodeFactory.TypeRef(typeof(Action<TArgs>)), context.ExpectMemberName(unit, handlerMemberOriginalName)));
			yield return new FlowMethodGenerationContext(context, unit.coroutine).GenerateMethod(
				CodeMemberModifiers.Public,
				CodeFactory.TypeRef(unit.coroutine ? typeof(IEnumerator) : typeof(void)),
				context.ExpectMemberName(unit, triggerMethodOriginalName),
				ShouldDiscardTriggerArgument()
					? Enumerable.Empty<CodeParameterDeclaration>()
					: new[] { new CodeParameterDeclaration(CodeFactory.TypeRef(typeof(TArgs)), argumentLocalName) },
				GenerateTriggerStatements);
		}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			throw new NotImplementedException();
		}

		protected abstract CodeExpression GenerateEventHookTargetExpression(FlowMethodGenerationContext context);

		public virtual IEnumerable<CodeStatement> GenerateStartListeningStatements(FlowMethodGenerationContext context)
		{
			var graphClassContext = context.graphClassContext;

			yield return CodeFactory.Assign(context.InstanceField(unit, hookMemberOriginalName),
				CodeFactory.TypeRef(typeof(EventHook)).ObjectCreate(CodeFactory.Primitive(hookName), GenerateEventHookTargetExpression(context))
			).Statement();

			CodeExpression handlerExpression;
			if (unit.coroutine)
			{
				if (ShouldDiscardTriggerArgument())
				{
					handlerExpression = CodeFactory.Lambda(
						new CodeParameterDeclaration[] { new CodeParameterDeclaration(CodeFactory.VarType, "args") },
						CodeFactory.ThisRef.Field("machineScript").Field("behaviour").Method("StartCoroutine").Invoke(
							context.InstanceMethod(unit, triggerMethodOriginalName).Invoke()));
				}
				else
				{
					handlerExpression = CodeFactory.Lambda(
						new CodeParameterDeclaration[] { new CodeParameterDeclaration(CodeFactory.VarType, "args") },
						CodeFactory.ThisRef.Field("machineScript").Field("behaviour").Method("StartCoroutine").Invoke(
							context.InstanceMethod(unit, triggerMethodOriginalName).Invoke(CodeFactory.VarRef("args"))));
				}
			}
			else
			{
				if (ShouldDiscardTriggerArgument())
				{
					handlerExpression = CodeFactory.Lambda(
						new CodeParameterDeclaration[] { new CodeParameterDeclaration(CodeFactory.VarType, "args") },
						context.InstanceMethod(unit, triggerMethodOriginalName).Invoke());
				}
				else
				{
					handlerExpression = context.InstanceMethod(unit, triggerMethodOriginalName);
				}
			}

			yield return CodeFactory.Assign(context.InstanceField(unit, handlerMemberOriginalName), handlerExpression).Statement();
			yield return CodeFactory.TypeRef(typeof(EventBus)).Expression().Method("Register").Invoke(
				context.InstanceField(unit, hookMemberOriginalName),
				context.InstanceField(unit, handlerMemberOriginalName)
			).Statement();
		}

		public IEnumerable<CodeStatement> GenerateStopListeningStatements(FlowMethodGenerationContext context)
		{
			var graphClassContext = context.graphClassContext;
			yield return CodeFactory.TypeRef(typeof(EventBus)).Expression().Method("Unregister").Invoke(
				context.InstanceField(unit, hookMemberOriginalName),
				context.InstanceField(unit, handlerMemberOriginalName)
			).Statement();
			yield return context.InstanceField(unit, handlerMemberOriginalName).Assign(CodeFactory.Primitive(null)).Statement();
		}

		protected abstract IEnumerable<CodeStatement> GenerateTriggerStatements(FlowMethodGenerationContext context);
	}
}
