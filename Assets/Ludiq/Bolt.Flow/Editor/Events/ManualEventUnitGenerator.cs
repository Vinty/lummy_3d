﻿using Ludiq.CodeDom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bolt
{
	public abstract class ManualEventUnitGenerator<TUnit, TArgs> : EventUnitGenerator<TUnit, TArgs>
		where TUnit : ManualEventUnit<TArgs>
	{
		public ManualEventUnitGenerator(TUnit unit) : base(unit)
		{
		}

		protected override string hookName => unit.hookName;
	}
}
