﻿using Ludiq;
using Ludiq.CodeDom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Bolt
{
	[Generator(typeof(OnInputFieldEndEdit))]
	public class OnInputFieldEndEditGenerator : GameObjectEventUnitGenerator<OnInputFieldEndEdit, string>
	{
		public OnInputFieldEndEditGenerator(OnInputFieldEndEdit unit) : base(unit) {}

		protected override string argumentLocalName => "value";

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput) => CodeFactory.VarRef(context.currentScope.ExpectValuePortLocal(valueOutput));

		protected override IEnumerable<CodeStatement> GenerateTriggerStatements(FlowMethodGenerationContext context)
		{
			context.currentScope.AliasValuePortLocal(unit.value, argumentLocalName);

			foreach (var next in unit.trigger.GenerateStatements(context)) yield return next;
		}
	}
}
