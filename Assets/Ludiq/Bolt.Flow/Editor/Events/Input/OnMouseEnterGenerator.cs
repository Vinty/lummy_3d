﻿using Ludiq;
using Ludiq.CodeDom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Bolt
{
	[Generator(typeof(OnMouseEnter))]
	public class OnMouseEnterGenerator : GameObjectEventUnitGenerator<OnMouseEnter, EmptyEventArgs>
	{
		public OnMouseEnterGenerator(OnMouseEnter unit) : base(unit) {}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput) => throw new NotImplementedException();
		protected override IEnumerable<CodeStatement> GenerateTriggerStatements(FlowMethodGenerationContext context) => unit.trigger.GenerateStatements(context);
	}
}
