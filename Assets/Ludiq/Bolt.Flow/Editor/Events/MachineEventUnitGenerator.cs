﻿using Ludiq;
using Ludiq.CodeDom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bolt
{
	public abstract class MachineEventUnitGenerator<TUnit, TArgs> : EventUnitGenerator<TUnit, TArgs>
		where TUnit : MachineEventUnit<TArgs>
	{
		public MachineEventUnitGenerator(TUnit unit) : base(unit)
		{
		}

		protected override string hookName => unit.hookName;
		protected override CodeExpression GenerateEventHookTargetExpression(FlowMethodGenerationContext context) => CodeFactory.ThisRef.Field("machineScript");

		public override void DeclareGraphMemberNames(GraphClassGenerationContext context)
		{
			base.DeclareGraphMemberNames(context);

			switch (hookName)
			{
				case EventHooks.Update:
				case EventHooks.LateUpdate:
				case EventHooks.FixedUpdate:
					context.requiredMachineEvents.Add(hookName);
					break;

				default:
					break;
			}
		}
	}
}
