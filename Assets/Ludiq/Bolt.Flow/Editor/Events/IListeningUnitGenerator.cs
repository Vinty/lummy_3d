﻿using Ludiq.CodeDom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bolt
{
	interface IListeningUnitGenerator : IUnitGenerator
	{
		IEnumerable<CodeStatement> GenerateStartListeningStatements(FlowMethodGenerationContext context);
		IEnumerable<CodeStatement> GenerateStopListeningStatements(FlowMethodGenerationContext context);
	}
}
