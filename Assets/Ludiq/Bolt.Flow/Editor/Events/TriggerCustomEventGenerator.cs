﻿using System;
using Ludiq.CodeDom;
using System.Collections.Generic;
using Ludiq;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(TriggerCustomEvent))]
	class TriggerCustomEventGenerator : UnitGenerator<TriggerCustomEvent>
	{
		public TriggerCustomEventGenerator(TriggerCustomEvent unit) : base(unit) {}

		public override IEnumerable<CodeStatement> GenerateControlInputBody(FlowMethodGenerationContext context, ControlInput controlInput)
		{
			if (controlInput == unit.enter)
			{
				var argumentExpressions = new List<CodeExpression>();
				foreach (var argument in unit.arguments)
				{
					argumentExpressions.Add(argument.GenerateExpression(context));
				}				

				yield return CodeFactory.TypeRef(typeof(CustomEvent)).Expression().Method("Trigger").Invoke(
					unit.target.GenerateExpression(context, typeof(GameObject)),
					unit.name.GenerateExpression(context, typeof(string)),
					argumentExpressions.Count == 0
						? (CodeExpression)CodeFactory.TypeRef(typeof(Empty<object>)).Expression().Field("array")
						: CodeFactory.ArrayInitializer(CodeFactory.ArrayTypeRef(CodeFactory.ObjectType), argumentExpressions)
				).Statement();

				foreach (var next in unit.exit.GenerateStatements(context)) yield return next;
			}
			else
			{
				throw new NotImplementedException();
			}
		}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput)
		{
			throw new NotImplementedException();
		}
	}
}