﻿using Ludiq;
using Ludiq.CodeDom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bolt
{
	[Generator(typeof(OnEnable))]
	public class OnEnableGenerator : MachineEventUnitGenerator<OnEnable, EmptyEventArgs>
	{
		public OnEnableGenerator(OnEnable unit) : base(unit) {}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput) => throw new NotImplementedException();
		protected override IEnumerable<CodeStatement> GenerateTriggerStatements(FlowMethodGenerationContext context) => unit.trigger.GenerateStatements(context);
	}
}
