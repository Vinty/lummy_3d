﻿using Ludiq;
using Ludiq.CodeDom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(CollisionEventUnit))]
	public class CollisionEventUnitGenerator : GameObjectEventUnitGenerator<CollisionEventUnit, Collision>
	{
		public CollisionEventUnitGenerator(CollisionEventUnit unit) : base(unit) {}

		protected override string argumentLocalName => "collision";

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput) => CodeFactory.VarRef(context.currentScope.ExpectValuePortLocal(valueOutput));

		protected override IEnumerable<CodeStatement> GenerateTriggerStatements(FlowMethodGenerationContext context)
		{
			yield return context.currentScope.DeclareValuePortLocal(CodeFactory.VarType, unit.collider, CodeFactory.VarRef(argumentLocalName).Field("collider"));
			yield return context.currentScope.DeclareValuePortLocal(CodeFactory.VarType, unit.contacts, CodeFactory.VarRef(argumentLocalName).Field("contacts"));
			yield return context.currentScope.DeclareValuePortLocal(CodeFactory.VarType, unit.impulse, CodeFactory.VarRef(argumentLocalName).Field("impulse"));
			yield return context.currentScope.DeclareValuePortLocal(CodeFactory.VarType, unit.relativeVelocity, CodeFactory.VarRef(argumentLocalName).Field("relativeVelocity"));
			context.currentScope.AliasValuePortLocal(unit.data, argumentLocalName);

			foreach (var next in unit.trigger.GenerateStatements(context)) yield return next;
		}
	}
}
