﻿using Ludiq;
using Ludiq.CodeDom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(OnParticleCollision))]
	public class OnParticleCollisionGenerator : GameObjectEventUnitGenerator<OnParticleCollision, GameObject>
	{
		public OnParticleCollisionGenerator(OnParticleCollision unit) : base(unit) {}

		protected override string argumentLocalName => "other";

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput) => CodeFactory.VarRef(context.currentScope.ExpectValuePortLocal(valueOutput));

		protected override IEnumerable<CodeStatement> GenerateTriggerStatements(FlowMethodGenerationContext context)
		{
			context.currentScope.AliasValuePortLocal(unit.other, argumentLocalName);

			var collisionEventsLocal = context.currentScope.DeclareValuePortLocal(CodeFactory.VarType, unit.collisionEvents, CodeFactory.TypeRef(typeof(List<ParticleCollisionEvent>)).ObjectCreate());

			yield return collisionEventsLocal;

			context.InstanceField(unit, hookMemberOriginalName).Field("target").Cast(CodeFactory.TypeRef(typeof(GameObject)))
				.Method("GetComponent", CodeFactory.TypeRef(typeof(ParticleSystem))).Invoke()
				.Method("GetCollisionEvents").Invoke(CodeFactory.VarRef(collisionEventsLocal.Name))
				.Statement();

			foreach (var next in unit.trigger.GenerateStatements(context)) yield return next;
		}
	}
}
