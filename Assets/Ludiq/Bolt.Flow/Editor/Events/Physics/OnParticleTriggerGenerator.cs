﻿using Ludiq;
using Ludiq.CodeDom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(OnParticleTrigger))]
	public class OnParticleTriggerGenerator : GameObjectEventUnitGenerator<OnParticleTrigger, EmptyEventArgs>
	{
		public OnParticleTriggerGenerator(OnParticleTrigger unit) : base(unit) {}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput) => CodeFactory.VarRef(context.currentScope.ExpectValuePortLocal(valueOutput));

		protected override IEnumerable<CodeStatement> GenerateTriggerStatements(FlowMethodGenerationContext context)
		{
			foreach (var next in unit.trigger.GenerateStatements(context)) yield return next;
		}
	}
}
