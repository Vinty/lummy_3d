﻿using Ludiq;
using Ludiq.CodeDom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(OnControllerColliderHit))]
	public class OnControllerColliderHitGenerator : GameObjectEventUnitGenerator<OnControllerColliderHit, ControllerColliderHit>
	{
		public OnControllerColliderHitGenerator(OnControllerColliderHit unit) : base(unit) {}

		protected override string argumentLocalName => "hitData";

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput) => CodeFactory.VarRef(context.currentScope.ExpectValuePortLocal(valueOutput));

		protected override IEnumerable<CodeStatement> GenerateTriggerStatements(FlowMethodGenerationContext context)
		{
			yield return context.currentScope.DeclareValuePortLocal(CodeFactory.VarType, unit.collider, CodeFactory.VarRef(argumentLocalName).Field("collider"));
			yield return context.currentScope.DeclareValuePortLocal(CodeFactory.VarType, unit.controller, CodeFactory.VarRef(argumentLocalName).Field("controller"));
			yield return context.currentScope.DeclareValuePortLocal(CodeFactory.VarType, unit.moveDirection, CodeFactory.VarRef(argumentLocalName).Field("moveDirection"));
			yield return context.currentScope.DeclareValuePortLocal(CodeFactory.VarType, unit.moveLength, CodeFactory.VarRef(argumentLocalName).Field("moveLength"));
			yield return context.currentScope.DeclareValuePortLocal(CodeFactory.VarType, unit.normal, CodeFactory.VarRef(argumentLocalName).Field("normal"));
			yield return context.currentScope.DeclareValuePortLocal(CodeFactory.VarType, unit.point, CodeFactory.VarRef(argumentLocalName).Field("point"));
			context.currentScope.AliasValuePortLocal(unit.data, argumentLocalName);

			foreach (var next in unit.trigger.GenerateStatements(context)) yield return next;
		}
	}
}
