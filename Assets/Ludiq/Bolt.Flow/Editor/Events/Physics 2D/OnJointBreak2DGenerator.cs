﻿using Ludiq;
using Ludiq.CodeDom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[Generator(typeof(OnJointBreak2D))]
	public class OnJointBreak2DGenerator : GameObjectEventUnitGenerator<OnJointBreak2D, Joint2D>
	{
		public OnJointBreak2DGenerator(OnJointBreak2D unit) : base(unit) {}

		protected override string argumentLocalName => "joint";

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput) => CodeFactory.VarRef(context.currentScope.ExpectValuePortLocal(valueOutput));

		protected override IEnumerable<CodeStatement> GenerateTriggerStatements(FlowMethodGenerationContext context)
		{
			yield return context.currentScope.DeclareValuePortLocal(CodeFactory.VarType, unit.breakForce, CodeFactory.VarRef(argumentLocalName).Field("breakForce"));
			yield return context.currentScope.DeclareValuePortLocal(CodeFactory.VarType, unit.breakTorque, CodeFactory.VarRef(argumentLocalName).Field("breakTorque"));
			yield return context.currentScope.DeclareValuePortLocal(CodeFactory.VarType, unit.connectedBody, CodeFactory.VarRef(argumentLocalName).Field("connectedBody"));
			yield return context.currentScope.DeclareValuePortLocal(CodeFactory.VarType, unit.reactionForce, CodeFactory.VarRef(argumentLocalName).Field("reactionForce"));
			yield return context.currentScope.DeclareValuePortLocal(CodeFactory.VarType, unit.reactionTorque, CodeFactory.VarRef(argumentLocalName).Field("reactionTorque"));
			context.currentScope.AliasValuePortLocal(unit.joint, argumentLocalName);

			foreach (var next in unit.trigger.GenerateStatements(context)) yield return next;
		}
	}
}
