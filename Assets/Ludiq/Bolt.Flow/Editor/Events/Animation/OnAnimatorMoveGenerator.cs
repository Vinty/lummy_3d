﻿using Ludiq;
using Ludiq.CodeDom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bolt
{
	[Generator(typeof(OnAnimatorMove))]
	public class OnAnimatorMoveGenerator : GameObjectEventUnitGenerator<OnAnimatorMove, EmptyEventArgs>
	{
		public OnAnimatorMoveGenerator(OnAnimatorMove unit) : base(unit) {}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput) => CodeFactory.VarRef(context.currentScope.ExpectValuePortLocal(valueOutput));

		protected override IEnumerable<CodeStatement> GenerateTriggerStatements(FlowMethodGenerationContext context)
		{
			foreach (var next in unit.trigger.GenerateStatements(context)) yield return next;
		}
	}
}
