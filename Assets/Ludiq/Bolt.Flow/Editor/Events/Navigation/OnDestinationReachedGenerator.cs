﻿using Ludiq;
using Ludiq.CodeDom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.AI;

namespace Bolt
{
	[Generator(typeof(OnDestinationReached))]
	public class OnDestinationReachedGenerator : MachineEventUnitGenerator<OnDestinationReached, EmptyEventArgs>
	{
		public OnDestinationReachedGenerator(OnDestinationReached unit) : base(unit) {}

		public override CodeExpression GenerateValueOutputExpression(FlowMethodGenerationContext context, ValueOutput valueOutput) => throw new NotImplementedException();

		protected override IEnumerable<CodeStatement> GenerateTriggerStatements(FlowMethodGenerationContext context)
		{
			var navMeshAgentLocal = context.currentScope.DeclareLocal(CodeFactory.VarType, "navMeshAgent",
				CodeFactory.ThisRef.Field("gameObject").Method("GetComponent", CodeFactory.TypeRef(typeof(NavMeshAgent))).Invoke());

			yield return navMeshAgentLocal;

			var thresholdExpression = unit.threshold.GenerateExpression(context, typeof(float));
			var requireSuccessExpression = unit.requireSuccess.GenerateExpression(context, typeof(bool));

			var checkNotNull = CodeFactory.VarRef(navMeshAgentLocal.Name).NotEqual(CodeFactory.Primitive(null));
			var checkThreshold = CodeFactory.VarRef(navMeshAgentLocal.Name).Field("remainingDistance").LessThanOrEqual(thresholdExpression);
			var checkPathStatus = CodeFactory.VarRef(navMeshAgentLocal.Name).Field("pathStatus").Equal(CodeFactory.TypeRef(typeof(NavMeshPathStatus)).Expression().Field("PathComplete"));
			var shouldTriggerExpression = checkNotNull.LogicalAnd(checkThreshold);

			if (requireSuccessExpression is CodePrimitiveExpression requireSuccessPrimitive)
			{
				if ((bool) requireSuccessPrimitive.Value)
				{
					shouldTriggerExpression = shouldTriggerExpression.LogicalAnd(checkPathStatus);
				}
			}
			else
			{
				shouldTriggerExpression = shouldTriggerExpression.LogicalAnd(checkPathStatus.LogicalOr(requireSuccessExpression.LogicalNot()));
			}

			yield return new CodeIfStatement(shouldTriggerExpression, unit.trigger.GenerateStatements(context).ToList());
		}
	}
}
