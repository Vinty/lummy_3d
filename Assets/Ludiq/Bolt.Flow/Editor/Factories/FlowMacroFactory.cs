﻿using Ludiq;
using UnityEditor;
using UnityEngine;

namespace Bolt
{
	[GraphFactory(typeof(FlowMacro))]
	public class FlowMacroFactory : GraphFactory<FlowMacro, FlowGraph>
	{
		public FlowMacroFactory(FlowMacro parent) : base(parent) { }

		public override FlowGraph DefaultGraph()
		{
			return FlowGraphFactory.WithInputOutput();
		}
		
		[MenuItem("Assets/Create/Bolt/Flow Macro (Start + Update)")]
		public static void CreateFlowMacroInProjectWithStartUpdate()
		{
			var macro = ScriptableObject.CreateInstance<FlowMacro>();
			macro.graph = FlowGraphFactory.WithStartUpdate();
			ProjectWindowUtil.CreateAsset(macro, "New Flow Macro.asset");
		}
		
		[MenuItem("Assets/Create/Bolt/Flow Macro (Input + Output)")]
		public static void CreateFlowMacroInProjectWithInputOutput()
		{
			var macro = ScriptableObject.CreateInstance<FlowMacro>();
			macro.graph = FlowGraphFactory.WithInputOutput();
			ProjectWindowUtil.CreateAsset(macro, "New Flow Macro.asset");
		}
	}
}
