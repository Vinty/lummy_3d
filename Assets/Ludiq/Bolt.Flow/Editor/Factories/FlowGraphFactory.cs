﻿using System;
using System.Linq;
using Ludiq;
using UnityEngine;

namespace Bolt
{
	public static class FlowGraphFactory
	{
		public static FlowGraph Blank()
		{
			var graph = new FlowGraph();

			graph.controlAxis = BoltFlow.Configuration.defaultControlAxis;

			return graph;
		}

		public static FlowGraph WithInputOutput(out GraphInput input, out GraphOutput output)
		{
			var graph = Blank();

			input = new GraphInput();
			output = new GraphOutput();

			graph.units.Add(input);
			graph.units.Add(output);
			
			switch (graph.controlAxis)
			{
				case Axis2.Horizontal:
					input.position = new Vector2(-250, -30);
					output.position = new Vector2(105, -30);
					break;
				case Axis2.Vertical:
					input.position = new Vector2(-250, -30);
					output.position = new Vector2(105, -30);
					break;
				default:
					throw new UnexpectedEnumValueException<Axis2>(graph.controlAxis);
			}

			return graph;
		}

		public static FlowGraph WithInputOutput()
		{
			return WithInputOutput(out var input, out var output);
		}

		public static FlowGraph WithStartUpdate(out Start start, out Update update)
		{
			var graph = Blank();

			start = new Start();
			update = new Update();

			graph.units.Add(start);
			graph.units.Add(update);

			switch (graph.controlAxis)
			{
				case Axis2.Horizontal:
					start.position = new Vector2(-204, -144);
					update.position = new Vector2(204, 60);
					break;
				case Axis2.Vertical:
					start.position = new Vector2(-220, -30);
					update.position = new Vector2(130, -30);
					break;
				default:
					throw new UnexpectedEnumValueException<Axis2>(graph.controlAxis);
			}

			return graph;
		}

		public static FlowGraph WithStartUpdate()
		{
			return WithStartUpdate(out var start, out var update);
		}
		
		private static string GetUniqueKey(FlowGraph graph, IUnitPort port)
		{
			return StringUtility.UniqueName(port.key.TrimStart("%"), graph.definitions.Select(x => x.key).ToHashSet());
		}

		private static string GetUniqueLabel(FlowGraph graph, IUnitPort port)
		{
			return StringUtility.UniqueName(UnitPortDescription.ResolveDisplayLabel(port), graph.definitions.Select(x => x.label).ToHashSet());
		}

		// Because adding definitions can redefine units in the graph,
		// including the unit of the port that was passed as the input,
		// we have to make it a ref parameter and ensure it gets properly
		// found and reassigned.

		public static IUnitPortDefinition AddCompatibleDefinition(this FlowGraph graph, ref IUnitPort port)
		{
			if (port is ControlInput controlInput)
			{
				var definition = graph.AddCompatibleDefinition(ref controlInput);
				port = controlInput;
				return definition;
			}
			else if (port is ControlOutput controlOutput)
			{
				var definition = graph.AddCompatibleDefinition(ref controlOutput);
				port = controlOutput;
				return definition ;
			}
			else if (port is ValueInput valueInput)
			{
				var definition = graph.AddCompatibleDefinition(ref valueInput);
				port = valueInput;
				return definition;
			}
			else if (port is ValueOutput valueOutput)
			{
				var definition = graph.AddCompatibleDefinition(ref valueOutput);
				port = valueOutput;
				return definition;
			}

			throw new NotSupportedException();
		}

		public static ControlInputDefinition AddCompatibleDefinition(this FlowGraph graph, ref ControlInput port)
		{
			var definition = new ControlInputDefinition()
			{
				key = GetUniqueKey(graph, port),
				label = GetUniqueLabel(graph, port)
			};

			graph.controlInputDefinitions.Add(definition);
			
			var reference = port.ToReference();
			graph.PortDefinitionsChanged();
			port = (ControlInput)reference.FindPort();

			return definition;
		}

		public static ControlOutputDefinition AddCompatibleDefinition(this FlowGraph graph, ref ControlOutput port)
		{
			var definition = new ControlOutputDefinition()
			{
				key = GetUniqueKey(graph, port),
				label = GetUniqueLabel(graph, port)
			};

			graph.controlOutputDefinitions.Add(definition);
			
			var reference = port.ToReference();
			graph.PortDefinitionsChanged();
			port = (ControlOutput)reference.FindPort();

			return definition;
		}

		public static ValueInputDefinition AddCompatibleDefinition(this FlowGraph graph, ref ValueInput port)
		{
			var definition = new ValueInputDefinition()
			{
				key = GetUniqueKey(graph, port),
				label = GetUniqueLabel(graph, port),
				type = port.type,
				hasDefaultValue = port.hasDefaultValue,
				allowsNull = port.allowsNull,
				nullMeansSelf = port.nullMeansSelf
			};

			if (port.hasDefaultValue)
			{
				definition.defaultValue = port.unit.defaultValues[port.key];
			}

			graph.valueInputDefinitions.Add(definition);
			
			var reference = port.ToReference();
			graph.PortDefinitionsChanged();
			port = (ValueInput)reference.FindPort();

			return definition;
		}

		public static ValueOutputDefinition AddCompatibleDefinition(this FlowGraph graph, ref ValueOutput port)
		{
			var definition = new ValueOutputDefinition()
			{
				key = GetUniqueKey(graph, port),
				label = GetUniqueLabel(graph, port),
				type = port.type
			};

			graph.valueOutputDefinitions.Add(definition);
			
			var reference = port.ToReference();
			graph.PortDefinitionsChanged();
			port = (ValueOutput)reference.FindPort();

			return definition;
		}

		public static IUnitPortDefinition AddExternalCompatibleDefinition(this FlowGraph graph, ref IUnitPort port)
		{
			if (port is ControlInput controlInput)
			{
				var definition = graph.AddExternalCompatibleDefinition(ref controlInput);
				port = controlInput;
				return definition;
			}
			else if (port is ControlOutput controlOutput)
			{
				var definition =  graph.AddExternalCompatibleDefinition(ref controlOutput);
				port = controlOutput;
				return definition;
			}
			else if (port is ValueInput valueInput)
			{
				var definition =  graph.AddExternalCompatibleDefinition(ref valueInput);
				port = valueInput;
				return definition;
			}
			else if (port is ValueOutput valueOutput)
			{
				var definition =  graph.AddExternalCompatibleDefinition(ref valueOutput);
				port = valueOutput;
				return definition;
			}

			throw new NotSupportedException();
		}

		public static ControlOutputDefinition AddExternalCompatibleDefinition(this FlowGraph graph, ref ControlInput port)
		{
			var definition = new ControlOutputDefinition()
			{
				key = GetUniqueKey(graph, port),
				label = GetUniqueLabel(graph, port)
			};

			graph.controlOutputDefinitions.Add(definition);
			
			var reference = port.ToReference();
			graph.PortDefinitionsChanged();
			port = (ControlInput)reference.FindPort();

			return definition;
		}

		public static ControlInputDefinition AddExternalCompatibleDefinition(this FlowGraph graph, ref ControlOutput port)
		{
			var definition = new ControlInputDefinition()
			{
				key = GetUniqueKey(graph, port),
				label = GetUniqueLabel(graph, port)
			};

			graph.controlInputDefinitions.Add(definition);
			
			var reference = port.ToReference();
			graph.PortDefinitionsChanged();
			port = (ControlOutput)reference.FindPort();

			return definition;
		}

		public static ValueOutputDefinition AddExternalCompatibleDefinition(this FlowGraph graph, ref ValueInput port)
		{
			var definition = new ValueOutputDefinition()
			{
				key = GetUniqueKey(graph, port),
				label = GetUniqueLabel(graph, port),
				type = port.type,
			};

			graph.valueOutputDefinitions.Add(definition);
			
			var reference = port.ToReference();
			graph.PortDefinitionsChanged();
			port = (ValueInput)reference.FindPort();

			return definition;
		}

		public static ValueInputDefinition AddExternalCompatibleDefinition(this FlowGraph graph, ref ValueOutput port)
		{
			var definition = new ValueInputDefinition()
			{
				key = GetUniqueKey(graph, port),
				label = GetUniqueLabel(graph, port),
				type = port.type
			};

			graph.valueInputDefinitions.Add(definition);
			
			var reference = port.ToReference();
			graph.PortDefinitionsChanged();
			port = (ValueOutput)reference.FindPort();

			return definition;
		}
	}
}
