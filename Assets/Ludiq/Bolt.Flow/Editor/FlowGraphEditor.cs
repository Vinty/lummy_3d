﻿using System.Collections.Generic;
using System.Linq;
using Ludiq;
using UnityEditor;
using UnityEngine;

namespace Bolt
{
	[Editor(typeof(FlowGraph))]
	public class FlowGraphEditor : GraphEditor<FlowGraphContext>
	{
		public FlowGraphEditor(Metadata metadata) : base(metadata) { }
		
		private new FlowGraph graph => (FlowGraph)base.graph;
		
		private IEnumerable<Warning> warnings => UnitPortDefinitionUtility.Warnings((FlowGraph)metadata.value);

		protected override float GetHeight(float width, GUIContent label)
		{
			var height = base.GetHeight(width, label);

			if (warnings.Any())
			{
				height += EditorGUIUtility.standardVerticalSpacing;

				foreach (var warning in warnings)
				{
					height += warning.GetHeight(width);
				}
			}

			return height;
		}

		protected override void OnGUI(Rect position, GUIContent label)
		{
			base.OnGUI(position, label);
			
			if (warnings.Any())
			{
				y += EditorGUIUtility.standardVerticalSpacing;

				foreach (var warning in warnings)
				{
					y--;
					warning.OnGUI(position.VerticalSection(ref y, warning.GetHeight(position.width) + 1));
				}
			}
		}
	}
}