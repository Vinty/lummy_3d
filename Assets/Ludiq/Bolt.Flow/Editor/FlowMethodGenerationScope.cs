﻿using Ludiq;
using Ludiq.CodeDom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bolt
{
	public sealed class FlowMethodGenerationScope : GraphMethodGenerationScope<FlowMethodGenerationScope>
	{
		private Dictionary<IUnitValuePort, string> valuePortLocals = new Dictionary<IUnitValuePort, string>();

		public FlowMethodGenerationScope(FlowMethodGenerationScope parentScope, IdentifierPool identifierPool) : base(parentScope, identifierPool) {}

		public CodeVariableDeclarationStatement DeclareValuePortLocal(CodeTypeReference type, IUnitValuePort valuePort, CodeExpression initExpression = null)
		{
			var local = DeclareLocal(type, valuePort.key, initExpression);
			AliasValuePortLocal(valuePort, local.Name);
			return local;
		}
	
		public void AliasValuePortLocal(IUnitValuePort valuePort, string name)
		{
			valuePortLocals.Add(valuePort, name);
		}

		public bool HasValuePortLocal(IUnitValuePort valuePort)
		{
			if (valuePortLocals.ContainsKey(valuePort))
			{
				return true;
			}
			else if (parentScope != null)
			{
				return parentScope.HasValuePortLocal(valuePort);
			}
			return false;
		}

		public bool TryGetValuePortLocal(IUnitValuePort valuePort, out string local)
		{
			if (valuePortLocals.TryGetValue(valuePort, out local))
			{
				return true;
			}
			else if (parentScope != null)
			{
				return parentScope.TryGetValuePortLocal(valuePort, out local);
			}
			return false;
		}

		public string ExpectValuePortLocal(IUnitValuePort valuePort)
		{			
			if (TryGetValuePortLocal(valuePort, out var local))
			{
				return local;
			}
			throw new KeyNotFoundException($"{valuePort.key} was not found in the port local dictionary");
		}
	}
}
