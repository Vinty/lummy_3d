﻿using Ludiq;
using UnityEngine;

namespace Bolt
{
	[Widget(typeof(ControlOutput))]
	public class ControlOutputWidget : UnitPortWidget<ControlOutput>
	{
		public ControlOutputWidget(FlowCanvas canvas, ControlOutput port) : base(canvas, port) { }
		
		public override Axis2 axis => unit.graph.controlAxis;

		public override Edge edge
		{
			get
			{
				switch (axis)
				{
					case Axis2.Horizontal: return Edge.Right;
					case Axis2.Vertical: return Edge.Bottom;
					default: throw new UnexpectedEnumValueException<Axis2>(axis);
				}
			}
		}
		
		protected override Edges proxyEdges
		{
			get
			{
				switch (axis)
				{
					case Axis2.Horizontal: return Edges.Top;
					case Axis2.Vertical: return Edges.Left | Edges.Right;
					default: throw new UnexpectedEnumValueException<Axis2>(axis);
				}
			}
		}

		protected override bool showIcon => base.showIcon && axis == Axis2.Horizontal;

		protected override Vector2 handleSize
		{
			get
			{
				switch (axis)
				{
					case Axis2.Horizontal: return new Vector2(9, 12);
					case Axis2.Vertical: return new Vector2(12, 9);
					default: throw new UnexpectedEnumValueException<Axis2>(axis);
				}
			}
		}

		protected override EditorTexture handleTextureConnected
		{
			get
			{
				switch (axis)
				{
					case Axis2.Horizontal: return  BoltFlow.Icons.controlPortHorizontalConnected;
					case Axis2.Vertical: return  BoltFlow.Icons.controlPortVerticalConnected;
					default: throw new UnexpectedEnumValueException<Axis2>(axis);
				}
			}
		}

		protected override EditorTexture handleTextureUnconnected
		{
			get
			{
				switch (axis)
				{
					case Axis2.Horizontal: return  BoltFlow.Icons.controlPortHorizontalUnconnected;
					case Axis2.Vertical: return  BoltFlow.Icons.controlPortVerticalUnconnected;
					default: throw new UnexpectedEnumValueException<Axis2>(axis);
				}
			}
		}

		protected override bool colorIfActive => !BoltFlow.Configuration.animateControlConnections || !BoltFlow.Configuration.animateValueConnections;
	}
}