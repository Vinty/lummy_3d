﻿using Ludiq;
using UnityEngine;

namespace Bolt
{
	[Widget(typeof(InvalidOutput))]
	public class InvalidOutputWidget : UnitPortWidget<InvalidOutput>
	{
		public InvalidOutputWidget(FlowCanvas canvas, InvalidOutput port) : base(canvas, port) { }
		
		public override Axis2 axis => Axis2.Horizontal;

		public override Edge edge => Edge.Right;

		protected override Edges proxyEdges => Edges.Top;

		protected override Vector2 handleSize => new Vector2(9, 9);

		protected override EditorTexture handleTextureConnected => BoltFlow.Icons.invalidPortConnected;

		protected override EditorTexture handleTextureUnconnected => BoltFlow.Icons.invalidPortUnconnected;

		protected override bool colorIfActive => false;

		protected override bool canStartConnection => false;
	}
}