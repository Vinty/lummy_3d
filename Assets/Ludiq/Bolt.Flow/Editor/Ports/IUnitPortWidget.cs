﻿using Ludiq;
using UnityEngine;

namespace Bolt
{
	public interface IUnitPortWidget : IWidget
	{
		IUnitPort port { get; }
		Axis2 axis { get; }
		Edge edge { get; }
		Edges drawnEdges { get; set; }
		Vector2 outerSize { get; }
		Rect outerPosition { get; set; }
		Rect handlePosition { get; }
		bool forceShowLabel { get; set; }
		void CacheSize();
		bool willDisconnect { get; }
		Vector2 dynamicHandleCenter { get; set; }
	}
}