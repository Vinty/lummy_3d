﻿using Ludiq;
using UnityEngine;

namespace Bolt
{
	[Widget(typeof(ValueInput))]
	public class ValueInputWidget : UnitPortWidget<ValueInput>
	{
		public ValueInputWidget(FlowCanvas canvas, ValueInput port) : base(canvas, port)
		{
			color = port.type.Color();
		}
		
		public override Axis2 axis => Axis2.Horizontal;

		public override Edge edge => Edge.Left;

		protected override Edges proxyEdges => Edges.Top;

		protected override bool showInspector => port.hasDefaultValue && !port.hasValidConnection;

		protected override bool colorIfActive => !BoltFlow.Configuration.animateControlConnections || !BoltFlow.Configuration.animateValueConnections;

		public override Color color { get; }

		protected override Vector2 handleSize => new Vector2(9, 9);

		protected override EditorTexture handleTextureConnected => BoltFlow.Icons.valuePortConnected;

		protected override EditorTexture handleTextureUnconnected => BoltFlow.Icons.valuePortUnconnected;

		public override Metadata FetchInspectorMetadata()
		{
			if (port.hasDefaultValue)
			{
				return metadata["_defaultValue"].Cast(port.type);
			}
			else
			{
				return null;
			}
		}
	}
}