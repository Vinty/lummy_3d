﻿using Ludiq;

namespace Bolt
{
	[Inspector(typeof(FlowGraph))]
	public class FlowGraphInspector : GraphInspector
	{
		public FlowGraphInspector(Metadata metadata) : base(metadata) { }

		protected override void OnMemberChange(MemberMetadata member)
		{
			base.OnMemberChange(member);

			if (member.name == nameof(FlowGraph.controlAxis))
			{
				LudiqGraphsEditorUtility.editedContext.value?.canvas.RepositionAll();
			}
			else if (member.name.EndsWith("Definitions"))
			{
				((FlowGraph)metadata.value).PortDefinitionsChanged();
			}
		}
	}
}