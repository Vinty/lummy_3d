﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bolt
{
	public abstract class FlowGraphScript : GraphScript<FlowGraph, FlowGraphData, FlowGraphScript>
	{
		public FlowGraphScript(IMachineScript machineScript) : base(machineScript) {}

		public object GetRootFlowGraphScript()
		{
			if (parent is FlowGraphScript parentFlowGraphScript)
			{
				return parentFlowGraphScript.GetRootFlowGraphScript();
			}
			return this;
		}

		public abstract void StartListening();
		public abstract void StopListening();
	}
}
