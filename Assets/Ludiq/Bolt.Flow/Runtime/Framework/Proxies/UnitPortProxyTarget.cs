﻿using Ludiq;

namespace Bolt
{
	[RenamedFrom("UnitPortProxyTargetType")]
	[RenamedFrom("UnitPortProxySource")]
	public enum UnitPortProxyTarget
	{
		Input,
		Output,
	}
}