﻿using System;
using Ludiq;

namespace Bolt
{
	/// <summary>
	/// A unit that forwards a control port connection between units.
	/// </summary>
	[SpecialUnit]
	[UnitTitle("Control Proxy")]
	[UnitShortTitle("Proxy")]
	public sealed class UnitControlPortProxy : UnitPortProxy<ControlInput, ControlOutput>
	{
		[Obsolete(Serialization.ConstructorWarning)]
		public UnitControlPortProxy() : base() {}

		public UnitControlPortProxy(UnitPortProxyTarget target) : base(target) { }

		protected override void Definition()
		{
			input = ControlInput(nameof(input), Enter);
			output = ControlOutput(nameof(output));
			Succession(input, output);
		}

		private ControlOutput Enter(Flow flow)
		{
			return output;
		}
	}
}