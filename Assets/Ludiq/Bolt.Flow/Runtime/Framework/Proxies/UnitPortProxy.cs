﻿using Ludiq;

namespace Bolt
{
	/// <summary>
	/// A special unit that creates a wireless connection between ports.
	/// </summary>
	public abstract class UnitPortProxy<TInput, TOutput> : Unit, IUnitPortProxy 
		where TInput : IUnitInputPort
		where TOutput: IUnitOutputPort
	{
		protected UnitPortProxy() : base() { }

		protected UnitPortProxy(UnitPortProxyTarget target) : base()
		{
			targetType = target;
		}

		[Serialize]
		[RenamedFrom("sourceType")]
		public UnitPortProxyTarget targetType { get; private set; }

		[DoNotSerialize]
		[UnitPrimaryPort]
		public TInput input { get; protected set; }

		[DoNotSerialize]
		[UnitPrimaryPort]
		public TOutput output { get; protected set; }
		
		[DoNotSerialize]
		IUnitInputPort IUnitPortProxy.input => input;
		
		[DoNotSerialize]
		IUnitOutputPort IUnitPortProxy.output => output;
		
		[DoNotSerialize]
		public IUnitPort target
		{
			get
			{
				switch (targetType)
				{
					case UnitPortProxyTarget.Input: return output;
					case UnitPortProxyTarget.Output: return input;
					default: throw new UnexpectedEnumValueException<UnitPortProxyTarget>(targetType);
				}
			}
		}
		
		[DoNotSerialize]
		public IUnitPort relay
		{
			get
			{
				switch (targetType)
				{
					case UnitPortProxyTarget.Input: return input;
					case UnitPortProxyTarget.Output: return output;
					default: throw new UnexpectedEnumValueException<UnitPortProxyTarget>(targetType);
				}
			}
		}
	}
}