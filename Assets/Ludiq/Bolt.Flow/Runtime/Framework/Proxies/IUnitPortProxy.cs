﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ludiq;

namespace Bolt
{
	public interface IUnitPortProxy : IUnit
	{
		UnitPortProxyTarget targetType { get; }

		IUnitPort target { get; }
		
		IUnitPort relay { get; }
		
		IUnitInputPort input { get; }
		
		IUnitOutputPort output { get; }
	}

	public static class XUnitPortProxy
	{
		public static IUnitPortProxy CreateProxy(this IUnitPort target)
		{
			if (target is ValueInput valueInput)
			{
				return new UnitValuePortProxy(UnitPortProxyTarget.Input, valueInput.type);
			}
			else if (target is ValueOutput valueOutput)
			{
				return new UnitValuePortProxy(UnitPortProxyTarget.Output, valueOutput.type);
			}
			else if (target is ControlInput)
			{
				return new UnitControlPortProxy(UnitPortProxyTarget.Input);
			}
			else if (target is ControlOutput)
			{
				return new UnitControlPortProxy(UnitPortProxyTarget.Output);
			}

			throw new NotSupportedException();
		}

		public static IEnumerable<IUnitPort> ResolveProxyTargets(this IUnitPort port)
		{
			Ensure.That(nameof(port)).IsNotNull(port);

			using (var recursion = Recursion.New(1))
			{
				return ResolveProxyTargets(port, recursion).ToArray();
			}
		}

		private static IEnumerable<IUnitPort> ResolveProxyTargets(this IUnitPort port, Recursion recursion)
		{
			if (!recursion.TryEnter(port))
			{
				yield break;
			}

			if (port.unit is IUnitPortProxy proxy)
			{
				foreach (var connectedPort in proxy.target.connectedPorts)
				{
					foreach (var proxiedPort in ResolveProxyTargets(connectedPort, recursion))
					{
						yield return proxiedPort;
					}
				}
			}
			else
			{
				yield return port;
			}

			recursion.Exit(port);
		}

		public static IEnumerable<IUnitPort> ResolveProxyRelays(this IUnitPort port)
		{
			Ensure.That(nameof(port)).IsNotNull(port);

			using (var recursion = Recursion.New(1))
			{
				return ResolveProxyRelays(port, recursion).ToArray();
			}
		}

		private static IEnumerable<IUnitPort> ResolveProxyRelays(this IUnitPort port, Recursion recursion)
		{
			if (!recursion.TryEnter(port))
			{
				yield break;
			}

			if (port.unit is IUnitPortProxy proxy)
			{
				foreach (var connectedPort in proxy.relay.connectedPorts)
				{
					foreach (var proxiedPort in ResolveProxyRelays(connectedPort, recursion))
					{
						yield return proxiedPort;
					}
				}
			}
			else
			{
				yield return port;
			}

			recursion.Exit(port);
		}
	}
}