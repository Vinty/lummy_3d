﻿using Ludiq;
using System;
using UnityEngine;

namespace Bolt
{
	/// <summary>
	/// A unit that forwards a value port connection between units.
	/// </summary>
	[SpecialUnit]
	[UnitTitle("Value Proxy")]
	[UnitShortTitle("Proxy")]
	public sealed class UnitValuePortProxy : UnitPortProxy<ValueInput, ValueOutput>
	{
		[Obsolete(Serialization.ConstructorWarning)]
		public UnitValuePortProxy() : base() { }

		public UnitValuePortProxy(UnitPortProxyTarget target, Type type) : base(target)
		{
			this.type = type;
		}

		[Serialize]
		public Type type { get; set; }
		
		protected override void Definition()
		{
			input = ValueInput(type, nameof(input));
			output = ValueOutput(type, nameof(output), Result).PredictableIf(IsPredictable);
			Requirement(input, output);
		}

		private object Result(Flow flow)
		{
			return flow.GetValue(input);
		}

		private bool IsPredictable(Flow flow)
		{
			return flow.CanPredict(input);
		}
	}
}