﻿using Ludiq;
using System;
using UnityEngine.EventSystems;

namespace Bolt
{
	public abstract class PointerEventUnit : GameObjectEventUnit<PointerEventData>
	{
		/// <summary>
		/// The pointer event data.
		/// </summary>
		[DoNotSerialize]
		[UnitPortLabelHidden]
		public ValueOutput data { get; private set; }

		public override Type eventProxyType => typeof(PointerEventProxy);

		protected override void Definition()
		{
			base.Definition();
			
			data = ValueOutput<PointerEventData>(nameof(data));
		}

		protected override void AssignArguments(Flow flow, PointerEventData data)
		{
			flow.SetValue(this.data, data);
		}
	}
}