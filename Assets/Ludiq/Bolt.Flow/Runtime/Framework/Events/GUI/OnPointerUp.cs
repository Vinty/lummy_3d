﻿using Ludiq;
using System;

namespace Bolt
{
	/// <summary>
	/// Called when the pointer releases the GUI element.
	/// </summary>
	[UnitCategory("Events/GUI")]
	[UnitOrder(13)]
	public sealed class OnPointerUp : PointerEventUnit
	{
		public override string hookName => EventHooks.OnPointerUp;
		public override Type eventProxyType => typeof(PointerEventProxy);
	}
}