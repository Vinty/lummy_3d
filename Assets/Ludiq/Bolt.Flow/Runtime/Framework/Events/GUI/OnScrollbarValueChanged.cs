﻿using Ludiq;
using System;
using UnityEngine.UI;

namespace Bolt
{
	/// <summary>
	/// Called when the current value of the scrollbar has changed.
	/// </summary>
	[UnitCategory("Events/GUI")]
	[TypeIcon(typeof(Scrollbar))]
	[UnitOrder(6)]
	public sealed class OnScrollbarValueChanged : GameObjectEventUnit<float>
	{
		public override string hookName => EventHooks.OnScrollbarValueChanged;

		public override Type eventProxyType => typeof(ScrollbarChangedEventProxy);

		/// <summary>
		/// The new position value of the scrollbar.
		/// </summary>
		[DoNotSerialize]
		[UnitPortLabelHidden]
		public ValueOutput value { get; private set; }

		protected override void Definition()
		{
			base.Definition();
			
			value = ValueOutput<float>(nameof(value));
		}

		protected override void AssignArguments(Flow flow, float value)
		{
			flow.SetValue(this.value, value);
		}
	}
}