﻿using Ludiq;
using System;

namespace Bolt
{
	/// <summary>
	/// Called when the pointer clicks the GUI element.
	/// </summary>
	[UnitCategory("Events/GUI")]
	[UnitOrder(11)]
	public sealed class OnPointerClick : PointerEventUnit
	{
		public override string hookName => EventHooks.OnPointerClick;
		public override Type eventProxyType => typeof(PointerEventProxy);
	}
}