﻿using Ludiq;
using System;

namespace Bolt
{
	/// <summary>
	/// Called on the drag object when a drag finishes.
	/// </summary>
	[UnitCategory("Events/GUI")]
	[TypeIcon(typeof(OnDrag))]
	[UnitOrder(18)]
	public sealed class OnEndDrag : PointerEventUnit
	{
		public override string hookName => EventHooks.OnEndDrag;
		public override Type eventProxyType => typeof(DragAndDropEventProxy);
	}
}