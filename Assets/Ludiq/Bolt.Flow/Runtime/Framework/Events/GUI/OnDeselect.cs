﻿using Ludiq;
using System;

namespace Bolt
{
	/// <summary>
	/// Called when the pointer deselects the GUI element.
	/// </summary>
	[UnitCategory("Events/GUI")]
	[UnitOrder(23)]
	public sealed class OnDeselect : GenericGuiEventUnit
	{
		public override string hookName => EventHooks.OnDeselect;
		public override Type eventProxyType => typeof(SelectionEventProxy);
	}
}