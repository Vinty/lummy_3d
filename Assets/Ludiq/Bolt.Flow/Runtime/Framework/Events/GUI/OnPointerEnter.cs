﻿using Ludiq;
using System;

namespace Bolt
{
	/// <summary>
	/// Called when the pointer enters the GUI element.
	/// </summary>
	[UnitCategory("Events/GUI")]
	[UnitOrder(14)]
	public sealed class OnPointerEnter : PointerEventUnit
	{
		public override string hookName => EventHooks.OnPointerEnter;
		public override Type eventProxyType => typeof(PointerEventProxy);
	}
}