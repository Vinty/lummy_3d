﻿using Ludiq;
using System;

namespace Bolt
{
	/// <summary>
	/// Called when a mouse wheel scrolls.
	/// </summary>
	[UnitCategory("Events/GUI")]
	[UnitOrder(20)]
	public sealed class OnScroll : PointerEventUnit
	{
		public override string hookName => EventHooks.OnScroll;
		public override Type eventProxyType => typeof(ScrollEventProxy);
	}
}