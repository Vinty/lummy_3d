﻿using Ludiq;
using System;

namespace Bolt
{
	/// <summary>
	/// Called on a target that can accept a drop.
	/// </summary>
	[UnitCategory("Events/GUI")]
	[TypeIcon(typeof(OnDrag))]
	[UnitOrder(19)]
	public sealed class OnDrop : PointerEventUnit
	{
		public override string hookName => EventHooks.OnDrop;
		public override Type eventProxyType => typeof(DragAndDropEventProxy);
	}
}