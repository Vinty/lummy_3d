using Ludiq;

namespace Bolt
{
	/// <summary>
	/// Called when the application gains focus.
	/// </summary>
	[UnitCategory("Events/Application")]
	public sealed class OnApplicationFocus : GlobalEventUnit<EmptyEventArgs>
	{
		public override string hookName => EventHooks.OnApplicationFocus;
	}
}
