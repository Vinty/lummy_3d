using Ludiq;

namespace Bolt
{
	/// <summary>
	/// Called when the application quits.
	/// </summary>
	[UnitCategory("Events/Application")]
	public sealed class OnApplicationQuit : GlobalEventUnit<EmptyEventArgs>
	{
		public override string hookName => EventHooks.OnApplicationQuit;
	}
}