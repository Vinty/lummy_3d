using Ludiq;

namespace Bolt
{
	/// <summary>
	/// Called when the application pauses.
	/// </summary>
	[UnitCategory("Events/Application")]
	public sealed class OnApplicationPause : GlobalEventUnit<EmptyEventArgs>
	{
		public override string hookName => EventHooks.OnApplicationPause;
	}
}