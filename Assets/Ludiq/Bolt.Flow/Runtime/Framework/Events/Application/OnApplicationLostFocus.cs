using Ludiq;

namespace Bolt
{
	/// <summary>
	/// Called when the application loses focus.
	/// </summary>
	[UnitCategory("Events/Application")]
	public sealed class OnApplicationLostFocus : GlobalEventUnit<EmptyEventArgs>
	{
		public override string hookName => EventHooks.OnApplicationLostFocus;
	}
}