using Ludiq;

namespace Bolt
{
	/// <summary>
	/// Called when the application resumes.
	/// </summary>
	[UnitCategory("Events/Application")]
	public sealed class OnApplicationResume : GlobalEventUnit<EmptyEventArgs>
	{
		public override string hookName => EventHooks.OnApplicationResume;
	}
}