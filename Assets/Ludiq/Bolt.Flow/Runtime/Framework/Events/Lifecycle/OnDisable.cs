using Ludiq;

namespace Bolt
{
	/// <summary>
	/// Called when the machine becomes disabled or inactive.
	/// </summary>
	[UnitCategory("Events/Lifecycle")]
	[UnitOrder(6)]
	public sealed class OnDisable : MachineEventUnit<EmptyEventArgs>
	{
		public override string hookName => EventHooks.OnDisable;
	}
}