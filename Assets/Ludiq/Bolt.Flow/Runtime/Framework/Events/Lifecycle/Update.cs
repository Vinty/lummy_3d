﻿using Ludiq;

namespace Bolt
{
	/// <summary>
	/// Called every frame.
	/// </summary>
	[UnitCategory("Events/Lifecycle")]
	[UnitOrder(3)]
	public sealed class Update : MachineEventUnit<EmptyEventArgs>
	{
		public override string hookName => EventHooks.Update;
	}
}