﻿using Ludiq;

namespace Bolt
{
	/// <summary>
	/// Called every fixed framerate frame.
	/// </summary>
	[UnitCategory("Events/Lifecycle")]
	[UnitOrder(4)]
	public sealed class FixedUpdate : MachineEventUnit<EmptyEventArgs>
	{
		public override string hookName => EventHooks.FixedUpdate;
	}
}