using Ludiq;
using System;

namespace Bolt
{
	/// <summary>
	/// Called when the mouse is not any longer over the GUI element or collider.
	/// </summary>
	[UnitCategory("Events/Input")]
	public sealed class OnMouseExit : GameObjectEventUnit<EmptyEventArgs>
	{
		public override string hookName => EventHooks.OnMouseExit;
		public override Type eventProxyType => typeof(MouseEventProxy);
	}
}