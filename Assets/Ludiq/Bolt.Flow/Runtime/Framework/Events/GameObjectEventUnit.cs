﻿using Ludiq;
using System;
using UnityEngine;

namespace Bolt
{
	public abstract class GameObjectEventUnit<TArgs> : EventUnit<TArgs>
	{
		protected sealed override bool register => true;

		public new class Data : EventUnit<TArgs>.Data
		{
			public GameObject target;
		}

		public override IGraphElementData CreateData()
		{
			return new Data();
		}

		/// <summary>
		/// The game object that listens for the event.
		/// </summary>
		[DoNotSerialize]
		[NullMeansSelf]
		[UnitPortLabel("Target")]
		[UnitPortLabelHidden]
		public ValueInput target { get; private set; }

		public abstract Type eventProxyType { get; } 

		protected override void Definition()
		{
			base.Definition();

			target = ValueInput<GameObject>(nameof(target), null).NullMeansSelf();
		}

		public override EventHook GetHook(GraphReference reference)
		{
			if (!reference.hasData)
			{
				return hookName;
			}

			var data = reference.GetElementData<Data>(this);

			return new EventHook(hookName, data.target);
		}

		public virtual string hookName => throw new InvalidImplementationException($"Missing event hook for '{this}'.");

		private void UpdateTarget(GraphStack stack)
		{
			var data = stack.GetElementData<Data>(this);

			var wasListening = data.isListening;

			var newTarget = Flow.FetchValue<GameObject>(target, stack.ToReference());

			if (newTarget != data.target)
			{
				if (wasListening)
				{
					StopListening(stack);
				}

				data.target = newTarget;

				if (wasListening)
				{
					StartListening(stack, false);
				}
			}
		}

		protected void StartListening(GraphStack stack, bool updateTarget)
		{
			if (updateTarget)
			{
				UpdateTarget(stack);
			}
			
			var data = stack.GetElementData<Data>(this);

			var target = data.target;
			if (target == null)
			{
				return;
			}

			if (UnityThread.allowsAPI)
			{
				if (eventProxyType != null)
				{
					if (target.GetComponent(eventProxyType) == null)
					{
						target.AddComponent(eventProxyType);
					}
				}
			}

			base.StartListening(stack);
		}

		public override void StartListening(GraphStack stack)
		{
			StartListening(stack, true);
		}
	}
}