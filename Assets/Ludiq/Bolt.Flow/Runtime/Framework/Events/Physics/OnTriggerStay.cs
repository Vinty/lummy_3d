namespace Bolt
{
	/// <summary>
	/// Called once per frame for every collider that is touching the trigger.
	/// </summary>
	public sealed class OnTriggerStay : TriggerEventUnit
	{
		public override string hookName => EventHooks.OnTriggerStay;
	}
}