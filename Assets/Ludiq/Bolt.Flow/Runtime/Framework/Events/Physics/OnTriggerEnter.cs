namespace Bolt
{
	/// <summary>
	/// Called when a collider enters the trigger.
	/// </summary>
	public sealed class OnTriggerEnter : TriggerEventUnit
	{
		public override string hookName => EventHooks.OnTriggerEnter;
	}
}