﻿using System;
using System.Collections;
using Ludiq;

namespace Bolt
{
	/// <summary>
	/// Removes the item at the specified index of a list.
	/// </summary>
	[UnitCategory("Collections/Lists")]
	[UnitSurtitle("List")]
	[UnitShortTitle("Remove Item At Index")]
	[UnitOrder(5)]
	[TypeIcon(typeof(RemoveListItem))]
	public sealed class RemoveListItemAt : Unit
	{
		/// <summary>
		/// The entry point for the node.
		/// </summary>
		[DoNotSerialize]
		[UnitPortLabelHidden]
		public ControlInput enter { get; private set; }

		/// <summary>
		/// The list.
		/// </summary>
		[DoNotSerialize]
		[UnitPortLabelHidden]
		[UnitPrimaryPort(Axes2.Vertical)]
		public ValueInput listInput { get; private set; }

		/// <summary>
		/// The list without the removed item.
		/// Note that the input list is modified directly and then returned,
		/// except if it is an array, in which case a new array without the item
		/// is returned instead.
		/// </summary>
		[DoNotSerialize]
		[UnitPortLabelHidden]
		[UnitPrimaryPort(Axes2.Vertical)]
		public ValueOutput listOutput { get; private set; }

		/// <summary>
		/// The zero-based index.
		/// </summary>
		[DoNotSerialize]
		public ValueInput index { get; private set; }

		/// <summary>
		/// The action to execute once the item has been removed.
		/// </summary>
		[DoNotSerialize]
		[UnitPortLabelHidden]
		public ControlOutput exit { get; private set; }

		protected override void Definition()
		{
			enter = ControlInput(nameof(enter), RemoveAt);
			listInput = ValueInput<IList>(nameof(listInput));
			listOutput = ValueOutput<IList>(nameof(listOutput));
			index = ValueInput(nameof(index), 0);
			exit = ControlOutput(nameof(exit));

			Requirement(listInput, enter);
			Requirement(index, enter);
			Assignment(enter, listOutput);
			Succession(enter, exit);
		}

		public ControlOutput RemoveAt(Flow flow)
		{
			var list = flow.GetValue<IList>(listInput);
			var index = flow.GetValue<int>(this.index);

			flow.SetValue(listOutput, ListUtility.RemoveAt(list, index));

			return exit;
		}
	}
}