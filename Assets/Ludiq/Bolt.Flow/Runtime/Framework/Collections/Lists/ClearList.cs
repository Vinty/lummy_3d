﻿using System;
using System.Collections;
using Ludiq;

namespace Bolt
{
	/// <summary>
	/// Clears all items from a list.
	/// </summary>
	[UnitCategory("Collections/Lists")]
	[UnitSurtitle("List")]
	[UnitShortTitle("Clear")]
	[UnitOrder(6)]
	[TypeIcon(typeof(RemoveListItem))]
	public sealed class ClearList : Unit
	{
		/// <summary>
		/// The entry point for the node.
		/// </summary>
		[DoNotSerialize]
		[UnitPortLabelHidden]
		public ControlInput enter { get; private set; }

		/// <summary>
		/// The list.
		/// </summary>
		[DoNotSerialize]
		[UnitPortLabel("List")]
		[UnitPortLabelHidden]
		[UnitPrimaryPort(Axes2.Vertical)]
		public ValueInput listInput { get; private set; }

		/// <summary>
		/// The cleared list.
		/// Note that the input list is modified directly and then returned,
		/// except if it is an array, in which case a new empty array
		/// is returned instead.
		/// </summary>
		[DoNotSerialize]
		[UnitPortLabel("List")]
		[UnitPortLabelHidden]
		[UnitPrimaryPort(Axes2.Vertical)]
		public ValueOutput listOutput { get; private set; }

		/// <summary>
		/// The action to execute once the list has been cleared.
		/// </summary>
		[DoNotSerialize]
		[UnitPortLabelHidden]
		public ControlOutput exit { get; private set; }

		protected override void Definition()
		{
			enter = ControlInput(nameof(enter), Clear);
			listInput = ValueInput<IList>(nameof(listInput));
			listOutput = ValueOutput<IList>(nameof(listOutput));
			exit = ControlOutput(nameof(exit));

			Requirement(listInput, enter);
			Assignment(enter, listOutput);
			Succession(enter, exit);
		}

		public ControlOutput Clear(Flow flow)
		{
			var list = flow.GetValue<IList>(listInput);

			flow.SetValue(listOutput, ListUtility.Clear(list));

			return exit;
		}
	}
}