﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ludiq;

namespace Bolt
{
	[UnitTitleHidden]
	[SpecialUnit]
	public class BinaryOperatorUnit : Unit
	{
		[Obsolete(Serialization.ConstructorWarning)]
		public BinaryOperatorUnit() { }

		public BinaryOperatorUnit(BinaryOperator @operator, Type leftType, Type rightType)
		{
			Ensure.That(nameof(leftType)).IsNotNull(leftType);
			Ensure.That(nameof(rightType)).IsNotNull(rightType);

			this.@operator = @operator;
			this.leftType = leftType;
			this.rightType = rightType;
		}

		[Serialize]
		public BinaryOperator @operator { get; private set; }

		[Serialize]
		public Type leftType { get; private set; }

		[Serialize]
		public Type rightType { get; private set; }

		/// <summary>
		/// The first input.
		/// </summary> 
		[DoNotSerialize]
		[UnitPrimaryPort]
		[UnitPortLabelHidden]
		public ValueInput a { get; private set; }

		/// <summary>
		/// The second input.
		/// </summary>
		[DoNotSerialize]
		[UnitPrimaryPort]
		[UnitPortLabelHidden]
		public ValueInput b { get; private set; }

		/// <summary>
		/// The result of the operator applied between A and B.
		/// </summary>
		[DoNotSerialize]
		[UnitPrimaryPort]
		[UnitPortLabelHidden]
		public ValueOutput result { get; private set; }

		[DoNotSerialize]
		public BinaryOperatorHandler handler => @operator.GetHandler();

		[DoNotSerialize]
		public Type resultType => canDefine ? handler.GetResultType(leftType, rightType) : null;

		public override bool canDefine => handler.HasOperatorDefined(leftType, rightType);

		protected override void Definition()
		{
			a = ValueInput(leftType, nameof(a));
			b = ValueInput(rightType, nameof(b));
			result = null;

			a.SetDefaultValue(leftType.PseudoDefault());
			b.SetDefaultValue(rightType.PseudoDefault());

			if (leftType.IsNullable())
			{
				a.AllowsNull();
			}
			if (rightType.IsNullable())
			{
				b.AllowsNull();
			}

			if (result == null)
			{
				result = ValueOutput(resultType, nameof(result), Result);
			}

			Requirement(a, result);
			Requirement(b, result);
		}

		public object Result(Flow flow)
		{
			return handler.Operate(flow.GetValue(a), flow.GetValue(b));
		}
	}
}
