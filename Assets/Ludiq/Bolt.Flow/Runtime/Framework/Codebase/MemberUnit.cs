﻿using System;
using System.Collections.Generic;
using Ludiq;
using UnityEngine;
using UnityObject = UnityEngine.Object;

namespace Bolt
{
	[SpecialUnit]
	public abstract class MemberUnit : Unit, IAotStubbable
	{
		protected MemberUnit() : base() { }

		protected MemberUnit(Member member) : this()
		{
			this.member = member;
		}

		[Serialize]
		[MemberFilter(Fields = true, Properties = true, Methods = true, Constructors = true)]
		public Member member { get; set; }

		/// <summary>
		/// The target object.
		/// </summary>
		[DoNotSerialize]
		[UnitPortLabelHidden]
		[NullMeansSelf]
		public ValueInput target { get; private set; }

		[DoNotSerialize]
		[InspectableIf(nameof(showTypeInspector))]
		[InspectorTypeHideRoot]
		public Type targetType
		{
			get => member.targetType;
			set => member.targetType = value;
		}

		protected bool showTypeInspector => canDefine && targetType.IsGenericType;
		
		public override bool canDefine => member != null;

		protected override void Definition()
		{
			member.EnsureReflected();

			if (!IsMemberValid(member))
			{
				throw new NotSupportedException("The member type is not valid for this unit.");
			}

			if (member.requiresTarget)
			{
				target = ValueInput(member.targetType, nameof(target));

				if (Bolt.ValueInput.SupportsDefaultValue(target.type))
				{
					target.SetDefaultValue(member.targetType.PseudoDefault());
				}

				if (typeof(UnityObject).IsAssignableFrom(member.targetType))
				{
					target.NullMeansSelf();
				}
			}
		}

		protected abstract bool IsMemberValid(Member member);

		protected virtual void UpdateTarget(Flow flow)
		{
			if (member.requiresTarget)
			{
				var targetValue = flow.GetValue(target, member.targetType);

				if (member.targetType.IsStruct())
				{
					targetValue = targetValue.CloneViaMarshalling();
				}

				member.target = targetValue;
			}
		}

		public override void Prewarm()
		{
			if (member != null && member.isReflected)
			{
				member.Prewarm();
			}
		}

		IEnumerable<object> IAotStubbable.aotStubs
		{
			get
			{
				if (member != null && member.isReflected)
				{
					yield return member.info;
				}
			}
		}
	}
}