using System;
using System.Collections.Generic;
using System.Linq;
using Ludiq;
using UnityEngine;

namespace Bolt
{
	/// <summary>
	/// Invokes a method or a constructor via reflection.
	/// </summary>
	public sealed class InvokeMember : MemberUnit
	{
		public InvokeMember() : base() { }

		public InvokeMember(Member member) : base(member) { }

		private bool useExpandedParameters;

		/// <summary>
		/// Whether the target should be output to allow for chaining.
		/// </summary>
		[Serialize]
		[InspectableIf(nameof(supportsChaining))]
		public bool chainable { get; set; }

		[DoNotSerialize]
		public bool supportsChaining => canDefine && !member.isOperator && member.requiresTarget;

		[DoNotSerialize]
		[MemberFilter(Methods = true, Constructors = true)]
		public Member invocation
		{
			get { return member; }
			set { member = value; }
		}

		[DoNotSerialize]
		[UnitPortLabelHidden]
		public ControlInput enter { get; private set; }

		[DoNotSerialize]
		public Dictionary<int, ValueInput> inputParameters { get; private set; }

		/// <summary>
		/// The target object used when setting the value.
		/// </summary>
		[DoNotSerialize]
		[UnitPortLabel("Target")]
		[UnitPortLabelHidden]
		public ValueOutput targetOutput { get; private set; }

		[DoNotSerialize]
		[UnitPortLabelHidden]
		public ValueOutput result { get; private set; }

		[DoNotSerialize]
		public Dictionary<int, ValueOutput> outputParameters { get; private set; }

		[DoNotSerialize]
		[UnitPortLabelHidden]
		public ControlOutput exit { get; private set; }

		[DoNotSerialize]
		private int parameterCount;

		protected override void Definition()
		{
			base.Definition();

			inputParameters = new Dictionary<int, ValueInput>();
			outputParameters = new Dictionary<int, ValueOutput>();
			useExpandedParameters = true;
			
			if (!member.isOperator || member.operatorCategory == OperatorCategory.Incrementation)
			{
				enter = ControlInput(nameof(enter), Enter);
				exit = ControlOutput(nameof(exit));
				Succession(enter, exit);
			}
			
			if (enter != null && member.requiresTarget)
			{
				Requirement(target, enter);
			}

			if (supportsChaining && chainable)
			{
				targetOutput = ValueOutput(member.targetType, nameof(targetOutput));

				if (enter != null)
				{
					Assignment(enter, targetOutput);
				}
			}

			if (member.isGettable)
			{
				result = ValueOutput(member.type, nameof(result), Result);

				if (member.requiresTarget)
				{
					Requirement(target, result);
				}
			}
			
			var parameterInfos = member.methodBase.GetParametersWithoutThis().ToArray();

			parameterCount = parameterInfos.Length;

			for (int parameterIndex = 0; parameterIndex < parameterCount; parameterIndex++)
			{
				var parameterInfo = parameterInfos[parameterIndex];

				var parameterType = parameterInfo.UnderlyingParameterType();

				if (!parameterInfo.IsOut)
				{
					var inputParameterKey = "%" + parameterInfo.Name;

					var inputParameter = ValueInput(parameterType, inputParameterKey);

					inputParameters.Add(parameterIndex, inputParameter);

					inputParameter.SetDefaultValue(parameterInfo.PseudoDefaultValue());

					if (parameterInfo.AllowsNull())
					{
						inputParameter.AllowsNull();
					}

					if (enter != null)
					{
						Requirement(inputParameter, enter);
					}

					if (member.isGettable)
					{
						Requirement(inputParameter, result);
					}
				}

				if (parameterInfo.ParameterType.IsByRef || parameterInfo.IsOut)
				{
					var outputParameterKey = "&" + parameterInfo.Name;

					var outputParameter = ValueOutput(parameterType, outputParameterKey);

					outputParameters.Add(parameterIndex, outputParameter);

					if (enter != null)
					{
						Assignment(enter, outputParameter);
					}

					useExpandedParameters = false;
				}
			}

			if (inputParameters.Count > 5)
			{
				useExpandedParameters = false;
			}
		}

		protected override bool IsMemberValid(Member member)
		{
			return member.isInvocable;
		}

		private object Invoke(Flow flow)
		{
			if (useExpandedParameters)
			{
				switch (inputParameters.Count)
				{
					case 0:

						return member.Invoke();

					case 1:

						return member.Invoke(flow.GetConvertedValue(inputParameters[0]));

					case 2:

						return member.Invoke(flow.GetConvertedValue(inputParameters[0]),
						                     flow.GetConvertedValue(inputParameters[1]));

					case 3:

						return member.Invoke(flow.GetConvertedValue(inputParameters[0]),
						                     flow.GetConvertedValue(inputParameters[1]),
						                     flow.GetConvertedValue(inputParameters[2]));

					case 4:

						return member.Invoke(flow.GetConvertedValue(inputParameters[0]),
						                     flow.GetConvertedValue(inputParameters[1]),
						                     flow.GetConvertedValue(inputParameters[2]),
						                     flow.GetConvertedValue(inputParameters[3]));

					case 5:

						return member.Invoke(flow.GetConvertedValue(inputParameters[0]),
						                     flow.GetConvertedValue(inputParameters[1]),
						                     flow.GetConvertedValue(inputParameters[2]),
						                     flow.GetConvertedValue(inputParameters[3]),
						                     flow.GetConvertedValue(inputParameters[4]));

					default:

						throw new NotSupportedException();
				}
			}
			else
			{
				var arguments = new object[parameterCount];

				for (int parameterIndex = 0; parameterIndex < parameterCount; parameterIndex++)
				{
					if (inputParameters.TryGetValue(parameterIndex, out var inputParameter))
					{
						arguments[parameterIndex] = flow.GetConvertedValue(inputParameter);
					}
				}

				var result = member.Invoke(arguments);

				for (int parameterIndex = 0; parameterIndex < parameterCount; parameterIndex++)
				{
					if (outputParameters.TryGetValue(parameterIndex, out var outputParameter))
					{
						flow.SetValue(outputParameter, arguments[parameterIndex]);
					}
				}

				return result;
			}
		}

		protected override void UpdateTarget(Flow flow)
		{
			base.UpdateTarget(flow);

			if (supportsChaining && chainable)
			{
				flow.SetValue(targetOutput, member.target);
			}
		}

		private object Result(Flow flow)
		{
			UpdateTarget(flow);

			return Invoke(flow);
		}

		private ControlOutput Enter(Flow flow)
		{
			UpdateTarget(flow);

			var result = Invoke(flow);

			if (this.result != null)
			{
				flow.SetValue(this.result, result);
			}

			return exit;
		}
	}
}
