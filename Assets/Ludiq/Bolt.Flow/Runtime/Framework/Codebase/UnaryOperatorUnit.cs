﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ludiq;

namespace Bolt
{
	[SpecialUnit]
	public class UnaryOperatorUnit : Unit
	{
		[Obsolete(Serialization.ConstructorWarning)]
		public UnaryOperatorUnit() { }

		public UnaryOperatorUnit(UnaryOperator @operator, Type type)
		{
			Ensure.That(nameof(type)).IsNotNull(type);

			this.@operator = @operator;
			this.type = type;
		}

		[Serialize]
		public UnaryOperator @operator { get; private set; }

		[Serialize]
		public Type type { get; private set; }

		/// <summary>
		/// The input.
		/// </summary>
		[DoNotSerialize]
		[UnitPortLabel("A")]
		[UnitPrimaryPort]
		[UnitPortLabelHidden]
		public ValueInput input { get; private set; }

		/// <summary>
		/// The result of applying the operator to the input.
		/// </summary>
		[DoNotSerialize]
		[UnitPortLabel("A'")]
		[UnitPrimaryPort]
		[UnitPortLabelHidden]
		public ValueOutput output { get; private set; }

		[DoNotSerialize]
		public UnaryOperatorHandler handler => @operator.GetHandler();

		public override bool canDefine => handler.HasOperatorDefined(type);

		protected override void Definition()
		{
			input = ValueInput(type, nameof(input));

			input.SetDefaultValue(type.PseudoDefault());

			if (type.IsNullable())
			{
				input.AllowsNull();
			}

			output = ValueOutput(type, nameof(output), Result);

			Requirement(input, output);
		}

		public object Result(Flow flow)
		{
			return handler.Operate(flow.GetValue(input));
		}
	}
}
