﻿using Ludiq;
using System;

namespace Bolt
{
	[UnitTitleHidden]
	[UnitOrder(103)]
	[Obsolete("Use strongly-typed Multiply units instead.")]
	public abstract class Multiply<T> : Unit
	{
		/// <summary>
		/// The first value.
		/// </summary>
		[DoNotSerialize]
		[UnitPrimaryPort]
		public ValueInput a { get; private set; }

		/// <summary>
		/// The second value.
		/// </summary>
		[DoNotSerialize]
		[UnitPrimaryPort]
		public ValueInput b { get; private set; }

		/// <summary>
		/// The product of A and B.
		/// </summary>
		[DoNotSerialize]
		[UnitPortLabel("A \u00D7 B")]
		[UnitPrimaryPort]
		public ValueOutput product { get; private set; }

		[DoNotSerialize]
		protected virtual T defaultB => default(T);

		protected override void Definition()
		{
			a = ValueInput<T>(nameof(a));
			b = ValueInput(nameof(b), defaultB);
			product = ValueOutput(nameof(product), Operation).Predictable();

			Requirement(a, product);
			Requirement(b, product);
		}

		private T Operation(Flow flow)
		{
			return Operation(flow.GetValue<T>(a), flow.GetValue<T>(b));
		}

		public abstract T Operation(T a, T b);
	}
}