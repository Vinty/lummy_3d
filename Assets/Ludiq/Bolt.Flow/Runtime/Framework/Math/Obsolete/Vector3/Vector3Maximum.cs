#pragma warning disable 618

using Ludiq;
using System.Collections.Generic;

namespace Bolt
{
	/// <summary>
	/// Returns the component-wise maximum between two or more 3D vectors.
	/// </summary>
	[UnitCategory("Math/Vector 3")]
	[UnitTitle("Maximum")]
	public sealed class Vector3Maximum : Maximum<UnityEngine.Vector3>
	{
		public override UnityEngine.Vector3 Operation(UnityEngine.Vector3 a, UnityEngine.Vector3 b)
		{
			return UnityEngine.Vector3.Max(a, b);
		}

		public override UnityEngine.Vector3 Operation(IEnumerable<UnityEngine.Vector3> values)
		{
			return MathUtility.Max(values);
		}
	}
}