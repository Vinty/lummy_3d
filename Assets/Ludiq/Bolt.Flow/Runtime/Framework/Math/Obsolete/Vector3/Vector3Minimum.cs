#pragma warning disable 618

using Ludiq;
using System.Collections.Generic;

namespace Bolt
{
	/// <summary>
	/// Returns the component-wise minimum between two or more 3D vectors.
	/// </summary>
	[UnitCategory("Math/Vector 3")]
	[UnitTitle("Minimum")]
	public sealed class Vector3Minimum : Minimum<UnityEngine.Vector3>
	{
		public override UnityEngine.Vector3 Operation(UnityEngine.Vector3 a, UnityEngine.Vector3 b)
		{
			return UnityEngine.Vector3.Min(a, b);
		}

		public override UnityEngine.Vector3 Operation(IEnumerable<UnityEngine.Vector3> values)
		{
			return MathUtility.Min(values);
		}
	}
}