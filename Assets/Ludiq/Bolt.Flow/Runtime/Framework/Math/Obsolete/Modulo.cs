﻿using Ludiq;
using System;

namespace Bolt
{
	[UnitTitleHidden]
	[UnitOrder(105)]
	[Obsolete("Use strongly-typed Modulo units instead.")]
	public abstract class Modulo<T> : Unit
	{
		/// <summary>
		/// The dividend (or numerator).
		/// </summary>
		[DoNotSerialize]
		[UnitPortLabel("A")]
		[UnitPrimaryPort]
		public ValueInput dividend { get; private set; }

		/// <summary>
		/// The divisor (or denominator).
		/// </summary>
		[DoNotSerialize]
		[UnitPortLabel("B")]
		[UnitPrimaryPort]
		public ValueInput divisor { get; private set; }

		/// <summary>
		/// The remainder of the division of dividend and divison (numerator / denominator).
		/// </summary>
		[DoNotSerialize]
		[UnitPortLabel("A % B")]
		[UnitPrimaryPort]
		public ValueOutput remainder { get; private set; }

		[DoNotSerialize]
		protected virtual T defaultDivisor => default(T);

		[DoNotSerialize]
		protected virtual T defaultDividend => default(T);

		protected override void Definition()
		{
			dividend = ValueInput(nameof(dividend), defaultDividend);
			divisor = ValueInput(nameof(divisor), defaultDivisor);
			remainder = ValueOutput(nameof(remainder), Operation).Predictable();

			Requirement(dividend, remainder);
			Requirement(divisor, remainder);
		}

		public abstract T Operation(T divident, T divisor);

		public T Operation(Flow flow)
		{
			return Operation(flow.GetValue<T>(dividend), flow.GetValue<T>(divisor));
		}
	}
}