using Ludiq;
using System;

namespace Bolt
{
	[UnitTitleHidden]
	[UnitOrder(404)]
	[Obsolete("Use Vector Dot instead")]
	public abstract class DotProduct<T> : Unit
	{
		/// <summary>
		/// The first vector.
		/// </summary>
		[DoNotSerialize]
		[UnitPrimaryPort]
		public ValueInput a { get; private set; }

		/// <summary>
		/// The second vector.
		/// </summary>
		[DoNotSerialize]
		[UnitPrimaryPort]
		public ValueInput b { get; private set; }

		/// <summary>
		/// The dot product of A and B.
		/// </summary>
		[DoNotSerialize]
		[UnitPortLabel("A\u2219B")]
		[UnitPrimaryPort]
		public ValueOutput dotProduct { get; private set; }

		protected override void Definition()
		{
			a = ValueInput<T>(nameof(a));
			b = ValueInput<T>(nameof(b));
			dotProduct = ValueOutput(nameof(dotProduct), Operation).Predictable();

			Requirement(a, dotProduct);
			Requirement(b, dotProduct);
		}

		private float Operation(Flow flow)
		{
			return Operation(flow.GetValue<T>(a), flow.GetValue<T>(b));
		}

		public abstract float Operation(T a, T b);
	}
}