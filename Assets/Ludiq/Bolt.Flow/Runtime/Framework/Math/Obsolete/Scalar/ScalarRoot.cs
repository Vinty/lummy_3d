﻿using Ludiq;
using System;
using UnityEngine;

namespace Bolt
{
	/// <summary>
	/// Returns the root at the nth degree of a radicand.
	/// </summary>
	[UnitCategory("Math/Scalar")]
	[UnitTitle("Root")]
	[UnitOrder(107)]
	[Obsolete("Use Mathf.Sqrt, or Mathf.Pow with a fractional power instead")]
	public sealed class ScalarRoot : Unit
	{
		/// <summary>
		/// The radicand.
		/// </summary>
		[DoNotSerialize]
		[UnitPortLabel("x")]
		public ValueInput radicand { get; private set; }

		/// <summary>
		/// The degree.
		/// </summary>
		[DoNotSerialize]
		[UnitPortLabel("n")]
		public ValueInput degree { get; private set; }

		/// <summary>
		/// The nth degree root of the radicand.
		/// </summary>
		[DoNotSerialize]
		[UnitPortLabel("\u207f\u221ax")]
		public ValueOutput root { get; private set; }

		protected override void Definition()
		{
			radicand = ValueInput<float>(nameof(radicand), 1);
			degree = ValueInput<float>(nameof(degree), 2);
			root = ValueOutput(nameof(root), Root);

			Requirement(radicand, root);
			Requirement(degree, root);
		}

		public float Root(Flow flow)
		{
			var degree = flow.GetValue<float>(this.degree);
			var radicand = flow.GetValue<float>(this.radicand);

			if (degree == 2)
			{
				return Mathf.Sqrt(radicand);
			}
			else
			{
				return Mathf.Pow(radicand, 1 / degree);
			}
		}
	}
}