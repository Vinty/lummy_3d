﻿using Ludiq;
using System;
using UnityEngine;

namespace Bolt
{
	/// <summary>
	/// Returns the unit length version of a scalar.
	/// </summary>
	[UnitCategory("Math/Scalar")]
	[UnitTitle("Normalize")]
	[Obsolete("Use Mathf.Sign instead")]
	public sealed class ScalarNormalize : Normalize<float>
	{
		public override float Operation(float input)
		{
			return MathUtility.Normalized(input);
		}
	}
}