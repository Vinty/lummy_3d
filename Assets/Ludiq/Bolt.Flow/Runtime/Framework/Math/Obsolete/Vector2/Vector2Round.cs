#pragma warning disable 618

using Ludiq;
using UnityEngine;

namespace Bolt
{
	/// <summary>
	/// Rounds each component of a 2D vector.
	/// </summary>
	[UnitCategory("Math/Vector 2")]
	[UnitTitle("Round")]
	public sealed class Vector2Round : Round<Vector2, Vector2>
	{
		protected override Vector2 Floor(Vector2 input)
		{
			return input.Floor();
		}

		protected override Vector2 AwayFromZero(Vector2 input)
		{
			return input.Round();
		}

		protected override Vector2 Ceiling(Vector2 input)
		{
			return input.Ceil();
		}
	}
}