#pragma warning disable 618

using Ludiq;
using System.Collections.Generic;

namespace Bolt
{
	/// <summary>
	/// Returns the component-wise minimum between two or more 2D vectors.
	/// </summary>
	[UnitCategory("Math/Vector 2")]
	[UnitTitle("Minimum")]
	public sealed class Vector2Minimum : Minimum<UnityEngine.Vector2>
	{
		public override UnityEngine.Vector2 Operation(UnityEngine.Vector2 a, UnityEngine.Vector2 b)
		{
			return UnityEngine.Vector2.Min(a, b);
		}

		public override UnityEngine.Vector2 Operation(IEnumerable<UnityEngine.Vector2> values)
		{
			return MathUtility.Min(values);
		}
	}
}