﻿#pragma warning disable 618

using Ludiq;
using UnityEngine;

namespace Bolt
{
	/// <summary>
	/// Returns a version of a 4D vector where each component is positive.
	/// </summary>
	[UnitCategory("Math/Vector 4")]
	[UnitTitle("Absolute")]
	public sealed class Vector4Absolute : Absolute<Vector4>
	{
		protected override Vector4 Operation(Vector4 input)
		{
			return input.Abs();
		}
	}
}