#pragma warning disable 618

using Ludiq;
using UnityEngine;

namespace Bolt
{
	/// <summary>
	/// Returns the component-wise product of two 4D vectors.
	/// </summary>
	[UnitCategory("Math/Vector 4")]
	[UnitTitle("Multiply")]
	public sealed class Vector4Multiply : Multiply<Vector4>
	{
		protected override Vector4 defaultB => Vector4.zero;

		public override Vector4 Operation(Vector4 a, Vector4 b)
		{
			return a.Multiply(b);
		}
	}
}