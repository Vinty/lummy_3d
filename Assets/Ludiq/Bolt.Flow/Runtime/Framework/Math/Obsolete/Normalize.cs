using Ludiq;
using System;

namespace Bolt
{
	[UnitTitleHidden]
	[UnitOrder(401)]
	[Obsolete("Use Mathf.Sign or Vector normalized property instead")]
	public abstract class Normalize<T> : Unit
	{
		/// <summary>
		/// The vector to normalize.
		/// </summary>
		[DoNotSerialize]
		[UnitPortLabelHidden]
		[UnitPrimaryPort]
		public ValueInput input { get; private set; }

		/// <summary>
		/// The normalized vector.
		/// </summary>
		[DoNotSerialize]
		[UnitPortLabelHidden]
		[UnitPrimaryPort]
		public ValueOutput output { get; private set; }

		protected override void Definition()
		{
			input = ValueInput<T>(nameof(input));
			output = ValueOutput(nameof(output), Operation).Predictable();

			Requirement(input, output);
		}

		private T Operation(Flow flow)
		{
			return Operation(flow.GetValue<T>(input));
		}

		public abstract T Operation(T input);
	}
}