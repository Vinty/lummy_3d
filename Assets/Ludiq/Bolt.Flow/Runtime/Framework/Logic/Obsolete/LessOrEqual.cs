﻿#pragma warning disable 618

using Ludiq;
using System;
using UnityEngine;

namespace Bolt
{
	/// <summary>
	/// Compares two inputs to determine whether the first is less than or equal to the second.
	/// </summary>
	[UnitCategory("Logic")]
	[UnitOrder(10)]
	[Obsolete("Use strongly-typed Less Than Or Equal units instead.")]
	public sealed class LessOrEqual : BinaryComparisonUnit
	{
		/// <summary>
		/// Whether A is greater than or equal to B.
		/// </summary>
		[UnitPortLabel("A \u2264 B")]
		public override ValueOutput comparison => base.comparison;

		protected override bool NumericComparison(float a, float b)
		{
			return a.LessThanOrApproximately(b);
		}

		protected override bool GenericComparison(object a, object b)
		{
			return OperatorUtility.LessThanOrEqual(a, b);
		}
	}
}