﻿#pragma warning disable 618

using Ludiq;
using System;
using UnityEngine;

namespace Bolt
{
	/// <summary>
	/// Compares two inputs to determine whether the first is greater than or equal to the second.
	/// </summary>
	[UnitCategory("Logic")]
	[UnitOrder(12)]
	[Obsolete("Use strongly-typed Greater Than Or Equal units instead.")]
	public sealed class GreaterOrEqual : BinaryComparisonUnit
	{
		/// <summary>
		/// Whether A is greater than or equal to B.
		/// </summary>
		[UnitPortLabel("A \u2265 B")]
		public override ValueOutput comparison => base.comparison;

		protected override bool NumericComparison(float a, float b)
		{
			return a.GreaterThanOrApproximately(b);
		}

		protected override bool GenericComparison(object a, object b)
		{
			return OperatorUtility.GreaterThanOrEqual(a, b);
		}
	}
}