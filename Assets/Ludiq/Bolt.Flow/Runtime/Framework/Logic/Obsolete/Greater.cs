﻿#pragma warning disable 618

using Ludiq;
using System;

namespace Bolt
{
	/// <summary>
	/// Compares two inputs to determine whether the first is greater than the second.
	/// </summary>
	[UnitCategory("Logic")]
	[UnitOrder(11)]
	[Obsolete("Use strongly-typed Greater Than units instead.")]
	public sealed class Greater : BinaryComparisonUnit
	{
		/// <summary>
		/// Whether A is greater than B.
		/// </summary>
		[UnitPortLabel("A > B")]
		public override ValueOutput comparison => base.comparison;

		protected override bool NumericComparison(float a, float b)
		{
			return a > b;
		}

		protected override bool GenericComparison(object a, object b)
		{
			return OperatorUtility.GreaterThan(a, b);
		}
	}
}