﻿using Ludiq;
using System;
using UnityEngine;

namespace Bolt
{
	/// <summary>
	/// Compares two inputs.
	/// </summary>
	[UnitCategory("Logic")]
	[UnitTitle("Comparison")]
	[UnitShortTitle("Comparison")]
	[UnitOrder(99)]
	[SpecialUnit]
	public sealed class TypedComparison : Unit
	{
		[Obsolete(Serialization.ConstructorWarning)]
		public TypedComparison() : base()
		{
			type = typeof(object);
		}
		
		public TypedComparison(Type type) : base()
		{
			Ensure.That(nameof(type)).IsNotNull(type);

			this.type = type;
		}

		/// <summary>
		/// The first input.
		/// </summary>
		[DoNotSerialize]
		public ValueInput a { get; private set; }

		/// <summary>
		/// The second input.
		/// </summary>
		[DoNotSerialize]
		public ValueInput b { get; private set; }

		/// <summary>
		/// Whether A is less than B.
		/// </summary>
		[DoNotSerialize]
		[UnitPortLabel("A < B")]
		public ValueOutput aLessThanB { get; private set; }

		/// <summary>
		/// Whether A is less than or equal to B.
		/// </summary>
		[DoNotSerialize]
		[UnitPortLabel("A \u2264 B")]
		public ValueOutput aLessThanOrEqualToB { get; private set; }

		/// <summary>
		/// Whether A is equal to B.
		/// </summary>
		[DoNotSerialize]
		[UnitPortLabel("A = B")]
		public ValueOutput aEqualToB { get; private set; }

		/// <summary>
		/// Whether A is not equal to B.
		/// </summary>
		[DoNotSerialize]
		[UnitPortLabel("A \u2260 B")]
		public ValueOutput aNotEqualToB { get; private set; }

		/// <summary>
		/// Whether A is greater than or equal to B.
		/// </summary>
		[DoNotSerialize]
		[UnitPortLabel("A \u2265 B")]
		public ValueOutput aGreaterThanOrEqualToB { get; private set; }

		/// <summary>
		/// Whether A is greater than B.
		/// </summary>
		[DoNotSerialize]
		[UnitPortLabel("A > B")]
		public ValueOutput aGreatherThanB { get; private set; }

		[Serialize]
		public Type type { get; set; }

		public bool hasEqual => type.IsEqualityComparable() || type.GetOperatorNames().Contains("op_Equality");
		public bool hasNotEqual => type.IsEqualityComparable() || type.GetOperatorNames().Contains("op_Inequality");
		public bool hasLessThan => type.IsOrderComparable() || type.GetOperatorNames().Contains("op_LessThan");
		public bool hasLessThanOrEqual => type.IsOrderComparable() || type.GetOperatorNames().Contains("op_LessThanOrEqual");
		public bool hasGreaterThan => type.IsOrderComparable() || type.GetOperatorNames().Contains("op_GreaterThan");
		public bool hasGreaterThanOrEqual => type.IsOrderComparable() || type.GetOperatorNames().Contains("op_GreaterThanOrEqual");

		protected override void Definition()
		{
			a = ValueInput(type, nameof(a));
			b = ValueInput(type, nameof(b));

			a.SetDefaultValue(type.PseudoDefault());
			b.SetDefaultValue(type.PseudoDefault());

			if (type.IsNullable())
			{
				a.AllowsNull();
				b.AllowsNull();
			}

			aLessThanB = hasLessThan ? ValueOutput(nameof(aLessThanB), (flow) => GenericLess(flow.GetValue(a, type), flow.GetValue(b, type))) : null;
			aLessThanOrEqualToB = hasLessThanOrEqual ? ValueOutput(nameof(aLessThanOrEqualToB), (flow) => GenericLessOrEqual(flow.GetValue(a, type), flow.GetValue(b, type))) : null;
			aEqualToB = hasEqual ? ValueOutput(nameof(aEqualToB), (flow) => GenericEqual(flow.GetValue(a, type), flow.GetValue(b, type))) : null;
			aNotEqualToB = hasNotEqual ? ValueOutput(nameof(aNotEqualToB), (flow) => GenericNotEqual(flow.GetValue(a, type), flow.GetValue(b, type))) : null;
			aGreaterThanOrEqualToB = hasGreaterThanOrEqual ? ValueOutput(nameof(aGreaterThanOrEqualToB), (flow) => GenericGreaterOrEqual(flow.GetValue(a, type), flow.GetValue(b, type))) : null;
			aGreatherThanB = hasGreaterThan ? ValueOutput(nameof(aGreatherThanB), (flow) => GenericGreater(flow.GetValue(a, type), flow.GetValue(b, type))) : null;

			if (aLessThanB != null)
			{
				Requirement(a, aLessThanB);
				Requirement(b, aLessThanB);
			}

			if (aLessThanOrEqualToB != null)
			{
				Requirement(a, aLessThanOrEqualToB);
				Requirement(b, aLessThanOrEqualToB);
			}

			if (aEqualToB != null)
			{
				Requirement(a, aEqualToB);
				Requirement(b, aEqualToB);
			}

			if (aNotEqualToB != null)
			{
				Requirement(a, aNotEqualToB);
				Requirement(b, aNotEqualToB);
			}

			if (aGreaterThanOrEqualToB != null)
			{
				Requirement(a, aGreaterThanOrEqualToB);
				Requirement(b, aGreaterThanOrEqualToB);
			}

			if (aGreatherThanB != null)
			{
				Requirement(a, aGreatherThanB);
				Requirement(b, aGreatherThanB);
			}
		}

		private bool GenericLess(object a, object b)
		{
			return OperatorUtility.LessThan(a, b);
		}

		private bool GenericLessOrEqual(object a, object b)
		{
			return OperatorUtility.LessThanOrEqual(a, b);
		}

		private bool GenericEqual(object a, object b)
		{
			return OperatorUtility.Equal(a, b);
		}

		private bool GenericNotEqual(object a, object b)
		{
			return OperatorUtility.NotEqual(a, b);
		}

		private bool GenericGreaterOrEqual(object a, object b)
		{
			return OperatorUtility.GreaterThanOrEqual(a, b);
		}

		private bool GenericGreater(object a, object b)
		{
			return OperatorUtility.GreaterThan(a, b);
		}
	}
}