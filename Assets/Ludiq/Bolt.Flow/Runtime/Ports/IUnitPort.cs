﻿using System.Collections.Generic;
using Ludiq;

namespace Bolt
{
	public interface IUnitPort : IGraphItem
	{
		IUnit unit { get; set; }
		string key { get; }

		IEnumerable<IUnitRelation> relations { get; }
		
		IEnumerable<IUnitConnection> connections { get; }
		IEnumerable<IUnitConnection> validConnections { get; }
		IEnumerable<InvalidConnection> invalidConnections { get; }
		IEnumerable<IUnitPort> connectedPorts { get; }
		IEnumerable<IUnitPort> validConnectedPorts { get; }
		IEnumerable<IUnitPort> invalidConnectedPorts { get; }
		bool hasAnyConnection { get; }
		bool hasValidConnection { get; }
		bool hasInvalidConnection { get; }
		bool CanInvalidlyConnectTo(IUnitPort port);
		bool CanValidlyConnectTo(IUnitPort port);
		InvalidConnection InvalidlyConnectTo(IUnitPort port);
		IUnitConnection ValidlyConnectTo(IUnitPort port);
		void Disconnect();
		IUnitPort CompatiblePort(IUnit unit);
	}

	public static class XUnitPort
	{
		public static IDictionary<IUnitConnection, IUnitPort> ConnectedPortsByConnection(this IUnitPort port)
		{
			var dictionary = new Dictionary<IUnitConnection, IUnitPort>();

			foreach (var connection in port.connections)
			{
				if (port == connection.source)
				{
					dictionary.Add(connection, connection.destination);
				}
				else if (port == connection.destination)
				{
					dictionary.Add(connection, connection.source);
				}
				else
				{
					throw new InvalidImplementationException();
				}
			}

			return dictionary;
		}
	}
}