﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Bolt
{
	public abstract class FlowMachineScript : MachineScript<FlowGraph, FlowGraphData, FlowMachineScript, FlowGraphScript>
	{
		public FlowMachineScript() {}
		 
		protected override void OnEnable()
		{
			graphScript.StartListening();
			base.OnEnable();
		}

		protected override void OnDisable()
		{
			base.OnDisable();
			graphScript.StopListening();
		}
	}
}
