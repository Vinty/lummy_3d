﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Ludiq;
using UnityEngine;

namespace Bolt
{
	public interface IMultiInputUnit : IUnit
	{
		Type type { get; }

		int minInputCount { get; }

		int maxInputCount { get; }

		int inputCount { get; set; }

		ReadOnlyCollection<ValueInput> multiInputs { get; }
	}

	public abstract class MultiInputUnit<T> : Unit, IMultiInputUnit
	{		
		[SerializeAs(nameof(inputCount))]
		private int _inputCount = 2;

		[DoNotSerialize]
		public virtual int minInputCount => 2;

		[DoNotSerialize]
		public virtual int maxInputCount => 10;

		[DoNotSerialize]
		public Type type => typeof(T);

		[DoNotSerialize]
		[Inspectable, UnitHeaderInspectable("Inputs")]
		public virtual int inputCount
		{
			get
			{
				return _inputCount;
			}
			set
			{
				_inputCount = Mathf.Clamp(value, minInputCount, maxInputCount);
			}
		}

		[DoNotSerialize]
		public ReadOnlyCollection<ValueInput> multiInputs { get; private set; }

		protected override void Definition()
		{
			var _multiInputs = new List<ValueInput>();

			multiInputs = _multiInputs.AsReadOnly();

			for (var i = 0; i < inputCount; i++)
			{
				_multiInputs.Add(ValueInput<T>(i.ToString()));
			}
		}
		
		protected void InputsAllowNull()
		{
			foreach (var input in multiInputs)
			{
				input.AllowsNull();
			}
		}
	}
}