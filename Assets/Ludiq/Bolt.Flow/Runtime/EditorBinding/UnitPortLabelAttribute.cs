﻿using System;

namespace Bolt
{
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false, Inherited = false)]
	public class UnitPortLabelAttribute : Attribute
	{
		public UnitPortLabelAttribute(string label)
		{
			this.label = label;
		}

		public string label { get; private set; }
		public bool hidden { get; set; }
	}
}