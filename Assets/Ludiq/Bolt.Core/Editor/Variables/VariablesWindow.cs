using System;
using System.Collections.Generic;
using System.Linq;
using Ludiq;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityObject = UnityEngine.Object;

namespace Bolt
{
	public class VariablesWindow : GraphContextualWindow
	{
		public static VariablesWindow instance { get; private set; }

		[MenuItem("Window/Bolt/Variables", priority = 3)]
		public static void Open()
		{
			if (instance == null)
			{
				GetWindow<VariablesWindow>().Show();
			}
			else
			{
				FocusWindowIfItsOpen<VariablesWindow>();
			}
		}

		protected override void OnEnable()
		{
			base.OnEnable();
			instance = this;
			minSize = new Vector2(335, 255);
			titleContent = new GUIContent("Variables", BoltCore.Icons.variablesWindow?[IconSize.Small]);
		}

		protected override void OnDisable()
		{
			base.OnDisable();
			instance = null;
		}

		protected override void _OnSelectionChange()
		{
			base._OnSelectionChange();
			UpdateTabs();
		}

		protected override void _OnProjectChange()
		{
			base._OnProjectChange();
			UpdateTabs();
		}

		protected override void _OnHierarchyChange()
		{
			base._OnHierarchyChange();

			// For some odd reason, OnHierarchyChange seems to trigger 
			// when changes that affect prefab instances or definitions,
			// causing a panel update and focus loss when typing. 
			// Disable for now.
			// UpdateTabs();
		}

		protected override void _OnModeChange()
		{
			base._OnModeChange();
			UpdateTabs();
		}

		protected override void _OnContextChange(IGraphContext previousContext, IGraphContext newContext)
		{
			base._OnContextChange(previousContext, newContext);
			UpdateTabs();
		}

		private void UpdateTabs()
		{
			tabs.Clear();
			
			if (context?.graph is IGraphWithVariables)
			{
				tabs.Add(Graph(context.reference));
			}
			else
			{
				tabs.Add(Graph(null));
			}

			tabs.Add(Object(context?.reference.gameObject ?? Selection.activeGameObject));
			tabs.Add(Scene());
			tabs.Add(Application());
			tabs.Add(Saved());

			// TODO: Find a way to reinstate / transfer currentTabIdentifier. Right now it's ignored.
			currentTab = tabs.FirstOrDefault(t => t.identifier == currentTabIdentifier) ?? tabs.FirstOrDefault(t => t.enabled);
			currentTabIdentifier = currentTab.identifier;
		}

		private readonly List<Tab> tabs = new List<Tab>();

		private Tab currentTab;

		private string currentTabIdentifier;

		public float GetHeight(float width)
		{
			var height = 0f;

			height += GetTabBarHeight(width);

			if (currentTab != null)
			{
				context?.BeginEdit(false);

				height += currentTab.GetHeight(width);

				context?.EndEdit();
			}

			return height;
		}

		protected override void OnGUI()
		{
			LudiqGUIUtility.BeginScrollableWindow(this.position, GetHeight, out var position, ref scroll);

			base.OnGUI();
			
			var y = position.y;

			var tabBarHeight = GetTabBarHeight(position.width);
			var tabButtonWidth = position.width / tabs.Count;
			for (var i = 0; i < tabs.Count; i++)
			{
				var tab = tabs[i];

				var tabButtonPosition = new Rect
				(
					position.x + i * tabButtonWidth,
					y,
					tabButtonWidth,
					tabBarHeight
				);

				OnTabButtonGUI(tabButtonPosition, tab);
			}

			y += tabBarHeight;

			y--;

			if (currentTab != null)
			{
				context?.BeginEdit();

				currentTab.OnGUI(position, ref y);

				context?.EndEdit();
			}

			LudiqGUIUtility.EndScrollableWindow();
		}

		private float GetTabBarHeight(float width)
		{
			return Styles.tab.fixedHeight;
		}

		private void OnTabButtonGUI(Rect position, Tab tab)
		{
			EditorGUI.BeginDisabledGroup(!tab.enabled);

			using (LudiqGUIUtility.iconSize.Override(IconSize.Small))
			{
				if (GUI.Toggle(position, currentTab == tab, tab.label, Styles.tab) && currentTab != tab)
				{
					currentTab = tab;
					currentTabIdentifier = tab.identifier;
					GUIUtility.keyboardControl = 0;
					GUIUtility.hotControl = 0;
				}
			}

			EditorGUI.EndDisabledGroup();
		}
		
		private Tab Graph(GraphReference reference)
		{
			var tab = new Tab
			(
				this,
				"Graph",
				"Graph Variables",
				"These variables are local to the current graph.",
				BoltCore.Icons.graphVariable
			);

			if (reference != null)
			{
				if (reference.hasData)
				{
					var instanceVariables = Variables.GraphInstance(reference);
					
					tab.subTabs.Add(new SubTab(tab, VariableKind.Graph, instanceVariables, reference.serializedObject, null, "Instance"));
				}

				var definitionVariables = Variables.GraphDefinition(reference);

				tab.subTabs.Add(new SubTab(tab, VariableKind.Graph, definitionVariables, reference.serializedObject, null, "Definition"));
			}

			tab.MakeFirstSubTabCurrent();

			return tab;
		}

		private Tab Object(GameObject @object)
		{
			var tab = new Tab
			(
				this,
				"Object",
				"Object Variables",
				"These variables are shared across the current game object.",
				BoltCore.Icons.objectVariable
			);

			if (@object != null)
			{
				if (@object.IsConnectedPrefabInstance())
				{
					var instance = @object;
					var definition = instance.GetPrefabDefinition();

					var instanceVariables = instance.GetComponent<Variables>();
					var definitionVariables = definition.GetComponent<Variables>();

					if (definitionVariables != null)
					{
						tab.subTabs.Add(new SubTab(tab, VariableKind.Object, definitionVariables.declarations, definitionVariables, null, "Prefab"));
					}

					if (instanceVariables != null)
					{
						tab.subTabs.Add(new SubTab(tab, VariableKind.Object, instanceVariables.declarations, instanceVariables, null, "Instance"));
					}
				}
				else
				{
					var variables = @object.GetComponent<Variables>();

					if (variables != null)
					{
						tab.subTabs.Add(new SubTab(tab, VariableKind.Object, variables.declarations, variables, null));
					}
				}

				tab.MakeFirstSubTabCurrent();
			}

			return tab;
		}

		private Tab Scene()
		{
			var tab = new Tab
			(
				this,
				"Scene",
				"Scene Variables",
				"These variables are shared across the current scene.",
				BoltCore.Icons.sceneVariable
			);
				
			for (int i = 0; i < SceneManager.sceneCount; i++)
			{
				var scene = SceneManager.GetSceneAt(i);

				// Trying to fetch the singleton while the scene isn't completely
				// loaded seems to mess with the instantiation or handles and create duplicates
				if (!scene.isLoaded)
				{
					continue;
				}

				if (BoltCore.Configuration.createSceneVariables || SceneVariables.InstantiatedIn(scene))
				{
					var sceneVariables = SceneVariables.Instance(scene);
					var declarations = sceneVariables.GetComponent<Variables>().declarations;
					var owner = sceneVariables;
					var title = StringUtility.FallbackWhitespace(sceneVariables.gameObject.scene.name, "Untitled");

					tab.subTabs.Add(new SubTab(tab, VariableKind.Scene, declarations, owner, null, title));
				}
			}

			tab.MakeFirstSubTabCurrent();

			return tab;
		}

		private Tab Application()
		{
			if (EditorApplication.isPlaying)
			{
				var tab = new Tab
				(
					this,
					"App",
					"Application Variables",
					"These variables are shared across scenes. They will be reset once you exit playmode.",
					BoltCore.Icons.applicationVariable
				);

				if (ApplicationVariables.runtime != null)
				{
					tab.subTabs.Add(new SubTab(tab, VariableKind.Application, ApplicationVariables.runtime, null, null));
				}

				tab.MakeFirstSubTabCurrent();

				return tab;
			}
			else
			{
				var tab = new Tab
				(
					this,
					"App",
					"Application Variables",
					"These variables are shared across scenes. They will be reset once the application quits.",
					BoltCore.Icons.applicationVariable
				);

				if (ApplicationVariables.asset?.declarations != null)
				{
					tab.subTabs.Add(new SubTab(tab, VariableKind.Application, ApplicationVariables.asset.declarations, ApplicationVariables.asset, null));
				}

				tab.MakeFirstSubTabCurrent();

				return tab;
			}
		}

		private Tab Saved()
		{
			if (EditorApplication.isPlaying)
			{
				var tab = new Tab
				(
					this,
					"Saved",
					"Saved Variables",
					"These variables will persist even after the application quits. Unity object references are not supported.",
					BoltCore.Icons.savedVariable
				);

				if (SavedVariables.merged != null)
				{
					tab.subTabs.Add(new SubTab(tab, VariableKind.Saved, SavedVariables.merged, null, null, "Merged", "The currently merged variables."));
				}

				tab.MakeFirstSubTabCurrent();

				return tab;
			}
			else
			{
				var tab = new Tab
				(
					this,
					"Saved",
					"Saved Variables",
					"These variables will persist even after the application quits. Unity object references are not supported.",
					BoltCore.Icons.savedVariable
				);

				if (SavedVariables.asset?.declarations != null)
				{
					tab.subTabs.Add(new SubTab(tab, VariableKind.Saved, SavedVariables.asset.declarations, SavedVariables.asset, null, "Initial", "Default variables for new games."));
				}

				if (SavedVariables.saved != null)
				{
					tab.subTabs.Add(new SubTab(tab, VariableKind.Saved, SavedVariables.saved, null, () => SavedVariables.SaveDeclarations(SavedVariables.saved), "Saved", "The currently saved variables."));
				}

				tab.MakeFirstSubTabCurrent();

				return tab;
			}
		}

		public static class Styles
		{
			static Styles()
			{
				tab = new GUIStyle(EditorStyles.toolbarButton);
				tab.margin = new RectOffset(0, 0, 0, 0);
				tab.padding = new RectOffset(0, 0, 0, 0);
				tab.fixedHeight = 22;

				subTab = new GUIStyle(tab);
			}

			public static readonly GUIStyle tab;
			public static readonly GUIStyle subTab;
		}

		private class Tab
		{
			public Tab(VariablesWindow panel, string shortTitle, string title, string description, EditorTexture icon, params SubTab[] subTabs)
			{
				Ensure.That(nameof(panel)).IsNotNull(panel);
				Ensure.That(nameof(shortTitle)).IsNotNull(shortTitle);
				Ensure.That(nameof(title)).IsNotNull(title);
				Ensure.That(nameof(description)).IsNotNull(description);
				Ensure.That(nameof(subTabs)).IsNotNull(subTabs);

				this.panel = panel;

				identifier = shortTitle;
				header = new GUIContent(title, icon?[IconSize.Medium], description);
				label = new GUIContent(" " + shortTitle, icon?[IconSize.Small]);

				this.subTabs = new List<SubTab>(subTabs.NotNull());
				currentSubTab = this.subTabs.FirstOrDefault();
			}

			public readonly VariablesWindow panel;

			public readonly List<SubTab> subTabs;

			private SubTab currentSubTab;

			public string identifier { get; }

			public GUIContent label { get; }

			public GUIContent header { get; }

			public bool enabled => subTabs.Count > 0;
			
			public float GetHeight(float width)
			{
				var height = 0f;

				if (BoltCore.Configuration.showVariablesHelp)
				{
					height += LudiqGUI.GetHeaderHeight(header, width, false);
					height--;
				}

				if (subTabs.Count > 1)
				{
					height += GetSubTabBarHeight(width);
				}

				if (enabled && currentSubTab != null)
				{
					height += currentSubTab.GetHeight(width);
				}

				return height;
			}

			public void OnGUI(Rect position, ref float y)
			{
				if (BoltCore.Configuration.showVariablesHelp)
				{
					EditorGUI.BeginDisabledGroup(!enabled);
					LudiqGUI.OnHeaderGUI(header, position, ref y, false);
					EditorGUI.EndDisabledGroup();
				}

				if (subTabs.Count > 1)
				{
					var subTabBarHeight = GetSubTabBarHeight(position.width);
					var subTabButtonWidth = position.width / subTabs.Count;

					for (var i = 0; i < subTabs.Count; i++)
					{
						var subTab = subTabs[i];

						var subTabButtonPosition = new Rect
						(
							position.x + i * subTabButtonWidth,
							y,
							subTabButtonWidth,
							subTabBarHeight
						);

						OnSubTabButtonGUI(subTabButtonPosition, subTab);
					}

					y += subTabBarHeight;
				}

				if (BoltCore.Configuration.showVariablesHelp || subTabs.Count > 1)
				{
					y--;
				}

				if (enabled)
				{
					currentSubTab?.OnGUI(position, ref y);
				}
			}

			private float GetSubTabBarHeight(float width)
			{
				return Styles.subTab.fixedHeight;
			}

			private void OnSubTabButtonGUI(Rect position, SubTab subTab)
			{
				if (GUI.Toggle(position, currentSubTab == subTab, subTab.label, Styles.subTab) && currentSubTab != subTab)
				{
					currentSubTab = subTab;
					GUIUtility.keyboardControl = 0;
					GUIUtility.hotControl = 0;
				}
			}

			public void MakeFirstSubTabCurrent()
			{
				currentSubTab = subTabs.FirstOrDefault();
			}
		}

		// Implementation note: We're being extra careful with null checks when instantiating
		// subtabs because the variable declarations have wildly different, hard to predict lifetimes.
		// This way, the tab will just show up as disabled in the first frame if the declarations
		// aren't yet deserialized/fetched/merged/etc.

		private class SubTab
		{
			public SubTab(Tab tab, VariableKind kind, VariableDeclarations declarations, UnityObject targetObject, Action save, string label = "Default", string tooltip = null)
			{
				Ensure.That(nameof(tab)).IsNotNull(tab);
				Ensure.That(nameof(declarations)).IsNotNull(declarations);
				Ensure.That(nameof(label)).IsNotNull(label);

				this.tab = tab;

				this.label = new GUIContent(label, tooltip);

				this.targetObject = targetObject;

				metadata = Metadata.Root().StaticObject(declarations);

				inspector = metadata.Inspector<VariableDeclarationsInspector>();

				inspector.kind = kind;

				this.save = save;
			}

			private readonly Tab tab;

			private readonly Metadata metadata;

			private readonly VariableDeclarationsInspector inspector;
			
			private readonly UnityObject targetObject;

			private readonly Action save;

			public GUIContent label { get; }
				
			public float GetHeight(float width)
			{
				using (LudiqEditorUtility.editedObject.Override(targetObject))
				{
					return GetDeclarationsHeight(width);
				}
			}

			public void OnGUI(Rect position, ref float y)
			{
				using (LudiqEditorUtility.editedObject.Override(targetObject))
				{
					EditorGUI.BeginChangeCheck();
						
					inspector.Draw(position.VerticalSection(ref y, GetDeclarationsHeight(position.width)), GUIContent.none);

					if (EditorGUI.EndChangeCheck())
					{
						save?.Invoke();
					}
				}
			}

			private float GetDeclarationsHeight(float width)
			{
				return inspector.GetCachedHeight(width, GUIContent.none, null);
			}
		}
	}
}