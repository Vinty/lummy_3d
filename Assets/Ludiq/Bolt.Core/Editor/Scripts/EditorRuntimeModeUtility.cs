﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;

namespace Bolt
{
	[InitializeOnLoad]
	public static class EditorRuntimeModeUtility
	{
		static EditorRuntimeModeUtility()
		{
			EditorRuntimeModeBinding.runtimeModeBinding = () => BoltCore.Configuration.runtimeMode;
		}
	}
}
