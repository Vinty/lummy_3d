﻿using System;
using Ludiq;

namespace Bolt
{
	[Plugin(BoltCore.ID)]
	internal abstract class BoltCoreMigration : PluginMigration
	{
		protected BoltCoreMigration(Plugin plugin) : base(plugin) { }

		protected void AddDefaultTypeExtraction(TypeExtraction typeExtraction)
		{
			if (!BoltCore.Configuration.typeExtractions.Contains(typeExtraction))
			{
				BoltCore.Configuration.typeExtractions.Add(typeExtraction);
				BoltCore.Configuration.Save();
			}
		}

		protected void AddDefaultNamespaceExtraction(NamespaceExtraction namespaceExtraction)
		{
			if (!BoltCore.Configuration.namespaceExtractions.Contains(namespaceExtraction))
			{
				BoltCore.Configuration.namespaceExtractions.Add(namespaceExtraction);
				BoltCore.Configuration.Save();
			}
		}

		protected void AddDefaultAssemblyExtraction(AssemblyExtraction assemblyExtraction)
		{
			if (!BoltCore.Configuration.assemblyExtractions.Contains(assemblyExtraction))
			{
				BoltCore.Configuration.assemblyExtractions.Add(assemblyExtraction);
				BoltCore.Configuration.Save();
			}
		}
	}
}
