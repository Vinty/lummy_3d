﻿using Ludiq;

namespace Bolt
{
	[Plugin(BoltCore.ID)]
	internal class Migration_1_4_0_to_1_4_1 : BoltCoreMigration
	{
		public Migration_1_4_0_to_1_4_1(Plugin plugin) : base(plugin) { }

		public override SemanticVersion @from => "1.4.0";
		public override SemanticVersion to => "1.4.1";

		public override void Run()
		{
			foreach (var legacyTypeOption in LudiqCore.Configuration.legacyTypeOptions)
			{
				AddDefaultTypeExtraction(legacyTypeOption);
			}
		}
	}
}