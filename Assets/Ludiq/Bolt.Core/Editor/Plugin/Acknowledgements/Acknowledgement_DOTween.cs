﻿using Ludiq;

namespace Bolt
{
	[Plugin(BoltCore.ID)]
	internal class Acknowledgement_DOTween : PluginAcknowledgement
	{
		public Acknowledgement_DOTween(Plugin plugin) : base(plugin) { }

		public override string title => "DOTween";
		public override string author => "Daniele Giardini - Demigiant";
		public override int? copyrightYear => 2018;
		public override string url => "http://dotween.demigiant.com/";
		public override string licenseText => @"DEFINITIONS
Copyright Holder
Daniele Giardini - Demigiant.

You/Your
Means any person who would like to copy, distribute, or modify the Package.

Package
Means the collection of files distributed by the Copyright Holder, and derivatives of that collection and/or of those files. A given Package may consist of either the Standard Version, or a Modified Version.

Distribute
Means providing a copy of the Package or making it accessible to anyone else, or in the case of a company or organization, to others outside of your company or organization.

Standard Version
Refers to the Package if it has not been modified, or has been modified only in ways explicitly requested by the Copyright Holder.

Modified Version
Means the Package, if it has been changed, and such changes were not explicitly requested by the Copyright Holder.

LICENSE
You are permitted to use the Standard Version and create and use Modified Versions for any purpose without restriction, provided that you do not Distribute the Modified Version.

You may Distribute verbatim copies of the Source form of the Standard Version of this Package in any medium without restriction, either gratis or for a Distributor Fee, provided that you duplicate all of the original copyright notices and associated disclaimers and also include the original readme.txt file. At your discretion, such verbatim copies may or may not include a Compiled form of the Package.

Any use, modification, and distribution of the Standard or Modified Versions is governed by this Artistic License. By using, modifying or distributing the Package, you accept this license. Do not use, modify, or distribute the Package, if you do not accept this license.

This license does not grant you the right to use any trademark, service mark, tradename, or logo of the Copyright Holder.

THE SOFTWARE IS PROVIDED ""AS IS"", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.";
	}
}