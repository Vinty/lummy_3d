﻿using Ludiq;

namespace Bolt
{
	[Plugin(BoltCore.ID)]
	public sealed class BoltCoreManifest : PluginManifest
	{
		private BoltCoreManifest(BoltCore plugin) : base(plugin) { }

		public override string name => "Bolt Core";
		public override string author => "Ludiq";
		public override string description => "Visual scripting for Unity.";
		public override SemanticVersion version => "2.0.0a3";
	}
}