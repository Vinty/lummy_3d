﻿using System.IO;
using Ludiq;

namespace Bolt
{
	[Plugin(BoltCore.ID)]
	public class BoltCorePaths : PluginPaths
	{
		public BoltCorePaths(Plugin plugin) : base(plugin) { }

		public string variableResources => Path.Combine(persistentGenerated, "Variables/Resources");
		
		public string restoredWindowLayoutName => "Bolt Restored";
		public string windowLayoutName => "Bolt";

		public string restoredWindowLayout => Path.Combine(transientGenerated, $"WindowLayouts/{restoredWindowLayoutName}.wlt");
		public string windowLayout => Path.Combine(transientGenerated, $"WindowLayouts/{windowLayoutName}.wlt");

		public string xmlDocumentation => Path.Combine(transientGenerated, "XmlDocumentation.extract");
	}
}