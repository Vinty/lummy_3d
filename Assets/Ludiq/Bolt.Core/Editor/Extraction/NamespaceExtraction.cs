﻿using Ludiq;

namespace Bolt
{
	public sealed class NamespaceExtraction
	{
		[Serialize]
		public bool enabled { get; set; } = true;

		[Serialize]
		public Namespace @namespace { get; set; }
		
		[Serialize]
		public bool hierarchy { get; set; }
		
		[DoNotSerialize]
		public bool included => enabled && @namespace != null;

		public NamespaceExtraction() { }

		public NamespaceExtraction(Namespace @namespace, bool hierarchy = false)
		{
			this.@namespace = @namespace;
			this.hierarchy = hierarchy;
		}

		public static implicit operator NamespaceExtraction(Namespace @namespace)
		{
			return new NamespaceExtraction(@namespace);
		}

		public static implicit operator NamespaceExtraction(string @namespace)
		{
			return new NamespaceExtraction(@namespace);
		}
	}
}
