﻿using System;
using System.Collections.Generic;

namespace Bolt
{
	public sealed class ExtractionConfiguration
	{
		public ExtractionMode mode { get; set; }
		public ICollection<Type> types { get; set; }

		public ExtractionConfiguration(ExtractionMode mode)
		{
			this.mode = mode;
		}
	}
}
