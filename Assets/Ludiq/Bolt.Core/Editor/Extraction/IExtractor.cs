﻿using System.IO;

namespace Bolt
{
	public interface IExtractor
	{
		string path { get; }
		string label { get; }
		IExtract Extract(ExtractionConfiguration configuration);
		void Load(IExtract extract);
		int Compare(ExtractionConfiguration configuration);
		void Fallback();
		int extractPriority { get; }
		int loadPriority { get; }
		
		IExtract ReadExtract(Stream stream);
		void WriteExtract(IExtract extract, Stream stream);
	}
}
