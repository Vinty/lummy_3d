﻿using System;
using Ludiq;

namespace Bolt
{
	public sealed class TypeExtraction
	{
		[Serialize]
		public bool enabled { get; set; } = true;

		[Serialize]
		public Type type { get; set; }
		
		[Serialize]
		public bool hierarchy { get; set; }
		
		[DoNotSerialize]
		public bool included => enabled && type != null;

		public TypeExtraction() { }

		public TypeExtraction(Type type, bool hierarchy = false)
		{
			this.type = type;
			this.hierarchy = hierarchy;
		}

		public static implicit operator TypeExtraction(Type type)
		{
			return new TypeExtraction(type);
		}
	}
}
