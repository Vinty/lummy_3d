﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Ludiq;
using UnityEditor;
using UnityEngine;

namespace Bolt
{
	[InitializeAfterPlugins]
	[BackgroundWorker]
	public static class Extraction
	{
		private static readonly List<IExtractor> extractors;

		private static readonly object @lock = new object();

		static Extraction()
		{
			extractors = Codebase.ludiqEditorTypes
				.Where(t => typeof(IExtractor).IsAssignableFrom(t) && t.IsConcrete())
				.Select(t => (IExtractor)Activator.CreateInstance(t)).ToList();
		}

		private static void BackgroundWork()
		{
			Load();

			if (BoltCore.Configuration.compareExtractions)
			{
				Compare();
			}
		}

		public static int changes { get; private set; }

		public static TExtractor Get<TExtractor>() where TExtractor : IExtractor
		{
			return extractors.OfType<TExtractor>().FirstOrDefault();
		}

		public static void Extract(ExtractionMode mode)
		{
			Extract(new ExtractionConfiguration(mode));
		}

		public static void Extract(ExtractionConfiguration configuration)
		{
			Task.Run($"Extracting ({configuration.mode})...", extractors.Count + 1, task =>
			{
				task.AllowCancellation();

				lock (@lock)
				{
					task.StartStep("Filtering Types...");
					var types = GetTypes().ToList();
					task.CompleteStep();

					configuration.types = types;

					foreach (var extractor in extractors.OrderBy(e => e.extractPriority))
					{
						task.AllowCancellation();

						try
						{
							task.StartStep("Extracting " + extractor.label + "...");

							IExtract extract;

							using (ProfilingUtility.SampleBlock($"Extract {extractor.label}"))
							{
								extract = extractor.Extract(configuration);
							}

							if (extract != null)
							{
								task.AllowCancellation();

								if (File.Exists(extractor.path))
								{
									VersionControlUtility.Unlock(extractor.path);
									File.Delete(extractor.path);
								}

								PathUtility.CreateParentDirectoryIfNeeded(extractor.path);

								using (ProfilingUtility.SampleBlock($"Write {extractor.label} Extract"))
								{
									using (var stream = File.Open(extractor.path, FileMode.Create))
									{
										extractor.WriteExtract(extract, stream);
									}
								}
								
								task.AllowCancellation();
							}

							task.CompleteStep();
						}
						catch (OperationCanceledException) { }
						catch (ThreadAbortException) { }
						catch (Exception ex)
						{
							Debug.LogError($"Failed to extract {extractor.label}:\n{ex}");
						}
					}

					changes = 0;
				}
			});

			ConsoleProfiler.Dump();
		}

		public static void Load()
		{
			Task.Run("Loading Extracts...", extractors.Count, task =>
			{
				lock (@lock)
				{
					foreach (var extractor in extractors.OrderBy(e => e.loadPriority))
					{
						task.AllowCancellation();

						try
						{
							task.StartStep("Loading " + extractor.label + "...");

							if (!File.Exists(extractor.path))
							{
								Debug.LogWarning($"Missing {extractor.label}.\nExtract with 'Tools > Bolt > Extractor'.");
								extractor.Fallback();
								continue;
							}
							
							IExtract extract;

							using (ProfilingUtility.SampleBlock($"Read {extractor.label} Extract"))
							{
								using (var stream = File.Open(extractor.path, FileMode.Open))
								{
									extract = extractor.ReadExtract(stream);
								}
							}

							task.AllowCancellation();

							using (ProfilingUtility.SampleBlock($"Load {extractor.label} Extract"))
							{
								extractor.Load(extract);
							}

							task.AllowCancellation();

							task.CompleteStep();
						}
						catch (OperationCanceledException) { }
						catch (ThreadAbortException) { }
						catch (Exception ex)
						{
							Debug.LogError($"Failed to load {extractor.label}\n{ex}");
							extractor.Fallback();
						}
					}
				}
			});

			ConsoleProfiler.Dump();
		}
		
		public static void Compare()
		{
			changes = 0;

			Task.Run("Comparing Extracts...", extractors.Count + 1, task =>
			{
				lock (@lock)
				{
					task.StartStep("Filtering Types...");
					var types = GetTypes().ToList();
					task.CompleteStep();

					var configuration = new ExtractionConfiguration(ExtractionMode.Fast) { types = types };

					foreach (var extractor in extractors.OrderBy(e => e.extractPriority))
					{
						task.AllowCancellation();

						try
						{
							task.StartStep("Comparing " + extractor.label + "...");

							if (!File.Exists(extractor.path))
							{
								continue;
							}

							changes += extractor.Compare(configuration);
							
							task.CompleteStep();
						}
						catch (OperationCanceledException) { }
						catch (ThreadAbortException) { }
						catch (Exception ex)
						{
							Debug.LogError($"Failed to compare {extractor.label}\n{ex}");
						}
					}
				}
			});
			
			ConsoleProfiler.Dump();
		}

		public static IEnumerable<Type> GetTypes()
		{
			using (ProfilingUtility.SampleBlock("Get Extracted Types"))
			{
				foreach (var assembly in Codebase.assemblies)
				{
					// For optimization, we bypass [Extract] for assemblies that could logically never have it.
					var couldHaveExtractAttribute = Codebase.ludiqAssemblies.Contains(assembly);

					foreach (var type in assembly.GetTypesSafely())
					{
						// Apparently void can be returned somehow:
						// http://support.ludiq.io/topics/483-/
						if (type == typeof(void))
						{
							continue;
						}

						if (couldHaveExtractAttribute ? ShouldExtractTypeWithAttribute(type) : ShouldExtractType(type))
						{
							yield return type;
						}
					}
				}
			}
		}

		private static bool ShouldExtractTypeWithAttribute(Type type)
		{
			var extractAttribute = type.GetAttribute<ExtractAttribute>();

			if (extractAttribute != null)
			{
				return extractAttribute.extract;
			}

			return ShouldExtractType(type);
		}

		private static bool ShouldExtractType(Type type)
		{
			foreach (var typeExtraction in BoltCore.Configuration.typeExtractions)
			{
				if (typeExtraction == null || !typeExtraction.included)
				{
					continue;
				}

				if (typeExtraction.hierarchy)
				{
					if (typeExtraction.type.IsAssignableFrom(type))
					{
						return ShouldExtractInferredType(type);
					}
					
					if (type.IsNested && type.DeclaringType == typeExtraction.type)
					{
						return true;
					}
				}
				else
				{
					if (typeExtraction.type == type)
					{
						return true;
					}
				}
			}

			foreach (var namespaceExtraction in BoltCore.Configuration.namespaceExtractions)
			{
				if (namespaceExtraction ==  null || !namespaceExtraction.included)
				{
					continue;
				}

				if (namespaceExtraction.hierarchy)
				{
					var @namespace = type.Namespace();

					if (namespaceExtraction.@namespace == @namespace || namespaceExtraction.@namespace.IsAncestorOf(@namespace))
					{
						return ShouldExtractInferredType(type);
					}
				}
				else
				{
					if (namespaceExtraction.@namespace == type.Namespace())
					{
						return true;
					}
				}
			}

			foreach (var assemblyExtraction in BoltCore.Configuration.assemblyExtractions)
			{
				if (assemblyExtraction ==  null || !assemblyExtraction.included)
				{
					continue;
				}

				if (type.Assembly.LooseName() == assemblyExtraction.assembly)
				{
					return true;
				}
			}
			
			return false;
		}

		private static bool ShouldExtractInferredType(Type type)
		{
			// These types are inferred from recursive extractions,
			// so we want to limit the ones that don't really make sense.

			if (Codebase.IsInternalType(type))
			{
				return false;
			}

			return true;
		}

		public static void TypesChanged()
		{
			typesChanged?.Invoke();
		}

		public static event Action typesChanged;

		[MenuItem("Tools/Bolt/Fast Extract", priority = BoltProduct.ToolsMenuPriority + 302)]
		private static void FastExtract()
		{
			Extraction.Extract(ExtractionMode.Fast);
		}

		[MenuItem("Tools/Bolt/Full Extract", priority = BoltProduct.ToolsMenuPriority + 303)]
		private static void FullExtract()
		{
			Extraction.Extract(ExtractionMode.Full);
		}
	}
}
