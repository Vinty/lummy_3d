﻿using Ludiq;

namespace Bolt
{
	public sealed class AssemblyExtraction
	{
		[Serialize]
		public bool enabled { get; set; } = true;

		[Serialize]
		public LooseAssemblyName assembly { get; set; }
		
		[DoNotSerialize]
		public bool included => enabled && assembly != null;

		public AssemblyExtraction() { }

		public AssemblyExtraction(LooseAssemblyName assembly)
		{
			this.assembly = assembly;
		}

		public static implicit operator AssemblyExtraction(LooseAssemblyName assembly)
		{
			return new AssemblyExtraction(assembly);
		}
	}
}
