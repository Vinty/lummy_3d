﻿using Ludiq;
using UnityEditor;
using UnityEngine;

namespace Bolt
{
	public sealed class BoltToolbar : LudiqEditorWindow
	{
		private enum ExtractionOperation
		{
			/// <summary>
			/// Extracts only the changes to your codebase since the last extraction.
			/// </summary>
			FastExtract,

			/// <summary>
			/// Rebuilds the entire extracted database, ensuring integrity.
			/// </summary>
			FullExtract,

			/// <summary>
			/// Displays the Extractor window to let you configure what to extract.
			/// </summary>
			OpenExtractor
		}

		public static BoltToolbar instance { get; private set; }

		private const string menuPath = "Window/Bolt/Toolbar";

		[MenuItem(menuPath, priority = -1)]
		static void Open()
		{
			if (instance == null)
			{
				CreateInstance<BoltToolbar>().ShowPopup();
			}
			else
			{
				instance.Close();
			}
		}

		protected override void OnEnable()
		{
			base.OnEnable();
			Menu.SetChecked(menuPath, true);
			instance = this;
		}

		protected override void OnDisable()
		{
			base.OnDisable();
			Menu.SetChecked(menuPath, false);
			instance = null;
		}

		private Placement placement => Placement.AfterPlay;

		private void Position()
		{
			if (!Styles.initialized)
			{
				return;
			}

			var editorPosition = LudiqGUIUtility.mainEditorWindowPosition;

			var width = (buttonCount * Styles.buttonWidth) + (spaceCount * Styles.spaceWidth);
			var height = Styles.height;

			var leftGap = (editorPosition.width / 2) - Styles.unityToolButtonsWidth - (Styles.unityPlayButtonsWidth / 2);
			var rightGap = (editorPosition.width / 2) - Styles.unityServiceButtonsWidth - (Styles.unityPlayButtonsWidth / 2);

			if (leftGap >= width + Styles.spacing && rightGap >= width + Styles.spacing)
			{
				float x;

				switch (placement)
				{
					case Placement.LeftGap:
						x = editorPosition.x + Styles.unityToolButtonsWidth + (leftGap / 2) - (width / 2);
						break;
					case Placement.RightGap:
						x = editorPosition.x + (editorPosition.width / 2) + (Styles.unityToolButtonsWidth / 2) + (rightGap / 2) - (width / 2);
						break;
					case Placement.BeforePlay:
						x = editorPosition.x + Styles.unityToolButtonsWidth + leftGap - width - Styles.spacing;
						break;
					case Placement.AfterPlay:
						x = editorPosition.x + Styles.unityToolButtonsWidth + leftGap + Styles.unityPlayButtonsWidth + Styles.spacing;
						break;
					default:
						throw new UnexpectedEnumValueException<Placement>(placement);
				}

				position = new Rect
				(
					x,
					editorPosition.y,
					width,
					height
				);

				minSize = new Vector2(0, height);
				maxSize = new Vector2(9999, height);
			}
			else
			{
				position = Rect.zero;
				minSize = maxSize = Vector2.zero;
			}
		}

		protected override void Update()
		{
			base.Update();
			
			Position();
		}
		
		private int buttonCount => 3;

		private int spaceCount => 0;

		protected override void OnGUI()
		{
			base.OnGUI();

			Styles.Initialize();

			Position();
			
			GUILayout.BeginHorizontal(Styles.background);

			OnExtractButtonGUI();
			OnGenerateButtonGUI();
			OnRuntimeButtonGUI();
			
			GUILayout.EndHorizontal();

			Repaint();
		}

		private void OnExtractButtonGUI()
		{
			var icon = BoltCore.Icons.extractButton.Single();

			if (Extraction.changes > 0)
			{
				icon = BoltCore.Icons.extractButtonOn.Single();
			}

			var style = Styles.buttonLeft;

			var content = new GUIContent(icon);

			var buttonPosition = GUILayoutUtility.GetRect(content, style);

			var clicked = GUI.Button(buttonPosition, content, style);

			if (Extraction.changes > 0)
			{
				//DrawBadge(buttonPosition, Extraction.changes.ToString());
			}

			if (clicked)
			{
				var optionTree = EnumOptionTree.For<ExtractionOperation>();
				optionTree.header = new GUIContent("Extract");

				if (Event.current.alt)
				{
					Extraction.Extract(ExtractionMode.Fast);
				}
				else
				{
					LudiqGUI.FuzzyDropdown
					(
						buttonPosition,
						optionTree,
						BoltCore.Configuration.runtimeMode,
						(_operation) =>
						{
							EditorApplication.delayCall += () =>
							{
								var operation = (ExtractionOperation)_operation;

								switch (operation)
								{
									case ExtractionOperation.FastExtract:
										Extraction.Extract(ExtractionMode.Fast);
										break;
									case ExtractionOperation.FullExtract:
										Extraction.Extract(ExtractionMode.Full);
										break;
									case ExtractionOperation.OpenExtractor:
										ExtractorWindow.Show();
										break;
								}
							};
						}
					);
				}
			}
		}

		private void OnGenerateButtonGUI()
		{
			var icon = BoltCore.Icons.generateButton.Single();
			
			if (Generation.changes > 0)
			{
				icon = BoltCore.Icons.generateButtonOn.Single();
			}

			var style = Styles.buttonMid;

			var content = new GUIContent(icon);

			var buttonPosition = GUILayoutUtility.GetRect(content, style);

			var clicked = GUI.Button(buttonPosition, content, style);

			if (Generation.changes > 0)
			{
				//DrawBadge(buttonPosition, Generation.changes.ToString());
			}

			if (clicked)
			{
				if (EditorApplication.isPlaying)
				{
					if (!EditorUtility.DisplayDialog("Change Runtime", "Do you want to exit play mode and generate?", "Exit and Generate", "Cancel"))
					{
						return;
					}

					EditorApplication.isPlaying = false;
					
					EditorApplicationUtility.onEnterEditMode += Generation.Generate;
				}
				else
				{
					Generation.Generate();
				}
			}
		}

		private EditorTexture runtimeIcon
		{
			get
			{
				if (Application.isPlaying)
				{
					switch (BoltCore.Configuration.runtimeMode)
					{
						case RuntimeMode.Live: return BoltCore.Icons.liveRuntimeButtonOn;
						case RuntimeMode.Generated: return BoltCore.Icons.generatedRuntimeButtonOn;
						case RuntimeMode.Hybrid: return BoltCore.Icons.hybridRuntimeButtonOn;
						default: throw new UnexpectedEnumValueException<RuntimeMode>(BoltCore.Configuration.runtimeMode);
					}
				}
				else
				{
					switch (BoltCore.Configuration.runtimeMode)
					{
						case RuntimeMode.Live: return BoltCore.Icons.liveRuntimeButton;
						case RuntimeMode.Generated: return BoltCore.Icons.generatedRuntimeButton;
						case RuntimeMode.Hybrid: return BoltCore.Icons.hybridRuntimeButton;
						default: throw new UnexpectedEnumValueException<RuntimeMode>(BoltCore.Configuration.runtimeMode);
					}
				}
			}
		}

		private void OnRuntimeButtonGUI()
		{
			var icon = runtimeIcon.Single();

			var style = EditorApplication.isPlayingOrWillChangePlaymode ? Styles.buttonRightOn : Styles.buttonRight;

			var content = new GUIContent(icon);

			var buttonPosition = GUILayoutUtility.GetRect(content, style);

			var activatorPosition = buttonPosition;
			activatorPosition.yMin = activatorPosition.yMin + 5;

			if (GUI.Button(buttonPosition, content, style))
			{
				var options = EnumOptionTree.For<RuntimeMode>();
				options.header = new GUIContent("Runtime");

				LudiqGUI.FuzzyDropdown
				(
					buttonPosition,
					EnumOptionTree.For<RuntimeMode>(),
					BoltCore.Configuration.runtimeMode,
					(_runtimeMode) =>
					{
						if (EditorApplication.isPlaying)
						{
							if (!EditorUtility.DisplayDialog("Change Runtime", "Do you want to exit play mode and change the runtime?", "Exit and Change", "Cancel"))
							{
								return;
							}

							EditorApplication.isPlaying = false;
						}

						var runtimeMode = (RuntimeMode)_runtimeMode;

						BoltCore.Configuration.runtimeMode = runtimeMode;

						BoltCore.Configuration.Save();
					}
				);
			}
		}

		private static void DrawBadge(Rect buttonPosition, string content)
		{
			if (Event.current.type == EventType.Repaint)
			{
				/*
				var _content = new GUIContent(content);

				var size = Styles.textBadge.CalcSize(_content);

				var position = new Rect
				(
					buttonPosition.xMax - size.x - Styles.textBadge.margin.right,
					buttonPosition.yMax - size.y - Styles.textBadge.margin.bottom,
					size.x,
					size.y
				);

				Styles.textBadge.Draw(position, _content, false, false, false, false);*/

				var badge = BoltCore.Icons.changeBadge.Single();

				var size = badge.Size();

				var position = new Rect
				(
					buttonPosition.xMax - size.x - 1,
					buttonPosition.y + 2,
					size.x,
					size.y
				);

				GUI.DrawTexture(position, badge);
			}
		}

		public enum Placement
		{
			LeftGap,
			BeforePlay,
			AfterPlay,
			RightGap
		}

		public static class Styles
		{
			public static float height = 30f;

			public static float unityToolButtonsWidth = 350f;
			public static float unityPlayButtonsWidth = 100f;
			public static float unityServiceButtonsWidth = 410f;
			public static float spacing = 12f;

			public static float buttonWidth = 36;

			public static float spaceWidth = 16;

			public static GUIStyle background;

			public static GUIStyle buttonLeft;

			public static GUIStyle buttonMid;

			public static GUIStyle buttonRight;

			public static GUIStyle buttonRightOn;

			public static GUIStyle button;

			public static GUIStyle textBadge;

			public static bool initialized;

			public static void Initialize()
			{
				initialized = true;

				background = ColorPalette.unityBackgroundDark.CreateBackground();
				background.padding = new RectOffset(0, 0, 5, 0);

				buttonLeft = new GUIStyle("CommandLeft");
				buttonMid = new GUIStyle("CommandMid");
				buttonRight = new GUIStyle("CommandRight");
				buttonRightOn = new GUIStyle("CommandRight");

				if (EditorGUIUtility.isProSkin)
				{
					//buttonRightOn.normal.background = buttonRightOn.onNormal.background;
				}
				else
				{
				//	buttonRightOn.normal.background = buttonRightOn.active.background;
				}

				button = new GUIStyle("Command");

				textBadge = new GUIStyle("sv_label_6");
				textBadge.fontSize = 8;
				textBadge.normal.textColor = Color.white;
				textBadge.alignment = TextAnchor.MiddleCenter;
				textBadge.margin = new RectOffset(0, 2, 0, 2);
				textBadge.padding = new RectOffset(6, 6, 2, 2);
			}
		}
	}
}
