﻿using System;
using UnityEngine;
using Ludiq;
using UnityEditor;

namespace Bolt
{
	[Inspector(typeof(TypeExtraction))]
	public sealed class TypeExtractionInspector : Inspector
	{
		public TypeExtractionInspector(Metadata metadata) : base(metadata) { }

		private Metadata typeMetadata => metadata[nameof(TypeExtraction.type)];
		private Metadata enabledMetadata => metadata[nameof(TypeExtraction.enabled)];
		private Metadata hierarchyMetadata => metadata[nameof(TypeExtraction.hierarchy)];
		private static readonly GUIContent hierarchyToggleContent = new GUIContent("Hierarchy");
		
		public override void Initialize()
		{
			metadata.instantiate = true;

			base.Initialize();
		}

		protected override float GetHeight(float width, GUIContent label)
		{
			return Styles.verticalPadding + EditorGUIUtility.singleLineHeight + Styles.verticalPadding;
		}

		protected override void OnGUI(Rect position, GUIContent label)
		{
			var y = position.y + Styles.verticalPadding;

			var hierarchyToggleWidth = EditorStyles.toggle.CalcSize(hierarchyToggleContent).x;
			
			var enabledTogglePosition = new Rect
			(
				position.x + Styles.horizontalSpacing,
				y,
				14,
				EditorGUIUtility.singleLineHeight
			);

			var typePosition = new Rect
			(
				enabledTogglePosition.xMax + Styles.horizontalSpacing,
				y - 2,
				position.width - enabledTogglePosition.width - (Styles.horizontalSpacing * 3) - hierarchyToggleWidth,
				LudiqStyles.largePopup.fixedHeight
			);

			var hierarchyTogglePosition = new Rect
			(
				typePosition.xMax + Styles.horizontalSpacing,
				y,
				hierarchyToggleWidth,
				EditorGUIUtility.singleLineHeight
			);

			var enabled = (bool)enabledMetadata.value;
			
			EditorGUI.BeginChangeCheck();

			var newEnabled = EditorGUI.ToggleLeft(enabledTogglePosition, GUIContent.none, enabled);

			if (EditorGUI.EndChangeCheck())
			{
				enabledMetadata.value = newEnabled;
			}

			EditorGUI.BeginDisabledGroup(!newEnabled);

			BeginBlock(typeMetadata, typePosition, GUIContent.none);
			
			var newType = LudiqGUI.TypeField(typePosition, GUIContent.none, (Type)typeMetadata.value, GetOptions, null, LudiqStyles.largePopup);

			if (EndBlock(typeMetadata))
			{
				typeMetadata.value = newType;
			}

			var hierarchy = (bool)hierarchyMetadata.value;

			var supportsHierarchy = false;
			
			var type = (Type)typeMetadata.value;

			if (type != null)
			{
				supportsHierarchy = (!type.IsSealed && type != typeof(object)) || (type.GetNestedTypes().Length > 0);
			}

			EditorGUI.BeginDisabledGroup(!supportsHierarchy);

			EditorGUI.BeginChangeCheck();

			var newHierarchy = EditorGUI.ToggleLeft(hierarchyTogglePosition, hierarchyToggleContent, supportsHierarchy && hierarchy);

			if (EditorGUI.EndChangeCheck())
			{
				hierarchyMetadata.value = newHierarchy;
			}

			EditorGUI.EndDisabledGroup();

			EditorGUI.EndDisabledGroup();
		}

		private IFuzzyOptionTree GetOptions()
		{
			return new TypeOptionTree(Codebase.types, TypeFilter.Any) { surfaceCommonTypes = false };
		}

		public static class Styles
		{
			public static readonly float verticalPadding = 6;

			public static readonly float horizontalSpacing = 4;
		}
	}
}