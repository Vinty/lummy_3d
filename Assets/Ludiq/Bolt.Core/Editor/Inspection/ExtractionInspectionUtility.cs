﻿using Ludiq;
using UnityEngine;

namespace Bolt
{
	public static class ExtractionInspectionUtility
	{
		public static void HierarchyToggle(Rect position, bool supportsHierarchy, ref bool enabled, ref bool hierarchy)
		{
			if (!enabled)
			{
				enabled = GUI.Toggle(position, false, GUIContent.none);
			}
			else if (supportsHierarchy)
			{
				if (hierarchy)
				{
					if (!GUI.Toggle(position, true, GUIContent.none))
					{
						enabled = false;
						hierarchy = false;
					}
				}
				else
				{
					if (!GUI.Toggle(position, true, GUIContent.none, LudiqStyles.mixedToggle))
					{
						hierarchy = true;
					}
				}
			}
			else
			{
				enabled = GUI.Toggle(position, enabled, GUIContent.none);
				hierarchy = false;
			}
		}
	}
}
