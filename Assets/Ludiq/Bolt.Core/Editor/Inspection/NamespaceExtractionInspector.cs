﻿using System;
using System.Linq;
using Ludiq;
using UnityEditor;
using UnityEngine;

namespace Bolt
{
	[Inspector(typeof(NamespaceExtraction))]
	public sealed class NamespaceExtractionInspector : Inspector
	{
		public NamespaceExtractionInspector(Metadata metadata) : base(metadata) { }

		private Metadata namespaceMetadata => metadata[nameof(NamespaceExtraction.@namespace)];

		private Metadata enabledMetadata => metadata[nameof(NamespaceExtraction.enabled)];

		private Metadata hierarchyMetadata => metadata[nameof(NamespaceExtraction.hierarchy)];

		private static readonly GUIContent hierarchyToggleContent = new GUIContent("Hierarchy");

		public override void Initialize()
		{
			metadata.instantiate = true;

			base.Initialize();
		}

		protected override float GetHeight(float width, GUIContent label)
		{
			return Styles.verticalPadding + EditorGUIUtility.singleLineHeight + Styles.verticalPadding;
		}

		protected override void OnGUI(Rect position, GUIContent label)
		{
			var y = position.y + Styles.verticalPadding;
			
			var hierarchyToggleWidth = EditorStyles.toggle.CalcSize(hierarchyToggleContent).x;

			var enabledTogglePosition = new Rect
			(
				position.x + Styles.horizontalSpacing,
				y,
				14,
				EditorGUIUtility.singleLineHeight
			);

			var namespacePosition = new Rect
			(
				enabledTogglePosition.xMax + Styles.horizontalSpacing,
				y - 2,
				position.width - enabledTogglePosition.width - (Styles.horizontalSpacing * 3) - hierarchyToggleWidth,
				LudiqStyles.largePopup.fixedHeight
			);

			var hierarchyTogglePosition = new Rect
			(
				namespacePosition.xMax + Styles.horizontalSpacing,
				y,
				hierarchyToggleWidth,
				EditorGUIUtility.singleLineHeight
			);

			var enabled = (bool)enabledMetadata.value;

			EditorGUI.BeginChangeCheck();

			var newEnabled = EditorGUI.ToggleLeft(enabledTogglePosition, GUIContent.none, enabled);

			if (EditorGUI.EndChangeCheck())
			{
				enabledMetadata.value = newEnabled;
			}

			EditorGUI.BeginDisabledGroup(!enabled);

			BeginBlock(namespaceMetadata, namespacePosition, GUIContent.none);

			var newNamespace = LudiqGUI.NamespaceField(namespacePosition, GUIContent.none, (Namespace)namespaceMetadata.value, GetOptions, null, LudiqStyles.largePopup);

			if (EndBlock(namespaceMetadata))
			{
				namespaceMetadata.value = newNamespace;
			}

			var hierarchy = (bool)hierarchyMetadata.value;
			
			EditorGUI.BeginChangeCheck();

			var newHierarchy = EditorGUI.ToggleLeft(hierarchyTogglePosition, hierarchyToggleContent, hierarchy);

			if (EditorGUI.EndChangeCheck())
			{
				hierarchyMetadata.value = newHierarchy;
			}

			EditorGUI.EndDisabledGroup();
		}

		private IFuzzyOptionTree GetOptions()
		{
			return new NamespaceOptionTree(Codebase.types.Select(t => t.Namespace()));
		}

		public static class Styles
		{
			public static readonly float verticalPadding = 6;

			public static readonly float horizontalSpacing = 4;
		}
	}
}
