﻿using UnityEngine;
using Ludiq;
using UnityEditor;

namespace Bolt
{
	[Inspector(typeof(AssemblyExtraction))]
	public sealed class AssemblyExtractionInspector : Inspector
	{
		public AssemblyExtractionInspector(Metadata metadata) : base(metadata) { }

		private Metadata assemblyMetadata => metadata[nameof(AssemblyExtraction.assembly)];
		private Metadata enabledMetadata => metadata[nameof(AssemblyExtraction.enabled)];
		
		public override void Initialize()
		{
			metadata.instantiate = true;

			base.Initialize();
		}

		protected override float GetHeight(float width, GUIContent label)
		{
			return Styles.verticalPadding + EditorGUIUtility.singleLineHeight + Styles.verticalPadding;
		}

		protected override void OnGUI(Rect position, GUIContent label)
		{
			var y = position.y + Styles.verticalPadding;
			
			var togglePosition = new Rect
			(
				position.x + Styles.horizontalSpacing,
				y,
				14,
				EditorGUIUtility.singleLineHeight
			);

			var assemblyPosition = new Rect
			(
				togglePosition.xMax + Styles.horizontalSpacing,
				y - 2,
				position.width - togglePosition.width - (Styles.horizontalSpacing * 2),
				LudiqStyles.largePopup.fixedHeight
			);

			var enabled = (bool)enabledMetadata.value;

			EditorGUI.BeginChangeCheck();

			enabled = GUI.Toggle(togglePosition, enabled, GUIContent.none);

			if (EditorGUI.EndChangeCheck())
			{
				metadata.RecordUndo();
				enabledMetadata.value = enabled;
			}

			EditorGUI.BeginDisabledGroup(!enabled);

			BeginBlock(assemblyMetadata, assemblyPosition, GUIContent.none);
			
			var newAssembly = LudiqGUI.AssemblyField(assemblyPosition, GUIContent.none, (LooseAssemblyName)assemblyMetadata.value, GetOptions, null, LudiqStyles.largePopup);

			if (EndBlock(assemblyMetadata))
			{
				assemblyMetadata.RecordUndo();
				assemblyMetadata.value = newAssembly;
			}

			EditorGUI.EndDisabledGroup();
		}

		private IFuzzyOptionTree GetOptions()
		{
			return new LooseAssemblyNameOptionTree();
		}

		public static class Styles
		{
			public static readonly float verticalPadding = 6;

			public static readonly float horizontalSpacing = 4;
		}
	}
}