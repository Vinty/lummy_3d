﻿using Ludiq;
using UnityEditor;
using UnityEngine;

namespace Bolt
{
	public class ExtractionsPage : Page
	{
		public ExtractionsPage(string title, PluginConfigurationItemMetadata metadata)
		{
			Ensure.That(nameof(title)).IsNotNull(title);
			Ensure.That(nameof(metadata)).IsNotNull(metadata);

			this.title = title;
			shortTitle = title;
			this.metadata = metadata;
		}

		public PluginConfigurationItemMetadata metadata { get; }

		private Vector2 scroll;

		protected override void OnContentGUI()
		{
			GUILayout.BeginVertical(Styles.background, GUILayout.ExpandWidth(true));

			scroll = GUILayout.BeginScrollView(scroll);
		
			LudiqGUI.Space(-1);

			LudiqGUI.BeginHorizontal();

			LudiqGUI.Space(-1);

			EditorGUI.BeginChangeCheck();
			
			LudiqGUI.InspectorLayout(metadata, GUIContent.none);

			if (EditorGUI.EndChangeCheck())
			{
				Extraction.TypesChanged();
				metadata.Save();
			}

			LudiqGUI.Space(-1);

			LudiqGUI.EndHorizontal();

			LudiqGUI.Space(5);

			GUILayout.EndScrollView();

			GUILayout.EndVertical();
		}

		public static class Styles
		{
			static Styles()
			{
				background = new GUIStyle(LudiqStyles.windowBackground);
				background.padding = new RectOffset(-1, -1, -1, -1);
			}

			public static readonly GUIStyle background;
		}
	}
}
