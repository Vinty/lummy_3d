﻿using System.Linq;
using System.Threading;
using Ludiq;
using UnityEditor;
using UnityEngine;

namespace Bolt
{
	public sealed class ExtractorPage : ListPage
	{
		public ExtractorPage() : base()
		{
			title = "Extractor";
			shortTitle = "Extractor";
			icon = BoltCore.Resources.LoadIcon("Icons/Windows/ExtractorWindow.png");
			
			pages.Add(new ExtractionsPage("Types", BoltCore.Configuration.GetMetadata(nameof(BoltCoreConfiguration.typeExtractions))));
			pages.Add(new ExtractionsPage("Namespaces", BoltCore.Configuration.GetMetadata(nameof(BoltCoreConfiguration.namespaceExtractions))));
			pages.Add(new ExtractionsPage("Assemblies", BoltCore.Configuration.GetMetadata(nameof(BoltCoreConfiguration.assemblyExtractions))));
			pages.Add(new ExtractionOptionsPage());
		}

		protected override bool flexible => true;

		protected override void OnShow()
		{
			base.OnShow();

			UpdateTypeCount();

			Extraction.typesChanged += UpdateTypeCount;
		}

		protected override void OnClose()
		{
			base.OnClose();

			Extraction.typesChanged -= UpdateTypeCount;
		}

		private int? typeCount;

		private void UpdateTypeCount()
		{
			typeCount = null;
			new Thread(() => { typeCount = Extraction.GetTypes().Count(); }).Start();
		}

		protected override void OnSidebarGUI()
		{
			LudiqGUI.BeginVertical(GUILayout.Width(140));

			base.OnSidebarGUI();
			
			GUILayout.Box(GUIContent.none, LudiqStyles.horizontalSeparator);
			
			GUILayout.BeginVertical(Styles.explanationSection);
			
			var label = $"Total Types: {typeCount?.ToString() ?? "Calculating..."}  ";
			var warn = typeCount > Styles.typeCountWarningThreshold;

			EditorGUILayout.HelpBox(label, warn ? MessageType.Warning : MessageType.Info);
			
			GUILayout.Space(EditorGUIUtility.standardVerticalSpacing);
			
			if (GUILayout.Button("Fast Extract", Styles.fastExtractButton))
			{
				Extraction.Extract(ExtractionMode.Fast);
				Complete();
				GUIUtility.ExitGUI();
			}

			GUILayout.Space(EditorGUIUtility.standardVerticalSpacing);

			if (GUILayout.Button("Full Extract", Styles.fullExtractButton))
			{
				Extraction.Extract(ExtractionMode.Full);
				Complete();
				GUIUtility.ExitGUI();
			}

			GUILayout.Space(EditorGUIUtility.standardVerticalSpacing);

			if (GUILayout.Button("Reset", Styles.resetButton) && EditorUtility.DisplayDialog("Reset", "Are you sure you want to reset your extraction options to defaults?", "Reset", "Cancel"))
			{
				foreach (var page in pages)
				{
					if (page is ExtractionsPage extractionPage)
					{
						extractionPage.metadata.Reset(true);
						extractionPage.metadata.Save();
					}
				}
			}

			LudiqGUI.EndVertical();

			LudiqGUI.EndVertical();
		}

		public new static class Styles
		{
			static Styles()
			{
				background = new GUIStyle(LudiqStyles.windowBackground);
				background.padding = new RectOffset(0, 0, 0, 0);
				
				explanationSection = new GUIStyle(LudiqStyles.windowBackground);
				explanationSection.padding = new RectOffset(12, 12, 12, 12);
				
				explanationLabel = new GUIStyle(EditorStyles.label);
				explanationLabel.wordWrap = true;
				explanationLabel.padding = new RectOffset(0, 0, 0, 0);
				explanationLabel.margin = new RectOffset(0, 0, 0, 0);

				fullExtractButton = new GUIStyle("Button");
				fullExtractButton.padding = new RectOffset(12, 12, 7, 7);
				
				fastExtractButton = new GUIStyle(fullExtractButton);
				fastExtractButton.padding = new RectOffset(12, 12, 7, 7);
				// fastExtractButton.fontStyle = FontStyle.Bold;

				resetButton = new GUIStyle("Button");
				resetButton.padding = new RectOffset(12, 12, 3, 3);
			}
			
			public static readonly GUIStyle background;
			public static readonly GUIStyle explanationSection;
			public static readonly GUIStyle explanationLabel;
			public static readonly GUIStyle fullExtractButton;
			public static readonly GUIStyle fastExtractButton;
			public static readonly GUIStyle resetButton;
			public static readonly int typeCountWarningThreshold = 3000;
		}
	}
}