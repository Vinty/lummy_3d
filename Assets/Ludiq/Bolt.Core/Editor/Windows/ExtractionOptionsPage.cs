﻿using System.Collections.Generic;
using Ludiq;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace Bolt
{
	public class ExtractionOptionsPage : Page
	{
		public ExtractionOptionsPage()
		{
			title = "Options";
			shortTitle = title;
			
			configuration.Add(BoltCore.Configuration.GetMetadata(nameof(BoltCoreConfiguration.compareExtractions)));
			configuration.Add(BoltCore.Configuration.GetMetadata(nameof(BoltCoreConfiguration.generateDocumentation)));
			configuration.Add(BoltCore.Configuration.GetMetadata(nameof(BoltCoreConfiguration.extractDocumentation)));
		}

		private List<Metadata> configuration = new List<Metadata>(); 

		private Vector2 scroll;

		protected override void OnContentGUI()
		{
			scroll = GUILayout.BeginScrollView(scroll, Styles.background, GUILayout.ExpandWidth(true));
			
			GUILayout.BeginVertical(Styles.background, GUILayout.ExpandWidth(true));

			using (Inspector.expandTooltip.Override(true))
			{
				foreach (var item in configuration)
				{
					EditorGUI.BeginChangeCheck();
							
					LudiqGUI.InspectorLayout(item);

					if (EditorGUI.EndChangeCheck())
					{
						if (item is PluginConfigurationItemMetadata configurationMetadata)
						{
							configurationMetadata.Save();
						}
					}

					GUILayout.Space(6);
				}
			}

			GUILayout.EndVertical();

			GUILayout.EndScrollView();
		}

		public static class Styles
		{
			static Styles()
			{
				background = new GUIStyle(LudiqStyles.windowBackground);
				background.padding = new RectOffset(10, 10, 10, 10);
			}

			public static readonly GUIStyle background;
		}
	}
}
