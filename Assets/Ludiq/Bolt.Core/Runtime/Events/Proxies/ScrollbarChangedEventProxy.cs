﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Bolt
{
	[AddComponentMenu("")]
	public sealed class ScrollbarChangedEventProxy : MonoBehaviour
	{
		public void Start()
		{
			GetComponent<Scrollbar>()?.onValueChanged?.AddListener((value) => EventBus.Trigger(EventHooks.OnScrollbarValueChanged, gameObject, value));
		}
	}
}
