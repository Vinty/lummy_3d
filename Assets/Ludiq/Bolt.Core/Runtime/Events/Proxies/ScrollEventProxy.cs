﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Bolt
{
	[AddComponentMenu("")]
	public sealed class ScrollEventProxy : MonoBehaviour
	{
		public void OnScroll(PointerEventData eventData)
		{
			EventBus.Trigger(EventHooks.OnScroll, gameObject, eventData);
		}
    }
}
