﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Bolt
{
	[AddComponentMenu("")]
	public sealed class InputFieldEndEditEventProxy : MonoBehaviour
	{
		public void Start()
		{
			GetComponent<InputField>()?.onEndEdit?.AddListener((value) => EventBus.Trigger(EventHooks.OnInputFieldValueChanged, gameObject, value));
		}
	}
}
