﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Bolt
{
	[AddComponentMenu("")]
	public sealed class ParticleCollisionEventProxy : MonoBehaviour
	{
		public void OnParticleCollision(GameObject other)
		{
			EventBus.Trigger(EventHooks.OnParticleCollision, gameObject, other);
		}
	}
}
