﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Bolt
{
	[AddComponentMenu("")]
	public sealed class DragAndDropEventProxy : MonoBehaviour
	{
		public void OnBeginDrag(PointerEventData eventData)
		{
			EventBus.Trigger(EventHooks.OnBeginDrag, gameObject, eventData);
		}

		public void OnDrag(PointerEventData eventData)
		{
			EventBus.Trigger(EventHooks.OnDrag, gameObject, eventData);
		}

		public void OnEndDrag(PointerEventData eventData)
		{
			EventBus.Trigger(EventHooks.OnEndDrag, gameObject, eventData);
		}

		public void OnDrop(PointerEventData eventData)
		{
			EventBus.Trigger(EventHooks.OnDrop, gameObject, eventData);
		}
	}
}
