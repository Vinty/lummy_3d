﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Bolt
{
	[AddComponentMenu("")]
	public sealed class SubmitEventProxy : MonoBehaviour
	{
		public void OnSubmit(BaseEventData eventData)
		{
			EventBus.Trigger(EventHooks.OnSubmit, gameObject, eventData);
		}

		public void OnCancel(BaseEventData eventData)
		{
			EventBus.Trigger(EventHooks.OnCancel, gameObject, eventData);
		}
	}
}
