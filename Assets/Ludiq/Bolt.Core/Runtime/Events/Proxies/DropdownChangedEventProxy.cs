﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Bolt
{
	[AddComponentMenu("")]
	public sealed class DropdownChangedEventProxy : MonoBehaviour
	{
		public void Start()
		{
			GetComponent<Dropdown>()?.onValueChanged?.AddListener((value) => EventBus.Trigger(EventHooks.OnDropdownValueChanged, gameObject, value));
		}
	}
}
