﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Bolt
{
	[AddComponentMenu("")]
	public sealed class Trigger2DEventProxy : MonoBehaviour
	{
		public void OnTriggerEnter2D(Collider2D other)
		{
			EventBus.Trigger(EventHooks.OnTriggerEnter2D, gameObject, other);
		}

		public void OnTriggerExit2D(Collider2D other)
		{
			EventBus.Trigger(EventHooks.OnTriggerExit2D, gameObject, other);
		}

		public void OnTriggerStay2D(Collider2D other)
		{
			EventBus.Trigger(EventHooks.OnTriggerStay2D, gameObject, other);
		}
	}
}
