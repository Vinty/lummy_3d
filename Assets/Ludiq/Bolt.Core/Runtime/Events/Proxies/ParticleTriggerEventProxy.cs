﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Bolt
{
	[AddComponentMenu("")]
	public sealed class ParticleTriggerEventProxy : MonoBehaviour
	{
		public void OnParticleTrigger()
		{
			EventBus.Trigger(EventHooks.OnParticleTrigger, gameObject, new EmptyEventArgs());
		}
	}
}
