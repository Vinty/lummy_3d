﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[AddComponentMenu("")]
	public sealed class JointBreakEventProxy : MonoBehaviour
	{
		public void OnJointBreak(float breakForce)
		{
			EventBus.Trigger(EventHooks.OnJointBreak, gameObject, breakForce);
		}
	}
}
