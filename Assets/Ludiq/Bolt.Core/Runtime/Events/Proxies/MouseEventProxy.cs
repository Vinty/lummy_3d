﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Bolt
{
	[AddComponentMenu("")]
	public sealed class MouseEventProxy : MonoBehaviour
	{
		public void OnMouseDown()
		{
			EventBus.Trigger(EventHooks.OnMouseDown, gameObject);
		}

		public void OnMouseDrag()
		{
			EventBus.Trigger(EventHooks.OnMouseDrag, gameObject);
		}

		public void OnMouseEnter()
		{
			EventBus.Trigger(EventHooks.OnMouseEnter, gameObject);
		}

		public void OnMouseExit()
		{
			EventBus.Trigger(EventHooks.OnMouseExit, gameObject);
		}

		public void OnMouseOver()
		{
			EventBus.Trigger(EventHooks.OnMouseOver, gameObject);
		}

		public void OnMouseUp()
		{
			EventBus.Trigger(EventHooks.OnMouseUp, gameObject);
		}

		public void OnMouseUpAsButton()
		{
			EventBus.Trigger(EventHooks.OnMouseUpAsButton, gameObject);
		}
	}
}
