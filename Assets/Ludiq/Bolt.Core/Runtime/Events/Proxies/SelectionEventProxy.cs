﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Bolt
{
	[AddComponentMenu("")]
	public sealed class SelectionEventProxy : MonoBehaviour
	{
		public void OnSelect(BaseEventData eventData)
		{
			EventBus.Trigger(EventHooks.OnSelect, gameObject, eventData);
		}

		public void OnDeselect(BaseEventData eventData)
		{
			EventBus.Trigger(EventHooks.OnDeselect, gameObject, eventData);
		}
	}
}
