﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Bolt
{
	[AddComponentMenu("")]
	public sealed class TransformEventProxy : MonoBehaviour
	{
		public void OnTransformChildrenChanged()
		{
			EventBus.Trigger(EventHooks.OnTransformChildrenChanged, gameObject);
		}

		public void OnTransformParentChanged()
		{
			EventBus.Trigger(EventHooks.OnTransformParentChanged, gameObject);
		}
	}
}
