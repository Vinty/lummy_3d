﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Bolt
{
	[AddComponentMenu("")]
	public sealed class InputFieldChangedEventProxy : MonoBehaviour
	{
		public void Start()
		{
			GetComponent<InputField>()?.onValueChanged?.AddListener((value) => EventBus.Trigger(EventHooks.OnInputFieldValueChanged, gameObject, value));
		}
	}
}
