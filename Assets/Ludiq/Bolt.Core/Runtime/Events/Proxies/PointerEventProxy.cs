﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Bolt
{
	[AddComponentMenu("")]
	public sealed class PointerEventProxy : MonoBehaviour
	{
		public void OnPointerEnter(PointerEventData eventData)
		{
			EventBus.Trigger(EventHooks.OnPointerEnter, gameObject, eventData);
		}

		public void OnPointerExit(PointerEventData eventData)
		{
			EventBus.Trigger(EventHooks.OnPointerExit, gameObject, eventData);
		}

		public void OnPointerDown(PointerEventData eventData)
		{
			EventBus.Trigger(EventHooks.OnPointerDown, gameObject, eventData);
		}

		public void OnPointerUp(PointerEventData eventData)
		{
			EventBus.Trigger(EventHooks.OnPointerUp, gameObject, eventData);
		}

		public void OnPointerClick(PointerEventData eventData)
		{
			EventBus.Trigger(EventHooks.OnPointerClick, gameObject, eventData);
		}
		
		public void OnScroll(PointerEventData eventData)
		{
			EventBus.Trigger(EventHooks.OnScroll, gameObject, eventData);
		}
	}
}
