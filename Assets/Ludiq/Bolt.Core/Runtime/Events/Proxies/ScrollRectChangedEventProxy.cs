﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Bolt
{
	[AddComponentMenu("")]
	public sealed class ScrollRectChangedEventProxy : MonoBehaviour
	{
		public void Start()
		{
			GetComponent<ScrollRect>()?.onValueChanged?.AddListener((value) => EventBus.Trigger(EventHooks.OnScrollRectValueChanged, gameObject, value));
		}
	}
}
