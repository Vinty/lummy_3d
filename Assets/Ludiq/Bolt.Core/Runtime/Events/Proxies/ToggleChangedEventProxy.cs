﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Bolt
{
	[AddComponentMenu("")]
	public sealed class ToggleChangedEventProxy : MonoBehaviour
	{
		public void Start()
		{
			GetComponent<Toggle>()?.onValueChanged?.AddListener((value) => EventBus.Trigger(EventHooks.OnToggleValueChanged, gameObject, value));
		}
	}
}
