﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Bolt
{
	[AddComponentMenu("")]
	public sealed class VisibilityEventProxy : MonoBehaviour
	{
		public void OnBecameInvisible()
		{
			EventBus.Trigger(EventHooks.OnBecameInvisible, gameObject);
		}

		public void OnBecameVisible()
		{
			EventBus.Trigger(EventHooks.OnBecameVisible, gameObject);
		}
	}
}
