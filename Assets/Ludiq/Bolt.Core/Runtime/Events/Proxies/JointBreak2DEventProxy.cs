﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Bolt
{
	[AddComponentMenu("")]
	public sealed class JointBreak2DEventProxy : MonoBehaviour
	{
		public void OnJointBreak2D(Joint2D breakForce)
		{
			EventBus.Trigger(EventHooks.OnJointBreak2D, gameObject, breakForce);
		}
	}
}
