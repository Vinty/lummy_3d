﻿using Ludiq;
using UnityEngine;

namespace Bolt
{
	public interface IGraphEventListener
	{
		void StartListening(GraphStack stack);

		void StopListening(GraphStack stack);

		bool IsListening(GraphPointer pointer);
	}

	public static class XGraphEventListener
	{
		public static void StartListening(this IGraphEventListener listener, GraphReference reference)
		{
			using (var stack = reference.ToStackPooled())
			{
				listener.StartListening(stack);
			}
		}

		public static void StopListening(this IGraphEventListener listener, GraphReference reference)
		{
			using (var stack = reference.ToStackPooled())
			{
				listener.StopListening(stack);
			}
		}

		public static bool IsHierarchyListening(GraphReference reference)
		{
			using (var stack = reference.ToStackPooled())
			{
				while (stack.isChild)
				{
					var parent = stack.parent;

					stack.ExitParentElement();

					if (parent is IGraphEventListener listener)
					{
						if (!listener.IsListening(stack))
						{
							return false;
						}
					}
				}

				return true;
			}
		}
	}
}
