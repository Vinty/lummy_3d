﻿using Ludiq;
using UnityEngine;

namespace Bolt
{
	public interface IEventMachine : IMachine
	{
		HybridRuntimeMode hybridRuntimeMode { get; }

		void TriggerAnimationEvent(AnimationEvent animationEvent);

		void TriggerUnityEvent(string name);
	}
}
