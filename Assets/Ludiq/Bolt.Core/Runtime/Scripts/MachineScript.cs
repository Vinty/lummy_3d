﻿using Ludiq;
using UnityEngine;

namespace Bolt
{
	[Extract(false)]
	public abstract class MachineScript<TGraph, TGraphData, TMachineScript, TGraphScript> : MonoBehaviour, IMachineScript
		where TGraph : class, IGraph
		where TGraphData : class, IGraphData
		where TMachineScript : MachineScript<TGraph, TGraphData, TMachineScript, TGraphScript>
		where TGraphScript : GraphScript<TGraph, TGraphData, TGraphScript>
	{
		protected TGraphData _graphData;

		public MachineScript()
		{
		}



		IGraphData IMachineScript.graphData
		{
			get => graphData;
			set => graphData = (TGraphData) value;
		}

		public TGraphData graphData
		{
			get => _graphData;
			set
			{
				_graphData = value;
				graphScript.graphData = value;
			}
		}

		public MonoBehaviour behaviour => this;

		GameObject IMachineScript.gameObject => gameObject;

		public TGraphScript graphScript { get; protected set; }



		protected void TriggerEvent(string name)
		{
			TriggerRegisteredEvent(new EventHook(name, this), new EmptyEventArgs());
		}

		protected void TriggerEvent<TArgs>(string name, TArgs args)
		{
			TriggerRegisteredEvent(new EventHook(name, this), args);
		}

		protected virtual void TriggerRegisteredEvent<TArgs>(EventHook hook, TArgs args)
		{
			EventBus.Trigger(hook, args);
		}

		protected virtual void Awake()
		{
			if (MachineScriptAwakeHelper.awakeData.HasValue)
			{
				var awakeData = MachineScriptAwakeHelper.awakeData.Value;
				MachineScriptAwakeHelper.awakeData = null;

				graphData = (TGraphData) awakeData.graphData;
			}

			GlobalMessageListener.Require();
		}

		protected virtual void OnEnable() 
		{
			TriggerEvent(EventHooks.OnEnable);
		}

		protected virtual void Start() 
		{
			TriggerEvent(EventHooks.Start);
		}

		protected virtual void OnDisable() 
		{ 
			TriggerEvent(EventHooks.OnDisable);
		}

		protected virtual void OnDestroy()
		{
			TriggerEvent(EventHooks.OnDestroy);
		}

		public virtual void TriggerAnimationEvent(AnimationEvent animationEvent)
		{
			TriggerEvent(EventHooks.AnimationEvent, animationEvent);
		}

		public virtual void TriggerUnityEvent(string name)
		{
			TriggerEvent(EventHooks.UnityEvent, name);
		}

		protected virtual void OnDrawGizmos()
		{
			TriggerEvent(EventHooks.OnDrawGizmos);
		}

		protected virtual void OnDrawGizmosSelected()
		{
			TriggerEvent(EventHooks.OnDrawGizmosSelected);
		}
	}
}
