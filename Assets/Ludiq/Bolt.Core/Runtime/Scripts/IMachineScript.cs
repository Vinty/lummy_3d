﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ludiq;
using UnityEngine;

namespace Bolt
{
	public interface IMachineScript
	{
		IGraphData graphData { get; set; }
		MonoBehaviour behaviour { get; }
		GameObject gameObject { get; }
	}
}
