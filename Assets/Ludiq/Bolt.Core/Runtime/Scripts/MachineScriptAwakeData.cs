﻿using Ludiq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bolt
{
	public struct MachineScriptAwakeData
	{
		public MachineScriptAwakeData(IGraphData graphData)
		{
			this.graphData = graphData;
		}

		public IGraphData graphData;
	}
}
