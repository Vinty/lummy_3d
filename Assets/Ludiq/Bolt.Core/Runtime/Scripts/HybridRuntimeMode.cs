﻿namespace Bolt
{
	public enum HybridRuntimeMode
	{
		Automatic,
		ForceGenerated,
		ForceLive,
	}
}
