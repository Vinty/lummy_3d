﻿using Ludiq;
using UnityEngine;

namespace Bolt
{
	[Extract(false)]
	public abstract class GraphScript<TGraph, TGraphData, TGraphScript>
		where TGraph : class, IGraph
		where TGraphData : class, IGraphData
		where TGraphScript : GraphScript<TGraph, TGraphData, TGraphScript>
	{
		public object parent;
		protected TGraphData _graphData;

		public GraphScript(IMachineScript machineScript)
		{
			this.machineScript = machineScript;
		}

		public IMachineScript machineScript { get; }
		public GameObject gameObject => machineScript.gameObject;
		public abstract TGraphData graphData { get; set; }
	}
}
