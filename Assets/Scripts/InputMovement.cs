﻿using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class InputMovement : MonoBehaviour {

    private RaycastHit   hitInfo;
    private NavMeshAgent agent;

    private void Start() {
        agent = GetComponent<NavMeshAgent>();
    }

    private void Update() {
        ClickLeftMouse();
    }

    private void ClickLeftMouse() {
        if(Input.GetMouseButtonDown(0) && !Input.GetKey(KeyCode.LeftShift)) {
            if(Camera.main == null) {
                return;
            }

            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if(Physics.Raycast(ray.origin, ray.direction, out hitInfo)) {
                agent.destination = hitInfo.point;
            }
        }
    }

    // private void OnDrawGizmos() 
    // {
    //     Debug.DrawLine(_agent.destination, pointPath.position + _moveInput * 5 * Vector3.right, Color.red);
    // }
}
