﻿using System;
using UnityEngine;
using UnityEngine.AI;

namespace Enemy {
    public enum StateEnemy {
        FIND_POINT,
        MOVE
    }

    public class EnemyMovement : MonoBehaviour {
        [SerializeField] private Transform[]  movementPointList;
        [SerializeField] private float        speed = 5f;
        private                  Transform    currentPointTransform;
        private                  int          currentIndex;
        private                  NavMeshAgent agent;
        public                   StateEnemy   CurrentState {get; set;}
        private                  int          lenghtMovementPointIndex;

        private void Start() {
            agent                    = GetComponent<NavMeshAgent>();
            currentPointTransform    = movementPointList[currentIndex];
            CurrentState             = StateEnemy.MOVE;
            lenghtMovementPointIndex = movementPointList.Length - 1;
            agent.speed              = speed;
        }

        private void Update() {
            switch(CurrentState) {
                case StateEnemy.FIND_POINT :
                    FindCurrentPoint();
                    CurrentState = StateEnemy.MOVE;
                    break;
                case StateEnemy.MOVE :
                    var distance = Vector3.Distance(transform.position, currentPointTransform.position);
                    if(distance > 2f) {
                        Move();
                        return;
                    }

                    CurrentState = StateEnemy.FIND_POINT;
                    break;
            }
        }

        private void FindCurrentPoint() {
            currentIndex          = currentIndex < lenghtMovementPointIndex ? currentIndex + 1 : currentIndex = 0;
            currentPointTransform = movementPointList[currentIndex];
        }

        private void Move() {
            agent.SetDestination(currentPointTransform.position);
        }
    }
}
