﻿using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class ClickToMove : MonoBehaviour {
    [SerializeField] private float     speed = 6f;
    [SerializeField] private Transform pointPath;
    [SerializeField] private Animator  animator;

    [SerializeField] private Transform graphic;
    private                  Vector3   _graphicTransformScale;
    private                  Vector3   _pointPath;

    private Quaternion   _rotation;
    private RaycastHit   _hitInfo = new RaycastHit();
    private NavMeshAgent _agent;
    private float        _moveInput;
    private Rigidbody    _rb;
    private bool         _isRight = true;

    private void Start() {
        _agent                 = GetComponent<NavMeshAgent>();
        _rb                    = GetComponent<Rigidbody>();
        _rotation              = transform.rotation;
        _graphicTransformScale = graphic.transform.localScale;
        _pointPath             = pointPath.transform.position;
    }

    private void Update() {
        // if(Input.GetMouseButtonDown(0) && !Input.GetKey(KeyCode.LeftShift)) {
        //     var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        //     if(Physics.Raycast(ray.origin, ray.direction, out _hitInfo)) {
        //         _agent.destination = _hitInfo.point;
        //     }
        // }
        MoveAxis();
    }

    private void MoveAxis() {
        // убираем хаотичное вращение полученное импульсом физики
        transform.rotation     = _rotation;
        _graphicTransformScale = graphic.transform.localScale;

        IfPressDownD(_graphicTransformScale);
        IfPressDownA(_graphicTransformScale);
        IfPressUp();

        // _moveInput  = Input.GetAxis("Horizontal");
        // _rb.velocity = new Vector2(_moveInput * Speed, _rb.velocity.y);
    }

    // Если нажали A
    private void IfPressDownA(Vector3 graphicTransformScale) {
        if(!Input.GetKeyDown(KeyCode.A)) {
            return;
        }

        if(!_isRight) {
            _moveInput = 1f;
        }
        else {
            _moveInput = -1f;
            _isRight   = !_isRight;
        }

        animator.SetTrigger("Walk");
        Move(graphicTransformScale);
    }

    // Если нажали D
    private void IfPressDownD(Vector3 graphicTransformScale) {
        if(!Input.GetKeyDown(KeyCode.D)) {
            return;
        }

        if(_isRight) {
            _moveInput = 1f;
        }
        else {
            _moveInput = -1f;
            _isRight   = !_isRight;
        }

        animator.SetTrigger("Walk");
        Move(graphicTransformScale);
    }

    private void IfPressUp() {
        if(!Input.GetKeyUp(KeyCode.D) && !Input.GetKeyUp(KeyCode.A)) {
            return;
        }

        animator.SetTrigger("Idle");
        StopMove();
    }

    // Двигаемся
    private void Move(Vector3 graphicTransformScale) {
        // поворачиваем визуально наследника Player, если надо, что бы смотрел в правильную сторону
        graphicTransformScale.z      *= _moveInput;
        graphic.transform.localScale =  graphicTransformScale;
        _pointPath                   =  pointPath.transform.position;

        // и двигаемся в эту сторону
        _agent.destination = pointPath.position;
    }

    private void StopMove() {
        _agent.destination = _agent.transform.position;
    }

    // private void OnDrawGizmos() {
    //     Debug.DrawLine(_agent.destination, pointPath.position + _moveInput * 5 * Vector3.right, Color.red);
    // }
}
