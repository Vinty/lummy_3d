﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerAnimController : MonoBehaviour
{
    private Animator anim;
    public NavMeshAgent navMeshAgent;
    public bool isWalking;
    
    void Start()
    {
        anim = GetComponent<Animator>();
        navMeshAgent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
//        isWalking = navMeshAgent.velocity != Vector3.zero ? true : false;
//        anim.SetBool("IsWalking", isWalking);  
        anim.SetFloat("PlayerVelocity", navMeshAgent.velocity.magnitude);
        print(navMeshAgent.velocity.magnitude);
    }
}
